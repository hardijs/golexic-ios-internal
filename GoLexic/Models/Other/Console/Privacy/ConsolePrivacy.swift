//
//  ConsolePrivacy.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 21/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit

class ConsolePrivacy: NSObject {
    
    // MARK: Enums
    
    enum Identifier: Int {
        case policy = 1
        case analytics
        case data
        case delete
    }
    
    // MARK: Properties

    let identifier: Identifier
    
    let titleKey: String
    
    // MARK: Initializers
    
    init(withData data: [String: Any]) {
        identifier = Identifier(rawValue: data["id"] as! Int)!
        titleKey = data["titleKey"] as! String
        super.init()
    }

}
