//
//  ConsoleSetting.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 21/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit

class ConsoleSetting: NSObject {

    // MARK: Enums
    
    enum Identifier: Int {
        case notifications = 1
        case reports
        case news
    }
    
    // MARK: Properties

    let identifier: Identifier
    
    let titleKey: String
    let iconName: String
    
    // MARK: Initializers
    
    init(withData data: [String: Any]) {
        identifier = Identifier(rawValue: data["id"] as! Int)!
        titleKey = data["titleKey"] as! String
        iconName = data["iconName"] as! String
        super.init()
    }
    
}
