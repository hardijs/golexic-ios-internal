//
//  MenuItem.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit

class MenuItem: NSObject {

    // MARK: Enums
    
    enum Identifier: Int {
        case console = 1
    }
    
    // MARK: Properties

    let identifier: Identifier
    
    let titleKey: String
    let iconName: String
    
    // MARK: Initializers
    
    init(withData data: [String: Any]) {
        identifier = Identifier(rawValue: data["id"] as! Int)!
        iconName = data["iconName"] as! String
        titleKey = data["titleKey"] as! String
        super.init()
    }
    
}
