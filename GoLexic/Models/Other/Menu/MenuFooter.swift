//
//  MenuFooter.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit

class MenuFooter: NSObject {

    // MARK: Enums
    
    enum Identifier: Int {
        case tips = 1
        case support
        case version
    }
    
    // MARK: Properties

    let identifier: Identifier
    
    let titleKey: String
    let iconName: String
    
    // MARK: Initializers
    
    init(withData data: [String: Any]) {
        identifier = Identifier(rawValue: data["id"] as! Int)!
        iconName = data["iconName"] as! String
        titleKey = data["titleKey"] as! String
        super.init()
    }
    
}
