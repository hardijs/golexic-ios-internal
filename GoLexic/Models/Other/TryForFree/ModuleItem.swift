//
//  ModuleItem.swift
//  GoLexic
//
//  Created by Denis Kuznetsov on 09.03.2021.
//  Copyright © 2021 CUBE Mobile. All rights reserved.
//

import UIKit

class ModuleItem: NSObject {
    
    // MARK: Properties

    let identifier: String
    
    let title: String
    let desc: String
    let position: Int

    // MARK: Initializers
    
    init(withData data: [String: Any]) {
        identifier = data["id"] as! String
        title = data["name"] as! String
        desc = data["description"] as! String
        position = data["position"] as! Int
        super.init()
    }
    
}
