//
//  Onboarding.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 21/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit

class Onboarding: NSObject {

    // MARK: Enums
    
    enum Identifier: Int {
        case first = 1
    }
    
    // MARK: Properties

    let identifier: Identifier
    
    let titleKey: String
    let infoKey: String
    let imageName: String
    
    // MARK: Initializers
    
    init(withData data: [String: Any]) {
        identifier = Identifier(rawValue: data["id"] as! Int)!
        titleKey = data["titleKey"] as! String
        infoKey = data["infoKey"] as! String
        imageName = data["imageName"] as! String
        super.init()
    }
    
}
