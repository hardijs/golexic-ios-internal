//
//  User.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import KeychainAccess
import RealmSwift

class User {
    
    // MARK: Enums
    
    enum Key: String, CaseIterable {
        case profile = "userProfile"
        case profileReminderTime = "userProfileReminderTime"
        case token = "userToken"
    }
    
    // MARK: Properties
    
    // Static
    fileprivate static var _current: User = {
        let user = User()
        user.loadPersistedData(.token)
        return user
    }()
    static var current: User? {
        guard _current.token != nil else {
            return nil
        }
        return _current
    }
    
    // Variables
    var profile: UserProfile? {
        return loadPersistedData(.profile) as? UserProfile
    }
    fileprivate(set) var token: String?
    
    // Getters
    var isBetaUser: Bool {
        guard let _profile = profile else {
            return false
        }
        return _profile.isBeta
    }
    var isProfileCompleted: Bool {
        guard let _profile = profile else {
            return false
        }
        return _profile.name != nil && _profile.age > 0
    }
    
}

// MARK: - Public Functions
extension User {
    
    @discardableResult
    static func create(withToken token: String) -> User {
        _current.delete(blockNotification: true)
        let keychain = Keychain(service: Identifiers.keychain)
        try? keychain.set(token, key: Key.token.rawValue)
        _current.token = token
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name.User.DidCreate, object: current)
        }
        return current!
    }
    
    func update(_ key: Key, withData data: Any?, blockNotification: Bool = false) {
        switch key {
        case .profile:
            if let _data = data as? [String: Any] {
                updateProfile(withData: _data)
            } else {
                removeProfile()
            }
        case .profileReminderTime:
            if let _reminderTime = data as? String {
                updateProfile(withReminderTime: _reminderTime)
            } else {
                updateProfile(withReminderTime: nil)
            }
        case .token:
            let keychain = Keychain(service: Identifiers.keychain)
            if let _token = data as? String {
                try? keychain.set(_token, key: Key.token.rawValue)
            } else {
                try? keychain.remove(Key.token.rawValue)
            }
            loadPersistedData(key)
        }
        guard blockNotification == false else {
            return
        }
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name.User.DidUpdate, object: key)
        }
    }
    
    func delete(blockNotification: Bool = false) {
        for key in Key.allCases {
            update(key, withData: nil)
        }
        guard blockNotification == false else {
            return
        }
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: NSNotification.Name.User.DidDelete, object: nil)
        }
    }
    
}

// MARK: - NSNotification.Name
extension NSNotification.Name {
    //Used as namespace for all user related notifications
    struct User {
        //Posted when new current user is added
        static let DidCreate = NSNotification.Name("notification.name.user.didCreate")
        //Posted when user data is updated
        static let DidUpdate = NSNotification.Name("notification.name.user.didUpdate")
        //Posted when current user is deleted
        static let DidDelete = NSNotification.Name("notification.name.user.didDelete")
    }
}

// MARK: - Private Functions
extension User {
    
    // MARK: Keys
    
    fileprivate struct Identifiers {
        static let keychain = "lv.cube.golexic"
    }
    
    // MARK: Helpers
    
    @discardableResult
    fileprivate func loadPersistedData(_ key: Key) -> Any? {
        switch key {
        case .profile:
            let realm = try! Realm()
            guard let profile = realm.objects(UserProfile.self).last,
                profile.isInvalidated == false else {
                return false
            }
            return profile
        case .token:
            let keychain = Keychain(service: Identifiers.keychain)
            token = keychain[Key.token.rawValue]
            return token
        default:
            return nil
        }
    }
    
    // MARK: Update
    
    fileprivate func updateProfile(withData data: [String: Any]) {
        let realm = try! Realm()
        realm.beginWrite()
        if let _profile = realm.objects(UserProfile.self).last {
            _profile.update(withData: data)
        } else {
            let profile = UserProfile(withData: data)
            realm.add(profile)
        }
        try? realm.commitWrite()
    }
    
    fileprivate func updateProfile(withReminderTime reminderTime: String?) {
        let realm = try! Realm()
        guard let _profile = realm.objects(UserProfile.self).last else {
            return
        }
        realm.beginWrite()
        _profile.reminderTime = reminderTime
        try? realm.commitWrite()
    }
    
    fileprivate func removeProfile() {
        let realm = try! Realm()
        realm.beginWrite()
        let profiles = realm.objects(UserProfile.self)
        realm.delete(profiles)
        try? realm.commitWrite()
    }
    
}
