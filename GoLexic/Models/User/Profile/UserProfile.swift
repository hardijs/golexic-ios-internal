//
//  UserProfile.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import RealmSwift

class UserProfile: Object, Archivable {
    
    // MARK: Properties
    
    @objc dynamic var name: String?
    @objc dynamic var age: Int = 0
    @objc dynamic var reminderTime: String?
    @objc dynamic var streakDays: Int = 0
    
    @objc dynamic var isBeta: Bool = false
    @objc dynamic var isTester: Bool = false
    @objc dynamic var isAcceptingTerms: Bool = false
    @objc dynamic var isAcceptingDataProcessing: Bool = false
    @objc dynamic var isAcceptingAnalytics: Bool = false
    @objc dynamic var isAcceptingReports: Bool = false
    @objc dynamic var isAcceptingNews: Bool = false
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        decode(data)
    }

}

// MARK: - Public Functions
extension UserProfile {
    
    func update(withData data: [String: Any]) {
        decode(data)
    }
    
}

// MARK: - Private Functions
extension UserProfile {
    
    fileprivate func decode(_ data: [String: Any]) {
        if let _name = data["name"] as? String {
            name = _name
        }
        if let _age = data["age"] as? Int {
            age = _age
        }
        if let _reminderTime = data["reminderTime"] as? String {
            reminderTime = _reminderTime
        }
        if let _streakDays = data["streakDays"] as? Int {
            streakDays = _streakDays
        }
        if let _isBeta = data["isBeta"] as? Bool {
            isBeta = _isBeta
        }
        if let _isTester = data["isTester"] as? Bool {
            isTester = _isTester
        }
        if let _isAcceptingTerms = data["isAcceptingTerms"] as? Bool {
            isAcceptingTerms = _isAcceptingTerms
        }
        if let _isAcceptingDataProcessing = data["isAcceptingDataProcessing"] as? Bool {
            isAcceptingDataProcessing = _isAcceptingDataProcessing
        }
        if let _isAcceptingAnalytics = data["isAcceptingAnalytics"] as? Bool {
            isAcceptingAnalytics = _isAcceptingAnalytics
        }
        if let _isAcceptingReports = data["isAcceptingReports"] as? Bool {
            isAcceptingReports = _isAcceptingReports
        }
        if let _isAcceptingNews = data["isAcceptingNews"] as? Bool {
            isAcceptingNews = _isAcceptingNews
        }
    }
    
}
