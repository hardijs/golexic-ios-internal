//
//  ScoringLetter.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ScoringLetter: Object, Updatable, Archivable {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    let steps = List<ScoringLetterStep>()
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    required convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        update(withData: data)
    }
    
    // MARK: Overridden Functions
    
    override class func primaryKey() -> String? {
        return "identifier"
    }
    
}

// MARK: - Updatable
extension ScoringLetter {
    
    func update(withData data: [String : Any]) {
        updateSteps(withData: data["steps"])
    }
    
}

// MARK: - Private Functions
extension ScoringLetter {
    
    // MARK: Helpers
    
    fileprivate func updateSteps(withData data: Any?) {
        for step in steps {
            step.archive(true)
        }
        steps.removeAll()
        guard let _steps = data as? [[String: Any]] else {
            return
        }
        for data in _steps {
            let step = ScoringLetterStep(withData: data)
            steps.append(step)
        }
    }
    
}
