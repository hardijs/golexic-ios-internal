//
//  ScoringLetterStep.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ScoringLetterStep: Object, Archivable {
    
    // MARK: Enums
    
    enum Status: Int {
        case unknown
        case success
        case maybe
        case failed
    }
    
    // MARK: Properties
    
    @objc dynamic var statusRaw: Int = 0
    var status: Status {
        return Status(rawValue: statusRaw) ?? .unknown
    }
    @objc dynamic var points: Double = 0
    @objc dynamic var confidence: Double = 0
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _statusRaw = data["type"] as? Int {
            statusRaw = _statusRaw
        }
        if let _points = data["points"] as? Double {
            points = _points
        }
        if let _confidence = data["confidence"] as? Double {
            confidence = _confidence
        }
    }
    
    // MARK: Overridden Functions
    
    override class func ignoredProperties() -> [String] {
        return ["status"]
    }

}
