//
//  Instruction.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class Instruction: Object, Updatable, Archivable {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    let images = List<String>()
    @objc dynamic var audioUrl: String?
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    required convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        update(withData: data)
    }
    
    // MARK: Overridden Functions
    
    override class func primaryKey() -> String? {
        return "identifier"
    }
    
}

// MARK: - Updatable
extension Instruction {
    
    func update(withData data: [String : Any]) {
        updateExerciseLessons(withData: data["images"])
        if let _audioUrl = data["audioFile"] as? String {
            audioUrl = _audioUrl
        }
    }
    
}

// MARK: - Private Functions
extension Instruction {
    
    fileprivate func updateExerciseLessons(withData data: Any?) {
        images.removeAll()
        guard let _images = data as? [String] else {
            return
        }
        for image in _images {
            images.append(image)
        }
    }
    
}
