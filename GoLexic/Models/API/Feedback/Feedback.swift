//
//  Feedback.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class Feedback: Object, Updatable, Archivable {
    
    // MARK: Enums
    
    enum Status: Int {
        case unknown
        case success
        case maybe
        case failed
    }

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var statusRaw: Int = 0
    var status: Status {
        return Status(rawValue: statusRaw) ?? .unknown
    }
    @objc dynamic var info: String = ""
    @objc dynamic var audioUrl: String?
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    required convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        update(withData: data)
    }
    
    // MARK: Overridden Functions
    
    override class func primaryKey() -> String? {
        return "identifier"
    }
    
    override class func ignoredProperties() -> [String] {
        return ["status"]
    }
    
}

// MARK: - Updatable
extension Feedback {
    
    func update(withData data: [String : Any]) {
        if let _statusRaw = data["type"] as? Int {
            statusRaw = _statusRaw
        }
        if let _info = data["description"] as? String {
            info = _info
        }
        if let _audioUrl = data["audioFile"] as? String {
            audioUrl = _audioUrl
        }
    }
    
}
