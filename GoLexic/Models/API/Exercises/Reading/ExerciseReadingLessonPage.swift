//
//  ExerciseReadingLessonPage.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 18/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseReadingLessonPage: Object, Archivable {
    
    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    let lines = List<ExerciseReadingLessonPageLine>()
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        updateLines(withData: data["lines"])
    }

}

// MARK: - Archive
extension ExerciseReadingLessonPage {
    
    func archive(_ archive: Bool) {
        isArchived = archive
        for line in lines {
            line.archive(archive)
        }
    }
    
}

// MARK: - Private Functions
extension ExerciseReadingLessonPage {
    
    // MARK: Helpers
    
    fileprivate func updateLines(withData data: Any?) {
        guard let _lines = data as? [[String: Any]] else {
            return
        }
        for data in _lines {
            let line = ExerciseReadingLessonPageLine(withData: data)
            lines.append(line)
        }
    }
    
}
