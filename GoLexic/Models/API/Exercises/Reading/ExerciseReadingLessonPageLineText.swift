//
//  ExerciseReadingLessonPageLineText.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 18/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseReadingLessonPageLineText: Object, Archivable {
    
    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var text: String = ""
    @objc dynamic var isSentence: Bool = false
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        if let _text = data["text"] as? String {
            text = _text
        }
        if let _isSentence = data["isSentence"] as? Bool {
            isSentence = _isSentence
        }
    }

}
