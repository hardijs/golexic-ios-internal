//
//  ExerciseReadingLessonPageLine.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 18/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseReadingLessonPageLine: Object, Archivable {
    
    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    let texts = List<ExerciseReadingLessonPageLineText>()
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        updateTexts(withData: data["texts"])
    }

}

// MARK: - Archive
extension ExerciseReadingLessonPageLine {
    
    func archive(_ archive: Bool) {
        isArchived = archive
        for text in texts {
            text.archive(archive)
        }
    }
    
}

// MARK: - Private Functions
extension ExerciseReadingLessonPageLine {
    
    // MARK: Helpers
    
    fileprivate func updateTexts(withData data: Any?) {
        guard let _texts = data as? [[String: Any]] else {
            return
        }
        for data in _texts {
            let text = ExerciseReadingLessonPageLineText(withData: data)
            texts.append(text)
        }
    }
    
}
