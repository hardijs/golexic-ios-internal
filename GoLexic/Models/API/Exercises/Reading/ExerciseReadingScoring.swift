//
//  ExerciseReadingScoring.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseReadingScoring: Object, Archivable {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var scoreRaw: Int = 0
    var score: Score {
        return Score(rawValue: scoreRaw) ?? .none
    }
    @objc dynamic var points: Double = 0
    @objc dynamic var confidence: Double = 0
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        if let _scoreRaw = data["type"] as? Int {
            scoreRaw = _scoreRaw
        }
        if let _points = data["points"] as? Double {
            points = _points
        }
        if let _confidence = data["confidence"] as? Double {
            confidence = _confidence
        }
    }
    
    // MARK: Overridden Functions
    
    override class func ignoredProperties() -> [String] {
        return ["score"]
    }
    
}
