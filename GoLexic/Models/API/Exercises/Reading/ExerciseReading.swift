//
//  ExerciseReading.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseReading: Object, Updatable, Archivable, Exercise {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var typeRaw: Int = 0
    var type: ExerciseType {
        return ExerciseType(rawValue: typeRaw) ?? .unknown
    }
    
    @objc dynamic var wordScoringId: String = ""
    var wordScoring: ScoringWord? {
        return availableRealm.object(ofType: ScoringWord.self, forPrimaryKey: wordScoringId)
    }
    
    let lessons = List<ExerciseReadingLesson>()
    let scorings = List<ExerciseReadingScoring>()
    
    @objc dynamic var instructionId: String = ""
    var instruction: Instruction? {
        return availableRealm.object(ofType: Instruction.self, forPrimaryKey: instructionId)
    }
    
    let feedbackIds = List<String>()
    var feedbacks: Results<Feedback> {
        return availableRealm.objects(Feedback.self).filter("identifier IN %@", feedbackIds)
    }
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    required convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        update(withData: data)
    }
    
    // MARK: Overridden Functions
    
    override class func primaryKey() -> String? {
        return "identifier"
    }
    
    override class func ignoredProperties() -> [String] {
        return ["type", "wordScoring", "instruction", "feedbacks"]
    }
    
}

// MARK: - Updatable
extension ExerciseReading {
    
    func update(withData data: [String : Any]) {
        if let _typeRaw = data["type"] as? Int {
            typeRaw = _typeRaw
        }
        if let _wordScoringId = data["wordScoringId"] as? String {
            wordScoringId = _wordScoringId
        }
        updateLessons(withData: data["lessons"])
        updateExerciseScorings(withData: data["exerciseScoring"])
        if let _instructionId = data["instructionId"] as? String {
            instructionId = _instructionId
        }
        updateFeedbacks(withData: data["feedbackIds"])
    }
    
}

// MARK: - Exercise
extension ExerciseReading {
    
    func hasRelatedAssets() -> Bool {
        return wordScoring != nil &&
            instruction != nil &&
            feedbacks.isEmpty == false
    }
    
}

// MARK: - Archive
extension ExerciseReading {
    
    func archive(_ archive: Bool) {
        isArchived = archive
        for lesson in lessons {
            lesson.archive(archive)
        }
        for scoring in scorings {
            scoring.archive(archive)
        }
    }
    
}

// MARK: - Private Functions
extension ExerciseReading {
    
    // MARK: Helpers
    
    fileprivate func updateLessons(withData data: Any?) {
        for lesson in lessons {
            lesson.archive(true)
        }
        lessons.removeAll()
        guard let _lessons = data as? [[String: Any]] else {
            return
        }
        for data in _lessons {
            let lesson = ExerciseReadingLesson(withData: data)
            lessons.append(lesson)
        }
    }
    
    fileprivate func updateExerciseScorings(withData data: Any?) {
        for scoring in scorings {
            scoring.archive(true)
        }
        scorings.removeAll()
        guard let _scorings = data as? [[String: Any]] else {
            return
        }
        for data in _scorings {
            let scoring = ExerciseReadingScoring(withData: data)
            scorings.append(scoring)
        }
    }
    
    fileprivate func updateFeedbacks(withData data: Any?) {
        feedbackIds.removeAll()
        guard let _identifiers = data as? [String] else {
            return
        }
        for _identifier in _identifiers {
            feedbackIds.append(_identifier)
        }
    }
    
}
