//
//  ExerciseReadingLesson.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 18/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseReadingLesson: Object, Lesson, Archivable {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var index: Int = 0
    @objc dynamic var name: String = ""
    let pages = List<ExerciseReadingLessonPage>()
    
    // Lesson
    var lessonIndex: Int {
        return index
    }
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        if let _index = data["index"] as? Int {
            index = _index
        }
        if let _name = data["name"] as? String {
            name = _name
        }
        updatePages(withData: data["pages"])
    }
    
    // MARK: Overridden Functions
    
    override class func ignoredProperties() -> [String] {
        return ["lessonIndex"]
    }
    
}

// MARK: - Archive
extension ExerciseReadingLesson {
    
    func archive(_ archive: Bool) {
        isArchived = archive
        for page in pages {
            page.archive(archive)
        }
    }
    
}

// MARK: - Private Functions
extension ExerciseReadingLesson {
    
    // MARK: Helpers
    
    fileprivate func updatePages(withData data: Any?) {
        guard let _pages = data as? [[String: Any]] else {
            return
        }
        for data in _pages {
            let page = ExerciseReadingLessonPage(withData: data)
            pages.append(page)
        }
    }
    
}
