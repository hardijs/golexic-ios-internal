//
//  ExerciseLetterBoard.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseLetterBoard: Object, Updatable, Archivable, Exercise {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var typeRaw: Int = 0
    var type: ExerciseType {
        return ExerciseType(rawValue: typeRaw) ?? .unknown
    }
    
    @objc dynamic var lettersId: String = ""
    var letters: Letters? {
       return availableRealm.object(ofType: Letters.self, forPrimaryKey: lettersId)
    }
    
    @objc dynamic var letterScoringId: String = ""
    var letterScoring: ScoringLetter? {
        return availableRealm.object(ofType: ScoringLetter.self, forPrimaryKey: letterScoringId)
    }
    @objc dynamic var passThreshold: Int = 0
    
    @objc dynamic var instructionId: String = ""
    var instruction: Instruction? {
        return availableRealm.object(ofType: Instruction.self, forPrimaryKey: instructionId)
    }
    
    let feedbackIds = List<String>()
    var feedbacks: Results<Feedback> {
        return availableRealm.objects(Feedback.self).filter("identifier IN %@", feedbackIds)
    }
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    required convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        update(withData: data)
    }
    
    // MARK: Overridden Functions
    
    override class func primaryKey() -> String? {
        return "identifier"
    }
    
    override class func ignoredProperties() -> [String] {
        return ["type", "letters", "letterScoring", "instruction", "feedbacks"]
    }
    
}

// MARK: - Updatable
extension ExerciseLetterBoard {
    
    func update(withData data: [String : Any]) {
        if let _typeRaw = data["type"] as? Int {
            typeRaw = _typeRaw
        }
        if let _lettersId = data["lettersId"] as? String {
            lettersId = _lettersId
        }
        if let _letterScoringId = data["letterScoringId"] as? String {
            letterScoringId = _letterScoringId
        }
        if let _passThreshold = data["passThreshold"] as? Int {
            passThreshold = _passThreshold
        }
        if let _instructionId = data["instructionId"] as? String {
            instructionId = _instructionId
        }
        updateFeedbacks(withData: data["feedbackIds"])
    }
    
}

// MARK: - Exercise
extension ExerciseLetterBoard {
    
    func hasRelatedAssets() -> Bool {
        return letters != nil &&
            letterScoring != nil &&
            instruction != nil &&
            feedbacks.isEmpty == false
    }
    
}

// MARK: - Private Functions
extension ExerciseLetterBoard {
    
    // MAKR: Helpers
    
    fileprivate func updateFeedbacks(withData data: Any?) {
        feedbackIds.removeAll()
        guard let _identifiers = data as? [String] else {
            return
        }
        for _identifier in _identifiers {
            feedbackIds.append(_identifier)
        }
    }
    
}
