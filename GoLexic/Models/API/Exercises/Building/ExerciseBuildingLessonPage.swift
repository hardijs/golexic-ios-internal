//
//  ExerciseBuildingLessonPage.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 18/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseBuildingLessonPage: Object, Archivable {
    
    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var text: String = ""
    @objc dynamic var mask: String = ""
    
    @objc dynamic var instructionText: String = ""
    @objc dynamic var instructionAudioUrl: String?
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        if let _text = data["text"] as? String {
            text = _text
        }
        if let _mask = data["mask"] as? String {
            mask = _mask
        }
        if let _instructions = data["instructions"] as? [String: Any] {
            if let _text = _instructions["text"] as? String {
                instructionText = _text
            }
            if let _audioUrl = _instructions["audioFile"] as? String {
                instructionAudioUrl = _audioUrl
            }
        }
    }


}

// MARK: - Public Functions
extension ExerciseBuildingLessonPage {
    
    func maskValue(forIndex index: Int) -> String? {
        guard index >= 0, index < mask.count else {
            return nil
        }
        let value = mask[index]
        guard value != "*" else {
            return nil
        }
        return String(value)
    }
    
}
