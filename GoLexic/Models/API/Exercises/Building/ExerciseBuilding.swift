//
//  ExerciseBuilding.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 18/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseBuilding: Object, Updatable, Archivable, Exercise {
    
    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var typeRaw: Int = 0
    var type: ExerciseType {
        return ExerciseType(rawValue: typeRaw) ?? .unknown
    }
    
    @objc dynamic var lettersId: String = ""
    var letters: Letters? {
       return availableRealm.object(ofType: Letters.self, forPrimaryKey: lettersId)
    }
    
    @objc dynamic var wordScoringId: String = ""
    var wordScoring: ScoringWord? {
        return availableRealm.object(ofType: ScoringWord.self, forPrimaryKey: wordScoringId)
    }
    
    let lessons = List<ExerciseBuildingLesson>()
    let scorings = List<ExerciseBuildingScoring>()
    
    @objc dynamic var instructionId: String = ""
    var instruction: Instruction? {
        return availableRealm.object(ofType: Instruction.self, forPrimaryKey: instructionId)
    }
    
    let feedbackIds = List<String>()
    var feedbacks: Results<Feedback> {
        return availableRealm.objects(Feedback.self).filter("identifier IN %@", feedbackIds)
    }
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    required convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        update(withData: data)
    }
    
    // MARK: Overridden Functions
    
    override class func primaryKey() -> String? {
        return "identifier"
    }
    
    override class func ignoredProperties() -> [String] {
        return ["type", "letters", "wordScoring", "instruction", "feedbacks"]
    }

}

// MARK: - Updatable
extension ExerciseBuilding {
    
    func update(withData data: [String : Any]) {
        if let _typeRaw = data["type"] as? Int {
            typeRaw = _typeRaw
        }
        if let _lettersId = data["lettersId"] as? String {
            lettersId = _lettersId
        }
        if let _wordScoringId = data["wordScoringId"] as? String {
            wordScoringId = _wordScoringId
        }
        updateLessons(withData: data["lessons"])
        updateExerciseScorings(withData: data["exerciseScoring"])
        if let _instructionId = data["instructionId"] as? String {
            instructionId = _instructionId
        }
        updateFeedbacks(withData: data["feedbackIds"])
    }
    
}

// MARK: - Archive
extension ExerciseBuilding {
    
    func archive(_ archive: Bool) {
        isArchived = archive
        for lesson in lessons {
            lesson.archive(archive)
        }
        for scoring in scorings {
            scoring.archive(archive)
        }
    }
    
}

// MARK: - Exercise
extension ExerciseBuilding {
    
    func hasRelatedAssets() -> Bool {
        return letters != nil &&
            wordScoring != nil &&
            instruction != nil &&
            feedbacks.isEmpty == false
    }
    
}

// MARK: - Private Functions
extension ExerciseBuilding {
    
    // MARK: Helpers
    
    fileprivate func updateLessons(withData data: Any?) {
        guard let _lessons = data as? [[String: Any]] else {
            return
        }
        lessons.removeAll()
        for data in _lessons {
            let lesson = ExerciseBuildingLesson(withData: data)
            lessons.append(lesson)
        }
    }
    
    fileprivate func updateExerciseScorings(withData data: Any?) {
        guard let _scorings = data as? [[String: Any]] else {
            return
        }
        scorings.removeAll()
        for data in _scorings {
            let scoring = ExerciseBuildingScoring(withData: data)
            scorings.append(scoring)
        }
    }
    
    fileprivate func updateFeedbacks(withData data: Any?) {
        feedbackIds.removeAll()
        guard let _identifiers = data as? [String] else {
            return
        }
        for _identifier in _identifiers {
            feedbackIds.append(_identifier)
        }
    }
    
}
