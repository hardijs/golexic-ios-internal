//
//  ExerciseBuildingLesson.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 18/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class ExerciseBuildingLesson: Object, Lesson, Archivable {
    
    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var index: Int = 0
    @objc dynamic var name: String = ""
    let pages = List<ExerciseBuildingLessonPage>()
    
    // Lesson
    var lessonIndex: Int {
        return index
    }
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        if let _index = data["index"] as? Int {
            index = _index
        }
        if let _name = data["name"] as? String {
            name = _name
        }
        updatePages(withData: data["pages"])
    }
    
    // MARK: Overridden Functions
    
    override class func ignoredProperties() -> [String] {
        return ["lessonIndex"]
    }

}

// MARK: - Archive
extension ExerciseBuildingLesson {
    
    func archive(_ archive: Bool) {
        isArchived = archive
        for page in pages {
            page.archive(archive)
        }
    }
    
}

// MARK: - Private Functions
extension ExerciseBuildingLesson {
    
    // MARK: Helpers
    
    fileprivate func updatePages(withData data: Any?) {
        guard let _texts = data as? [[String: Any]] else {
            return
        }
        for data in _texts {
            let page = ExerciseBuildingLessonPage(withData: data)
            pages.append(page)
        }
    }
    
}
