//
//  LetterItem.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class LetterItem: Object, Archivable {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var letter: String = ""
    @objc dynamic var isVowel: Bool = false
    let similarities = List<String>()
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        if let _letter = data["letter"] as? String {
            letter = _letter
        }
        if let _isVowel = data["isVowel"] as? Bool {
            isVowel = _isVowel
        }
        if let _similarities = data["similarities"] as? [String] {
            _similarities.forEach({ similarities.append($0) })
        }
    }
    
}
