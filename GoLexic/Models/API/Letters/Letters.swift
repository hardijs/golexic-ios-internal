//
//  Letters.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class Letters: Object, Updatable, Archivable {
    
    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    let items = List<LetterItem>()
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    required convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        update(withData: data)
    }
    
    // MARK: Overridden Functions
    
    override class func primaryKey() -> String? {
        return "identifier"
    }

}

// MARK: - Updatable
extension Letters {
    
    func update(withData data: [String : Any]) {
        updateLetters(withData: data["letters"])
    }
    
}

// MARK: - Private Functions
extension Letters {
    
    // MARK: Helpers
    
    fileprivate func updateLetters(withData data: Any?) {
        for item in items {
            item.archive(true)
        }
        items.removeAll()
        guard let _letters = data as? [[String: Any]] else {
            return
        }
        for data in _letters {
            let item = LetterItem(withData: data)
            items.append(item)
        }
    }
    
}
