//
//  PhaseExercise.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 19/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class PhaseExercise: Object, Archivable {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var exerciseTypeRaw: Int = 0
    var exerciseType: ExerciseType {
        return ExerciseType(rawValue: exerciseTypeRaw) ?? .unknown
    }
    @objc dynamic var exerciseId: String = ""
    var exercise: Exercise? {
       return exercise(forType: exerciseType, identifier: exerciseId)
    }
    let exerciseLessons = List<PhaseExerciseLesson>()
    
    @objc dynamic var isCompleted: Bool = false
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        if let _isCompleted = data["isCompleted"] as? Bool {
            isCompleted = _isCompleted
        }
        if let _exerciseTypeRaw = data["exerciseType"] as? Int {
            exerciseTypeRaw = _exerciseTypeRaw
        }
        if let _exerciseId = data["exerciseId"] as? String {
            exerciseId = _exerciseId
        }
        updateExerciseLessons(withData: data["exerciseLessons"])
    }
    
    // MARK: Overridden Functions
    
    override class func ignoredProperties() -> [String] {
        return ["exerciseType", "exercise"]
    }
    
}

// MARK: - Public Functions
extension PhaseExercise {
    
    func isAvailable() -> Bool {
        guard let _exercise = exercise,
            _exercise.hasRelatedAssets() else {
            return false
        }
        return true
    }
    
    func requiresMicAccess() -> Bool {
        let types: [ExerciseType] = [.alphabetPractice, .vowels,
                                     .reading, .building, .revise]
        return types.contains(exerciseType)
    }
    
    func indexOfFirstAvailableLesson() -> Int? {
        for x in 0..<exerciseLessons.count {
            let lesson = exerciseLessons[x]
            if lesson.isCompleted == false {
                return x
            }
        }
        return nil
    }
    
    func firstAvailableLesson() -> PhaseExerciseLesson? {
        guard let index = indexOfFirstAvailableLesson() else {
            return nil
        }
        return exerciseLessons[index]
    }
    
    func markAsCompleted() {
        guard isCompleted == false else {
            return
        }
        isCompleted = true
        for lesson in exerciseLessons {
            lesson.isCompleted = true
        }
    }
    
    func markLessonAsCompleted(_ lesson: PhaseExerciseLesson) {
        guard isCompleted == false,
            exerciseLessons.isEmpty == false else {
            return
        }
        lesson.isCompleted = true
        let incompleteLessons = exerciseLessons.filter("isCompleted == false")
        if incompleteLessons.isEmpty {
            isCompleted = true
        }
    }
    
}

// MAKR: - Private Functions
extension PhaseExercise {
    
    // MARK: Helpers
    
    fileprivate func updateExerciseLessons(withData data: Any?) {
        for exerciseLesson in exerciseLessons {
            exerciseLesson.archive(true)
        }
        exerciseLessons.removeAll()
        guard let _lessons = data as? [[String: Any]] else {
            return
        }
        for data in _lessons {
            let exerciseLesson = PhaseExerciseLesson(withData: data, exerciseType: exerciseType)
            exerciseLessons.append(exerciseLesson)
        }
    }
    
    // MARK: Make
    
    fileprivate func exercise(forType type: ExerciseType, identifier: String) -> Exercise? {
        let realm = try! Realm()
        switch type {
        case .alphabetPractice:
            return realm.object(ofType: ExerciseAlphabetPractice.self, forPrimaryKey: identifier)
        case .lostLetters:
            return realm.object(ofType: ExerciseLostLetters.self, forPrimaryKey: identifier)
        case .alphabetSorting:
            return realm.object(ofType: ExerciseAlphabetSorting.self, forPrimaryKey: identifier)
        case .letterBoard:
            return realm.object(ofType: ExerciseLetterBoard.self, forPrimaryKey: identifier)
        case .vowels:
            return realm.object(ofType: ExerciseVowels.self, forPrimaryKey: identifier)
        case .reading:
            return realm.object(ofType: ExerciseReading.self, forPrimaryKey: identifier)
        case .building:
            return realm.object(ofType: ExerciseBuilding.self, forPrimaryKey: identifier)
        case .revise:
            return realm.object(ofType: ExerciseRevise.self, forPrimaryKey: identifier)
        default:
            return nil
        }
    }
    
}
