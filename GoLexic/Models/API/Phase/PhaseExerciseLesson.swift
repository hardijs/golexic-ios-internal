//
//  PhaseExerciseLesson.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 27/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class PhaseExerciseLesson: Object, Archivable {

    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    @objc dynamic var exerciseTypeRaw: Int = 0
    var exerciseType: ExerciseType {
        return ExerciseType(rawValue: exerciseTypeRaw) ?? .unknown
    }
    
    @objc dynamic var lessonId: String = ""
    var lesson: Lesson? {
        return lesson(forType: exerciseType, identifier: lessonId)
    }
    
    @objc dynamic var isCompleted: Bool = false
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    convenience init(withData data: [String: Any], exerciseType: ExerciseType) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        exerciseTypeRaw = exerciseType.rawValue
        if let _lessonId = data["lessonId"] as? String {
            lessonId = _lessonId
        }
        if let _isCompleted = data["isCompleted"] as? Bool {
            isCompleted = _isCompleted
        }
    }
    
    // MARK: Overridden Functions
    
    override class func ignoredProperties() -> [String] {
        return ["exerciseType", "lesson"]
    }
    
}

// MAKR: - Private Functions
extension PhaseExerciseLesson {
    
    // MARK: Make
    
    fileprivate func lesson(forType type: ExerciseType, identifier: String) -> Lesson? {
        let realm = try! Realm()
        switch type {
        case .reading:
            let results = realm.objects(ExerciseReadingLesson.self).filter("identifier = %@", identifier)
            return results.last
        case .building:
            let results = realm.objects(ExerciseBuildingLesson.self).filter("identifier = %@", identifier)
            return results.last
        default:
            return nil
        }
    }
    
}
