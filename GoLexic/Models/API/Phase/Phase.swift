//
//  Phase.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 19/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift

class Phase: Object, Updatable, Archivable {
    
    // MARK: Properties
    
    @objc dynamic var identifier: String = UUID().uuidString
    
    let exercises = List<PhaseExercise>()
    
    @objc dynamic var isActivated: Bool = false // Set manually
    @objc dynamic var isCompleted: Bool = false // Set manually
    
    // Archivable
    @objc dynamic var isArchived: Bool = false
    
    // MARK: Initializers
    
    required convenience init(withData data: [String: Any]) {
        self.init()
        if let _identifier = data["id"] as? String {
            identifier = _identifier
        }
        update(withData: data)
    }
    
    // MARK: Overridden Functions
    
    override class func primaryKey() -> String? {
        return "identifier"
    }

}

// MARK: - Public Functions
extension Phase {
    
    func hasExercise(ofType type: ExerciseType) -> Bool {
        for exercise in exercises {
            guard exercise.exerciseType == type else {
                continue
            }
            return true
        }
        return false
    }
    
    func hasIncompleteExercises() -> Bool {
        for exercise in exercises {
            if exercise.isCompleted == false {
                return true
            }
        }
        return false
    }
    
    func indexOfFirstAvailableExercise() -> Int? {
        for x in 0..<exercises.count {
            let exercise = exercises[x]
            guard exercise.isCompleted == false else {
                continue
            }
            if exercise.exerciseType == .reading {
                guard let firstReading = exercise.indexOfFirstAvailableLesson() else {
                    // No reading lesson - skip
                    continue
                }
                guard let firstBuilding = indexOfFirstAvailableLesson(forType: .building) else {
                    // No building lessons - reading is always valid
                    return x
                }
                if firstReading == firstBuilding {
                    return x
                }
            } else if exercise.exerciseType == .building {
                guard let firstBuilding = exercise.indexOfFirstAvailableLesson() else {
                    // No building lesson - skip
                    continue
                }
                guard let firstReading = indexOfFirstAvailableLesson(forType: .reading) else {
                    // No reading lessons - building is always valid
                    return x
                }
                if firstBuilding < firstReading {
                    return x
                }
            } else {
                return x
            }
        }
        return nil
    }
    
    func indexOfFirstAvailableLesson(forType type: ExerciseType) -> Int? {
        for exercise in exercises {
            guard exercise.exerciseType == type else {
                continue
            }
            return exercise.indexOfFirstAvailableLesson()
        }
        return nil
    }
    
}

// MARK: - Updatable
extension Phase {
    
    func update(withData data: [String : Any]) {
        updateExercises(withData: data["exercises"])
        isCompleted = !hasIncompleteExercises()
    }
    
}

// MARK: - Archive
extension Phase {
    
    func archive(_ archive: Bool) {
        isArchived = archive
        for exercise in exercises {
            exercise.archive(archive)
        }
    }
    
}

// MARK: - Private Functions
extension Phase {
    
    // MARK: Helpers
    
    fileprivate func updateExercises(withData data: Any?) {
        for exercise in exercises {
            exercise.archive(true)
        }
        exercises.removeAll()
        guard let _exercises = data as? [[String: Any]] else {
            return
        }
        for data in _exercises {
            let exercise = PhaseExercise(withData: data)
            exercises.append(exercise)
        }
    }
    
}
