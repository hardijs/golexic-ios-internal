//
//  ExerciseLetter.swift
//  GoLexic
//
//  Created by Armands L. on 22/03/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import MobileCoreServices

final class ExerciseLetter: NSObject, Codable, Recognizable {

    // MARK: Properties
    
    var identifier = UUID().uuidString
    
    // Recognizable
    var status: RecognitionStatus = .none
    var value: String = ""
    var confidence: Double = 0
    var isSuggested: Bool = false
    var isFailed: Bool = false
    
    // Other
    var isVowel: Bool = false
    var similarities: Set<String> = []
    
    // MARK: Initializers
    
    init(withValue value: String) {
        super.init()
        self.value = value.lowercased()
    }
    
    init(withLetterItem letterItem: LetterItem) {
        super.init()
        self.value = letterItem.letter.lowercased()
        self.isVowel = letterItem.isVowel
        self.similarities = makeSimilarities(fromLetterItem: letterItem)
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        identifier = try values.decode(String.self, forKey: .identifier)
        // Recognizable
        status = try {
            let value = try values.decode(Int.self, forKey: .status)
            guard let _status = RecognitionStatus(rawValue: value) else {
                return .none
            }
            return _status
        }()
        value = try values.decode(String.self, forKey: .value)
        confidence = try values.decode(Double.self, forKey: .confidence)
        isSuggested = try values.decode(Bool.self, forKey: .isSuggestable)
        isFailed = try values.decode(Bool.self, forKey: .isFailed)
        isVowel = try values.decode(Bool.self, forKey: .isVowel)
        similarities = try values.decode(Set.self, forKey: .similarities)
        
    }
    
    // MARK: Encodable
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(identifier, forKey: .identifier)
        try container.encode(value, forKey: .value)
        // Recognizable
        try container.encode(status.rawValue, forKey: .status)
        try container.encode(confidence, forKey: .confidence)
        try container.encode(isSuggested, forKey: .isSuggestable)
        try container.encode(isFailed, forKey: .isFailed)
        try container.encode(isVowel, forKey: .isVowel)
        try container.encode(similarities, forKey: .similarities)
    }
    
    // MARK: Equatable
    
    static func == (lhs: ExerciseLetter, rhs: ExerciseLetter) -> Bool {
        return lhs.value == rhs.value
    }
    
    static func != (lhs: ExerciseLetter, rhs: ExerciseLetter) -> Bool {
        return lhs.value != rhs.value
    }

}

// MARK: - Recognizable
extension ExerciseLetter {
    
    @discardableResult
    func isEqualToAny(_ words: [KIOSWord]?, includeSimilarities: Bool) -> Bool {
        guard let _words = words else {
            return false
        }
        for word in _words {
            guard word.isTag == false else {
                continue
            }
            let text = word.text.lowercased()
            if value == text {
                return true
            } else if includeSimilarities, similarities.contains(text) {
                return true
            }
        }
        return false
    }
    
    @discardableResult
    func updateConfidenceIfMatches(_ words: [KIOSWord]?, includeSimilarities: Bool) -> Bool {
        guard let _words = words, confidence == 0,
            let _avarageConfidence = makeAvarageConfidence(forWords: _words, includeSimilarities: includeSimilarities) else {
            return false
        }
        confidence = _avarageConfidence
        return true
    }
    
}

// MARK: - NSItemProviderWriting
extension ExerciseLetter: NSItemProviderWriting {
    
    static var writableTypeIdentifiersForItemProvider: [String] {
        return [(kUTTypeData as String)]
    }
    
    func loadData(withTypeIdentifier typeIdentifier: String,
                  forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) -> Progress? {
        let progress = Progress(totalUnitCount: 100)
        do {
            let data = try JSONEncoder().encode(self)
            progress.completedUnitCount = 100
            completionHandler(data, nil)
        } catch {
            completionHandler(nil, error)
        }
        return progress
    }
    
}

// MARK: - NSItemProviderReading
extension ExerciseLetter: NSItemProviderReading {
    
    static var readableTypeIdentifiersForItemProvider: [String] {
        return [(kUTTypeData) as String]
    }
    
    static func object(withItemProviderData data: Data, typeIdentifier: String) throws -> ExerciseLetter {
        let decoder = JSONDecoder()
        do {
            //Here we decode the object back to it's class representation and return it
            let subject = try decoder.decode(ExerciseLetter.self, from: data)
            return subject
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
}

// MAKR: - Private Functions
extension ExerciseLetter {
    
    // MARK: Enums
    
    fileprivate enum CodingKeys: String, CodingKey {
        case identifier
        case value
        case status
        case confidence
        case isSuggestable
        case isFailed
        case isVowel
        case similarities
    }
    
    // MARK: Make
    
    fileprivate func makeSimilarities(fromLetterItem item: LetterItem) -> Set<String>{
        var _texts: Set<String> = []
        for text in item.similarities {
            let lowercaseText = text.lowercased()
            _texts.insert(lowercaseText)
        }
        return _texts
    }
    
    fileprivate func makeAvarageConfidence(forWords words: [KIOSWord], includeSimilarities: Bool) -> Double? {
        var matchingWords: [KIOSWord] = []
        for word in words {
            let text = word.text.lowercased()
            if text == value {
                matchingWords.append(word)
            }
            guard includeSimilarities else {
                continue
            }
            if similarities.contains(text) {
                matchingWords.append(word)
            }
        }
        guard matchingWords.isEmpty == false else {
            return nil
        }
        var totalConfidence: Double = 0
        for word in matchingWords {
            guard let _confidence = word.confidence else {
                continue
            }
            totalConfidence += _confidence.doubleValue
        }
        return totalConfidence/Double(matchingWords.count)
    }
    
}
