//
//  ExerciseText.swift
//  GoLexic
//
//  Created by Armands L. on 16/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit

class ExerciseText: NSObject, Recognizable {

    // MARK: Properties
    
    var identifier = UUID().uuidString
    
    // Recognizable
    var status: RecognitionStatus = .none
    var value: String = ""
    var confidence: Double = 0
    var isSuggested: Bool = false
    var isFailed: Bool = false
    
    // Other
    fileprivate(set) lazy var words: [ExerciseText] = {
       return makeWords()
    }()
    fileprivate(set) lazy var letters: [ExerciseLetter] = {
       return makeLetters()
    }()
    var comparableValue: String = ""
    var isSentence: Bool = false
    
    // MARK: Initializers
    
    init(withValue value: String) {
        super.init()
        self.value = value
        self.comparableValue = value.lowercased()
    }
    
    init(withReadingText readingText: ExerciseReadingLessonPageLineText) {
        super.init()
        self.value = readingText.text
        self.comparableValue = value.lowercased()
        self.isSentence = readingText.isSentence
    }
    
    init(withRevisePage revisePage: ExerciseRevisePage) {
        super.init()
        self.value = revisePage.text
        self.comparableValue = value.lowercased()
        self.isSentence = revisePage.isSentence
    }
    
    // MARK: Equatable
    
    static func == (lhs: ExerciseText, rhs: ExerciseText) -> Bool {
        return lhs.value == rhs.value
    }
    
    static func != (lhs: ExerciseText, rhs: ExerciseText) -> Bool {
        return lhs.value != rhs.value
    }
    
}

// MARK: - Recognizable
extension ExerciseText {
    
    func isEqualToAny(_ words: [KIOSWord]?, includeSimilarities: Bool) -> Bool {
        guard let _words = words else {
            return false
        }
        if isSentence {
            for word in self.words {
                if word.isEqualToAny(_words, includeSimilarities: includeSimilarities) == false {
                    return false
                }
            }
            return true
        } else {
            for word in _words {
                guard word.isTag == false else {
                    continue
                }
                let text = word.text.lowercased()
                if comparableValue == text {
                    return true
                }
            }
            return false
        }
    }
    
    @discardableResult
    func updateConfidenceIfMatches(_ words: [KIOSWord]?, includeSimilarities: Bool) -> Bool {
        guard let _words = words, confidence == 0 else {
            return false
        }
        if isSentence {
            guard let _avarageConfidence = updateConfidenceOfWords(withWords: _words) else {
                return false
            }
            confidence = _avarageConfidence
            return true
        } else {
            guard let _avarageConfidence = makeAvarageConfidence(forWords: _words) else {
                return false
            }
            confidence = _avarageConfidence
            return true
        }
    }
    
}

// MARK: - Private Functions
extension ExerciseText {
    
    // MARK: Helpers
    
    fileprivate func updateConfidenceOfWords(withWords words: [KIOSWord]) -> Double? {
        var didMatchWord: Bool = false
        var confidences: [Double] = []
        for word in self.words {
            let hasMatch = word.updateConfidenceIfMatches(words, includeSimilarities: false)
            if hasMatch {
                didMatchWord = true
            }
            confidences.append(word.confidence)
        }
        guard didMatchWord else {
            return nil
        }
        let total = confidences.reduce(0, +)
        let count = Double(confidences.count)
        return total/count
    }
    
    // MARK: Make
    
    fileprivate func makeWords() -> [ExerciseText] {
        var _words: [ExerciseText] = []
        let values = comparableValue.components(separatedBy: " ")
        for value in values {
            let word = ExerciseText(withValue: value)
            _words.append(word)
        }
        return _words
    }
    
    fileprivate func makeLetters() -> [ExerciseLetter] {
        var _letters: [ExerciseLetter] = []
        for character in value {
            let value = String(character)
            let letter = ExerciseLetter(withValue: value)
            _letters.append(letter)
        }
        return _letters
    }
    
    fileprivate func makeAvarageConfidence(forWords words: [KIOSWord]) -> Double? {
        var matchingWords: [KIOSWord] = []
        for word in words {
            let text = word.text.lowercased()
            if text == value {
                matchingWords.append(word)
            }
        }
        guard matchingWords.isEmpty == false else {
            return nil
        }
        var totalConfidence: Double = 0
        for word in matchingWords {
            guard let _confidence = word.confidence else {
                continue
            }
            totalConfidence += _confidence.doubleValue
        }
        return totalConfidence/Double(matchingWords.count)
    }
    
}
