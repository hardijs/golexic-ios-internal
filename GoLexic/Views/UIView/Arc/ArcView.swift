//
//  ArcView.swift
//
//  Created by Armands L.
//  Copyright © 2018 Cube Mobile. All rights reserved.
//

import UIKit

class ArcView: UIView {
    
    // MARK: Enums
    
    enum Direction {
        case right
        case bottom
    }
    
    // MARK: Properties
    
    // UI
    fileprivate lazy var shape: CAShapeLayer = {
        return makeShape()
    }()
    
    // Variables
    var direction: Direction = .right
    fileprivate var currentBounds: CGRect = .zero

    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        guard currentBounds != bounds else {
            return
        }
        currentBounds = bounds
        shape.frame = bounds
        shape.path = drawPath()
    }

}

// MARK: - Private Functions
extension ArcView {
    
    // MARK: Helpers
    
    fileprivate func configureView() {
        layer.mask = shape
    }
    
    // MARK: Draw
    
    fileprivate func drawPath() -> CGPath {
        switch direction {
        case .right:
            return drawRightPath()
        case .bottom:
            return drawBottomPath()
        }
    }
    
    fileprivate func drawRightPath() -> CGPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 0, y: frame.height))
        path.addQuadCurve(to: CGPoint(x: 0, y: 0),
                          controlPoint: CGPoint(x: frame.width*2, y: frame.height/2))
        return path.cgPath
    }
    
    fileprivate func drawBottomPath() -> CGPath {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: frame.width, y: 0))
        path.addQuadCurve(to: CGPoint(x: 0, y: 0),
                          controlPoint: CGPoint(x: frame.width/2, y: frame.height*2))
        return path.cgPath
    }
    
    // MARK: Make
    
    fileprivate func makeShape() -> CAShapeLayer {
        let shape = CAShapeLayer()
        shape.fillColor = UIColor.black.cgColor
        shape.fillRule = .evenOdd
        return shape
    }
    
}
