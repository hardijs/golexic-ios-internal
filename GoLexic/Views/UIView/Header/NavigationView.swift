//
//  NavigationView.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 28/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

protocol NavigationViewDelegate: class {
    
    func navigationView(_ view: NavigationView, didSelect action: NavigationView.Action)
    
}

class NavigationView: UIView, StoryboardAddable {
    
    // MARK: Enums
    
    enum Action {
        case back
    }

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var backButton: BackButton!
    @IBOutlet fileprivate var titleLabel: UILabel!
    
    // StoryboardAddable
    var contentView: UIView!
    
    // Variables
    weak var delegate: NavigationViewDelegate?
    @IBInspectable var titleKey: String? {
        didSet {
            updateContent()
        }
    }
    @IBInspectable var backKey: String? {
        didSet {
            updateContent()
        }
    }
    var isBackEnabled: Bool = true {
        didSet {
            backButton.isEnabled = isBackEnabled
        }
    }
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibWithConstraints()
        configureView()
        updateContent()
        subscribeToNotifications()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadXibWithConstraints()
    }
    
    // MAKR: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
        updateContent()
        subscribeToNotifications()
    }

}

// MARK: - Targets
extension NavigationView {
    
    @IBAction fileprivate func backButtonPressed(_ sender: UIButton) {
        if let _delegate = delegate {
            _delegate.navigationView(self, didSelect: .back)
        }
    }
    
}

// MARK: - Private Functions
extension NavigationView {
    
    // MAKR: Helpers
    
    fileprivate func configureView() {
        layer.shadowRadius = 24
        layer.shadowOffset = CGSize(width: 0, height: 12)
        layer.shadowColor = #colorLiteral(red: 0.7490196078, green: 0.7490196078, blue: 0.7490196078, alpha: 1).cgColor
        layer.shadowOpacity = 0.25
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.Localization.DidChange,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            _self.updateContent()
        }
    }
    
    fileprivate func updateContent() {
        if let _titleKey = titleKey {
            titleLabel.text = Localization.string(forKeyPath: _titleKey)
        } else {
            titleLabel.text = nil
        }
        if let _backKey = backKey {
            backButton.title = Localization.string(forKeyPath: _backKey)
        } else {
            backButton.title = nil
        }
    }
    
}
