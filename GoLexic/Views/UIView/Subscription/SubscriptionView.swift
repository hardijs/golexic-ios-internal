//
//  SubscriptionView.swift
//  GoLexic
//
//  Created by Denis Kuznetsov on 09.03.2021.
//  Copyright © 2021 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class SubscriptionView: UIView, StoryboardAddable {

    //MARK: Properties
    
    // IB
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var discountLabel: UILabel!
    @IBOutlet fileprivate var actionButton: InspectableButton!
    @IBOutlet fileprivate var discountView: UIView!

    // StoryboardAddable
    var contentView: UIView!
    
    // Variables
    weak var delegate: SubscriptionViewDelegate?
    
    @IBInspectable var titleKey: String? {
        didSet {
            if let _key = titleKey {
                titleText = Localization.string(forKeyPath: _key)
            }
        }
    }
    @IBInspectable var descriptionKey: String? {
        didSet {
            if let _key = descriptionKey {
                descriptionText = Localization.string(forKeyPath: _key)
            }
        }
    }
    
    @IBInspectable var isSelected: Bool = false {
        didSet {
            actionButton.isSelected = isSelected
            actionButton.borderColor = actionButton.isSelected ? selectedStrokeColor : normalStrokeColor
        }
    }
    
    var titleText: String? {
        didSet {
            titleLabel.text = titleText
        }
    }
    
    var descriptionText: String? {
        didSet {
            descriptionLabel.text = descriptionText
        }
    }
    var discountText: String? {
        didSet {
            if let _key = discountText {
                discountLabel.text = discountText
                discountView.isHidden = false
            } else {
                discountView.isHidden = true
            }
        }
    }
    fileprivate let normalStrokeColor = #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 0)
    fileprivate let selectedStrokeColor = #colorLiteral(red: 0.2980392157, green: 0.2862745098, blue: 0.5058823529, alpha: 1)
    
    // MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibWithConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadXibWithConstraints()
    }
    
    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction fileprivate func actionButtonPressed(_ sender: InspectableButton) {
        sender.isSelected = true
        sender.borderColor = sender.isSelected ? selectedStrokeColor : normalStrokeColor
        delegate?.subscriptionSelected(tag, sender.isSelected)
    }
    
}

// MARK: - SubscriptionViewDelegate
protocol SubscriptionViewDelegate: class {
    func subscriptionSelected(_ index: Int, _ isSelected: Bool)
}
