//
//  LetterView.swift
//  GoLexic
//
//  Created by Armands L. on 09/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class LetterView: UIView, StoryboardAddable {

    // MARK: Properties
    
    // Static
    static var defaultBorderColor: UIColor = #colorLiteral(red: 0.6431372549, green: 0.6941176471, blue: 0.768627451, alpha: 1)
    
    // StoryboardAddable
    var contentView: UIView!
    
    // IB
    @IBOutlet fileprivate var letterLabel: UILabel!
    
    // Variables
    var letter: ExerciseLetter? {
        didSet {
            if let _letter = letter {
                letterLabel.text = _letter.value
            } else {
                letterLabel.text = nil
            }
        }
    }
    var color: UIColor = .white {
        didSet {
            contentView.backgroundColor = color
        }
    }
    var borderColor: UIColor = defaultBorderColor {
        didSet {
            contentView.layer.borderColor = borderColor.cgColor
        }
    }
    
    // MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibWithConstraints()
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadXibWithConstraints()
    }
    
    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }

}

// MARK: - Private Functions
extension LetterView {
    
    fileprivate func configureView() {
        layer.shadowOffset = CGSize(width: 0, height: 6)
        layer.shadowColor = #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1).cgColor
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 4
        contentView.layer.cornerRadius = 7
        contentView.layer.borderWidth = 1
        contentView.layer.borderColor = LetterView.defaultBorderColor.cgColor
        contentView.layer.masksToBounds = true
    }
    
}
