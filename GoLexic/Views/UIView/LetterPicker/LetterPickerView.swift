//
//  LetterPickerView.swift
//  GoLexic
//
//  Created by Armands L. on 12/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class LetterPickerView: UIView, StoryboardAddable {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var containerView: BoundlessView!
    
    // StoryboardAddable
    var contentView: UIView!
    
    // Variables
    fileprivate var letterSize: CGSize = CGSize(width: 40, height: 40)
    fileprivate var letterViews: [LetterView] = []
    
    fileprivate var letterLines = 5
    fileprivate var letterColumns = 11
    fileprivate lazy var letterPositions: [CGPoint] = {
        // Generates 28 random positions
        return makeLetterPositions(lines: letterLines, columns: letterColumns)
    }()
    
    fileprivate var currentFrame: CGRect = .zero
    
    // Flags
    fileprivate var didLayoutSubviews: Bool = false
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibWithConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadXibWithConstraints()
    }
    
    // MARK: Overridden Functions
    
    override func layoutSubviews() {
        super.layoutSubviews()
        didLayoutSubviews = true
        guard frame != currentFrame else {
            return
        }
        currentFrame = frame
        contentView.layoutIfNeeded()
        updateLetterViews()
    }
    
}

// MARK: - Public Functions
extension LetterPickerView {
    
    func update(withLetters letters: [ExerciseLetter], shuffle: Bool = true) {
        for letterView in letterViews {
            letterView.removeFromSuperview()
        }
        letterViews.removeAll()
        let _letters: [ExerciseLetter] = {
            if shuffle {
                return letters.shuffled()
            }
            return letters
        }()
        for letter in _letters {
            guard letter.status == .none else {
                continue
            }
            let letterView = makeLetterView(withLetter: letter, size: letterSize)
            addAndConfigureLetterView(letterView)
        }
        if didLayoutSubviews {
            updateLetterViews()
        }
    }
    
    func updateAppearances() {
        for letterView in letterViews {
            guard let _letter = letterView.letter else {
                continue
            }
            if _letter.isSuggested {
                letterView.color = #colorLiteral(red: 0.9647058824, green: 0.7294117647, blue: 0.6, alpha: 1)
            } else {
                letterView.color = .white
            }
        }
    }
    
    func updateLayout(withLetterSize letterSize: CGSize) {
        guard letterSize != .zero else {
            return
        }
        self.letterSize = letterSize
        if didLayoutSubviews {
            updateLetterViews()
        }
    }
    
}

// MARK: - UIDragInteractionDelegate
extension LetterPickerView: UIDragInteractionDelegate {
    
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        let location = session.location(in: containerView)
        let letterView = letterViewForPointInContainer(location)
        guard let _letterView = letterView,
            let _letter = letterView?.letter,
            _letter.status == .none else {
                return []
        }
        // Reset letter suggested status
        _letter.isSuggested = false
        updateAppearances()
        // Return drag item
        let provider = NSItemProvider(object: _letter)
        let item = UIDragItem(itemProvider: provider)
        item.localObject = _letterView
        return [item]
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
        guard let letterView = item.localObject as? LetterView else {
            return nil
        }
        // Make preview view
        let _letterView = makeLetterView(withLetter: letterView.letter!, size: letterSize)
        _letterView.frame = letterView.frame
        // Make preview parameters
        let previewParameters = UIDragPreviewParameters()
        previewParameters.visiblePath = UIBezierPath(roundedRect: _letterView.bounds, cornerRadius: 7)
        previewParameters.backgroundColor = .clear
        // Make preview target
        let center = CGPoint(x: _letterView.bounds.midX, y: _letterView.bounds.midY)
        let target = UIDragPreviewTarget(container: letterView, center: center)
        return UITargetedDragPreview(view: _letterView, parameters: previewParameters, target: target)
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, sessionWillBegin session: UIDragSession) {
        guard let letterView = session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return
        }
        letterView.alpha = .leastNonzeroMagnitude
        Analytics.logLetterLift(letter.value)
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, previewForCancelling item: UIDragItem,
                         withDefault defaultPreview: UITargetedDragPreview) -> UITargetedDragPreview? {
        return defaultPreview
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, session: UIDragSession, didEndWith operation: UIDropOperation) {
        guard let letterView = session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return
        }
        if letter.status != .none {
            letterView.isHidden = true
        }
        letterView.alpha = 1
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, sessionIsRestrictedToDraggingApplication session: UIDragSession) -> Bool {
        return true
    }
    
}

// MARK: - Private Functions
extension LetterPickerView {
    
    // MARK: Helpers
    
    fileprivate func addAndConfigureLetterView(_ letterView: LetterView) {
        // Add to superview
        letterView.isHidden = true
        containerView.addSubview(letterView)
        letterViews.append(letterView)
        // Drag interaction
        let dragInteraction = UIDragInteraction(delegate: self)
        dragInteraction.isEnabled = true
        dragInteraction.allowsSimultaneousRecognitionDuringLift = false
        letterView.addInteraction(dragInteraction)
        // Reduce press duration of UIDragInteraction
        if let longPressRecognizer = letterView.gestureRecognizers?.compactMap({ $0 as? UILongPressGestureRecognizer}).first {
            longPressRecognizer.minimumPressDuration = .leastNonzeroMagnitude
        }
    }
    
    func letterPosition(forIndex index: Int) -> CGPoint? {
        guard index < letterPositions.count else {
            return nil
        }
        return letterPositions[index]
    }
    
    fileprivate func letterViewForPointInContainer(_ point: CGPoint) -> LetterView? {
        var _letterView: LetterView?
        for view in letterViews {
            if view.frame.contains(point) {
                _letterView = view
                break
            }
        }
        return _letterView
    }
    
    // MARK: Update
    
    fileprivate func updateLetterViews() {
        for x in 0..<letterViews.count {
            let view = letterViews[x]
            guard let position = letterPosition(forIndex: x) else {
                continue
            }
            let location = makeLetterLocation(forPosition: position,
                                              offset: letterViews.count > 1)
            let width = letterSize.width
            let height = letterSize.height
            view.frame = CGRect(x: location.x-width/2, y: location.y-height/2,
                                width: width, height: height)
            if let _letter = view.letter, _letter.status != .recognized {
                view.isHidden = false
            } else {
                view.isHidden = true
            }
        }
    }
    
    // MARK: Make
    
    fileprivate func makeLetterView(withLetter letter: ExerciseLetter, size: CGSize) -> LetterView {
        let frame = CGRect(origin: .zero, size: size)
        let letterView = LetterView(frame: frame)
        letterView.letter = letter
        if letter.isSuggested {
            letterView.color = #colorLiteral(red: 0.9647058824, green: 0.7294117647, blue: 0.6, alpha: 1)
        } else {
            letterView.color = .white
        }
        return letterView
    }
    
    func makeLetterLocation(forPosition position: CGPoint, offset: Bool) -> CGPoint {
        let horizontalSpacing = containerView.frame.width/CGFloat(letterColumns-1)
        let verticalSpacing = containerView.frame.height/CGFloat(letterLines-1)
        let maxOffset = offset ? floor(horizontalSpacing/8) : 0
        return makeLetterLocation(forPosition: position, horizontalSpacing: horizontalSpacing,
                                  verticalSpacing: verticalSpacing, maxOffset: maxOffset)
    }
    
    fileprivate func makeLetterPositions(lines: Int, columns: Int) -> [CGPoint] {
        var positions: [CGPoint] = []
        // Generate positions
        let maxCountPerLine = (columns-1)/2+1
        for y in 0..<lines {
            let startX = y % 2
            let count = maxCountPerLine-startX
            for x in 0..<count {
                let posX = startX + x * 2
                let position = CGPoint(x: posX, y: y)
                positions.append(position)
            }
        }
        // Sort positions
        let lineCenter = (lines-1)/2
        let columntCenter = (columns-1)/2
        let center = CGPoint(x: columntCenter, y: lineCenter)
        positions.sort { (lhs, rhs) -> Bool in
            // LHS delta
            let xDeltaLhs = abs(center.x-lhs.x)
            let yDeltaLhs = abs(center.y-lhs.y)
            let deltaLhs = xDeltaLhs+yDeltaLhs
            // RHS delta
            let xDeltaRhs = abs(center.x-rhs.x)
            let yDeltaRhs = abs(center.y-rhs.y)
            let deltaRhs = xDeltaRhs+yDeltaRhs
            return deltaLhs < deltaRhs
        }
        return positions
    }
    
    fileprivate func makeLetterLocation(forPosition position: CGPoint, horizontalSpacing: CGFloat,
                                        verticalSpacing: CGFloat, maxOffset: CGFloat) -> CGPoint {
        let x = position.x*horizontalSpacing
        let y = position.y*verticalSpacing
        let xOffset: CGFloat = {
            let random = CGFloat(arc4random_uniform(100))
            let direction = Int(random) % 2 == 0 ? -1 : 1
            let offset = CGFloat(maxOffset) * CGFloat(random/100) * CGFloat(direction)
            return floor(offset)
        }()
        let yOffset: CGFloat = {
            let random = CGFloat(arc4random_uniform(100))
            let direction = Int(random) % 2 == 0 ? -1 : 1
            let offset = CGFloat(maxOffset) * CGFloat(random/100) * CGFloat(direction)
            return floor(offset)
        }()
        return CGPoint(x: x+xOffset, y: y+yOffset)
    }
    
}
