//
//  FadeView.swift
//
//  Created by Armands Lazdiņš.
//  Copyright © 2017 Cube Mobile. All rights reserved.
//

import UIKit

class FadeView: UIView {
    
    //MARK: Enums
    
    enum Direction {
        case down
        case up
    }
    
    //MARK: Properties
    
    //UI
    fileprivate var gradientLayer: CAGradientLayer!
    
    //Variables
    var direction: Direction = .down {
        didSet {
            guard gradientLayer != nil else {
                return
            }
            updateGradient(withDirection: direction)
        }
    }
    @IBInspectable var color: UIColor = .black {
        didSet {
            guard gradientLayer != nil else {
                return
            }
            updateGradient(withDirection: direction)
        }
    }
    @IBInspectable var isUpside: Bool = false {
        didSet {
            if isUpside == true {
                direction = .up
            } else {
                direction = .down
            }
        }
    }
    fileprivate var currentFrame = CGRect.zero
    
    //MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if frame.equalTo(currentFrame) == false {
            currentFrame = frame
            gradientLayer.frame = bounds
        }
    }
    
}

//MARK: - Private Functions
extension FadeView {
    
    fileprivate func configureView() {
        clipsToBounds = true
        layer.masksToBounds = true
        gradientLayer = CAGradientLayer()
        updateGradient(withDirection: direction)
        layer.addSublayer(gradientLayer!)
    }
    
    fileprivate func updateGradient(withDirection direction: Direction) {
        if direction == .down {
            gradientLayer.colors = [color.cgColor, color.withAlphaComponent(0).cgColor]
        } else if direction == .up {
            gradientLayer.colors = [color.withAlphaComponent(0).cgColor, color.cgColor]
        }
    }
    
}
