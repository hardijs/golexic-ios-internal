//
//  InputView.swift
//
//  Created by Armands L.
//  Copyright © 2018 Cube Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class InputView: UIView, StoryboardAddable {
    
    // MARK: Enums
    
    enum State {
        case normal
        case error
    }

    //MARK: Properties
    
    // IB
    @IBOutlet var placeholderLabel: UILabel!
    @IBOutlet var inputContainer: InspectableView!
    @IBOutlet var accessoryImageView: UIImageView!
    @IBOutlet var inputTextField: LocalizedTextField!
    @IBOutlet var errorView: UIView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var secureButton: UIButton!

    // StoryboardAddable
    var contentView: UIView!
    
    // Variables
    weak var delegate: InputViewDelegate?
    @IBInspectable var icon: UIImage? {
        didSet {
            accessoryImageView.image = icon
        }
    }
    
    @IBInspectable var placeholderKey: String? {
        didSet {
            if let _key = placeholderKey {
                placeholderText = Localization.string(forKeyPath: _key)
            }
        }
    }
    @IBInspectable var errorKey: String? {
        didSet {
            if let _key = errorKey {
                errorText = Localization.string(forKeyPath: _key)
            }
        }
    }
    @IBInspectable var secureButtonEnabled: Bool = false {
        didSet {
            secureButton.isHidden = !secureButtonEnabled
        }
    }
    var placeholderText: String? {
        didSet {
            placeholderLabel.text = placeholderText
        }
    }
    var errorText: String? {
        didSet {
            errorLabel.text = errorText
        }
    }
    var prefix: String?
    var toolbar: UIToolbar? {
        didSet {
            inputTextField.inputAccessoryView = toolbar
        }
    }
    var keyboardType: UIKeyboardType = .default {
        didSet {
            inputTextField.keyboardType = keyboardType
        }
    }
    var textContentType: UITextContentType? {
        didSet {
            guard let _textContentType = textContentType else {
                return
            }
            inputTextField.textContentType = _textContentType
        }
    }
    var isSecureTextEntry: Bool = false {
        didSet {
            inputTextField.isSecureTextEntry = isSecureTextEntry
        }
    }
    var capitalization: UITextAutocapitalizationType = .allCharacters {
        didSet {
            inputTextField.autocapitalizationType = capitalization
        }
    }
    var returnKeyType: UIReturnKeyType = .done {
        didSet {
            inputTextField.returnKeyType = returnKeyType
        }
    }
    fileprivate(set) var state: State = .normal
    fileprivate let normalStrokeColor = #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 0.5)
	fileprivate let selectedStrokeColor = #colorLiteral(red: 0.2980392157, green: 0.2862745098, blue: 0.5058823529, alpha: 1)
    fileprivate let errorStrokeColor = #colorLiteral(red: 0.6588235294, green: 0.1490196078, blue: 0.1490196078, alpha: 1)
    
    // MARK: Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibWithConstraints()
        configureView()
        subscribeToNotifications()
        updateAppearance()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadXibWithConstraints()
    }
    
    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
        subscribeToNotifications()
        updateAppearance()
    }
    
    @discardableResult
    override func becomeFirstResponder() -> Bool {
        inputTextField.becomeFirstResponder()
        return true
    }

    @discardableResult
    override func resignFirstResponder() -> Bool {
        inputTextField.resignFirstResponder()
        return true
    }
    
}

// MARK - Public Functions
extension InputView {
    
    func clear() {
        inputTextField.text = nil
    }
    
    func update(withText text: String?, state: State? = nil) {
        let processedText: String? = {
            guard let _text = text,
                _text.isEmpty == false else {
                return nil
            }
            if let _prefix = prefix {
                return "\(_prefix)\(_text)"
            }
            return _text
        }()
        inputTextField.text = processedText
        if let _state = state {
            change(state: _state)
        }
    }
    
    func change(state: State) {
        guard self.state != state else {
            return
        }
        self.state = state
        updateAppearance()
    }
    
    func makeText() -> String? {
        guard var _input = inputTextField.text else {
            return nil
        }
        if let _prefix = prefix {
            let cleanInput = _input.dropFirst(_prefix.count)
            _input = String(cleanInput)
        }
        guard _input.isEmpty == false else {
            return nil
        }
        return _input
    }
    
}

// MARK: - Targets
extension InputView {
    
    @objc fileprivate func doneToolbarButtonPressed(_ sender: UIBarButtonItem) {
        inputTextField.resignFirstResponder()
    }
    
}

// MARK: - InputViewDelegate
protocol InputViewDelegate: class {
    
    func inputViewShouldBeginEditting(_ view: InputView) -> Bool
    func inputViewDidBeginEditting(_ view: InputView)
    func inputViewShouldEndEditting(_ view: InputView) -> Bool
    func inputViewDidEndEditting(_ view: InputView)
    func inputViewShouldReturn(_ view: InputView) -> Bool
    func inputView(_ view: InputView, shouldChangeCharacters string: String, inRage: NSRange) -> Bool
    func inputView(_ view: InputView, didChange text: String?)
    
}

// MARK: - InputViewDelegate
extension InputViewDelegate {
    
    func inputViewShouldBeginEditting(_ view: InputView) -> Bool {
        return true
    }
    func inputViewDidBeginEditting(_ view: InputView) {}
    func inputViewShouldEndEditting(_ view: InputView) -> Bool {
        return true
    }
    func inputViewDidEndEditting(_ view: InputView) {}
    func inputViewShouldReturn(_ view: InputView) -> Bool {
        return true
    }
    func inputView(_ view: InputView, shouldChangeCharacters string: String, inRage: NSRange) -> Bool {
        return true
    }
    func inputView(_ view: InputView, didChange text: String?) {}
    
}

// MARK: - UITextFieldDelegate
extension InputView: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let _delegate = delegate {
            return _delegate.inputViewShouldBeginEditting(self)
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let _prefix = prefix, isInputTextFieldEmpty() {
            textField.text = _prefix
        }
        if let _delegate = delegate {
            _delegate.inputViewDidBeginEditting(self)
        }
        updateAppearance()
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let _delegate = delegate {
            return _delegate.inputViewShouldEndEditting(self)
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let _prefix = prefix,
            textField.text == _prefix {
            textField.text = nil
        }
        if let _delegate = delegate {
            _delegate.inputViewDidEndEditting(self)
        }
        updateAppearance()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let _prefix = prefix,
            string.isEmpty, textField.text == _prefix {
            return false
        }
        if let _delegate = delegate,
            _delegate.inputView(self, shouldChangeCharacters: string, inRage: range) == false {
            return false
        }
        let text: String = {
            let text = textField.text ?? ""
            let _text = text as NSString
            return _text.replacingCharacters(in: range, with: string)
        }()
        let selectedRange  = textField.selectedTextRange
        textField.text = text
        if let _selectedRange = selectedRange {
            let offset = string.count > 0 ? string.count : -1
            if let newPosition = textField.position(from: _selectedRange.start, offset: offset) {
                textField.selectedTextRange = textField.textRange(from: newPosition, to: newPosition)
            }
        }
        if let _delegate = delegate {
            let text = makeText()
            _delegate.inputView(self, didChange: text)
        }
        return false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let _delegate = delegate {
            return _delegate.inputViewShouldReturn(self)
        }
        return true
    }
    
}

// MARK: - Private Functions
extension InputView {
    
    // MARK: Helpers
    
    fileprivate func configureView() {
        inputContainer.borderColor = normalStrokeColor
        secureButton.isHidden = !secureButtonEnabled
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.Localization.DidChange,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            if let _placeholderKey = _self.placeholderKey {
                _self.placeholderText = Localization.string(forKeyPath: _placeholderKey)
            }
        }
    }
    
    fileprivate func updateAppearance() {
        if state == .error {
            inputContainer.borderColor = errorStrokeColor
			placeholderLabel.textColor = normalStrokeColor
			accessoryImageView.alpha = 0.5
            secureButton.alpha = 0.5
		} else if inputTextField.isEditing {
			inputContainer.borderColor = selectedStrokeColor
			placeholderLabel.textColor = selectedStrokeColor
			accessoryImageView.alpha = 1.0
            secureButton.alpha = 1.0
        } else {
            inputContainer.borderColor = normalStrokeColor
			placeholderLabel.textColor = normalStrokeColor
			accessoryImageView.alpha = 0.5
            secureButton.alpha = 0.5
        }
        errorView.isHidden = state != .error
    }
    
    fileprivate func isInputTextFieldEmpty() -> Bool {
        return inputTextField.text == nil || inputTextField.text!.isEmpty
    }
    
    @IBAction fileprivate func secureButtonPressed(_ sender: Any) {
        inputTextField.isSecureTextEntry = !inputTextField.isSecureTextEntry
    }

}
