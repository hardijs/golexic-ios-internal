//
//  ProgressView.swift
//  GoLexic
//
//  Created by Armands L. on 15/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit

class ProgressView: UIView {

    // MARK: Properties
    
    // UI
    fileprivate lazy var baseLineView: UIView = {
        let lineView = makeLineView()
        lineView.backgroundColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        return lineView
    }()
    fileprivate lazy var progressLineView: UIView = {
        let lineView = makeLineView()
        lineView.backgroundColor = #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1)
        return lineView
    }()
    fileprivate lazy var dotStackView: UIStackView = {
        return makeDotStackView()
    }()
    
    // Constraints
    fileprivate var progressLineWidthConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate(set) var index: Int = 0
    fileprivate(set) var count: Int = 0
    fileprivate var currentFrame: CGRect = .zero

    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if frame != currentFrame {
            currentFrame = frame
            updateProgressLineLayout()
        }
    }
    
}

// MAKR: Public Functions
extension ProgressView {
    
    func update(withIndex index: Int, count: Int, isCompleted: Bool) {
        self.index = index
        self.count = count
        configureDotViews(withCount: count)
        updateDotViewStyles(withIndex: index, isCompleted: isCompleted)
        updateProgressLineLayout()
    }
    
}

// MARK: - Private Functions
extension ProgressView {
    
    // MARK: Helpers
    
    fileprivate func configureView() {
        // Base line view
        addSubview(baseLineView)
        baseLineView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        baseLineView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        baseLineView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        baseLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        // Base line view
        addSubview(progressLineView)
        progressLineView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        progressLineView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        progressLineView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        progressLineWidthConstraint = progressLineView.widthAnchor.constraint(equalToConstant: 0)
        progressLineWidthConstraint.isActive = true
        // Dot stack view
        addSubview(dotStackView)
        dotStackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        dotStackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        dotStackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        dotStackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        dotStackView.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
    }
    
    fileprivate func configureDotViews(withCount count: Int) {
        guard dotStackView.subviews.count != count else {
            return
        }
        for dotView in dotStackView.arrangedSubviews {
            dotStackView.removeArrangedSubview(dotView)
        }
        for _ in 0..<count {
            let dotView = makeDotView()
            dotStackView.addArrangedSubview(dotView)
            dotView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
            dotView.widthAnchor.constraint(equalTo: heightAnchor).isActive = true
        }
    }
    
    fileprivate func updateDotViewStyles(withIndex index: Int, isCompleted: Bool) {
        for x in 0..<dotStackView.arrangedSubviews.count {
            let dotView = dotStackView.arrangedSubviews[x] as! ProgressDotView
            if x < index {
                dotView.style = .selected
            } else if x == index {
                if isCompleted {
                    dotView.style = .selected
                } else {
                    dotView.style = .current
                }
            } else {
                dotView.style = .empty
            }
        }
    }
    
    fileprivate func updateProgressLineLayout() {
        let width = makePosition(forIndex: index, count: count)
        progressLineWidthConstraint.constant = width
    }
    
    // MARK: Make
    
    fileprivate func makeLineView() -> UIView {
        let lineView = UIView()
        lineView.translatesAutoresizingMaskIntoConstraints = false
        return lineView
    }
    
    fileprivate func makeDotView() -> ProgressDotView {
        let dotView = ProgressDotView()
        dotView.translatesAutoresizingMaskIntoConstraints = false
        return dotView
    }
    
    fileprivate func makeDotStackView() -> UIStackView {
        let stackView = UIStackView()
        stackView.spacing = 20
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .equalSpacing
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }
    
    fileprivate func makePosition(forIndex index: Int, count: Int) -> CGFloat {
        guard count > 1 else {
            return 0
        }
        guard index < count else {
            return frame.width
        }
        let spaces = CGFloat(count-1)
        let spaceSize = frame.width/spaces
        return spaceSize*CGFloat(index)
    }
    
}
