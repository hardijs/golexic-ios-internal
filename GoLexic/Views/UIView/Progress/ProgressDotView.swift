//
//  ProgressDotView.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 05/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit

class ProgressDotView: UIView {

    // MARK: Enums
    
    enum Style {
        case empty
        case current
        case selected
    }
    
    // MARK: Properties
    
    // UI
    fileprivate lazy var centerView: UIView = {
        return makeCenterView()
    }()
    
    // Variables
    var style: Style = .empty {
        didSet {
            updateAppearance()
        }
    }
    
    // MARK: Initialziers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
        updateAppearance()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: Oerridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
        updateAppearance()
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = frame.size.height/2
        centerView.layoutIfNeeded()
        centerView.layer.cornerRadius = centerView.frame.size.height/2
    }
    
}

// MARK: - Private Functions
extension ProgressDotView {
    
    // MARK: Helpers
    
    fileprivate func configureView() {
        layer.borderWidth = 1
        layer.masksToBounds = true
        // CenterView
        addSubview(centerView)
        centerView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        centerView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        centerView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/2).isActive = true
        centerView.heightAnchor.constraint(equalTo: widthAnchor, multiplier: 1/2).isActive = true
    }
    
    fileprivate func updateAppearance() {
        switch style {
        case .empty:
            backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9647058824, blue: 0.9803921569, alpha: 1)
            layer.borderColor = #colorLiteral(red: 0.8, green: 0.8, blue: 0.8, alpha: 1).cgColor
            centerView.isHidden = true
        case .current:
            backgroundColor = .white
            layer.borderColor = #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1).cgColor
            centerView.isHidden = false
        case .selected:
            backgroundColor = #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1)
            layer.borderColor = #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1).cgColor
            centerView.isHidden = true
        }
    }
    
    // MARK: Make
    
    fileprivate func makeCenterView() -> UIView {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }
    
}
