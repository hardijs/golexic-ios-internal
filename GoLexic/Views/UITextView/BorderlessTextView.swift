//
//  BorderlessTextView.swift
//  Rimi
//
//  Created by Armands L. on 18/07/2018.
//  Copyright © 2018 Interesting Things. All rights reserved.
//

import UIKit

class BorderlessTextView: UITextView {

    override func awakeFromNib() {
        super.awakeFromNib()
        contentInset = .zero
        textContainerInset = .zero
        textContainer.lineFragmentPadding = 0
    }
    
}
