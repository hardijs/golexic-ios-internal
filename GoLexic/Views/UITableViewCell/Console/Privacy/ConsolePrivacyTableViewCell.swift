//
//  ConsolePrivacyTableViewCell.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 21/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ConsolePrivacyTableViewCell: UITableViewCell {

    // MARK: Properties
    
    // Static
    static let identifier = String(describing: ConsolePrivacyTableViewCell.self)
    
    // IB
    @IBOutlet fileprivate var customTitleLabel: UILabel!
    @IBOutlet fileprivate var arrowImageView: UIImageView!
    @IBOutlet fileprivate var separatorView: UIView!
    
    // Flags
    var isSeparatorHidden: Bool = false {
        didSet {
            separatorView.isHidden = isSeparatorHidden
        }
    }
    
    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        if selected {
            backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            arrowImageView.tintColor = #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1)
            customTitleLabel.textColor = #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1)
            customTitleLabel.font = UIFont(name: UIFont.Name.Montserrat.semibold, size: 16)
        } else {
            backgroundColor = .white
            arrowImageView.tintColor = #colorLiteral(red: 0.7176470588, green: 0.7450980392, blue: 0.8392156863, alpha: 1)
            customTitleLabel.textColor = #colorLiteral(red: 0.1882352941, green: 0.1882352941, blue: 0.1882352941, alpha: 1)
            customTitleLabel.font = UIFont(name: UIFont.Name.Montserrat.medium, size: 16)
        }
    }
    
}

// MARK: - Public Functions
extension ConsolePrivacyTableViewCell {
    
    func update(withPrivacy privacy: ConsolePrivacy) {
        customTitleLabel.text = Localization.string(forKeyPath: privacy.titleKey)
    }
    
}

