//
//  ConsoleSettingTableViewCell.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 21/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

protocol ConsoleSettingTableViewCellDelegate: class {
    
    func consoleSettingTableViewCell(_ cell: ConsoleSettingTableViewCell, shouldChange toOn: Bool) -> Bool
    
}

class ConsoleSettingTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    // Static
    static let identifier = String(describing: ConsoleSettingTableViewCell.self)
    
    // IB
    @IBOutlet fileprivate var customTitleLabel: UILabel!
    @IBOutlet fileprivate var iconImageView: UIImageView!
    @IBOutlet fileprivate var switchControl: UISwitch!
    @IBOutlet fileprivate var separatorView: UIView!
    
    // Variables
    weak var delegate: ConsoleSettingTableViewCellDelegate?
    
    // Flags
    var isSeparatorHidden: Bool = false {
        didSet {
            separatorView.isHidden = isSeparatorHidden
        }
    }
    var isOn: Bool = false {
        didSet {
            switchControl.setOn(isOn, animated: false)
        }
    }
    
    // Flags
    fileprivate var blockSwitchValueChange: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        isSeparatorHidden = false
        isOn = false
    }
    
}

// MARK: - Public Functions
extension ConsoleSettingTableViewCell {
    
    func update(withSetting setting: ConsoleSetting) {
        iconImageView.image = UIImage(named: setting.iconName)
        customTitleLabel.text = Localization.string(forKeyPath: setting.titleKey)
    }
    
}

// MARK: - Targets
extension ConsoleSettingTableViewCell {
    
    @IBAction fileprivate func switchControlValueChanged(_ sender: UISwitch) {
        var shouldChange = true
        if let _delegate = self.delegate {
            shouldChange = _delegate.consoleSettingTableViewCell(self, shouldChange: sender.isOn)
        }
        if shouldChange == true {
            self.isOn = sender.isOn
        } else {
            sender.setOn(!sender.isOn, animated: true)
        }
    }
    
}
