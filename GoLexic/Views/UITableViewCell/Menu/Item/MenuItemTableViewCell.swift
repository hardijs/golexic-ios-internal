//
//  MenuItemTableViewCell.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class MenuItemTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    // Static
    static let identifier = String(describing: MenuItemTableViewCell.self)
    
    // IB
    @IBOutlet fileprivate var iconImageView: UIImageView!
    @IBOutlet fileprivate var customTitleLabel: UILabel!
    
    // MARK: Overridden Functions

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        iconImageView.alpha = highlighted ? 0.4 : 1
        customTitleLabel.alpha = highlighted ? 0.4 : 1
    }
    
}

extension MenuItemTableViewCell {
    
    func update(withItem item: MenuItem) {
        iconImageView.image = UIImage(named: item.iconName)
        customTitleLabel.text = Localization.string(forKeyPath: item.titleKey)
    }
    
}
