//
//  MenuFooterTableViewCell.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class MenuFooterTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    // Static
    static let identifier = String(describing: MenuFooterTableViewCell.self)
    
    // IB
    @IBOutlet fileprivate var iconImageView: UIImageView!
    @IBOutlet fileprivate var customTitleLabel: UILabel!
    
    // MARK: Overridden Functions

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        iconImageView.alpha = highlighted ? 0.4 : 1
        customTitleLabel.alpha = highlighted ? 0.4 : 1
    }
    
}

// MARK: - Public Functions
extension MenuFooterTableViewCell {
    
    func update(withFooter footer: MenuFooter, title: String? = nil) {
        iconImageView.image = UIImage(named: footer.iconName)
        customTitleLabel.text = Localization.string(forKeyPath: footer.titleKey)
        if let _title = title {
            customTitleLabel.text = _title
        }
    }
    
}
