//
//  MoreModulesTableViewCell.swift
//  GoLexic
//
//  Created by Denis Kuznetsov on 09.03.2021.
//  Copyright © 2021 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class MoreModulesTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    // Static
    static let identifier = String(describing: MoreModulesTableViewCell.self)
    
    // IB
    @IBOutlet fileprivate var customTitleLabel: UILabel!

    // MARK: Overridden Functions

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
