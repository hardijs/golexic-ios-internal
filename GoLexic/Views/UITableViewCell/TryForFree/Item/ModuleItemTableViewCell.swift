//
//  ModuleItemTableViewCell.swift
//  GoLexic
//
//  Created by Denis Kuznetsov on 09.03.2021.
//  Copyright © 2021 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ModuleItemTableViewCell: UITableViewCell {
    
    // MARK: Properties
    
    // Static
    static let identifier = String(describing: ModuleItemTableViewCell.self)
    
    // IB
    @IBOutlet fileprivate var customTitleLabel: UILabel!
    @IBOutlet fileprivate var customDescriptionLabel: UILabel!

    // MARK: Overridden Functions

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

extension ModuleItemTableViewCell {
    
    func update(withModule item: ModuleItem) {
        customTitleLabel.text = item.title
        customDescriptionLabel.text = item.desc
    }
    
}
