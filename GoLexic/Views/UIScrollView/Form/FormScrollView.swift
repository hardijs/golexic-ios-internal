//
//  FormScrollView.swift
//  Rimi
//
//  Created by Armands L. on 25/07/2018.
//  Copyright © 2018 Interesting Things. All rights reserved.
//

import UIKit
import pop

class FormScrollView: UIScrollView {
    
    //MARK: Properties
    
    //Variables
    var paddingAboveKeyboard: CGFloat = 16
    fileprivate var keyboardHeight: CGFloat = 0
    fileprivate var targetViewFrame: CGRect = .zero
    
    var keyboardWillShow: ((CGFloat) -> Void)?
    var keyboardWillHide: (() -> Void)?
    
    //Flags
    fileprivate var isKeyboardVisible: Bool = false
    
    //MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        subscribeToNotifications()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        subscribeToNotifications()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: Overridden Functions
    
    override func scrollRectToVisible(_ rect: CGRect, animated: Bool) {
        //Leave empty
    }

}

//MARK: - Public Functions
extension FormScrollView {
    
    func viewDidRespond(_ view: UIView) {
        targetViewFrame = .zero
        guard let _superview = view.superview else {
            return
        }
        let _frame = _superview.convert(view.frame, to: self)
        targetViewFrame = _frame
        if isKeyboardVisible == true {
            scroll(toRect: _frame, keyboardHeight: keyboardHeight)
        }
    }
    
}

//MARK: - Targets
extension FormScrollView {
    
    @objc fileprivate func keyboardWillShow(_ sender: NSNotification) {
        guard let _keyboardFrame = sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        if let _keyboardWillShow = keyboardWillShow {
            _keyboardWillShow(_keyboardFrame.height)
        }
        isKeyboardVisible = true
        keyboardHeight = _keyboardFrame.height
        contentInset = UIEdgeInsets(top: 0, left: 0, bottom: _keyboardFrame.height, right: 0)
        guard targetViewFrame.isEmpty == false else {
            return
        }
        scroll(toRect: targetViewFrame, keyboardHeight: keyboardHeight)
    }
    
    @objc fileprivate func keyboardWillHide(_ sender: NSNotification) {
        if let _keyboardWillHide = keyboardWillHide {
            _keyboardWillHide()
        }
        isKeyboardVisible = false
        targetViewFrame = .zero
        contentInset = .zero
        pop_removeAllAnimations()
    }
    
}

//MARK: - Private Functions
extension FormScrollView {
    
    //MARK: Helpers
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    fileprivate func scroll(toRect rect: CGRect, keyboardHeight: CGFloat) {
        let visibleHeight = frame.height - keyboardHeight
        let maxPositionY = contentSize.height - visibleHeight
        var targetPositionY = rect.origin.y - visibleHeight + rect.height + paddingAboveKeyboard
        if targetPositionY < 0 {
            targetPositionY = 0
        } else if targetPositionY > maxPositionY {
            targetPositionY = maxPositionY
        }
        pop_removeAllAnimations()
        let animation = POPSpringAnimation(propertyNamed: kPOPScrollViewContentOffset)!
        animation.springSpeed = 7
        animation.springBounciness = 3
        animation.fromValue = NSValue(cgPoint: contentOffset)
        animation.toValue = NSValue(cgPoint: CGPoint(x: 0, y: targetPositionY))
        pop_add(animation, forKey: "formScrollViewOffsetAnimation")
    }
    
}
