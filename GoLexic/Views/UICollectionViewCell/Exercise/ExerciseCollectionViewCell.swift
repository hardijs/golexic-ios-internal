//
//  ExerciseCollectionViewCell.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 02/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ExerciseCollectionViewCell: UICollectionViewCell {
    
    // MARK: Enums
    
    enum Status {
        case completed
        case active
        case inactive
        case loading
    }
    
    // MARK: Properties
    
    // Static
    static let identifier = String(describing: ExerciseCollectionViewCell.self)
    static let elevation: CGFloat = 10
    static let ratio: CGFloat = 0.9333
    
    // IB
    @IBOutlet fileprivate var elevationView: UIView!
    @IBOutlet fileprivate var containerView: UIView!
    @IBOutlet fileprivate var nameLabel: UILabel!
    @IBOutlet fileprivate var lessonLabel: UILabel!
    @IBOutlet fileprivate var circleView: UIView!
    @IBOutlet fileprivate var circleImageView: UIImageView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Constraints
    @IBOutlet fileprivate var containerViewBottomConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate(set) var status: Status = .completed
    
    // MARK: Overridden Functions

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        updateAppearances(withStatus: status, exerciseType: .unknown)
    }
    
    override var isHighlighted: Bool {
        didSet {
            guard status == .active else {
                return
            }
            let value = isHighlighted ? 0 : ExerciseCollectionViewCell.elevation
            containerViewBottomConstraint.constant = value
        }
    }

}

// MARK: - Public Functions
extension ExerciseCollectionViewCell {
    
    func update(withPhaseExercise phaseExercise: PhaseExercise, status: Status) {
        self.status = status
        updateContent(withPhaseExercise: phaseExercise)
        updateAppearances(withStatus: status, exerciseType: phaseExercise.exerciseType)
    }
    
}

// MARK: - Private Functions
extension ExerciseCollectionViewCell {
    
    // MARK: Update
    
    fileprivate func updateContent(withPhaseExercise phaseExercise: PhaseExercise) {
        let titleKey = phaseExercise.exerciseType.makeLocalizationKey()
        nameLabel.text = Localization.string(forKeyPath: titleKey)!
        if let _lesson = phaseExercise.firstAvailableLesson(),
            let _index = _lesson.lesson?.lessonIndex {
            let lessonText = Localization.string(forKeyPath: "Phase.lesson")!
            lessonLabel.text = "\(lessonText) \(_index)"
        } else {
            lessonLabel.text = nil
        }
    }
    
    fileprivate func updateAppearances(withStatus status: Status, exerciseType: ExerciseType) {
        elevationView.backgroundColor = makeElevationColor(forExerciseType: exerciseType)
        containerView.backgroundColor = makeBackgroundColor(forExerciseType: exerciseType)
        if status == .completed || status == .inactive {
            nameLabel.alpha = 0.5
            lessonLabel.alpha = 0.5
            circleView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
            circleImageView.image = UIImage(named: "ic_exercise_check")
            circleImageView.tintColor = #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1)
            containerViewBottomConstraint.constant = 0
            elevationView.isHidden = true
        } else if status == .active {
            nameLabel.alpha = 1
            lessonLabel.alpha = 1
            circleView.backgroundColor = UIColor.white
            circleImageView.image = UIImage(named: "ic_exercise_forward")
            containerViewBottomConstraint.constant = ExerciseCollectionViewCell.elevation
            elevationView.isHidden = false
        } else if status == .loading {
            nameLabel.alpha = 1
            lessonLabel.alpha = 1
            circleView.backgroundColor = UIColor.white
            circleImageView.image = nil
            containerViewBottomConstraint.constant = ExerciseCollectionViewCell.elevation
            elevationView.isHidden = false
        }
        circleView.isHidden = status == .inactive
        if status == .loading {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
    // MARK: Make
    
    fileprivate func makeBackgroundColor(forExerciseType exerciseType: ExerciseType) -> UIColor {
        switch exerciseType {
        case .alphabetPractice:
            return #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1)
        case .lostLetters:
            return #colorLiteral(red: 0.9725490196, green: 0.4823529412, blue: 0.3803921569, alpha: 1)
        case .alphabetSorting:
            return #colorLiteral(red: 0.9725490196, green: 0.7921568627, blue: 0.5607843137, alpha: 1)
        case .letterBoard:
            return #colorLiteral(red: 0.4745098039, green: 0.8352941176, blue: 0.7568627451, alpha: 1)
        case .vowels:
            return #colorLiteral(red: 1, green: 0.4156862745, blue: 0.4509803922, alpha: 1)
        case .reading:
            return #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1)
        case .building:
            return #colorLiteral(red: 1, green: 0.6901960784, blue: 0.137254902, alpha: 1)
        case .revise:
            return #colorLiteral(red: 0.7019607843, green: 0.5254901961, blue: 0.9921568627, alpha: 1)
        default:
            return #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1)
        }
    }
    
    fileprivate func makeElevationColor(forExerciseType exerciseType: ExerciseType) -> UIColor {
        switch exerciseType {
        case .alphabetPractice:
            return #colorLiteral(red: 0.2980392157, green: 0.6823529412, blue: 0.8980392157, alpha: 1)
        case .lostLetters:
            return #colorLiteral(red: 0.8, green: 0.3843137255, blue: 0.2980392157, alpha: 1)
        case .alphabetSorting:
            return #colorLiteral(red: 0.9294117647, green: 0.6823529412, blue: 0.4196078431, alpha: 1)
        case .letterBoard:
            return #colorLiteral(red: 0.1176470588, green: 0.6078431373, blue: 0.5019607843, alpha: 1)
        case .vowels:
            return #colorLiteral(red: 0.7137254902, green: 0.2784313725, blue: 0.3058823529, alpha: 1)
        case .reading:
            return #colorLiteral(red: 0.2980392157, green: 0.6823529412, blue: 0.8980392157, alpha: 1)
        case .building:
            return #colorLiteral(red: 0.8588235294, green: 0.5803921569, blue: 0.09019607843, alpha: 1)
        case .revise:
            return #colorLiteral(red: 0.4588235294, green: 0.3764705882, blue: 0.6941176471, alpha: 1)
        default:
            return #colorLiteral(red: 0.2980392157, green: 0.6823529412, blue: 0.8980392157, alpha: 1)
        }
    }
    
}
