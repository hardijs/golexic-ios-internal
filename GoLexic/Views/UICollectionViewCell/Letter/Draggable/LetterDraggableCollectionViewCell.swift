//
//  LetterDraggableCollectionViewCell.swift
//  GoLexic
//
//  Created by Armands L. on 29/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class LetterDraggableCollectionViewCell: UICollectionViewCell {

    // MARK: Properties
    
    // Static
    static let identifier = String(describing: LetterDraggableCollectionViewCell.self)
    
    // IB
    weak var dragDelegate: UIDragInteractionDelegate! {
        didSet {
            guard oldValue == nil else {
                return
            }
            attachDragDelegate()
        }
    }
    @IBOutlet var letterView: LetterView!
    
    // Variables
    fileprivate var letter: ExerciseLetter? {
        return letterView.letter
    }
    
    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

// MARK: - Public Functions
extension LetterDraggableCollectionViewCell {
    
    func update(withLetter letter: ExerciseLetter) {
        letterView.letter = letter
        letterView.isHidden = letter.status == .recognized
    }
    
}

// MARK: - Private Functions
extension LetterDraggableCollectionViewCell {
    
    // MARK: Helpers
    
    fileprivate func attachDragDelegate() {
        let dragInteraction = UIDragInteraction(delegate: dragDelegate)
        dragInteraction.isEnabled = true
        dragInteraction.allowsSimultaneousRecognitionDuringLift = false
        addInteraction(dragInteraction)
        // Reduce press duration of UIDragInteraction
        if let longPressRecognizer = gestureRecognizers?.compactMap({ $0 as? UILongPressGestureRecognizer}).first {
            longPressRecognizer.minimumPressDuration = .leastNonzeroMagnitude
        }
    }
    
}
