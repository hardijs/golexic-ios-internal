//
//  LetterDroppableCollectionViewCell.swift
//  GoLexic
//
//  Created by Armands L. on 10/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class LetterDroppableCollectionViewCell: UICollectionViewCell {
    
    // MARK: Enums
    
    enum Style {
        case gray
        case white
        case suggested
    }
    
    // MARK: Properties
    
    // Static
    static let identifier = String(describing: LetterDroppableCollectionViewCell.self)
    
    // IB
    @IBOutlet fileprivate var baseView: InspectableView!
    @IBOutlet fileprivate var suggestedLetterLabel: UILabel!
    @IBOutlet fileprivate var letterView: LetterView!
    
    // Constraints
    @IBOutlet fileprivate var baseViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var baseViewTopConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate(set) var style: Style = .gray
    fileprivate(set) var letter: ExerciseLetter?
    var padding: CGFloat = 8 {
        didSet {
            guard padding != oldValue else {
                return
            }
            updateLayout()
        }
    }
    
    // Flags
    fileprivate var onlyFocus: Bool = false
    fileprivate var markFailed: Bool = false
    fileprivate var isHovering: Bool = false
    
    // MARK: Overridden Functions

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        updateAppearances()
        updateLayout()
    }

}

// MARK: - Public Functions
extension LetterDroppableCollectionViewCell {
    
    func update(withLetter letter: ExerciseLetter, style: Style = .gray,
                onlyFocus: Bool = false, markFailed: Bool = false, isHovering: Bool) {
        self.letter = letter
        self.style = style
        self.onlyFocus = onlyFocus
        self.markFailed = markFailed
        self.isHovering = isHovering
        updateContent(withLetter: letter)
        updateAppearances()
    }
    
}

// MARK: - Private Functions
extension LetterDroppableCollectionViewCell {
    
    // MARK: Helpers
    
    fileprivate func updateContent(withLetter letter: ExerciseLetter) {
        suggestedLetterLabel.text = letter.value
        letterView.letter = letter
    }
    
    fileprivate func updateAppearances() {
        switch style {
        case .gray:
            baseView.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9607843137, blue: 0.9764705882, alpha: 1)
            baseView.layer.borderColor = #colorLiteral(red: 0.6431372549, green: 0.6941176471, blue: 0.768627451, alpha: 1).cgColor
        case .white:
            baseView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            baseView.layer.borderColor = #colorLiteral(red: 0.6470588235, green: 0.6980392157, blue: 0.7725490196, alpha: 1).cgColor
        case .suggested:
            baseView.backgroundColor = #colorLiteral(red: 0.9843137255, green: 0.9843137255, blue: 0.9921568627, alpha: 1)
            if isHovering {
                baseView.layer.borderColor = #colorLiteral(red: 0.6470588235, green: 0.6980392157, blue: 0.7725490196, alpha: 1).cgColor
            } else {
                baseView.layer.borderColor = #colorLiteral(red: 0.8862745098, green: 0.8980392157, blue: 0.937254902, alpha: 1).cgColor
            }
        }
        if isHovering {
            baseView.borderWidth = 2
        } else {
            baseView.borderWidth = 1
        }
        guard let _letter = letter else {
            return
        }
        if _letter.isFailed, markFailed {
            letterView.color = #colorLiteral(red: 0.7137254902, green: 0.7647058824, blue: 0.8352941176, alpha: 1)
            letterView.borderColor = LetterView.defaultBorderColor
        } else if _letter.status == .recognized, onlyFocus == false {
            letterView.color = #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1)
            letterView.borderColor = LetterView.defaultBorderColor
        } else if _letter.status == .recognized, onlyFocus == true {
            letterView.color = .white
            letterView.borderColor = #colorLiteral(red: 0.1098039216, green: 0.6901960784, blue: 0.9647058824, alpha: 1)
        } else {
            letterView.color = .white
            letterView.borderColor = LetterView.defaultBorderColor
        }
        letterView.isHidden = _letter.status == .none
        let isSuggested = _letter.isSuggested || style == .suggested
        suggestedLetterLabel.isHidden = !isSuggested
    }
    
    fileprivate func updateLayout() {
        baseViewLeadingConstraint.constant = padding
        baseViewTopConstraint.constant = padding
    }
    
}
