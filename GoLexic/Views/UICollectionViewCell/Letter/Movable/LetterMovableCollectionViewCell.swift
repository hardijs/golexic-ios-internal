//
//  LetterMovableCollectionViewCell.swift
//  GoLexic
//
//  Created by Armands L. on 15/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import pop

protocol LetterMovableCollectionViewCellDelegate: class {
    
    func letterMovableCollectionViewCellShouldInteract(_ cell: LetterMovableCollectionViewCell) -> Bool
    func letterMovableCollectionViewCell(_ cell: LetterMovableCollectionViewCell,
                                         didChangeStateTo isMoved: Bool)
    
}

class LetterMovableCollectionViewCell: UICollectionViewCell {
    
    // MARK: Properties
    
    // Static
    static let identifier = String(describing: LetterMovableCollectionViewCell.self)
    static let spacing: CGFloat = 12
    
    // IB
    @IBOutlet fileprivate var letterView: LetterView!
    
    // Constraints
    @IBOutlet var letterViewBottomConstraint: NSLayoutConstraint!
    
    // Variables
    weak var delegate: LetterMovableCollectionViewCellDelegate?
    fileprivate(set) var letter: ExerciseLetter?
    
    // Flags
    fileprivate(set) var isMoved: Bool = false
    
    // MARK: Overridden Functions

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        configureView()
        updateAppearances()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        updateState(toIsMoved: false, animated: false)
    }
    
}

// MARK: - Public Functions
extension LetterMovableCollectionViewCell {
    
    func update(withLetter letter: ExerciseLetter, isMoved: Bool) {
        self.letter = letter
        updateContent(withLetter: letter)
        updateState(toIsMoved: isMoved, animated: false)
        updateAppearances()
    }
    
    @discardableResult
    func updateState(toIsMoved isMoved: Bool, animated: Bool) -> Bool {
        guard self.isMoved != isMoved else {
            return false
        }
        self.isMoved = isMoved
        letterViewBottomConstraint.pop_removeAllAnimations()
        let toValue = makeLetterViewPosition(isMoved: isMoved)
        if animated {
            let animation = makeLetterViewMoveAnimation(toValue: toValue)
            letterViewBottomConstraint.pop_add(animation, forKey: "letterViewMoveAnimation")
        } else {
            letterViewBottomConstraint.constant = toValue
        }
        return true
    }
    
}

// MARK: - Targets
extension LetterMovableCollectionViewCell {
    
    @objc fileprivate func viewTapped(_ sender: UITapGestureRecognizer) {
        guard let _delegate = delegate,
            _delegate.letterMovableCollectionViewCellShouldInteract(self) else {
            return
        }
        updateState(toIsMoved: !isMoved, animated: true)
        _delegate.letterMovableCollectionViewCell(self, didChangeStateTo: isMoved)
    }
    
    @objc fileprivate func viewSwiped(_ sender: UISwipeGestureRecognizer) {
        guard let _delegate = delegate,
            _delegate.letterMovableCollectionViewCellShouldInteract(self) else {
            return
        }
        var didMove: Bool = false
        if sender.direction == .up {
            didMove = updateState(toIsMoved: true, animated: true)
        } else if sender.direction == .down {
            didMove = updateState(toIsMoved: false, animated: true)
        }
        if didMove {
            _delegate.letterMovableCollectionViewCell(self, didChangeStateTo: isMoved)
        }
    }
    
}

// MARK: - UIGestureRecognizerDelegate
extension LetterMovableCollectionViewCell: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

// MARK: - Private Functions
extension LetterMovableCollectionViewCell {
    
    // MARK: Helpers
    
    fileprivate func configureView() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        tapRecognizer.delegate = self
        addGestureRecognizer(tapRecognizer)
        let swipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(viewSwiped(_:)))
        swipeUpRecognizer.direction = .up
        swipeUpRecognizer.delegate = self
        addGestureRecognizer(swipeUpRecognizer)
        let swipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(viewSwiped(_:)))
        swipeDownRecognizer.direction = .down
        swipeDownRecognizer.delegate = self
        addGestureRecognizer(swipeDownRecognizer)
    }
    
    fileprivate func updateContent(withLetter letter: ExerciseLetter) {
        letterView.letter = letter
    }
    
    // MARK: Update
    
    fileprivate func updateAppearances() {
        guard let _letter = letter else {
            return
        }
        if _letter.status == .recognized {
            letterView.color = #colorLiteral(red: 0.568627451, green: 0.862745098, blue: 0.9568627451, alpha: 1)
        } else {
            letterView.color = .white
        }
    }
    
    // MARK: Make
    
    fileprivate func makeLetterViewPosition(isMoved: Bool) -> CGFloat {
        if isMoved {
            return frame.width + LetterMovableCollectionViewCell.spacing
        }
        return 0
    }
    
    fileprivate func makeLetterViewMoveAnimation(toValue: CGFloat) -> POPBasicAnimation {
        let animation = POPBasicAnimation(propertyNamed: kPOPLayoutConstraintConstant)!
        animation.duration = 0.25
        animation.fromValue = letterViewBottomConstraint.constant
        animation.toValue = toValue
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        return animation
    }
    
}
