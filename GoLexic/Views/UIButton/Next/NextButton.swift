//
//  NextButton.swift
//  GoLexic
//
//  Created by Armands L. on 15/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class NextButton: UIButton, StoryboardAddable {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var customTitleLabel: UILabel!
    @IBOutlet fileprivate var arrowImageView: UIImageView!
    
    
    // StoryboardAddable
    var contentView: UIView!
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibWithConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadXibWithConstraints()
    }
    
    // MARK: Overridden Functions
    
    override var isHighlighted: Bool {
        didSet {
            customTitleLabel.alpha = isHighlighted ? 0.4 : 1
            arrowImageView.alpha = isHighlighted ? 0.4 : 1
        }
    }

}
