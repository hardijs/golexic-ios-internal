//
//  StandardButton.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class StandardButton: LocalizedButton {
    
    // MARK: Properties
    
    @IBInspectable open var yOffset: Double = 0 {
        didSet {
            layer.shadowOffset = CGSize(width: 0, height: yOffset)
        }
    }

    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.shadowOffset = CGSize(width: 0, height: yOffset)
    }

}
