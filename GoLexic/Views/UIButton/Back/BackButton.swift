//
//  BackButton.swift
//  GoLexic
//
//  Created by Armands L. on 12/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class BackButton: UIButton, StoryboardAddable {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var backImageView: UIImageView!
    @IBOutlet fileprivate var customTitleLabel: UILabel!
    
    // StoryboardAddable
    var contentView: UIView!
    
    // Variables
    var title: String? {
        didSet {
            customTitleLabel.text = title
        }
    }
    
    // MARK: Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibWithConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadXibWithConstraints()
    }
    
    // MARK: Overridden Functions
    
    override var isHighlighted: Bool {
        didSet {
            guard isEnabled else {
                return
            }
            backImageView.alpha = isHighlighted ? 0.4 : 1
            customTitleLabel.alpha = isHighlighted ? 0.4 : 1
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            backImageView.alpha = isEnabled ? 1 : 0.4
            customTitleLabel.alpha = isEnabled ? 1 : 0.4
        }
    }
    

}
