//
//  SegmentButton.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 27/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class SegmentButton: UIButton, StoryboardAddable {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var customTitleLabel: UILabel!
    @IBOutlet fileprivate var indicatorView: UIView!
    
    // StoryboardAddable
    var contentView: UIView!
    
    // Variables
    @IBInspectable var key: String? {
        didSet {
            if let _key = key {
                customTitleLabel.text = Localization.string(forKeyPath: _key)
            } else {
                customTitleLabel.text = nil
            }
        }
    }
    
    // MARK: Initialziers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadXibWithConstraints()
        subscribeToNotifications()
        cleanup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadXibWithConstraints()
        subscribeToNotifications()
    }
    
    // MARK: Overridden Functions
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
        cleanup()
    }
    
    override var isHighlighted: Bool {
        didSet {
            customTitleLabel.alpha = isHighlighted ? 0.4 : 1
        }
    }
    
    override var isSelected: Bool {
        didSet {
            indicatorView.isHidden = !isSelected
        }
    }

}

// MARK: - Private functions
extension SegmentButton {
    
    // MARK: Helpers
    
    fileprivate func configureView() {
        indicatorView.isHidden = !isSelected
    }
    
    fileprivate func cleanup() {
        setTitle(nil, for: .normal)
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: Notification.Name.Localization.DidChange,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self else {
                return
            }
            if let _key = _self.key {
                _self.customTitleLabel.text = Localization.string(forKeyPath: _key)
            }
        }
    }
    
}
