//
//  InspectableButton.swift
//  GoLexic
//
//  Created by Armands L. on 26/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class InspectableButton: UIButton, LayerPropertyInspectable {

    // MARK: Properties
    
    // Corners
    @IBInspectable open var rounded: Bool = false {
        didSet {
            if rounded == true {
                layer.masksToBounds = true
            }
            if didLayout == true {
                layoutIfNeeded()
            }
        }
    }
    @IBInspectable open var cornerRadius: Double = 0 {
        didSet {
            if cornerRadius > 0 {
                layer.masksToBounds = true
            }
            layer.cornerRadius = CGFloat(cornerRadius)
        }
    }
    
    // Border
    @IBInspectable open var borderWidth: Double = 0 {
        didSet {
            layer.borderWidth = CGFloat(borderWidth)
        }
    }
    @IBInspectable open var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    // Shadow
    @IBInspectable open var shadowOpacity: Double = 0 {
        didSet {
            if shadowOpacity > 0 {
                layer.masksToBounds = false
            }
            layer.shadowOpacity = Float(shadowOpacity)
        }
    }
    @IBInspectable open var shadowRadius: Double = 0 {
        didSet {
            layer.shadowRadius = CGFloat(shadowRadius)
        }
    }
    @IBInspectable open var shadowColor: UIColor? {
        didSet {
            layer.shadowColor = shadowColor?.cgColor
        }
    }
    
    // Flags
    fileprivate var didLayout: Bool = false
    
    // MARK: Overridden Functions
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        didLayout = true
        if rounded == true {
            layer.cornerRadius = bounds.size.height/2
        }
        if layer.shadowOpacity > 0 {
            let bezierPath = UIBezierPath(roundedRect: bounds, cornerRadius: layer.cornerRadius)
            layer.shadowPath = bezierPath.cgPath
        }
    }

}
