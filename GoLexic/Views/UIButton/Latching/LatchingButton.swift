//
//  LatchingButton.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 27/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit

class LatchingButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        let _image = image(for: .selected)
        setImage(_image, for: [.selected, .highlighted])
    }

}
