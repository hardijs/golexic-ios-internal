//
//  GoLexicTarget.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Moya

class GoLexicTarget: NSObject, NSCoding {
    
    // MARK: Enums
    
    enum Endpoint: String {
        
        // MARK: Authorization
        
        // POST register
        case register
        
        // POST authorization/login
        case login
        
        // PUT authorization/refresh
        case refreshToken
        
        // DELETE authorization/logout
        case logout
        
        // MARK: User
        
        // GET, PATCH user
        case user
        case userEdit
        
        // PUT user/password
        case userPassword
        
        // PUT user/deleteData
        case userDeleteData
        
        // MARK: Exercises
        
        // GET letters
        case letters
        
        // GET scorings
        case scoringLetters
        case scoringWords
        
        // GET instructions
        case instructions
        
        // GET feedback
        case feedback
        
        // GET, POST exercise/alphabetPractices
        case alphabetPractices
        case alphabetPracticeResults
        
        // GET, POST exercise/lostLetters
        case lostLetters
        case lostLettersResults
        
        // GET, POST exercise/alphabetSortings
        case alphabetSortings
        case alphabetSortingResults
        
        // GET, POST exercise/letterBoards
        case letterBoards
        case letterBoardResults
        
        // GET, POST exercise/vowels
        case vowels
        case vowelsResults
        
        // GET, POST exercise/readings
        case readings
        case readingResults
        
        // GET, POST exercise/buildings
        case buildings
        case buildingResults
        
        // GET, POST exercise/revise
        case revises
        case reviseResults
        
        // GET phase
        case phase
        
        // MARK: Other
        
        // GET statistics/letters
        case statisticsLetters
        
        // GET statistics/words
        case statisticsWords
        
        // GET modules for TryForFree
        case tryForFree
        
        // POST, DELETE subscriptions
        case subscriptionsAdd
        case subscriptionsDelete
    }
    
    // MARK: Properties
    
    let endpoint: Endpoint
    let resourceIDs: [String]?
    let params: [String: Any]?
    var stub: String?
    var requiresAuthorization: Bool {
        let unauthorizedEndpoints: [Endpoint] = [.register, .login]
        return unauthorizedEndpoints.contains(endpoint) == false
    }
    
    // MARK: Initializers
    
    init(withEndpoint endpoint: Endpoint, resourceIDs: [String]? = nil, params: [String: Any]? = nil, stub: String? = nil) {
        self.endpoint = endpoint
        self.resourceIDs = resourceIDs
        self.params = params
        self.stub = stub
    }
    
    required init?(coder aDecoder: NSCoder) {
        endpoint = {
            let rawValue = aDecoder.decodeObject(forKey: EncodingKeys.endpoint.rawValue) as! String
            return Endpoint(rawValue: rawValue)!
        }()
        resourceIDs = aDecoder.decodeObject(forKey: EncodingKeys.resourceIDs.rawValue) as? [String]
        params = aDecoder.decodeObject(forKey: EncodingKeys.params.rawValue) as? [String: Any]
    }
    
    // MARK: Equatable
    static func ==(lhs: GoLexicTarget, rhs: GoLexicTarget) -> Bool {
        return lhs.endpoint == rhs.endpoint
    }

}

// MARK: - NSCoding
extension GoLexicTarget {
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(endpoint.rawValue, forKey: EncodingKeys.endpoint.rawValue)
        aCoder.encode(resourceIDs, forKey: EncodingKeys.resourceIDs.rawValue)
        aCoder.encode(params, forKey: EncodingKeys.params.rawValue)
    }
    
}

// MARK: - TargetType
extension GoLexicTarget: TargetType {
    
    public var baseURL: URL {
        let host = GoLexicAPI.host
        let version = GoLexicAPI.version
        return URL(string: "\(host)/api/v\(version)/")!
    }
    
    public var path: String {
        switch endpoint {
        case .register:
            return "register"
        case .login:
            return "authorization/login"
        case .refreshToken:
            return "authorization/refresh"
        case .logout:
            return "authorization/logout"
        case .user, .userEdit:
            return "user"
        case .userPassword:
            return "user/password"
        case .userDeleteData:
            return "user/deleteData"
        case .letters:
            return "letters"
        case .scoringLetters:
            return "scoring/letters"
        case .scoringWords:
            return "scoring/words"
        case .instructions:
            return "instructions"
        case .feedback:
            return "feedback"
        case .alphabetPractices:
            return "exercises/alphabetPractices"
        case .alphabetPracticeResults:
            return "exercises/alphabetPractices/results"
        case .lostLetters:
            return "exercises/lostLetters"
        case .lostLettersResults:
            return "exercises/lostLetters/results"
        case .alphabetSortings:
            return "exercises/alphabetSortings"
        case .alphabetSortingResults:
            return "exercises/alphabetSortings/results"
        case .letterBoards:
            return "exercises/letterBoards"
        case .letterBoardResults:
            return "exercises/letterBoards/results"
        case .vowels:
            return "exercises/vowels"
        case .vowelsResults:
            return "exercises/vowels/results"
        case .readings:
            return "exercises/readings"
        case .readingResults:
            return "exercises/readings/results"
        case .buildings:
            return "exercises/buildings"
        case .buildingResults:
            return "exercises/buildings/results"
        case .revises:
            return "exercises/revises"
        case .reviseResults:
            return "exercises/revises/results"
        case .phase:
            return "phase"
        case .statisticsLetters:
            return "statistics/letters"
        case .tryForFree:
            return "modules"
        case .statisticsWords:
            return "statistics/words"
        case .subscriptionsAdd, .subscriptionsDelete:
            return "subscriptions"
        }
    }
    
    public var headers: [String : String]? {
        var _headers: [String: String] = [:]
        _headers.updateValue(Globals.userAgent, forKey: "User-Agent")
        return _headers
    }
    
    public var method: Moya.Method {
        switch endpoint {
        case .register, .login, .alphabetPracticeResults, .lostLettersResults, .alphabetSortingResults,
             .letterBoardResults, .vowelsResults, .readingResults, .buildingResults, .reviseResults,
             .statisticsLetters, .statisticsWords, .subscriptionsAdd:
            return .post
        case .refreshToken, .userPassword, .userDeleteData:
            return .put
        case .userEdit:
            return .patch
        case .logout, .subscriptionsDelete:
            return .delete
        default:
            return .get
        }
    }
    
    public var task: Task {
        if let _params = params {
            return .requestParameters(parameters: _params, encoding: JSONEncoding.default)
        } else {
            return .requestPlain
        }
    }
    
    var validationType: ValidationType {
        return .successCodes
    }
    
    public var sampleData: Data {
        return makeStubData(forEndpoint: endpoint)
    }
}

// MARK: - AccessTokenAuthorizable
extension GoLexicTarget: AccessTokenAuthorizable {
    
    public var authorizationType: AuthorizationType? {
        guard User.current?.token != nil else {
            return nil
        }
        return .bearer
    }
    
}

// MARK: - Private Functions
extension GoLexicTarget {
    
    // MARK: Enums
    
    fileprivate enum EncodingKeys: String {
        case queue = "TargetTypeQueue"
        case endpoint = "TargetTypeEndpoint"
        case resourceIDs = "TargetTypeResourceIDs"
        case params = "TargetTypeParams"
    }
    
    // MARK: Make

    fileprivate func makeStubData(forEndpoint endpoint: Endpoint) -> Data {
        var fileUrl: URL?
        switch endpoint {
        case .letters:
            fileUrl = Bundle.main.url(forResource: "letters", withExtension: "json")
        case .scoringLetters:
            fileUrl = Bundle.main.url(forResource: "scoring_letters", withExtension: "json")
        case .scoringWords:
            fileUrl = Bundle.main.url(forResource: "scoring_words", withExtension: "json")
        case .instructions:
            fileUrl = Bundle.main.url(forResource: "instructions", withExtension: "json")
        case .feedback:
            fileUrl = Bundle.main.url(forResource: "feedback", withExtension: "json")
        case .alphabetPractices:
            fileUrl = Bundle.main.url(forResource: "alphabetPractices", withExtension: "json")
        case .lostLetters:
            fileUrl = Bundle.main.url(forResource: "lostLetters", withExtension: "json")
        case .alphabetSortings:
            fileUrl = Bundle.main.url(forResource: "alphabetSortings", withExtension: "json")
        case .letterBoards:
            fileUrl = Bundle.main.url(forResource: "letterBoards", withExtension: "json")
        case .vowels:
            fileUrl = Bundle.main.url(forResource: "vowels", withExtension: "json")
        case .readings:
            fileUrl = Bundle.main.url(forResource: "readings", withExtension: "json")
        case .buildings:
            fileUrl = Bundle.main.url(forResource: "buildings", withExtension: "json")
        case .revises:
            fileUrl = Bundle.main.url(forResource: "revises", withExtension: "json")
        case .phase:
            fileUrl = Bundle.main.url(forResource: "phase", withExtension: "json")
        default:
            break
        }
        guard let _url = fileUrl,
            let data = try? Data(contentsOf: _url) else {
            return Data()
        }
        return data
    }
    
}
