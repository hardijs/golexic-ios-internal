//
//  GoLexicAPI.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Moya
import Alamofire

class GoLexicAPI {
    
    // MARK: Properties
    
    // Static
    static var host: String {
        switch Globals.enviroment {
        case .production:
            return "https://app.golexic.com"
        case .staging:
            return "https://staging.golexic.com"
        case .test:
            return "https://golexic.cubemobile.lv"
        }
    }
    static let version: Int = 1
    static let accessPlugin = AccessTokenPlugin { (type) -> String in
        return User.current?.token ?? ""
    }
    static let provider = RefreshableMoyaProvider<GoLexicTarget>(plugins: [GoLexicAPI.accessPlugin])
    static let stubProvider = RefreshableMoyaProvider<GoLexicTarget>(stubClosure: MoyaProvider.immediatelyStub,
                                                                     plugins: [GoLexicAPI.accessPlugin])
    
}
