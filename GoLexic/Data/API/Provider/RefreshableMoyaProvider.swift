//
//  RefreshableMoyaProvider.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import Moya

typealias RefreshCompletion = (_ success: Bool) -> Void

class RefreshableMoyaProvider<Target: TargetType>: MoyaProvider<Target> {
    
    // MARK: Properties

    // Variables
    var refreshHandler: ((@escaping RefreshCompletion) -> Void)!
    var didCompleteRefresh: ((Bool) -> Void)!
    
    // MARK: Overridden Functions
    
    @discardableResult
    override func request(_ target: Target, callbackQueue: DispatchQueue? = .none,
                          progress: ProgressBlock? = .none, completion: @escaping Completion) -> Cancellable {
        guard let _refreshHandler = refreshHandler, let _didCompleteRefresh = didCompleteRefresh else {
            return super.request(target, callbackQueue: callbackQueue, progress: progress, completion: completion)
        }
        let cancellable = super.request(target, callbackQueue: callbackQueue) { result in
            switch result {
            case .success:
                completion(result)
            case let .failure(error):
                guard let statusCode = error.response?.statusCode,
                    statusCode == 401 else {
                        completion(result)
                        return
                }
                _refreshHandler({ success in
                    if success {
                        super.request(target, callbackQueue: callbackQueue, completion: { _result in
                            completion(_result)
                        })
                        _didCompleteRefresh(true)
                    } else {
                        completion(result)
                        _didCompleteRefresh(false)
                    }
                    
                })
            }
        }
        return cancellable
    }
    
}
