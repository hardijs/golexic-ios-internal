//
//  NotificationProvider.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 23/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import Firebase
import SwiftyUserDefaults
import Cubemobile

class NotificationProvider {
    
    // MARK: Propertoes
    
    // Static
    static let shared = NotificationProvider()
    
    // Variables
    fileprivate var fcmToken: String?
    fileprivate(set) var status: UNAuthorizationStatus = .notDetermined {
        didSet {
            if status != oldValue {
                NotificationCenter.default.post(name: NSNotification.Name.NotificationProvider.didChangeAuthorizationStatus, object: status)
            }
        }
    }
    var isEnabled: Bool {
        return Defaults[\.appIsNotificationsEnabled]
    }
    
    // MARK: Initializers
    
    init() {
        updateNotificationAuthorizationStatus()
        subscribeToNotifications()
    }

}

// MARK: - Remote Notifications
extension NotificationProvider {
    
    func updateNotificationAuthorizationStatus() {
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings(completionHandler: { settings in
            DispatchQueue.main.async {
                self.status = settings.authorizationStatus
            }
        })
    }
    
    func registerForNotifications() {
        let options: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: options, completionHandler: {_, _ in })
        UIApplication.shared.registerForRemoteNotifications()
        changeState(toIsOn: true)
    }
    
    func changeState(toIsOn isOn: Bool) {
        Defaults[\.appIsNotificationsEnabled] = isOn
        updateNotificationTopics()
        syncrhonize()
    }
    
    func updateNotificationTopics() {
        guard fcmToken != nil else {
            return
        }
        let isNotificationsEnabled = Defaults[\.appIsNotificationsEnabled]
        if isNotificationsEnabled {
            Messaging.messaging().subscribe(toTopic: Topic.general.rawValue)
        } else {
            Messaging.messaging().unsubscribe(fromTopic: Topic.general.rawValue)
        }
        if isNotificationsEnabled, User.current?.profile?.isAcceptingReports == true {
            Messaging.messaging().subscribe(toTopic: Topic.reports.rawValue)
        } else {
            Messaging.messaging().unsubscribe(fromTopic: Topic.reports.rawValue)
        }
        if isNotificationsEnabled, User.current?.profile?.isAcceptingNews == true {
            Messaging.messaging().subscribe(toTopic: Topic.news.rawValue)
        } else {
            Messaging.messaging().unsubscribe(fromTopic: Topic.news.rawValue)
        }
    }
    
    func update(withToken token: String?) {
        fcmToken = token
        updateNotificationTopics()
        syncrhonize()
    }
    
    func syncrhonize() {
        guard User.current != nil else {
            return
        }
        removePendingTarget(forEndpoint: .subscriptionsAdd)
        removePendingTarget(forEndpoint: .subscriptionsDelete)
        if isEnabled, let _fcmToken = fcmToken {
            let params: [String: Any] = ["fcmToken": _fcmToken]
            let target = GoLexicTarget(withEndpoint: .subscriptionsAdd, params: params)
            TargetEngine.execute(target, disposition: .override, queue: .async)
        } else {
            let target = GoLexicTarget(withEndpoint: .subscriptionsDelete)
            TargetEngine.execute(target, disposition: .override, queue: .async)
        }
    }
    
}

// MARK: - Local Notifications
extension NotificationProvider {
    
    @discardableResult
    func scheduleLocalNotification(withTitle title: String, message: String, dateComponents: DateComponents, repeats: Bool) -> UNNotificationRequest {
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.sound = .default
        // Create notification trigger
        let identifier = UUID().uuidString
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: repeats)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        // Schedule notification
        UNUserNotificationCenter.current().add(request)
        return request
    }
    
    func removeLocalNotification(withIdentifier identifier: String) {
        let center = UNUserNotificationCenter.current()
        center.removePendingNotificationRequests(withIdentifiers: [identifier])
    }
    
    func removeAllPendingLocalNotifications() {
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
    }
    
}

// MARK: - NSNotification.Name
extension NSNotification.Name {

    struct NotificationProvider {
        static let didChangeAuthorizationStatus = NSNotification.Name("notification.name.notificationProvider.didChangeAuthorizationStatus")
    }
}

// MARK: - Private Functions
extension NotificationProvider {
    
    // MARK: Enums
    
    fileprivate enum Topic: String {
        case general
        case reports
        case news
    }
    
    // MARK: Helpers
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                               object: nil, queue: nil) { [unowned self] _ in
            self.updateNotificationAuthorizationStatus()
        }
    }
    
    fileprivate func removePendingTarget(forEndpoint endpoint: GoLexicTarget.Endpoint) {
        guard let _pendingItems = TargetEngine.pendingItems() else {
            return
        }
        var identifiers: [String] = []
        for item in _pendingItems {
            let target = item.target as! GoLexicTarget
            if target.endpoint == endpoint {
                identifiers.append(item.identifier)
            }
        }
        for identifier in identifiers {
            TargetEngine.removePendingItem(withIdentifier: identifier)
        }
    }
    
}
