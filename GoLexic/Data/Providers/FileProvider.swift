//
//  FileProvider.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 09/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import Alamofire
import Kingfisher

class FileProvider {
    
    // MARK: Propertoes
    
    // Static
    static let shared = FileProvider()
    
    static let folderName = "PhaseAssets"

}

// MARK: - Public Functions
extension FileProvider {
    
    func fileUrl(forUrlString urlString: String) -> URL? {
        let fileUrl = makeFileUrl(forUrlString: urlString)
        guard FileManager.default.fileExists(atPath: fileUrl.path) else {
            return nil
        }
        return fileUrl
    }
    
    func updateFilesForPhase(withIdentifier identifier: String, completion: @escaping ((Bool) -> Void)) {
        DispatchQueue.global(qos: .utility).async {
            let realm = try! Realm()
            guard let phase = realm.object(ofType: Phase.self, forPrimaryKey: identifier) else {
                DispatchQueue.main.async {
                    completion(false)
                }
                return
            }
            var counter: Int = 0
            var hasAnyError: Bool = false
            func complete(success: Bool) {
                counter += 1
                if success == false {
                    hasAnyError = true
                }
                guard counter >= 2 else {
                    return
                }
                DispatchQueue.main.async {
                    completion(hasAnyError == false)
                }
            }
            self.updateImages(forPhase: phase) { success in
                complete(success: success)
            }
            self.updateAudios(forPhase: phase) { success in
                complete(success: success)
            }
        }
    }
    
}

// MARK: - Private Functions
extension FileProvider {
    
    // MARK: Helpers
    
    fileprivate func fileExists(withUrlString urlString: String) -> Bool {
        let fileUrl = makeFileUrl(forUrlString: urlString)
        return FileManager.default.fileExists(atPath: fileUrl.path)
    }
    
    // MARK: Update
    
    fileprivate func updateImages(forPhase phase: Phase, completion: @escaping ((Bool) -> Void)) {
        let imageUrls = makeImageUrls(forPhase: phase)
        if imageUrls.isEmpty {
            completion(true)
        } else {
            let prefetcher = ImagePrefetcher(urls: imageUrls, completionHandler: { (_, failed, _) in
                completion(failed.isEmpty)
            })
            prefetcher.start()
        }
    }
    
    fileprivate func updateAudios(forPhase phase: Phase, completion: @escaping ((Bool) -> Void)) {
        var counter: Int = 0
        var hasAnyError: Bool = false
        func complete(success: Bool) {
            counter += 1
            if success == false {
                hasAnyError = true
            }
            guard counter >= urlStrings.count else {
                return
            }
            completion(hasAnyError == false)
        }
        let urlStrings = makeAudioUrlStrings(forPhase: phase)
        if urlStrings.isEmpty {
            complete(success: true)
        } else {
            for urlString in urlStrings {
                guard fileExists(withUrlString: urlString) == false else {
                    complete(success: true)
                    continue
                }
                downloadFile(withUrlString: urlString) { success in
                    complete(success: success)
                }
            }
        }
    }
    
    // MARK: Networking
    
    fileprivate func downloadFile(withUrlString urlString: String, completion: @escaping ((Bool) -> Void)) {
        let fileUrl = makeFileUrl(forUrlString: urlString)
        let destination: DownloadRequest.Destination = { _, _ in
            return (fileUrl, [.removePreviousFile, .createIntermediateDirectories])
        }
        AF.download(urlString, to: destination).response { response in
            completion(response.error == nil)
        }
    }
    
    // MARK: Make
    
    fileprivate func makeAssetFolderPath() -> URL {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsDirectory.appendingPathComponent(FileProvider.folderName)
    }
    
    fileprivate func makeFileUrl(forUrlString urlString: String) -> URL {
        let path = makeAssetFolderPath()
        return path.appendingPathComponent(urlString)
    }
    
    fileprivate func makeImageUrls(forPhase phase: Phase) -> [URL] {
        var _urls: Set<URL> = []
        for exercise in phase.exercises {
            guard let _exercise = exercise.exercise,
                let _instruction = _exercise.instruction else {
                continue
            }
            for image in _instruction.images {
                guard let _url = URL(string: image) else {
                    continue
                }
                _urls.insert(_url)
            }
        }
        return Array(_urls)
    }
    
    fileprivate func makeAudioUrlStrings(forPhase phase: Phase) -> [String] {
        var _urlStrings: Set<String> = []
        for exercise in phase.exercises {
            guard let _exercise = exercise.exercise else {
                continue
            }
            for lesson in exercise.exerciseLessons {
                guard let _lesson = lesson.lesson as? ExerciseBuildingLesson else {
                    continue
                }
                let urlStrings = makeAudioUrlStrings(forBuildingLesson: _lesson)
                urlStrings.forEach( { _urlStrings.insert($0) })
            }
            if let _instruction = _exercise.instruction {
                guard let _audioUrl = _instruction.audioUrl else {
                    continue
                }
                _urlStrings.insert(_audioUrl)
            }
            for feedback in _exercise.feedbacks {
                guard let _audioUrl = feedback.audioUrl else {
                    continue
                }
                _urlStrings.insert(_audioUrl)
            }
        }
        return Array(_urlStrings)
    }
    
    fileprivate func makeAudioUrlStrings(forBuildingLesson buildingLesson: ExerciseBuildingLesson) -> [String] {
        var _urls: Set<String> = []
        for page in buildingLesson.pages {
            guard let audioUrl = page.instructionAudioUrl else {
                continue
            }
            _urls.insert(audioUrl)
        }
        return Array(_urls)
    }
    
}
