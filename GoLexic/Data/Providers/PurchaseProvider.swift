//
//  PurchaseProvider.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 10/08/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import StoreKit
import TPInAppReceipt
import SwiftyStoreKit
import SwiftyUserDefaults

class PurchaseProvider: NSObject {
    
    // MARK Properties
    
    // Static
    static let shared = PurchaseProvider()
    
    // Varaibles
    fileprivate var monthlyIdentifier: String {
        let identifier = "com.golexic.subscriptions.monthly"
        switch Globals.enviroment {
        case .production:
            return identifier
        case .staging:
            return identifier + ".staging"
        case .test:
            return identifier + ".test"
        }
    }
    
    fileprivate var halfYearIdentifier: String {
        let identifier = " com.golexic.subscriptions.6months"
        switch Globals.enviroment {
        case .production:
            return identifier
        case .staging:
            return identifier + ".staging"
        case .test:
            return identifier + ".test"
        }
    }
    
    // Flags
    fileprivate(set) lazy var hasActiveSubscription: Bool = {
        return hasActiveMonthlySubscription()
    }()
    
    // MARK: Initializers
    
    override init() {
        super.init()
        if isSubscriptionUpdateRequired() {
            refreshReceipt()
        }
    }

}

// MARK: - Public Functions
extension PurchaseProvider {
    
    func fetchProducts(withCompletion completion: @escaping (([SKProduct]) -> Void)) {
        let productIDs = Set([monthlyIdentifier, halfYearIdentifier])
        SwiftyStoreKit.retrieveProductsInfo(productIDs) { results in
            completion(Array(results.retrievedProducts))
        }
    }
    
    func purchase(_ product: SKProduct, withCompletion completion: @escaping ((Bool) -> Void)) {
        SwiftyStoreKit.purchaseProduct(product) { [unowned self] results in
            self.updateSubscriptionStatus()
            if self.hasActiveSubscription {
                Defaults[\.receiptUpdatedAt] = Date().timeIntervalSince1970
            }
            completion(self.hasActiveSubscription)
        }
    }
    
    func restorePurchases(withCompletion completion: @escaping ((Bool) -> Void)) {
        SwiftyStoreKit.restorePurchases { [unowned self] results in
            self.updateSubscriptionStatus()
            if self.hasActiveSubscription {
                Defaults[\.receiptUpdatedAt] = Date().timeIntervalSince1970
            }
            completion(self.hasActiveSubscription)
        }
    }
    
}

// MARK: - NSNotification.Name
extension NSNotification.Name {
    
    public struct PurchaseProvider {
        public static let DidUpdateSubscriptions = NSNotification.Name("notification.name.purchaseProvider.didUpdateSubscriptions")
    }
    
}

// MARK: - Private Functions
extension PurchaseProvider {
    
    // MARK: Helpers
    
    fileprivate func isSubscriptionUpdateRequired() -> Bool {
        guard hasActiveSubscription,
            let _ = try? InAppReceipt.localReceipt() else {
            // Return false if receipt doesn't exist or subscription is inactive
            return false
        }
        let currentTime = Date().timeIntervalSince1970
        let receiptUpdatedAt = Defaults[\.receiptUpdatedAt]
        return currentTime-receiptUpdatedAt > 2419200 // 4 weeks
    }
    
    fileprivate func refreshReceipt() {
        InAppReceipt.refresh { error in
            guard error == nil else {
                return
            }
            Defaults[\.receiptUpdatedAt] = Date().timeIntervalSince1970
            self.updateSubscriptionStatus()
        }
    }
    
    fileprivate func updateSubscriptionStatus() {
        hasActiveSubscription = hasActiveMonthlySubscription()
        NotificationCenter.default.post(name: NSNotification.Name.PurchaseProvider.DidUpdateSubscriptions, object: hasActiveSubscription)
    }
    
    fileprivate func hasActiveMonthlySubscription() -> Bool {
        guard let receipt = try? InAppReceipt.localReceipt() else {
            return false
        }
        let date = Date()
        let purchase = receipt.activeAutoRenewableSubscriptionPurchases(ofProductIdentifier: monthlyIdentifier, forDate: date)
        if purchase == nil {
            return false
        }
        return true
    }
    
}
