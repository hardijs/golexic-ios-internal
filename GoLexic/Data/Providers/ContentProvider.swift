//
//  ContentProvider.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import RealmSwift
import Moya

class ContentProvider: NSObject {

    // MARK: Properties
    
    // Static
    static let shared = ContentProvider()
    
    // Variables
    fileprivate(set) lazy var contentEndpoints: [GoLexicTarget.Endpoint] = {
        return makeContentEndpoints()
    }()
    
    // Flags
    fileprivate var isRefreshingToken: Bool = false
    
    // MARK: Initializers
    
    override init() {
        super.init()
        configureRepository()
        subscribeToProvider()
        subscribeToTargetEngine()
    }
    
}

// MARK: - Public Functions
extension ContentProvider {
    
    func update(_ endpoints: [GoLexicTarget.Endpoint], disposition: TargetEngine.Disposition) {
        for endpoint in endpoints {
            let target = GoLexicTarget(withEndpoint: endpoint)
            guard target.method == .get else {
                continue
            }
            TargetEngine.add(target, disposition: disposition, queue: .async)
        }
        TargetEngine.executePendingItems()
    }
    
    func cleanup() {
        deleteArchived()
    }
    
}

// MARK: - Private Functions
extension ContentProvider {
    
    // MARK: Helpers
    
    fileprivate func configureRepository() {
        TargetEngine.store = .persisted
    }
    
    fileprivate func subscribeToProvider() {
        GoLexicAPI.provider.refreshHandler = { [unowned self] completion in
            guard User.current != nil,
                self.isRefreshingToken == false else {
                completion(false)
                return
            }
            self.isRefreshingToken = true
            self.refreshToken(withCompletion: { success in
                if success == false {
                    User.current?.delete()
                }
                self.isRefreshingToken = false
                completion(success)
            })
        }
        GoLexicAPI.provider.didCompleteRefresh = { success in
            guard success else {
                return
            }
            DispatchQueue.main.async {
                TargetEngine.executePendingItems()
            }
        }
    }
    
    fileprivate func subscribeToTargetEngine() {
        TargetEngine.requestHandler = { (target: TargetType, completion: @escaping Completion) in
            let _target = target as! GoLexicTarget
            let queue = DispatchQueue(label: _target.endpoint.rawValue)
            let cancellable = GoLexicAPI.provider.request(_target, callbackQueue: queue, completion: { [unowned self] result in
                switch result {
                case let .success(moyaResponse):
                    let response = try? moyaResponse.mapJSON()
                    self.processResponse(withData: response, target: _target, success: true)
                case .failure:
                    self.processResponse(withData: nil, target: _target, success: false)
                }
                completion(result)
            })
            return cancellable
        }
        TargetEngine.isItemExecutable = { [unowned self] item in
            let target = item.target as! GoLexicTarget
            if target.requiresAuthorization == false {
                return true
            }
            if self.isRefreshingToken ||
                User.current == nil {
                return false
            }
            return true
        }
        TargetEngine.isFailedItemDiscardable = { (item, error) in
            guard let statusCode = error.response?.statusCode else {
                return false
            }
            if statusCode == 401 ||
                statusCode == 500 {
                return false
            }
            return true
        }
    }
    
    fileprivate func processResponse(withData data: Any?, target: GoLexicTarget, success: Bool) {
        /*
         Background thread!!!
         */
        guard success == true else {
            return
        }
        switch target.endpoint {
        case .user:
            User.current?.update(.profile, withData: data)
        case .letters:
            Realm.update(Letters.self, withData: data)
        case .scoringLetters:
            Realm.update(ScoringLetter.self, withData: data)
        case .scoringWords:
            Realm.update(ScoringWord.self, withData: data)
        case .instructions:
            Realm.update(Instruction.self, withData: data)
        case .feedback:
            Realm.update(Feedback.self, withData: data)
        case .alphabetPractices:
            Realm.update(ExerciseAlphabetPractice.self, withData: data)
        case .lostLetters:
            Realm.update(ExerciseLostLetters.self, withData: data)
        case .alphabetSortings:
            Realm.update(ExerciseAlphabetSorting.self, withData: data)
        case .letterBoards:
            Realm.update(ExerciseLetterBoard.self, withData: data)
        case .vowels:
            Realm.update(ExerciseVowels.self, withData: data)
        case .readings:
            Realm.update(ExerciseReading.self, withData: data)
        case .buildings:
            Realm.update(ExerciseBuilding.self, withData: data)
        case .revises:
            Realm.update(ExerciseRevise.self, withData: data)
        case .phase:
            if let _data = data as? [String: Any] {
                Realm.update(Phase.self, withData: [_data])
            } else {
                Realm.update(Phase.self, withData: nil)
            }
        default:
            break
        }
    }
    
    fileprivate func deleteArchived() {
        let realm = try! Realm()
        try! realm.write {
            Realm.deleteArchived(Letters.self, realm: realm)
            Realm.deleteArchived(LetterItem.self, realm: realm)
            Realm.deleteArchived(ScoringWord.self, realm: realm)
            Realm.deleteArchived(ScoringWordStep.self, realm: realm)
            Realm.deleteArchived(ScoringLetter.self, realm: realm)
            Realm.deleteArchived(ScoringLetterStep.self, realm: realm)
            Realm.deleteArchived(Instruction.self, realm: realm)
            Realm.deleteArchived(Feedback.self, realm: realm)
            Realm.deleteArchived(ExerciseAlphabetPractice.self, realm: realm)
            Realm.deleteArchived(ExerciseLostLetters.self, realm: realm)
            Realm.deleteArchived(ExerciseAlphabetSorting.self, realm: realm)
            Realm.deleteArchived(ExerciseLetterBoard.self, realm: realm)
            Realm.deleteArchived(ExerciseVowels.self, realm: realm)
            Realm.deleteArchived(ExerciseReading.self, realm: realm)
            Realm.deleteArchived(ExerciseReadingScoring.self, realm: realm)
            Realm.deleteArchived(ExerciseReadingLesson.self, realm: realm)
            Realm.deleteArchived(ExerciseReadingLessonPage.self, realm: realm)
            Realm.deleteArchived(ExerciseReadingLessonPageLine.self, realm: realm)
            Realm.deleteArchived(ExerciseReadingLessonPageLineText.self, realm: realm)
            Realm.deleteArchived(ExerciseBuilding.self, realm: realm)
            Realm.deleteArchived(ExerciseBuildingScoring.self, realm: realm)
            Realm.deleteArchived(ExerciseBuildingLesson.self, realm: realm)
            Realm.deleteArchived(ExerciseBuildingLessonPage.self, realm: realm)
            Realm.deleteArchived(ExerciseRevise.self, realm: realm)
            Realm.deleteArchived(ExerciseReviseScoring.self, realm: realm)
            Realm.deleteArchived(ExerciseRevisePage.self, realm: realm)
            Realm.deleteArchived(Phase.self, realm: realm)
            Realm.deleteArchived(PhaseExercise.self, realm: realm)
            Realm.deleteArchived(PhaseExerciseLesson.self, realm: realm)
        }
    }
    
    // MARK: Networking
    
    func refreshToken(withCompletion completion: @escaping ((Bool) -> Void)) {
        let target = GoLexicTarget(withEndpoint: .refreshToken)
        GoLexicAPI.provider.request(target) { result in
            switch result {
            case let .success(moyaResponse):
                guard let response = try? moyaResponse.mapJSON(),
                    let _response = response as? NSDictionary,
                    let token = _response["accessToken"] as? String else {
                    completion(false)
                    return
                }
                User.current?.update(.token, withData: token)
                completion(true)
            case .failure:
                completion(false)
            }
        }
    }
    
    // MARK: Make
    
    fileprivate func makeContentEndpoints() -> [GoLexicTarget.Endpoint] {
        return [.letters, .scoringLetters, .scoringWords, .instructions,
                .feedback, .alphabetPractices, .lostLetters, .alphabetSortings,
                .letterBoards, .vowels, .readings, .buildings, .revises, .phase]
    }
    
}
