//
//  UserDefaults+Keys.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    
    // App
    var appHasBeenLaunched: DefaultsKey<Bool> { return .init("appHasBeenLaunched", defaultValue: false) }
    var appIsNotificationsEnabled: DefaultsKey<Bool> { return .init("appIsNotificationsEnabled", defaultValue: false) }
    
    // Other
    var didShowOnboardingTips: DefaultsKey<Bool> { return .init("didShowOnboardingTips", defaultValue: false) }
    
    var introViewsAlphabetPractice: DefaultsKey<Int> { return .init("introViewsAlphabetPractice", defaultValue: 0) }
    var introViewsLostLetters: DefaultsKey<Int> { return .init("introViewsLostLetters", defaultValue: 0) }
    var introViewsAlphabetSorting: DefaultsKey<Int> { return .init("introViewsAlphabetSorting", defaultValue: 0) }
    var introViewsLetterBoard: DefaultsKey<Int> { return .init("introViewsLetterBoard", defaultValue: 0) }
    var introViewsVowels: DefaultsKey<Int> { return .init("introViewsVowels", defaultValue: 0) }
    var introViewsReading: DefaultsKey<Int> { return .init("introViewsReading", defaultValue: 0) }
    var introViewsBuilding: DefaultsKey<Int> { return .init("introViewsBuilding", defaultValue: 0) }
    var introViewsRevise: DefaultsKey<Int> { return .init("introViewsRevise", defaultValue: 0) }
    
    // IAP
    var receiptUpdatedAt: DefaultsKey<TimeInterval> { return .init("receiptUpdatedAt", defaultValue: 0) }
    
    // Phase
    var lastPhaseCompletedAt: DefaultsKey<TimeInterval> { return .init("lastPhaseCompletedAt", defaultValue: 0) }
    
}
