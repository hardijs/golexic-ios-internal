//
//  UIApplication.swift
//  GoLexic
//
//  Created by Denis Kuznetsov on 10.03.2021.
//  Copyright © 2021 CUBE Mobile. All rights reserved.
//

import UIKit

extension UIApplication {
    /// The top most view controller
    static var topMostViewController: UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController?.visibleViewController
    }
}
