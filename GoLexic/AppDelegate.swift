//
//  AppDelegate.swift
//  GoLexic
//
//  Created by Armands L. on 22/03/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import Firebase
import SwiftyUserDefaults
import Siren
import SwiftyStoreKit
import RealmSwift
import Mixpanel
import Sentry

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    fileprivate var isSuspended: Bool = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Configure
        FirebaseApp.configure()
        SentrySDK.start { options in
            options.dsn = "https://d081606e556142c69f79f0154ef60aa5@o436839.ingest.sentry.io/5587763"
            options.environment = Globals.enviroment.rawValue
        }
        Mixpanel.initialize(token: "4c9ac3f03489ca889b788f312459b912")
        configureLocalization()
        configureDatabase()
        Appearances.loadDefaults()
        configureSiren()
        // Cleanup
        if Defaults[\.appHasBeenLaunched] == false {
            Defaults[\.appHasBeenLaunched] = true
            User.current?.delete()
        }
        // Application
        application.applicationIconBadgeNumber = 0
        application.isIdleTimerDisabled = true
        // Messaging
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        // IAP
        completeTransactions()
        // Initial view controller
        let homeViewController: UIViewController = {
            let controllerName = String(describing: HomeViewController.self)
            let navigationName = controllerName.replacingOccurrences(of: "View", with: "Navigation")
            let storyboard = UIStoryboard(name: controllerName, bundle: nil)
            return storyboard.instantiateViewController(withIdentifier: navigationName)
        }()
        // Main window
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.rootViewController = homeViewController;
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        window!.makeKeyAndVisible()
        // Analytics
        Analytics.logAppStart()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        Analytics.logAppSuspend()
        isSuspended = true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if isSuspended {
            Analytics.logAppReopen()
        }
        isSuspended = false
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        HTTPCookieStorage.shared.cookieAcceptPolicy = .always
        NotificationProvider.shared.updateNotificationAuthorizationStatus()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        Analytics.logAppClose()
    }
	
	/*
	func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
		switch UIDevice.current.userInterfaceIdiom {
			case .phone:
				return .portrait
			case .pad:
				return .landscape
			default:
				return .landscape
		}
	}
	*/

}

// MARK: - UNUserNotificationCenterDelegate
extension AppDelegate : UNUserNotificationCenterDelegate {

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
                              withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([[.alert]])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
}

// MARK: - MessagingDelegate
extension AppDelegate: MessagingDelegate {
    
	func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
		if let fcmToken = fcmToken {
			NotificationProvider.shared.update(withToken: fcmToken)
		}
    }
    
}

// MARK: - Private Functions
extension AppDelegate {
    
    // MARK: Helpers
    
    fileprivate func configureLocalization() {
        Localization.language = "en"
        Localization.resourceName = "LocalizedTexts"
    }
    
    fileprivate func configureDatabase() {
        let config = Realm.Configuration(schemaVersion: 4,
                                         migrationBlock: { migration, oldSchemaVersion in
            if oldSchemaVersion < 3 {
                // Set previous phases as active
                migration.enumerateObjects(ofType: Phase.className()) { oldObject, newObject in
                    newObject!["isActivated"] = true
                }
            }
        })
        Realm.Configuration.defaultConfiguration = config
        let _ = try! Realm()
        // Disable realm backup
        var resourceValues = URLResourceValues()
        resourceValues.isExcludedFromBackup = true
        guard var url = config.fileURL else {
            return
        }
        try? url.setResourceValues(resourceValues)
    }
    
    fileprivate func configureSiren() {
        Siren.shared.apiManager = APIManager(countryCode: "GB")
        Siren.shared.rulesManager = RulesManager(globalRules: .critical)
        Siren.shared.presentationManager = PresentationManager(forceLanguageLocalization: .english)
        Siren.shared.wail()
    }
    
    fileprivate func completeTransactions() {
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                case .failed, .purchasing, .deferred:
                    break // Do nothing
                @unknown default:
                    break // Do nothing
                }
            }
        }
    }
    
}
