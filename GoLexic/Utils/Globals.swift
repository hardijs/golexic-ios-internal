//
//  Globals.swift
//
//  Created by Armands L.
//  Copyright © 2018 Cube Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class Globals {
    
    // MARK: Enums
    
    enum Enviroment: String {
        case production
        case staging
        case test
    }
    
    // MARK: Variables
    
    static let userAgent = "GoLexic(OS: iOS)"
    static var enviroment: Enviroment {
        #if STAGING
        return .staging
        #elseif TEST
        return .test
        #else
        return .production
        #endif
    }
    
}

// MARK: - Validation
extension Globals {
    
    static func isEmailValid(_ email: String?) -> Bool {
        guard let _email = email else {
            return false
        }
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: _email)
    }
    
}

// MARK: - Formatters
extension Globals {
    
    static func currrencyFormatter(withLocale locale: Locale) -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter
    }
    
}

// MARK: - Presentations
extension Globals {
    
    static func presentAlert(withErrorCode errorCode: Int?) {
        let message: String = {
            guard let _code = errorCode,
                let message = Localization.string(forKeyPath: "Globals.errors.\(_code)") else {
                return Localization.string(forKeyPath: "Globals.alerts.connectionError")!
            }
            return message
        }()
        let confirm = Localization.string(forKeyPath: "Globals.confirm")!
        presentAlert(withTitle: "", message: message, cancel: confirm)
    }
    
    @discardableResult
    static func presentAlert(withTitle title: String?, message: String?, cancel cancelText: String?,
                             actions actionTexts: [String]? = nil, style: UIAlertController.Style = .alert,
                             animated: Bool = true, completion: ((Int) -> Void)? = nil) -> UIAlertController {
        // Create alert window
        var window:UIWindow? = UIWindow()
        window!.windowLevel = UIWindow.Level.alert
        window!.backgroundColor = .clear
        window!.rootViewController = UIViewController()
        if #available(iOS 13.0, *) {
            window!.overrideUserInterfaceStyle = .light
        }
        window!.makeKeyAndVisible()
        // Create completion functions
        func actionCompletion(withIndex index: Int) {
            let appWindow = UIApplication.shared.delegate?.window
            appWindow??.makeKeyAndVisible()
            window = nil
            completion?(index)
        }
        // Create alert controller
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        if let _cancelText = cancelText {
            let cancel = UIAlertAction(title: _cancelText, style: .cancel) { action in
                actionCompletion(withIndex: 0)
            }
            alertController.addAction(cancel)
        }
        if let _actionTexts = actionTexts {
            for (index, actionText) in _actionTexts.enumerated() {
                let action = UIAlertAction(title: actionText, style: .default) { action in
                    actionCompletion(withIndex: index+1)
                }
                alertController.addAction(action)
            }
        }
        // Present in alert window
        window!.rootViewController?.present(alertController, animated: animated, completion: nil)
        return alertController
    }
    
}

// MARK: - Device Type
extension Globals {
	
	static var isPad: Bool { UIDevice.current.userInterfaceIdiom == .pad }
	static var isPhone: Bool { UIDevice.current.userInterfaceIdiom == .phone }
	
}
