//
//  TargetEngine+HElpers.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 19/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import Cubemobile

extension TargetEngine {
    
    static func pending(_ endpoint: GoLexicTarget.Endpoint) -> GoLexicTarget? {
        guard let items = TargetEngine.pendingItems() else {
            return nil
        }
        let first = items.first(where: { item -> Bool in
            guard let target = item.target as? GoLexicTarget else {
                return false
            }
            return target.endpoint == endpoint
        })
        guard let _first = first else {
            return nil
        }
        return _first.target as? GoLexicTarget
    }
    
    static func pendingContainsAny(_ endpoints: [GoLexicTarget.Endpoint]) -> Bool {
        guard let items = TargetEngine.pendingItems() else {
            return false
        }
        var contains: Bool = false
        for item in items {
            guard let target = item.target as? GoLexicTarget else {
                continue
            }
            if endpoints.contains(target.endpoint) == true {
                contains = true
                break
            }
        }
        return contains
    }
    
}
