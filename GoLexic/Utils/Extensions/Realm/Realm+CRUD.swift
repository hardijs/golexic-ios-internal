//
//  DatabaseFactory.swift
//
//  Created by Armands L.
//  Copyright © 2018 Cube Mobile. All rights reserved.
//

import Foundation
import RealmSwift

extension Realm {
    
    static func update<Element: Updatable & Archivable>(_ type: Element.Type, withData data: Any?) where Element: Object {
        let realm = try! Realm()
        var identifiers: Set<String> = []
        func archiveMissing() {
            let results = realm.objects(type.self)
            for case let object in results {
                guard identifiers.contains(object.identifier) == false else {
                    continue
                }
                object.archive(true)
            }
        }
        try! realm.write {
            guard let list = data as? [[String: Any]] else {
                archiveMissing()
                return
            }
            for item in list {
                guard let _identifier = item["id"] as? String else  {
                    continue
                }
                identifiers.insert(_identifier)
                if let _object = realm.object(ofType: type.self, forPrimaryKey: _identifier) {
                    _object.archive(false)
                    _object.update(withData: item)
                } else {
                    let _object = type.init(withData: item)
                    realm.add(_object)
                }
            }
            archiveMissing()
        }
    }
    
    static func archive<Element: Archivable>(_ type: Element.Type, realm: Realm) where Element: Object {
        let results = realm.objects(type.self)
        for object in results {
            object.archive(true)
        }
    }
    
    static func deleteArchived<Element: Archivable>(_ type: Element.Type, realm: Realm) where Element: Object {
        let items = realm.objects(type.self).filter("isArchived == true")
        realm.delete(items)
    }
    
}
