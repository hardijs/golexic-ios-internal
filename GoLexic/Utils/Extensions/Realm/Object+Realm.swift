//
//  Object+Realm.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 20/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift

extension Object {
    
    var availableRealm: Realm {
        if let _realm = realm {
            return _realm
        }
        return try! Realm()
    }
    
}
