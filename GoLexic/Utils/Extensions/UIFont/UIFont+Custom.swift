//
//  UIFont+Custom.swift
//  GoLexic
//
//  Created by Armands L. on 09/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit

extension UIFont {

    struct Name {
        struct Montserrat {
            static let light = "Montserrat-Light"
            static let regular = "Montserrat-Regular"
            static let medium = "Montserrat-Medium"
            static let semibold = "Montserrat-SemiBold"
            static let bold = "Montserrat-Bold"
            static let italic = "Montserrat-Italic"
        }
    }
    
}
