//
//  Scorable.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 31/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation

enum Score: Int {
    case none
    case success
    case maybe
    case failed
}
