//
//  Appearances.swift
//
//  Created by Armands Lazdiņš.
//  Copyright © 2017 Cube Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import BonMot

class Appearances {
    
    static func loadDefaults() {
        loadNavigationBarStyles()
        loadBonMotStyles()
    }
    
}

//MARK: - UINavigationBar
extension Appearances {
    
    fileprivate static func loadNavigationBarStyles() {
        let font = UIFont(name: UIFont.Name.Montserrat.medium, size: 16)!
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithTransparentBackground()
            appearance.titleTextAttributes = [.font: font, .foregroundColor: UIColor.white]
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
        } else {
            UINavigationBar.appearance().tintColor = #colorLiteral(red: 0.2156862745, green: 0.2392156863, blue: 0.2470588235, alpha: 1)
            UINavigationBar.appearance().barTintColor = UIColor.clear
            UINavigationBar.appearance().shadowImage = UIImage(withColor: .clear)
            UINavigationBar.appearance().setBackgroundImage(UIImage(withColor: .clear), for: .default)
            UINavigationBar.appearance().titleTextAttributes = [.font: font, .foregroundColor: UIColor.white]
        }
    }
    
}

// MARK: - BonMot Styles
extension Appearances {
    
    fileprivate static func loadBonMotStyles() {
        let instructionText = StringStyle(
            .tracking(.point(1.3)),
            .lineHeightMultiple(1.2),
            .alignment(.center)
        )
        NamedStyles.shared.registerStyle(forName: "InstructionText", style: instructionText)
		
		let underlinedText = StringStyle(
			.underline(.single, nil)
		)
		NamedStyles.shared.registerStyle(forName: "UnderlinedText", style: underlinedText)
    }
    
}
