//
//  Recognizable.swift
//  GoLexic
//
//  Created by Armands L. on 16/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation

enum RecognitionStatus: Int {
    case none
    case selected
    case recognized
}

protocol Recognizable: class {
    
    var status: RecognitionStatus { get set }
    var value: String { get set }
    var confidence: Double { get set }
    var isSuggested: Bool { get set }
    var isFailed: Bool { get set }
    
    @discardableResult
    func isEqualToAny(_ words: [KIOSWord]?, includeSimilarities: Bool) -> Bool
    
    @discardableResult
    func updateConfidenceIfMatches(_ words: [KIOSWord]?, includeSimilarities: Bool) -> Bool
    
}
