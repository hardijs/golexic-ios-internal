//
//  Exercise.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 13/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift

enum ExerciseType: Int {
    
    case unknown
    case alphabetPractice
    case lostLetters
    case alphabetSorting
    case letterBoard
    case vowels
    case reading
    case building
    case revise
    
    func hasSpeechRecongnition() -> Bool {
        switch self {
        case .alphabetPractice, .vowels, .reading, .building, .revise:
            return true
        default:
            return false
        }
    }
    
    func makeLocalizationKey() -> String {
        switch self {
        case .alphabetPractice:
            return "Globals.exercise.alphabetPractice"
        case .lostLetters:
            return "Globals.exercise.lostLetters"
        case .alphabetSorting:
            return "Globals.exercise.alphabetSorting"
        case .letterBoard:
            return "Globals.exercise.letterBoard"
        case .vowels:
            return "Globals.exercise.vowels"
        case .reading:
            return "Globals.exercise.reading"
        case .building:
            return "Globals.exercise.building"
        case .revise:
            return "Globals.exercise.revise"
        default:
            return ""
        }
    }
    
    func makeAnalyticsValue() -> String {
        switch self {
        case .alphabetPractice:
            return "alphabet_practice"
        case .lostLetters:
            return "lost_letters"
        case .alphabetSorting:
            return "alphabet_sorting"
        case .letterBoard:
            return "letter_board"
        case .vowels:
            return "find_vowels"
        case .reading:
            return "word_reading"
        case .building:
            return "word_building"
        case .revise:
            return "revise"
        default:
            return ""
        }
    }
    
}

protocol Exercise: class {
    
    var type: ExerciseType { get }
    var instruction: Instruction? { get }
    var feedbacks: Results<Feedback> { get }
    
    func hasRelatedAssets() -> Bool
    
}
