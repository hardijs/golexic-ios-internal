//
//  Lesson.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 30/06/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation

protocol Lesson: class {
    
    var lessonIndex: Int { get }
    
}
