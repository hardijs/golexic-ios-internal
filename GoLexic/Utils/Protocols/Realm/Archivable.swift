//
//  Realm+Archivable.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation

protocol Archivable: class {
    
    var isArchived: Bool { get set }
    func archive(_ archive: Bool)
    
}

extension Archivable {
    
    func archive(_ archive: Bool) {
        isArchived = archive
    }
    
}
