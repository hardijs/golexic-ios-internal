//
//  UnequallyPersistable.swift
//  Narkefrakt
//
//  Created by Armands L. on 21/12/2017.
//  Copyright © 2017 Cube Mobile. All rights reserved.
//

import Foundation

protocol Updatable: class {
    
    var identifier: String { get set }
    
    init(withData data: [String: Any])
    func update(withData data: [String: Any])
    
}
