//
//  Analytics.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 02/11/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import Mixpanel
import Cubemobile

class Analytics {
    
    struct Events {
        // App
        static let appStart = "app_start"
        static let appSuspended = "app_suspend"
        static let appReoponed = "app_reopen"
        static let appClose = "app_close"
        // Instructions
        static let instructionsViewed = "instructions_viewed"
        // Exercise
        static let startExercise = "start_exercise"
        static let completeExercise = "complete_exercise"
        static let cancelExercise = "cancel_exercise"
        static let nextExercisePage = "next_exercise_page"
        static let nextExerciseLesson = "next_exercise_lesson"
        static let redoExercise = "redo_exercise"
        // Letter
        static let letterLift = "letter_lift"
        static let letterDropSuccess = "letter_drop_success"
        static let letterDropFail = "letter_drop_fail"
        // ASR
        static let voiceRecognitionSuccess = "voice_recognition_success"
        static let voiceRecognitionFailed = "voice_recognition_failed"
        // Buttons
        static let mic = "mic"
        static let help = "help"
        static let intro = "intro"
        // Other
        static let screenTouch = "screen_touch"
        static let screenViewed = "screen_viewed"
        static let controlChanged = "control_changed"
    }
    
    struct Parameters {
        static let id = "id"
        static let char = "char"
        static let text = "text"
        static let exercise = "exercise"
        static let lesson = "lesson"
        static let action = "action"
        static let controlName = "controlName"
        static let screenName = "screenName"
    }
    
    struct Screens {
        // Console
        static let consoleDashboard = "dashboard"
        static let consoleSettings = "settings"
        static let consoleAbout = "about"
        static let consoleHelp = "help"
    }
    
    struct Controls {
        // Console
        static let reminders = "reminders"
        static let notifications = "notifications"
    }
    
    struct Action {
        static let on = "on"
        static let off = "off"
        static let yes = "yes"
        static let no = "no"
        static let open = "open"
        static let start = "start"
        static let back = "back"
    }

    
}

// MARK: - Log
extension Analytics {
    
    // MARK: App
    
    static func logAppStart() {
        let identifier = AppInfo.deviceUUID() ?? "unknown"
        Mixpanel.mainInstance().track(event: Analytics.Events.appStart,
                                      properties: [Analytics.Parameters.id: identifier])
    }
    
    static func logAppSuspend() {
        let identifier = AppInfo.deviceUUID() ?? "unknown"
        Mixpanel.mainInstance().track(event: Analytics.Events.appSuspended,
                                      properties: [Analytics.Parameters.id: identifier])
    }
    
    static func logAppReopen() {
        let identifier = AppInfo.deviceUUID() ?? "unknown"
        Mixpanel.mainInstance().track(event: Analytics.Events.appReoponed,
                                      properties: [Analytics.Parameters.id: identifier])
    }
    
    static func logAppClose() {
        let identifier = AppInfo.deviceUUID() ?? "unknown"
        Mixpanel.mainInstance().track(event: Analytics.Events.appClose,
                                      properties: [Analytics.Parameters.id: identifier])
    }
    
    // MARK: Letters
    
    static func logLetterLift(_ character: String) {
        Mixpanel.mainInstance().track(event: Analytics.Events.letterLift,
                                      properties: [Analytics.Parameters.char: character])
    }
    
    static func logLetterDropSuccess(_ character: String) {
        Mixpanel.mainInstance().track(event: Analytics.Events.letterDropSuccess,
                                      properties: [Analytics.Parameters.char: character])
    }
    
    static func logLetterDropFail(_ character: String) {
        Mixpanel.mainInstance().track(event: Analytics.Events.letterDropFail,
                                      properties: [Analytics.Parameters.char: character])
    }
    
    // MARK: ASR
    
    static func logVoiceRecognitionSuccess(_ text: String) {
        Mixpanel.mainInstance().track(event: Analytics.Events.voiceRecognitionSuccess,
                                      properties: [Analytics.Parameters.text: text])
    }
    
    static func logVoiceRecognitionFailed(_ text: String) {
        Mixpanel.mainInstance().track(event: Analytics.Events.voiceRecognitionFailed,
                                      properties: [Analytics.Parameters.text: text])
    }
    
    // MARK: Screen
    
    static func logScreenView(_ screenName: String) {
        Mixpanel.mainInstance().track(event: Analytics.Events.screenViewed,
                                      properties: [Analytics.Parameters.screenName: screenName])
    }
    
    // MARK: Other
    
    static func logControlChanged(_ controlName: String, action: String) {
        Mixpanel.mainInstance().track(event: Analytics.Events.controlChanged,
                                      properties: [Analytics.Parameters.controlName: controlName,
                                                   Analytics.Parameters.action: action])
    }
    
}
