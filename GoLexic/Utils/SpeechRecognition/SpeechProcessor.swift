//
//  SpeechProcessor.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 24/11/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation

class SpeechProcessor {
    
    // MARK: Properties
    fileprivate var wordHistory: [KIOSWord] = []
    fileprivate var currentWords: [KIOSWord] = []
    
}

// MARK: - Public Functions
extension SpeechProcessor {
    
    func processResult(_ result: KIOSResult, isNewSearch: Bool) -> [KIOSWord] {
        if isNewSearch {
            currentWords.removeAll()
        }
        guard let _words = result.words else {
            return currentWords
        }
        guard wordHistory.isEmpty == false, _words.count > wordHistory.count,
            let firstDiffIndex = makeFirstDiffIndex(_words, secondWords: wordHistory) else {
            // No previuos words, new results or no word matches
            currentWords.append(contentsOf: _words)
            wordHistory = _words
            return _words
        }
        wordHistory = _words
        // Add new words to current words and return
        let newWords = Array(_words[firstDiffIndex..<_words.count])
        currentWords.append(contentsOf: newWords)
        return currentWords
    }
    
    func reset() {
        wordHistory.removeAll()
    }
    
}

extension SpeechProcessor {
    
    // MARK: Make
    
    fileprivate func makeFirstDiffIndex(_ firstWords: [KIOSWord], secondWords: [KIOSWord]) -> Int? {
        for x in 0..<firstWords.count {
            guard x < secondWords.count else {
                return x
            }
            let firstWord = firstWords[x]
            let secondWord = secondWords[x]
            if firstWord.text.uppercased() != secondWord.text.uppercased() {
                return x
            }
        }
        return nil
    }
    
}
