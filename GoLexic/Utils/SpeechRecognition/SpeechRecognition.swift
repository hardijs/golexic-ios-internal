//
//  SpeechRecognition.swift
//  GoLexic
//
//  Created by Armands L. on 08/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation
import AVFoundation

protocol SpeechRecognitionDelegate: class {
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, didChange state: KIOSRecognizerState)
    func speechRecognition(_ speechRecognition: SpeechRecognition, partial result: KIOSResult)
    func speechRecognition(_ speechRecognition: SpeechRecognition, final result: KIOSResult)
    func speechRecognitionReadyToListenAfterInterruption(_ speechRecognition: SpeechRecognition)
    
}

class SpeechRecognition: NSObject {
    
    // MARK: Properties
    
    static var shared = SpeechRecognition()
    
    // Variables
    weak var delegate: SpeechRecognitionDelegate?
    var state: KIOSRecognizerState {
        return recognizer.recognizerState
    }
    fileprivate lazy var recognizer = KIOSRecognizer.sharedInstance()!
    
    // Flags
    fileprivate(set) var isConfigured: Bool = false
    
}

// MARK: - Public Functions
extension SpeechRecognition {
    
    // MARK: Configure
    
    static func configure() {
        guard shared.isConfigured == false else {
            return
        }
        shared.isConfigured = true
        shared.configureRecognizer()
    }
    
    static func isRecordPermissionGranted() -> Bool {
        return AVAudioSession.sharedInstance().recordPermission == .granted
    }
    
    static func requestRecordPermission(withCompletion completion: ((Bool) -> Void)? = nil) {
        guard isRecordPermissionGranted() == false else {
            completion?(true)
            return
        }
        AVAudioSession.sharedInstance().requestRecordPermission { granted in
            completion?(granted)
        }
    }
    
    // MARK: Recognizer
    
    @discardableResult
    func startListening() -> Bool {
        guard SpeechRecognition.isRecordPermissionGranted(),
            isConfigured, state == .readyToListen else {
            return false
        }
        updateVADParameters(toStop: false)
        return recognizer.startListening()
    }
    
    func stopListening(withResults results: Bool = true) {
        guard isConfigured,
            state == .listening else {
            return
        }
        if results == false {
            recognizer.stopListening()
        } else {
            updateVADParameters(toStop: true)
        }
    }
    
    @discardableResult
    func prepareForListening(withDecodingGraph decodingGraph: String) -> Bool {
        return recognizer.prepareForListeningWithCustomDecodingGraph(withName: decodingGraph)
    }
    
    // MARK: Decoding Graph
    
    func decodingGraphExists(forName name: String) -> Bool {
        return KIOSDecodingGraph.decodingGraph(withNameExists: name, for: recognizer)
    }
    
    func urlForDecodingGraph(withName name: String) -> URL? {
        return KIOSDecodingGraph.getDirURL(name, for: recognizer)
    }
    
    @discardableResult
    func createDecodingGraph(withName name: String, sentences: Set<String>) -> Bool {
        guard isConfigured else {
            return false
        }
        if let _url = urlForDecodingGraph(withName: name) {
            try? FileManager.default.removeItem(at: _url)
        }
        return KIOSDecodingGraph.createDecodingGraph(fromSentences: Array(sentences), for: recognizer,
                                                     andTask: .default, andSaveWithName: name)
    }
    
}

// MARK: - KIOSRecognizerDelegate
extension SpeechRecognition: KIOSRecognizerDelegate {
    
    func recognizerPartialResult(_ result: KIOSResult, for recognizer: KIOSRecognizer) {
        guard state == .listening else {
            return
        }
        DispatchQueue.global(qos: .utility).async {
            if let _delegate = self.delegate {
                _delegate.speechRecognition(self, partial: result)
            }
        }
    }
    
    func recognizerFinalResult(_ result: KIOSResult, for recognizer: KIOSRecognizer) {
        DispatchQueue.global(qos: .utility).async {
            if let _delegate = self.delegate {
                _delegate.speechRecognition(self, final: result)
            }
        }
    }
    
    func unwindAppAudioBeforeAudioInterrupt() {
        // Do nothing
    }
    
    func recognizerReadyToListen(afterInterrupt recognizer: KIOSRecognizer) {
        if let _delegate = delegate {
            _delegate.speechRecognitionReadyToListenAfterInterruption(self)
        }
    }
    
}

// MARK: - KVO
extension SpeechRecognition {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let _change = change,
            keyPath == "recognizerState" else {
            return
        }
        guard let oldValue = _change[.oldKey] as? Int,
            let newValue = _change[.newKey] as? Int,
            oldValue != newValue,
            let newState = KIOSRecognizerState(rawValue: newValue) else {
                return
        }
        if let _delegate = delegate {
            DispatchQueue.main.async {
                _delegate.speechRecognition(self, didChange: newState)
            }
        }
    }
    
}

// MARK - Private Functions
extension SpeechRecognition {
    
    // MARK: Helpers
    
    fileprivate func configureRecognizer() {
        // KIOSRecognizer
        KIOSRecognizer.setLogLevel(.warning)
        KIOSRecognizer.initWithASRBundle("keenB2mQTUNK-nnet3chain-en-us")
        // Recognizer instance
        recognizer.delegate = self
        recognizer.rescore = false
        recognizer.createJSONMetadata = false
        recognizer.createAudioRecordings = false
        recognizer.addObserver(self, forKeyPath: "recognizerState", options: [.new, .old], context: nil)
    }
    
    fileprivate func updateVADParameters(toStop stop: Bool) {
        let duration: Float = stop == true ? 0.01 : .greatestFiniteMagnitude
        recognizer.setVADParameter(KIOSVadParameter.timeoutForNoSpeech, toValue: duration)
        recognizer.setVADParameter(KIOSVadParameter.timeoutEndSilenceForAnyMatch, toValue: duration)
        recognizer.setVADParameter(KIOSVadParameter.timeoutEndSilenceForGoodMatch, toValue: duration)
        recognizer.setVADParameter(KIOSVadParameter.timeoutMaxDuration, toValue: duration)
    }
    
}
