//
//  SpeechRepository.swift
//  GoLexic
//
//  Created by Armands L. on 16/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift

class SpeechRepository {
    
    // MARK: Propertoes
    
    // Static
    static let shared = SpeechRepository()
    
    // Variables
    fileprivate(set) var updatedDecodingGraphs: Set<String> = []

}

// MARK: - Public Functions
extension SpeechRepository {
    
    func decodingGraphExists(forPhaseExercise phaseExercise: PhaseExercise) -> Bool {
        switch phaseExercise.exerciseType {
        case .reading, .building, .revise:
            guard updatedDecodingGraphs.contains(phaseExercise.exerciseId) else {
                return false
            }
            return SpeechRecognition.shared.decodingGraphExists(forName: phaseExercise.exerciseId)
        default:
            guard let _letters = letters(forExercise: phaseExercise.exercise),
                updatedDecodingGraphs.contains(_letters.identifier) else {
                return false
            }
            return SpeechRecognition.shared.decodingGraphExists(forName: _letters.identifier)
        }
    }
    
    func updateDecodingGraph(forPhaseExercise phaseExercise: PhaseExercise, completion: @escaping ((Bool) -> Void)) {
        guard let _exercise = phaseExercise.exercise else {
            completion(false)
            return
        }
        switch phaseExercise.exerciseType {
        case .reading:
            guard let _readingExercise = _exercise as? ExerciseReading,
                let _sentences = makeSentences(fromReadingExercise: _readingExercise) else {
                completion(false)
                return
            }
            let _decodingGraphName = _readingExercise.identifier
            updatedDecodingGraphs.insert(_decodingGraphName)
            DispatchQueue.global(qos: .utility).async {
                SpeechRecognition.shared.createDecodingGraph(withName: _decodingGraphName, sentences: _sentences)
                DispatchQueue.main.async {
                    completion(true)
                }
            }
        case .building:
            guard let _buildingExercise = _exercise as? ExerciseBuilding,
                let _sentences = makeSentences(fromBuildingExercise: _buildingExercise) else {
                completion(false)
                return
            }
            let _decodingGraphName = _buildingExercise.identifier
            updatedDecodingGraphs.insert(_decodingGraphName)
            DispatchQueue.global(qos: .utility).async {
                SpeechRecognition.shared.createDecodingGraph(withName: _decodingGraphName, sentences: _sentences)
                DispatchQueue.main.async {
                    completion(true)
                }
            }
        case .revise:
            guard let _reviseExercise = _exercise as? ExerciseRevise,
                let _sentences = makeSentences(fromReviseExercise: _reviseExercise) else {
                completion(false)
                return
            }
            let _decodingGraphName = _reviseExercise.identifier
            updatedDecodingGraphs.insert(_decodingGraphName)
            DispatchQueue.global(qos: .utility).async {
                SpeechRecognition.shared.createDecodingGraph(withName: _decodingGraphName, sentences: _sentences)
                DispatchQueue.main.async {
                    completion(true)
                }
            }
        default:
            guard let _letters = letters(forExercise: _exercise),
                let _sentences = makeSentences(fromLetters: _letters) else {
                completion(false)
                return
            }
            let _decodingGraphName = _letters.identifier
            updatedDecodingGraphs.insert(_decodingGraphName)
            DispatchQueue.global(qos: .utility).async {
                SpeechRecognition.shared.createDecodingGraph(withName: _decodingGraphName, sentences: _sentences)
                DispatchQueue.main.async {
                    completion(true)
                }
            }
        }
    }
    
    func clearDecodingGraphs() {
        updatedDecodingGraphs.removeAll()
    }
    
}

// MARK: - Private Functions
extension SpeechRepository {
    
    // MARK: Helpers
    
    fileprivate func letters(forExercise exercise: Exercise?) -> Letters? {
        guard let _exercise = exercise else {
            return nil
        }
        if let _alphabetExercise = _exercise as? ExerciseAlphabetPractice {
            return _alphabetExercise.letters
        } else if let _vowelsExercise = _exercise as? ExerciseVowels {
            return _vowelsExercise.letters
        }
        return nil
    }
    
    // MARK: Make
    
    fileprivate func makeSentences(fromLetters letters: Letters) -> Set<String>? {
        var _sentences: Set<String> = []
        for item in letters.items {
            _sentences.insert(item.letter)
        }
        guard _sentences.isEmpty == false else {
            return nil
        }
        return _sentences
    }
    
    fileprivate func makeSentences(fromReadingExercise readingExercise: ExerciseReading) -> Set<String>? {
        var _sentences: Set<String> = []
        for lesson in readingExercise.lessons {
            for page in lesson.pages {
                for line in page.lines {
                    for text in line.texts {
                        _sentences.insert(text.text)
                    }
                }
            }
        }
        guard _sentences.isEmpty == false else {
            return nil
        }
        return _sentences
    }
    
    fileprivate func makeSentences(fromBuildingExercise buildingExercise: ExerciseBuilding) -> Set<String>? {
        var _sentences: Set<String> = []
        for lesson in buildingExercise.lessons {
            for page in lesson.pages {
                _sentences.insert(page.text)
            }
        }
        if let _letters = buildingExercise.letters {
            for item in _letters.items {
                _sentences.insert(item.letter)
            }
        }
        guard _sentences.isEmpty == false else {
            return nil
        }
        return _sentences
    }
    
    fileprivate func makeSentences(fromReviseExercise reviseExercise: ExerciseRevise) -> Set<String>? {
        var _sentences: Set<String> = []
        for page in reviseExercise.pages {
            _sentences.insert(page.text)
        }
        guard _sentences.isEmpty == false else {
            return nil
        }
        return _sentences
    }
    
}
