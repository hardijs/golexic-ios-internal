//
//  BoundlessView.swift
//  GoLexic
//
//  Created by Armands L. on 11/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit

class BoundlessView: UIView {

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        for subview in subviews as [UIView] {
            if !subview.isHidden
                && subview.alpha > 0
                && subview.isUserInteractionEnabled
                && subview.point(inside: convert(point, to: subview), with: event) {
                return true
            }
        }
        return false
    }

}
