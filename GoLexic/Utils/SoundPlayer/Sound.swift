//
//  Sound.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 28/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation

class Sound {
    
    // MARK: Properties
    
    let identifier = UUID().uuidString
    let fileUrl: URL
    
    // MARK: Initializers
    
    convenience init?(withResourceName name: String, ofType type: String? = nil) {
        guard let url = Bundle.main.url(forResource: name, withExtension: type) else {
            return nil
        }
        self.init(withUrl: url)
    }
    
    init(withUrl url: URL) {
        fileUrl = url
    }
    
}
