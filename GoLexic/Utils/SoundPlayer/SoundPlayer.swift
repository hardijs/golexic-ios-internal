//
//  SoundPlayer.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 28/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import MediaPlayer
import AVFoundation

class SoundPlayer: NSObject {
    
    // MARK: Enums
    
    enum SoundStatus {
        case none
        case playing
        case stopped
    }
        
    enum PlayerStatus {
        case stopped
        case paused
        case playing
        case failed
    }
    
    // MARK: Typealiases
    
    typealias SoundCompletion = (_ status: SoundPlayer.SoundStatus) -> Void
    
    // MARK: Properties
    
    // Static
    static let shared = SoundPlayer()
    
    // Closure
    var completion: SoundCompletion?
    
    // Variables
    fileprivate lazy var player: AVPlayer = {
        return makePlayer()
    }()
    fileprivate(set) var playerItem: AVPlayerItem?
    fileprivate(set) var status: SoundStatus = .none {
        didSet {
            guard status != oldValue else {
                return
            }
            if let _completion = completion {
                _completion(status)
            }
        }
    }
    
    // MARK: Initializers
    
    override init() {
        super.init()
        configureMPRemoteCommands()
        subscribeToNotifications()
    }
    
    // MARK: Overridden Functions
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        if let _item = playerItem {
            removeObservers(fromPlayerItem: _item)
        }
        self.player.removeObserver(self, forKeyPath: #keyPath(AVPlayer.rate))
        self.player.removeObserver(self, forKeyPath: #keyPath(AVPlayer.currentItem))
    }
    
}

// MARK: - Public Functions
extension SoundPlayer {
    
    func play(sound: Sound, completion: SoundCompletion?) throws {
        // Stop previous playback and remove completion closure
        stopPlayback()
        self.completion = nil
        // Reset status to .none
        status = .none
        // Capture completion block for further updates
        self.completion = completion
        // Configure audio session
        configureAudioSession()
        // Load new player item and play
        let item = AVPlayerItem(url: sound.fileUrl)
        player.replaceCurrentItem(with: item)
        player.play()
    }
    
    func stop() {
        stopPlayback()
    }
    
}

// MARK: - KVO
extension SoundPlayer {
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(AVPlayer.rate) {
            guard let rate = change?[.newKey] as? Float else {
                return
            }
            if rate > 0 {
                status = .playing
            } else {
                status = .stopped
            }
        } else if keyPath == #keyPath(AVPlayer.currentItem) {
            if let oldItem = change?[.oldKey] as? AVPlayerItem {
                removeObservers(fromPlayerItem: oldItem)
            }
            if let newItem = change?[.newKey] as? AVPlayerItem {
                addObservers(toPlayerItem: newItem)
            }
        } else if keyPath == #keyPath(AVPlayerItem.status) {
            guard let newValue = change?[.newKey] as? NSNumber else {
                return
            }
            let status = AVPlayerItem.Status(rawValue: newValue.intValue)!
            if status == .failed {
                stopPlayback()
            }
        }
    }
    
}

// MARK: - Private Functions
extension SoundPlayer {
    
    // MARK: Helpers
    
    fileprivate func configureAudioSession() {
        do {
            let session = AVAudioSession.sharedInstance()
            if session.category != .playback && session.category != .playAndRecord {
                try session.setCategory(.playAndRecord)
            }
            try session.setActive(true)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    fileprivate func configureMPRemoteCommands() {
        let center = MPRemoteCommandCenter.shared()
        center.playCommand.isEnabled = false
        center.pauseCommand.isEnabled = false
        center.stopCommand.isEnabled = false
        center.togglePlayPauseCommand.isEnabled = false
        center.enableLanguageOptionCommand.isEnabled = false
        center.disableLanguageOptionCommand.isEnabled = false
        center.changePlaybackRateCommand.isEnabled = false
        center.changeRepeatModeCommand.isEnabled = false
        center.changeShuffleModeCommand.isEnabled = false
        center.nextTrackCommand.isEnabled = false
        center.previousTrackCommand.isEnabled = false
        center.skipForwardCommand.isEnabled = false
        center.skipForwardCommand.isEnabled = false
        center.skipBackwardCommand.isEnabled = false
        center.seekForwardCommand.isEnabled = false
        center.seekBackwardCommand.isEnabled = false
        center.changePlaybackPositionCommand.isEnabled = false
        center.ratingCommand.isEnabled = false
        center.likeCommand.isEnabled = false
        center.dislikeCommand.isEnabled = false
        center.bookmarkCommand.isEnabled = false
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: AVAudioSession.interruptionNotification,
                                               object: nil, queue: nil) { [unowned self] notification in
            let typeKey = notification.userInfo?[AVAudioSessionInterruptionTypeKey] as? AVAudioSession.InterruptionType
            guard let type = typeKey else {
                return
            }
            switch type {
            case .began:
                self.stopPlayback()
            default:
                break
            }
        }
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime,
                                               object: nil, queue: nil, using: { [unowned self] notification in
            self.stopPlayback()
        })
    }
    
    fileprivate func addObservers(toPlayerItem item: AVPlayerItem) {
        item.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.new, .old], context: nil)
    }
    
    fileprivate func removeObservers(fromPlayerItem item: AVPlayerItem) {
        item.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
    }
    
    fileprivate func stopPlayback() {
        status = .stopped
        player.pause()
        player.replaceCurrentItem(with: nil)
    }
    
    // MARK: Make
    
    fileprivate func makePlayer() -> AVPlayer {
        let player = AVPlayer()
        player.actionAtItemEnd = .pause
        player.addObserver(self, forKeyPath: #keyPath(AVPlayer.rate), options: [.new], context: nil)
        player.addObserver(self, forKeyPath: #keyPath(AVPlayer.currentItem), options: [.new, .old], context: nil)
        return player
    }
    
}
