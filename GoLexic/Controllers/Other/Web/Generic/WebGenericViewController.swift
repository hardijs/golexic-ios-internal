//
//  WebGenericViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import WebKit
import Cubemobile

class WebGenericViewController: UIViewController {
    
    // MAKR: Properties
    
    // IB
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var containerView: UIView!
    fileprivate var webViewController: WebViewController!
    
    // Variables
    var urlString: String?
    
    // Falgs
    fileprivate var isPresentingAlert: Bool = false

    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let webVC = segue.destination as? WebViewController {
            if let _urlString = urlString {
                webVC.load(urlString: _urlString)
            }
            webVC.delegate = self
            webViewController = webVC
        }
    }

}

// MARK: - Targets
extension WebGenericViewController {
    
    @IBAction fileprivate func closeBarButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }
    
}

// MARK: - WebViewControllerDelegate
extension WebGenericViewController: WebViewControllerDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIView.animate(withDuration: 0.2, animations: {
            self.containerView.alpha = 1
        }) { _ in
            self.activityIndicator.stopAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        guard isPresentingAlert == false else {
            return
        }
        isPresentingAlert = true
        let message = Localization.string(forKeyPath: "Globals.alerts.connectionError")!
        let cancel = Localization.string(forKeyPath: "Globals.cancel")!
        let retry = Localization.string(forKeyPath: "Globals.retry")!
        Globals.presentAlert(withTitle: "", message: message, cancel: cancel, actions: [retry]) { [weak self] index in
            guard let _self = self else {
                return
            }
            _self.isPresentingAlert = false
            if index == 0 {
                _self.dismiss(animated: true)
            } else {
                webView.reload()
            }
        }
    }
    
}

// MARK: - UIViewControllerTransitioningDelegate
extension WebGenericViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .backward
        return transition
    }
    
}
