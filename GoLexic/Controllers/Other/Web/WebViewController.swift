//
//  WebViewController.swift
//
//  Created by Armands Lazdiņš.
//  Copyright © 2017 Cube Mobile. All rights reserved.
//

import UIKit
import WebKit
import Cubemobile
import Reachability

class WebViewController: UIViewController {
    
    // MARK: Enums
    enum WebMessages: String, CaseIterable {
        case subscribeToNotifications = "subscribeToNotifications"
        case didSetReminder = "didSetReminder"
    }
    
    // MARK: Properties
    
    // Static
    static var processPool: WKProcessPool = WKProcessPool()
    static var customUserAgent: String?
    
    // UI
    fileprivate var webView: WKWebView?
    
    // Variables
    weak var delegate: WebViewControllerDelegate?
    fileprivate var initialRequest: URLRequest?
    fileprivate var pendingRequest: URLRequest?
    fileprivate let reachability = try! Reachability(hostname: "google.com")
    
    //Flags
    fileprivate var isScriptMessageHandlerAdded: Bool = false
    fileprivate var isWebViewConfigured: Bool = false
    fileprivate var isWebViewFailed: Bool = false
    fileprivate var isReachable: Bool = false
    
    //MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureWebView()
        subscribeToNotifications()
        if let request = pendingRequest {
            load(request: request)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        guard let _webView = webView else {
            return
        }
        if isScriptMessageHandlerAdded == false {
            addScriptMessageHandlers(toWebView: _webView)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        guard let _webView = webView else {
            return
        }
        WebMessages.allCases.forEach { message in
            let contentController = _webView.configuration.userContentController
            contentController.removeScriptMessageHandler(forName: message.rawValue)
        }
        isScriptMessageHandlerAdded = false
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let _delegate = delegate else {
            return
        }
        if keyPath == "canGoBack" || keyPath == "canGoForward" {
            let webView = object as! WKWebView
            _delegate.webViewController(self, canGoBack: webView.canGoBack, canGoForward: webView.canGoForward)
        } else if keyPath == "estimatedProgress" {
            let webView = object as! WKWebView
            _delegate.webView(webView, didUpdate: webView.estimatedProgress)
        }
    }
    
    deinit {
        guard let _webView = webView else {
            return
        }
        _webView.removeObserver(self, forKeyPath: "canGoBack")
        _webView.removeObserver(self, forKeyPath: "canGoForward")
        _webView.removeObserver(self, forKeyPath: "estimatedProgress")
    }
    
}

// MARK: - Public Functions
extension WebViewController {
    
    func load(urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        let request = URLRequest(url: url)
        load(request: request)
    }
    
    func load(request: URLRequest) {
        if isWebViewConfigured == false {
            if isViewLoaded == false {
                pendingRequest = request
                return
            } else {
                configureWebView()
            }
        }
        pendingRequest = nil
        if initialRequest == nil {
            initialRequest = request
        }
        guard let _webView = webView else {
            return
        }
        if let _customUserAgent = WebViewController.customUserAgent {
            _webView.customUserAgent = _customUserAgent
        }
        _webView.load(request)
    }
    
    func reloadOrLoadInitialRequest() {
        guard let _initialRequest = initialRequest else {
            return
        }
        if let _webView = webView, _webView.url != nil {
            _webView.reload()
        } else {
            load(request: _initialRequest)
        }
    }
    
    func goBack() {
        guard let _webView = webView else {
            return
        }
        _webView.goBack()
    }
    
    func goForward() {
        guard let _webView = webView else {
            return
        }
        _webView.goForward()
    }
    
    func evaluateJavaScript(_ javaScriptString: String, completionHandler: ((Any?, Error?) -> Swift.Void)? = nil) {
        guard let _webView = webView else {
            return
        }
        _webView.evaluateJavaScript(javaScriptString, completionHandler: { (data, error) in
            if let _completionHandler = completionHandler {
                _completionHandler(data, error)
            }
        })
    }
    
    func cancelLoading() {
        guard let _webView = webView else {
            return
        }
        _webView.stopLoading()
    }
    
}

// MARK: - WebViewControllerDelegate
protocol WebViewControllerDelegate: class {
    
    // WKWebView delegates
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge,
                 completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void)
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error)
    func webView(_ webView: WKWebView, didUpdate progress: Double)
    
    // WebViewController delegates
    func webViewController(_ webViewController: WebViewController,
                           didReceive message: WebViewController.WebMessages, payload: NSDictionary?)
    func webViewController(_ webViewController: WebViewController, canGoBack: Bool, canGoForward: Bool)
}

extension WebViewControllerDelegate {
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge,
                 completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(.performDefaultHandling, nil)
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {}
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {}
    func webView(_ webView: WKWebView, didUpdate progress: Double) {}
    
    //WebViewController delegates
    func webViewController(_ webViewController: WebViewController,
                           didReceive message: WebViewController.WebMessages, payload: NSDictionary?) {}
    func webViewController(_ webViewController: WebViewController, canGoBack: Bool, canGoForward: Bool) {}
}

// MARK: - WKUIDelegate Functions
extension WebViewController: WKUIDelegate {
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration,
                 for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame?.isMainFrame == false,
            let url = navigationAction.request.url,
            UIApplication.shared.canOpenURL(url) == true {
                UIApplication.shared.open(url)
        }
        return nil
    }
    
}

// MARK: - WKNavigationDelegate Functions
extension WebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge,
                 completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let delegate = delegate {
            delegate.webView(webView, didReceive: challenge, completionHandler: completionHandler)
        } else {
            completionHandler(.performDefaultHandling, nil)
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let _url = navigationAction.request.url, let _scheme = _url.scheme,
            _scheme.lowercased() == "mailto", UIApplication.shared.canOpenURL(_url) {
            UIApplication.shared.open(_url)
            decisionHandler(.cancel)
            return
        }
        if let delegate = delegate {
            delegate.webView(webView, decidePolicyFor: navigationAction, decisionHandler: decisionHandler)
        } else {
            decisionHandler(.allow)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        isWebViewFailed = false
        if let delegate = delegate {
            delegate.webView(webView, didFinish: navigation)
        }
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        isWebViewFailed = true
        if let delegate = delegate {
            delegate.webView(webView, didFail: navigation, withError: error)
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        isWebViewFailed = true
        if let delegate = delegate {
            delegate.webView(webView, didFail: navigation, withError: error)
        }
    }
}

// MARK: - WKScriptMessageHandler
extension WebViewController: WKScriptMessageHandler {
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let webMessage = WebMessages(rawValue: message.name) else {
            return
        }
        let payload = message.body as? NSDictionary
        if let delegate = delegate {
            delegate.webViewController(self, didReceive: webMessage, payload: payload)
        }
    }
    
}

// MARK: - Private Functions
extension WebViewController {
    
    // MARK: Helpers
    
    fileprivate func configureWebView() {
        if isWebViewConfigured == true {
            return
        }
        isWebViewConfigured = true
        //Create webView configuration with shared processPool
        let configuration = WKWebViewConfiguration()
        configuration.processPool = WebViewController.processPool
        configuration.allowsInlineMediaPlayback = true
        configuration.applicationNameForUserAgent = Globals.userAgent
        //Create webView and add observers
        let _webView: WKWebView = {
            let webView = WKWebView(frame: self.view.bounds, configuration: configuration)
            webView.uiDelegate = self
            webView.navigationDelegate = self
            webView.translatesAutoresizingMaskIntoConstraints = false
            return webView
        }()
        _webView.addObserver(self, forKeyPath: "canGoBack", options: .new, context: nil)
        _webView.addObserver(self, forKeyPath: "canGoForward", options: .new, context: nil)
        _webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        view.insertSubview(_webView, at: 0)
        //Add webView anchors to view
        _webView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        _webView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        _webView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        _webView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        webView = _webView
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: Notification.Name.reachabilityChanged,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self else {
                return
            }
            let reachability = notification.object as! Reachability
            if reachability.connection != .unavailable, _self.isReachable == false, _self.isWebViewFailed {
                _self.reloadOrLoadInitialRequest()
            }
            _self.isReachable = reachability.connection != .unavailable
        }
        try? reachability.startNotifier()
    }
    
    fileprivate func addScriptMessageHandlers(toWebView webView: WKWebView) {
        isScriptMessageHandlerAdded = true
        WebMessages.allCases.forEach { message in
            let contentController = webView.configuration.userContentController
            contentController.add(self, name: message.rawValue)
        }
    }
    
}
