//
//  HomeViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import SideMenu
import RealmSwift
import Reachability

class HomeViewController: UIViewController {

    // MAKR: Properties
    
    // IB
    @IBOutlet fileprivate var arcView: ArcView!
    @IBOutlet fileprivate var exercisesLabel: UILabel!
    @IBOutlet fileprivate var overlayView: UIView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var startButton: AttributedButton!
    @IBOutlet fileprivate var statusLabel: UILabel!
    fileprivate weak var introViewController: UIViewController?
    fileprivate weak var subscriptionViewController: UIViewController?
    
    // Variables
    fileprivate var viewModel = HomeViewModel()
    fileprivate var phasesToken: NotificationToken?
    fileprivate let reachability = try! Reachability(hostname: "google.com")
    var backgroundStartedAt: TimeInterval = 0
    
    // Flags
    fileprivate var isPhaseUpdated: Bool = false
    fileprivate var isPendingAssetUpdate: Bool = true
    fileprivate var isPendingContentUpdate: Bool = false
    fileprivate var isUpdating: Bool = false {
        didSet {
            if isUpdating {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
            overlayView.isHidden = !isUpdating
            navigationItem.leftBarButtonItem?.isEnabled = !isUpdating
        }
    }
    fileprivate var isPresentingReachability: Bool = false
    
    // MARK: Initializers
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        SpeechRecognition.configure()
        ContentProvider.shared.cleanup()
    }
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotificatios()
        updateContent(includingControls: false)
        if User.current == nil {
            let launchScreen = presentLaunchScreen()
            introViewController = show(OnboardingViewController.self, useNavigationController: true, animated: false, completion: {
                DispatchQueue.main.async {
                    launchScreen.removeFromSuperview()
                }
            })
        } else {
            if User.current!.isBetaUser == false, PurchaseProvider.shared.hasActiveSubscription == false {
                let launchScreen = presentLaunchScreen()
                subscriptionViewController = show(SubscriptionViewController.self, useNavigationController: true, animated: false, completion: {
                    DispatchQueue.main.async {
                        launchScreen.removeFromSuperview()
                    }
                })
            }
            updateRemoteContent()
        }
    }
    
}

// MARK: - Targets
extension HomeViewController {
    
    @IBAction fileprivate func menuBarButtonPressed(_ sender: UIBarButtonItem) {
        let sideMenuViewController = makeSideMenuNavigationController()
        present(sideMenuViewController, animated: true, completion: nil)
    }
    
    @IBAction fileprivate func startButtonPressed(_ sender: UIButton) {
        guard let _phase = viewModel.phase else {
            return
        }
        let phaseVC = show(PhaseViewController.self)
        phaseVC.phase = _phase
    }
    
}

// MARK: - MenuViewControllerDelegate
extension HomeViewController: MenuViewControllerDelegate {
    
    func menuViewController(_ viewController: MenuViewController, didSelect item: MenuItem) {
        viewController.dismiss(animated: true)
        switch item.identifier {
        case .console:
            navigationController!.push(ConsoleViewController.self, animated: false)
        }
    }
    
    func menuViewController(_ viewController: MenuViewController, didSelect footer: MenuFooter) {
        viewController.dismiss(animated: true)
        switch footer.identifier {
        case .tips:
            navigationController!.push(TipsViewController.self, animated: false)
        case .support:
            guard let url = makeSupportURL(),
                UIApplication.shared.canOpenURL(url) else {
                return
            }
            UIApplication.shared.open(url)
        default:
            break
        }
    }
    
}

// MARK: - Private Functions
extension HomeViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        arcView.direction = .bottom
    }
    
    fileprivate func subscribeToNotificatios() {
        phasesToken = viewModel.phases.observe({ [unowned self] change in
            if case .update = change {
                self.isPhaseUpdated = true
                if self.viewModel.phase == nil {
                    // Activate next phase if possible
                    if self.viewModel.activateNextPhaseIfPossible() == false {
                        self.isUpdating = false
                    }
                } else if self.isPendingAssetUpdate, let _phase = self.viewModel.phase,
                    TargetEngine.pendingContainsAny(ContentProvider.shared.contentEndpoints) == false {
                    // Update assets once everything is available
                    self.updateAssets(forPhase: _phase)
                }
                self.viewModel.completeCurrentPhaseIfPossible()
                self.updateContent(includingControls: true)
            }
        })
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                               object: nil, queue: nil) { [unowned self] _ in
            guard self.shouldUpdateRemoteContent() else {
                return
            }
            self.backgroundStartedAt = 0
            self.updateRemoteContent()
            if self.isTopViewControllerDismissable() {
                self.dismissAllAnimated()
            }
        }
        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification,
                                               object: nil, queue: nil) { [unowned self] _ in
            self.backgroundStartedAt = Date().timeIntervalSince1970
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.User.DidCreate,
                                               object: nil, queue: nil) { [unowned self] _ in
            self.isUpdating = true
            SpeechRepository.shared.clearDecodingGraphs()
            let endpoints = ContentProvider.shared.contentEndpoints
            ContentProvider.shared.update(endpoints, disposition: .override)
            NotificationProvider.shared.syncrhonize()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.User.DidUpdate,
                                               object: nil, queue: nil) { [unowned self] notification in
            guard let key = notification.object as? User.Key else {
                return
            }
            if key == .profile || key == .profileReminderTime {
                self.viewModel.rescheduleReminders()
            }
            NotificationProvider.shared.updateNotificationTopics()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.User.DidDelete,
                                               object: nil, queue: nil) { [unowned self] _ in
            guard self.introViewController == nil else {
                return
            }
            TargetEngine.removePendingItems()
            self.presentLoginViewController()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.TargetEngine.DidEndItem,
                                               object: nil, queue: nil) { [unowned self] notification in
            guard let item = notification.object as? TargetEngineStoreItem,
                let target = item.target as? GoLexicTarget else {
                return
            }
            if self.isPendingContentUpdate {
                guard TargetEngine.pendingItems(inQueue: .sync) == nil else {
                    return
                }
                self.isPendingContentUpdate = false
                SpeechRepository.shared.clearDecodingGraphs()
                let endpoints = ContentProvider.shared.contentEndpoints + [.user]
                ContentProvider.shared.update(endpoints, disposition: .override)
            } else if self.isPendingAssetUpdate {
                guard ContentProvider.shared.contentEndpoints.contains(target.endpoint),
                    TargetEngine.pendingContainsAny(ContentProvider.shared.contentEndpoints) == false,
                    let _phase = self.viewModel.phase, self.isPhaseUpdated else {
                    return
                }
                // Update assets once everything is available
                self.updateAssets(forPhase: _phase)
            }
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.NotificationProvider.didChangeAuthorizationStatus,
                                               object: nil, queue: nil) { [unowned self] _ in
            self.viewModel.rescheduleReminders()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name.PurchaseProvider.DidUpdateSubscriptions,
                                               object: nil, queue: nil) { [unowned self] notification in
            guard let _user = User.current, _user.isBetaUser == false else {
                return
            }
            guard self.introViewController == nil, self.subscriptionViewController == nil,
                let hasSubscription = notification.object as? Bool, hasSubscription == false else {
                return
            }
            self.presentSubscriptionViewController()
        }
        NotificationCenter.default.addObserver(forName: Notification.Name.reachabilityChanged,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self else {
                return
            }
            let reachability = notification.object as! Reachability
            if reachability.connection == .unavailable, _self.isPresentingReachability == false {
                _self.isPresentingReachability = true;
                let message = Localization.string(forKeyPath: "Globals.alerts.connectionError")!
                let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                Globals.presentAlert(withTitle: "", message: message, cancel: confirm) { index in
                    _self.isPresentingReachability = false;
                }
            }
        }
        try? reachability.startNotifier()
    }
    
    fileprivate func shouldUpdateRemoteContent() -> Bool {
        guard User.current != nil, backgroundStartedAt > 0 else {
            return false // Not logged in user or app hasn't been minimized yet
        }
        guard let _phase = viewModel.phase, _phase.hasIncompleteExercises() else {
            return true // No phase or all exercises completed, always update
        }
        let currentTime = Date().timeIntervalSince1970
        if currentTime-backgroundStartedAt > 600 {
            return true // App has been minimized for more than 10 minutes, update
        }
        return false
    }
    
    fileprivate func isTopViewControllerDismissable() -> Bool {
        guard let topVC = makeTopPresentedViewController(fromViewController: self) else {
            return false
        }
        let targetVC: UIViewController = {
            if let topNC = topVC as? UINavigationController {
                return topNC.viewControllers.first!
            }
            return topVC
        }()
        if targetVC is LoginViewController ||
            targetVC is SubscriptionViewController {
            return false
        }
        return true
    }
    
    // MARK: Update
    
    fileprivate func updateContent(includingControls: Bool) {
        exercisesLabel.text = makeExercisesText(forCount: viewModel.phase?.exercises.count)
        guard includingControls else {
            return
        }
        if let _phase = viewModel.phase, _phase.exercises.isEmpty == false {
            startButton.isHidden = false
            statusLabel.isHidden = true
        } else {
            if viewModel.phases.isEmpty, viewModel.isNextPhaseActivatable() {
                statusLabel.text = Localization.string(forKeyPath: "Home.comingSoon")
            } else {
                statusLabel.text = Localization.string(forKeyPath: "Home.todayDone")
            }
            startButton.isHidden = true
            statusLabel.isHidden = false
        }
    }
    
    fileprivate func updateRemoteContent() {
        guard isUpdating == false else {
            return
        }
        isUpdating = true
        isPhaseUpdated = false
        isPendingAssetUpdate = true
        if TargetEngine.pendingItems(inQueue: .sync) != nil {
            isPendingContentUpdate = true
            TargetEngine.executePendingItems()
        } else {
            isPendingContentUpdate = false
            SpeechRepository.shared.clearDecodingGraphs()
            let endpoints = ContentProvider.shared.contentEndpoints + [.user]
            ContentProvider.shared.update(endpoints, disposition: .override)
        }
    }
    
    fileprivate func updateAssets(forPhase phase: Phase) {
        guard isPendingAssetUpdate else {
            return
        }
        isPendingAssetUpdate = false
        FileProvider.shared.updateFilesForPhase(withIdentifier: phase.identifier) { success in
            if success {
                self.isUpdating = false
            } else {
                let message = Localization.string(forKeyPath: "Globals.alerts.connectionError")!
                let cancel = Localization.string(forKeyPath: "Globals.cancel")!
                let retry = Localization.string(forKeyPath: "Globals.retry")!
                Globals.presentAlert(withTitle: "", message: message, cancel: cancel, actions: [retry]) { index in
                    guard index == 1 else {
                        return
                    }
                    self.updateAssets(forPhase: phase)
                }
            }
        }
    }
    
    // MARK: Presentation
    
    fileprivate func dismissAllAnimated() {
        guard let _presentedVC = presentedViewController else {
            return
        }
        if let _topVC = makeTopPresentedViewController(fromViewController: self),
            let _snapshotView = _topVC.view.snapshotView(afterScreenUpdates: false) {
            _presentedVC.view.addSubview(_snapshotView)
        }
        dismiss(animated: true)
    }
    
    fileprivate func presentLaunchScreen() -> UIView {
		let storyboard = UIStoryboard(name: Globals.isPhone ? "LaunchScreeniPhone" : "LaunchScreeniPad", bundle: nil)
        let launchScreen = storyboard.instantiateInitialViewController()!
        launchScreen.view.frame = view.frame
        launchScreen.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        navigationController!.view.addSubview(launchScreen.view)
        return launchScreen.view
    }
    
    fileprivate func presentLoginViewController() {
        var snapshotView: UIView?
        if let _topVC = makeTopPresentedViewController(fromViewController: self),
            let _snapshotView = _topVC.view.snapshotView(afterScreenUpdates: false) {
            navigationController!.view.addSubview(_snapshotView)
            snapshotView = _snapshotView
            dismiss(animated: false)
        }
        introViewController = show(LoginViewController.self, useNavigationController: true, completion: {
            if let _snapshotView = snapshotView {
                _snapshotView.removeFromSuperview()
            }
        })
    }
    
    fileprivate func presentSubscriptionViewController() {
        var snapshotView: UIView?
        if let _topVC = makeTopPresentedViewController(fromViewController: self),
            let _snapshotView = _topVC.view.snapshotView(afterScreenUpdates: false) {
            navigationController!.view.addSubview(_snapshotView)
            snapshotView = _snapshotView
            dismiss(animated: false)
        }
        subscriptionViewController = show(SubscriptionViewController.self, useNavigationController: true, completion: {
            if let _snapshotView = snapshotView {
                _snapshotView.removeFromSuperview()
            }
        })
    }
    
    // MARK: Make
    
    fileprivate func makeSideMenuNavigationController() -> SideMenuNavigationController {
        let menuViewController = instantiate(MenuViewController.self)
        menuViewController.delegate = self
        let viewController = SideMenuNavigationController(rootViewController: menuViewController)
        viewController.allowPushOfSameClassTwice = false
        viewController.menuWidth = 256
        viewController.presentationStyle = .menuSlideIn
        viewController.leftSide = true
        return viewController
    }
    
    fileprivate func makeStreakText(forCount count: Int?) -> String {
        let _count = count ?? 0
        let text = Localization.string(forKeyPath: "Home.streaks")!
        return "\(_count) \(text)"
    }
    
    fileprivate func makeExercisesText(forCount count: Int?) -> String {
        let _count = count ?? 0
        if count == 1 {
            let text = Localization.string(forKeyPath: "Home.trainings.one")!
            return "\(_count) \(text)"
        } else {
            let text = Localization.string(forKeyPath: "Home.trainings.many")!
            return "\(_count) \(text)"
        }
    }
    
    fileprivate func makeTopPresentedViewController(fromViewController viewController: UIViewController) -> UIViewController? {
        if let _presentedVC = viewController.presentedViewController {
            if let _topVC = makeTopPresentedViewController(fromViewController: _presentedVC) {
                return _topVC
            }
            return _presentedVC
        }
        return nil
    }
    
    fileprivate func makeSupportURL() -> URL? {
        guard let email = Localization.string(forKeyPath: "Globals.emails.support") else {
            return nil
        }
        return URL(string: "mailto:\(email)")
    }
    
}
