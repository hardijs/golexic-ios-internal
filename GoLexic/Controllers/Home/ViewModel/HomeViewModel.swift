//
//  HomeViewModel.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 16/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift
import Cubemobile
import SwiftyUserDefaults

class HomeViewModel: NSObject {
    
    // MARK: Properties
    
    fileprivate(set) lazy var phases: Results<Phase> = {
        let realm = try! Realm()
        var results = realm.objects(Phase.self)
        return results.filter("isArchived == false")
    }()
    var phase: Phase? {
        return makeActivatedPhase()
    }
    
}

// MARK: - Public Functions
extension HomeViewModel {
    
    // MARK: Helpers
    
    @discardableResult
    func activateNextPhaseIfPossible() -> Bool {
        guard phase == nil, isNextPhaseActivatable() else {
            return false
        }
        let newPhases = phases.filter("isCompleted == false")
        guard let _phase = newPhases.first else {
            return false
        }
        activate(_phase)
        return true
    }
    
    func isNextPhaseActivatable() -> Bool {
        if let _userProfile = User.current?.profile, _userProfile.isTester {
            return true
        }
        guard let phaseCompletedDate = makePhaseCompletedDate() else {
            return true
        }
        if Calendar.current.isDateInToday(phaseCompletedDate) {
            return false
        }
        let date = Date()
        let componets = Calendar.current.dateComponents([.hour], from: date)
        guard let _hour = componets.hour else {
            return false
        }
        return _hour >= 8
    }

    func completeCurrentPhaseIfPossible() {
        guard let _phase = phase, _phase.isCompleted == false,
            _phase.hasIncompleteExercises() == false else {
            return
        }
        let currentTime = Date().timeIntervalSince1970
        Defaults[\.lastPhaseCompletedAt] = currentTime
        completePhase(_phase)
    }
    
    func rescheduleReminders() {
        NotificationProvider.shared.removeAllPendingLocalNotifications()
        guard NotificationProvider.shared.status == .authorized,
            let reminderTime = User.current?.profile?.reminderTime,
            let dateComponents = makeReminderDateComponents(fromReminderTime: reminderTime) else {
            return
        }
        let title = Localization.string(forKeyPath: "Globals.reminder.title")!
        let message = Localization.string(forKeyPath: "Globals.reminder.message")!
        NotificationProvider.shared.scheduleLocalNotification(withTitle: title, message: message,
                                                              dateComponents: dateComponents, repeats: true)
    }
    
}

// MARK: - Private Functions
extension HomeViewModel {
    
    // MARK: Helpers
    
    fileprivate func activate(_ phase: Phase) {
        let realm = try! Realm()
        try! realm.write {
            phase.isActivated = true
        }
    }
    
    fileprivate func completePhase(_ phase: Phase) {
        let realm = try! Realm()
        try! realm.write {
            phase.isActivated = false
            phase.isCompleted = true
        }
    }
    
    // MARK: Make
    
    fileprivate func makeActivatedPhase() -> Phase? {
        let results = phases.filter("isArchived == false")
        return results.filter("isActivated == true AND isCompleted == false").first
    }
    
    fileprivate func makeReminderDateComponents(fromReminderTime reminderTime: String) -> DateComponents? {
        let reminderComponents = reminderTime.components(separatedBy: ":")
        guard reminderComponents.count == 2,
            let reminderHour = Int(reminderComponents[0]),
            let reminderMinute = Int(reminderComponents[1]) else {
            return nil
        }
        return DateComponents(hour: reminderHour, minute: reminderMinute)
    }
    
    fileprivate func makePhaseCompletedDate() -> Date? {
        let phaseCompletedAt = Defaults[\.lastPhaseCompletedAt]
        guard phaseCompletedAt > 0 else {
            return nil
        }
        return Date(timeIntervalSince1970: phaseCompletedAt)
    }
    
}
