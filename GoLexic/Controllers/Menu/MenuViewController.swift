//
//  MenuViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

protocol MenuViewControllerDelegate: class {
    
    func menuViewController(_ viewController: MenuViewController, didSelect item: MenuItem)
    func menuViewController(_ viewController: MenuViewController, didSelect footer: MenuFooter)
    
}

class MenuViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var itemTableView: UITableView!
    @IBOutlet fileprivate var footerTableView: UITableView!
    
    // Variables
    weak var delegate: MenuViewControllerDelegate?
    fileprivate lazy var items: [MenuItem] = {
       return makeMenuItems()
    }()
    fileprivate lazy var footers: [MenuFooter] = {
       return makeMenuFooters()
    }()
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
    }
    
    
}

// MARK: - Targets
extension MenuViewController {
    
    @IBAction fileprivate func backButtonPressed(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
}

// MARK: - UITableViewDataSource
extension MenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let _tableView = TableView(rawValue: tableView.tag)!
        switch _tableView {
        case .item:
            return items.count
        case .footer:
            return footers.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let _tableView = TableView(rawValue: tableView.tag)!
        var cell: UITableViewCell!
        switch _tableView {
        case .item:
            let itemCell = tableView.dequeueReusableCell(withIdentifier: MenuItemTableViewCell.identifier,
                                                         for: indexPath) as! MenuItemTableViewCell
            let item = items[indexPath.row]
            itemCell.update(withItem: item)
            cell = itemCell
        case .footer:
            let footerCell = tableView.dequeueReusableCell(withIdentifier: MenuFooterTableViewCell.identifier,
                                                           for: indexPath) as! MenuFooterTableViewCell
            let footer = footers[indexPath.row]
            if footer.identifier == .version {
                let title = makeVersionText(withKey: footer.titleKey)
                footerCell.update(withFooter: footer, title: title)
            } else {
                footerCell.update(withFooter: footer)
            }
            cell = footerCell
        }
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension MenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        let _tableView = TableView(rawValue: tableView.tag)!
        guard _tableView == .footer else {
            return true
        }
        let footer = footers[indexPath.row]
        guard footer.identifier == .version else {
            return true
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let _tableView = TableView(rawValue: tableView.tag)!
        guard _tableView == .footer else {
            return indexPath
        }
        let footer = footers[indexPath.row]
        guard footer.identifier == .version else {
            return indexPath
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let _delegate = delegate else {
            return
        }
        let _tableView = TableView(rawValue: tableView.tag)!
        switch _tableView {
        case .item:
            let item = items[indexPath.row]
            _delegate.menuViewController(self, didSelect: item)
        case .footer:
            let footer = footers[indexPath.row]
            _delegate.menuViewController(self, didSelect: footer)
        }
    }
    
}

// MARK: - Private Functions
extension MenuViewController {
    
    // MARK: Enums
    
    fileprivate enum TableView: Int {
        case item
        case footer
    }
    
    fileprivate func configureController() {
        itemTableView.contentInset = UIEdgeInsets(top: 18, left: 0, bottom: 0, right: 0)
        itemTableView.register(UINib(withType: MenuItemTableViewCell.self),
                               forCellReuseIdentifier: MenuItemTableViewCell.identifier)
        footerTableView.register(UINib(withType: MenuFooterTableViewCell.self),
                                 forCellReuseIdentifier: MenuFooterTableViewCell.identifier)
    }
    
    // MARK: Make
    
    fileprivate func makeVersionText(withKey key: String) -> String {
        let version = AppInfo.appVersion()
        let text = Localization.string(forKeyPath: key)!
        return "\(text) \(version)"
    }
    
    fileprivate func makeMenuItems() -> [MenuItem] {
        var items: [MenuItem] = []
        guard let resourcePath = Bundle.main.path(forResource: "Menu", ofType: "plist") else {
            return items
        }
        guard let data = NSDictionary(contentsOfFile: resourcePath),
            let _itemData = data["items"] as? NSArray else {
            return items
        }
        for case let _data as [String: Any] in _itemData {
            let item = MenuItem(withData: _data)
            items.append(item)
        }
        return items
    }
    
    fileprivate func makeMenuFooters() -> [MenuFooter] {
        var items: [MenuFooter] = []
        guard let resourcePath = Bundle.main.path(forResource: "Menu", ofType: "plist") else {
            return items
        }
        guard let data = NSDictionary(contentsOfFile: resourcePath),
            let _footerData = data["footer"] as? NSArray else {
            return items
        }
        for case let _data as [String: Any] in _footerData {
            let item = MenuFooter(withData: _data)
            items.append(item)
        }
        return items
    }
    
}
