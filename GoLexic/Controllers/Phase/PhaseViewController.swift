//
//  PhaseViewController.swift
//  GoLexic
//
//  Created by Armands L. on 22/03/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import Firebase
import RealmSwift
import Mixpanel

class PhaseViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var navigationView: NavigationView!
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var completedContainerView: UIView!
    weak var completedViewController: PhaseCompletedViewController!
    
    // MARK: Constraints
    @IBOutlet fileprivate var collectionViewHeightConstraint: NSLayoutConstraint!
    
    // Variables
    var phase: Phase!
    fileprivate var phaseToken: NotificationToken?
    fileprivate let viewModel = PhaseViewModel()
    fileprivate lazy var exerciseIndex: Int? = {
       return phase.indexOfFirstAvailableExercise()
    }()
    fileprivate var pendingPhaseExercise: PhaseExercise? {
        didSet {
            navigationView.isBackEnabled = pendingPhaseExercise == nil
        }
    }
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
        updateContent()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let itemSize = makeItemSize()
        if collectionViewHeightConstraint.constant != itemSize.height {
            collectionViewHeightConstraint.constant = itemSize.height
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let _completedVC = segue.destination as? PhaseCompletedViewController {
            completedViewController = _completedVC
            completedViewController.delegate = self
        }
    }
    
}

// MARK: - Targets
extension PhaseViewController {
    
    @IBAction fileprivate func nextExerciseButtonPressed(_ sender: UIButton) {
        guard let _index = exerciseIndex else {
            return
        }
        let exercise = phase.exercises[_index]
        let realm = try! Realm()
        try! realm.write {
            exercise.markAsCompleted()
        }
    }
    
}

// MARK: - PhaseCompletedViewControllerDelegate
extension PhaseViewController: PhaseCompletedViewControllerDelegate {
    
    func phaseCompletedViewController(_ viewController: PhaseCompletedViewController,
                                      didSelect action: PhaseCompletedViewController.Action) {
        guard action == .tomorrow else {
            return
        }
        dismiss(animated: true)
    }
}

// MARK: - NavigationViewDelegate
extension PhaseViewController: NavigationViewDelegate {
    
    func navigationView(_ view: NavigationView, didSelect action: NavigationView.Action) {
        guard action == .back else {
            return
        }
        dismiss(animated: true)
    }
    
}

// MARK: - UICollectionViewDataSource
extension PhaseViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return phase.exercises.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExerciseCollectionViewCell.identifier,
                                                      for: indexPath) as! ExerciseCollectionViewCell
        let phaseExercise = phase.exercises[indexPath.row]
        if let _exerciseIndex = exerciseIndex {
            if let _pendingPhaseExercise = pendingPhaseExercise,
               _pendingPhaseExercise.identifier == phaseExercise.identifier {
                cell.update(withPhaseExercise: phaseExercise, status: .loading)
            } else if indexPath.row < _exerciseIndex {
                cell.update(withPhaseExercise: phaseExercise, status: .completed)
            } else if indexPath.row == _exerciseIndex, phaseExercise.isAvailable() {
                cell.update(withPhaseExercise: phaseExercise, status: .active)
            } else {
                cell.update(withPhaseExercise: phaseExercise, status: .inactive)
            }
        } else {
            cell.update(withPhaseExercise: phaseExercise, status: .completed)
        }
        return cell
    }
    
}

// MARK: - UICollectionViewDelegate
extension PhaseViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        guard pendingPhaseExercise == nil,
            let _exerciseIndex = exerciseIndex,
            _exerciseIndex == indexPath.row else {
            return false
        }
        let phaseExercise = phase.exercises[indexPath.row]
        if phaseExercise.isAvailable() == false {
            let message = Localization.string(forKeyPath: "Phase.alerts.exerciseUnavailable")
            let confirm = Localization.string(forKeyPath: "Globals.confirm")!
            Globals.presentAlert(withTitle: "", message: message, cancel: confirm)
            return false
        } else if phaseExercise.requiresMicAccess(),
            SpeechRecognition.isRecordPermissionGranted() == false {
            let message = Localization.string(forKeyPath: "Phase.alerts.micAccessDenied")
            let confirm = Localization.string(forKeyPath: "Globals.confirm")!
            let settings = Localization.string(forKeyPath: "Globals.settings")!
            Globals.presentAlert(withTitle: "", message: message, cancel: confirm, actions: [settings]) { [weak self] index in
                guard self != nil, index == 1 else {
                    return
                }
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url)
            }
            return false
        } else if phaseExercise.exerciseType.hasSpeechRecongnition(),
            SpeechRepository.shared.decodingGraphExists(forPhaseExercise: phaseExercise) == false {
            // Change cell state to loading
            pendingPhaseExercise = phaseExercise
            collectionView.reloadData()
            // Update decoding graph
            SpeechRepository.shared.updateDecodingGraph(forPhaseExercise: phaseExercise) { [weak self] success in
                guard let _self = self, let _pendingPhaseExericse = _self.pendingPhaseExercise else {
                    return
                }
                if success {
                    _self.selectPhaseExercise(_pendingPhaseExericse)
                } else {
                    let message = Localization.string(forKeyPath: "Phase.alerts.exerciseUnavailable")
                    let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                    Globals.presentAlert(withTitle: "", message: message, cancel: confirm)
                }
                _self.pendingPhaseExercise = nil
                _self.collectionView.reloadData()
            }
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        guard pendingPhaseExercise == nil,
            let _exerciseIndex = exerciseIndex,
            _exerciseIndex == indexPath.row else {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let phaseExercise = phase.exercises[indexPath.row]
        selectPhaseExercise(phaseExercise)
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PhaseViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return makeItemSize()
    }
    
}

// MARK: - ExerciseViewControllerDelegate
extension PhaseViewController: ExerciseViewControllerDelegate {
    
    func exerciseViewController(_ viewController: ExerciseViewController,
                                didSelect action: ExerciseViewController.Action) {
        if let presentedVC = viewController.presentedViewController,
            let firstSnapshot = presentedVC.view.snapshotView(afterScreenUpdates: false) {
            viewController.view.addSubview(firstSnapshot)
            if action == .home,
                let secondSnapshot = presentedVC.view.snapshotView(afterScreenUpdates: false) {
                view.addSubview(secondSnapshot)
            }
        }
        if action == .home {
            dismiss(animated: false)
        }
        dismiss(animated: true)
    }
    
}

// MARK: - UIViewControllerTransitioningDelegate
extension PhaseViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension PhaseViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        navigationView.delegate = self
        collectionView.register(UINib(withType: ExerciseCollectionViewCell.self),
                                forCellWithReuseIdentifier: ExerciseCollectionViewCell.identifier)
        if let _layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let inset = _layout.minimumLineSpacing
            collectionView.contentInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
        }
    }
    
    fileprivate func subscribeToNotifications() {
        phaseToken = phase.exercises.observe({ [weak self] change in
            guard let _self = self else {
                return
            }
            if case .update = change {
                _self.exerciseIndex = _self.phase.indexOfFirstAvailableExercise()
                _self.collectionView.reloadData()
                _self.updateContent()
            }
        })
    }
    
    fileprivate func updateContent() {
        completedContainerView.isHidden = exerciseIndex != nil
    }
    
    fileprivate func selectPhaseExercise(_ phaseExercise: PhaseExercise) {
        var exerciseVC: ExerciseViewController?
        switch phaseExercise.exerciseType {
        case .alphabetPractice:
            exerciseVC = show(ExerciseAlphabetPracticeViewController.self)
        case .lostLetters:
            exerciseVC = show(ExerciseLostLettersViewController.self)
        case .alphabetSorting:
            exerciseVC = show(ExerciseAlphabetSortingViewController.self)
        case .letterBoard:
            exerciseVC = show(ExerciseLetterBoardViewController.self)
        case .vowels:
            exerciseVC = show(ExerciseVowelsViewController.self)
        case .reading:
            exerciseVC = show(ExerciseReadingViewController.self)
        case .building:
            exerciseVC = show(ExerciseBuildingViewController.self)
        case .revise:
            exerciseVC = show(ExerciseReviseViewController.self)
        default:
            break
        }
        if let _exerciseVC = exerciseVC {
            _exerciseVC.phase = phase
            _exerciseVC.phaseExercise = phaseExercise
            _exerciseVC.delegate = self
        }
    }
    
    // MAKR: Make
    
    fileprivate func makeItemSize() -> CGSize {
        let itemsPerPage: CGFloat = 3
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let availableWidth: CGFloat = {
            let spaces = itemsPerPage-1
            let spaceWidth = layout.minimumLineSpacing*spaces
            let insets = collectionView.contentInset.left+collectionView.contentInset.right
            return collectionView.frame.width-spaceWidth-insets
        }()
        let ratio = ExerciseCollectionViewCell.ratio
        let elevation = ExerciseCollectionViewCell.elevation
        let itemWidth = floor(availableWidth/itemsPerPage)
        let itemHeight = floor(itemWidth/ratio)+elevation
        return CGSize(width: itemWidth, height: itemHeight)
    }
    
}
