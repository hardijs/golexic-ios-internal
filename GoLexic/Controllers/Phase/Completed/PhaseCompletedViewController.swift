//
//  PhaseCompletedViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 10/08/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

protocol PhaseCompletedViewControllerDelegate: class {
    
    func phaseCompletedViewController(_ viewController: PhaseCompletedViewController,
                                      didSelect action: PhaseCompletedViewController.Action)
    
}

class PhaseCompletedViewController: UIViewController {
    
    // MARK: Enums
    
    enum Action: Int {
        case tomorrow
    }
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var contentView: AttributedView!
    @IBOutlet fileprivate var badgeView: AttributedView!
    
    // Variables
    weak var delegate: PhaseCompletedViewControllerDelegate?
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
    }

}

// MARK: - Targets
extension PhaseCompletedViewController {
    
    @IBAction fileprivate func tomorrowButtonPressed(_ sender: UIButton) {
        if let _delegate = delegate {
            _delegate.phaseCompletedViewController(self, didSelect: .tomorrow)
        }
    }
    
}

// MARK: - Private Functions
extension PhaseCompletedViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        contentView.shadowOffset = CGSize(width: 0, height: 1)
        badgeView.shadowOffset = CGSize(width: 0, height: -2)
    }
    
}
