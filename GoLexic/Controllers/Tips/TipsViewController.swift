//
//  TipsViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 27/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import WebKit
import Cubemobile

class TipsViewController: UIViewController {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var arcView: ArcView!
    @IBOutlet fileprivate var contentView: AttributedView!
    @IBOutlet fileprivate var containerView: UIView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var leadingConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var trailingConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var bottomConstraint: NSLayoutConstraint!
    fileprivate var webViewController: WebViewController!
    
    // Constraints
    @IBOutlet fileprivate var contentViewTopConstraint: NSLayoutConstraint!
    
    // Falgs
    var canGoBack: Bool = true
    fileprivate var isPresentingAlert: Bool = false
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        loadWebView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let webVC = segue.destination as? WebViewController {
            webVC.delegate = self
            webViewController = webVC
        }
    }
    
}

// MARK: - Targets
extension TipsViewController {
    
    @IBAction fileprivate func backBarButtonPressed(_ sender: UIBarButtonItem) {
        navigationController!.popViewController(animated: true)
    }
    
    @IBAction fileprivate func skipButtonPressed(_ sender: UIButton) {
        if canGoBack {
            navigationController!.popViewController(animated: true)
        } else {
            dismiss(animated: true)
        }
    }
    
}

// MARK: - WebViewControllerDelegate
extension TipsViewController: WebViewControllerDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIView.animate(withDuration: 0.2, animations: {
            self.containerView.alpha = 1
        }) { _ in
            self.activityIndicator.stopAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        guard isPresentingAlert == false else {
            return
        }
        isPresentingAlert = true
        let message = Localization.string(forKeyPath: "Globals.alerts.connectionError")!
        let cancel = Localization.string(forKeyPath: "Globals.cancel")!
        let retry = Localization.string(forKeyPath: "Globals.retry")!
        Globals.presentAlert(withTitle: "", message: message, cancel: cancel, actions: [retry]) { [weak self] index in
            guard let _self = self else {
                return
            }
            _self.isPresentingAlert = false
            if index == 0 {
                _self.navigationController!.popViewController(animated: true)
            } else {
                webView.reload()
            }
        }
    }
    
}

// MARK: - Private Functions
extension TipsViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        if canGoBack == false {
            navigationItem.leftBarButtonItem = nil
        }
        if navigationController?.navigationBar.isHidden ?? true {
            contentViewTopConstraint.constant = 44
        }
        arcView.direction = .bottom
        contentView.shadowOffset = CGSize(width: 0, height: 4)
        containerView.layer.cornerRadius = 15
        containerView.layer.masksToBounds = true
        
        leadingConstraint.constant = Globals.isPhone ? 8.0 : 70.0
        trailingConstraint.constant = Globals.isPhone ? 8.0 : 70.0
        bottomConstraint.constant = Globals.isPhone ? 20.0 : 60.0

    }
    
    fileprivate func loadWebView() {
        if let _request = makeRequest() {
            webViewController.load(request: _request)
        }
    }
    
    // MARK: Make
    
    fileprivate func makeRequest() -> URLRequest? {
        guard let urlString = Localization.string(forKeyPath: "Tips.url"),
            let url = URL(string: urlString) else {
            return nil
        }
        var request = URLRequest(url: url)
        if let _token = User.current?.token {
            let tokenValue = "Bearer \(_token)"
            request.setValue(tokenValue, forHTTPHeaderField: "Authorization")
        }
        return request
    }
    
}
