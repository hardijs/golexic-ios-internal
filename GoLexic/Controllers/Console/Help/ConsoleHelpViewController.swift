//
//  ConsoleHelpViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 27/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import WebKit
import Cubemobile

class ConsoleHelpViewController: UIViewController {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var containerView: UIView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    fileprivate var webViewController: WebViewController!
    
    // Falgs
    fileprivate var isPresentingAlert: Bool = false
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard containerView.alpha == 1 else {
            return
        }
        containerView.alpha = 0
        loadWebView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let webVC = segue.destination as? WebViewController {
            webVC.delegate = self
            webViewController = webVC
        }
    }
    
}

// MARK: - WebViewControllerDelegate
extension ConsoleHelpViewController: WebViewControllerDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIView.animate(withDuration: 0.2, animations: {
            self.containerView.alpha = 1
        }) { _ in
            self.activityIndicator.stopAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        guard isPresentingAlert == false else {
            return
        }
        isPresentingAlert = true
        let message = Localization.string(forKeyPath: "Globals.alerts.connectionError")!
        let cancel = Localization.string(forKeyPath: "Globals.cancel")!
        let retry = Localization.string(forKeyPath: "Globals.retry")!
        Globals.presentAlert(withTitle: "", message: message, cancel: cancel, actions: [retry]) { [weak self] index in
            guard let _self = self else {
                return
            }
            _self.isPresentingAlert = false
            if index == 1 {
                webView.reload()
            }
        }
    }
    
}

// MARK: - Private Functions
extension ConsoleHelpViewController {
    
    // MARK: Helpers
    
    fileprivate func loadWebView() {
        activityIndicator.startAnimating()
        if let _request = makeRequest() {
            webViewController.load(request: _request)
        }
    }
    
    // MARK: Make
    
    fileprivate func makeRequest() -> URLRequest? {
        guard let urlPath = Localization.string(forKeyPath: "Console.urlPaths.help"),
            let url = URL(string: "\(GoLexicAPI.host)\(urlPath)") else {
            return nil
        }
        var request = URLRequest(url: url)
        if let _token = User.current?.token {
            let tokenValue = "Bearer \(_token)"
            request.setValue(tokenValue, forHTTPHeaderField: "Authorization")
        }
        return request
    }
    
}
