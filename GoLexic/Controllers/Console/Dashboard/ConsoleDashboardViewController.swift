//
//  ConsoleDashboardViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 27/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import WebKit
import Cubemobile

class ConsoleDashboardViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var containerView: UIView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    fileprivate var webViewController: WebViewController!
    
    // Falgs
    fileprivate var isPresentingAlert: Bool = false
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadWebView()
        subscribeToNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard containerView.alpha == 1 else {
            return
        }
        containerView.alpha = 0
        loadWebView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let webVC = segue.destination as? WebViewController {
            webVC.delegate = self
            webViewController = webVC
        }
    }

}

// MARK: - WebViewControllerDelegate
extension ConsoleDashboardViewController: WebViewControllerDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        UIView.animate(withDuration: 0.2, animations: {
            self.containerView.alpha = 1
        }) { _ in
            self.activityIndicator.stopAnimating()
        }
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        guard isPresentingAlert == false else {
            return
        }
        isPresentingAlert = true
        let message = Localization.string(forKeyPath: "Globals.alerts.connectionError")!
        let cancel = Localization.string(forKeyPath: "Globals.cancel")!
        let retry = Localization.string(forKeyPath: "Globals.retry")!
        Globals.presentAlert(withTitle: "", message: message, cancel: cancel, actions: [retry]) { [weak self] index in
            guard let _self = self else {
                return
            }
            _self.isPresentingAlert = false
            if index == 1 {
                webView.reload()
            }
        }
    }
    
    func webViewController(_ webViewController: WebViewController,
                           didReceive message: WebViewController.WebMessages, payload: NSDictionary?) {
        switch message {
        case .subscribeToNotifications:
            if NotificationProvider.shared.status == .authorized {
                NotificationProvider.shared.changeState(toIsOn: true)
                let javascriptString = WebMethods.didSubscribeToNotifications
                webViewController.evaluateJavaScript(javascriptString)
            } else if NotificationProvider.shared.status == .provisional ||
                NotificationProvider.shared.status == .notDetermined {
                NotificationProvider.shared.registerForNotifications()
            } else {
                let message = Localization.string(forKeyPath: "Console.Settings.alerts.enableNotifications")!
                let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                let settings = Localization.string(forKeyPath: "Globals.settings")!
                Globals.presentAlert(withTitle: "", message: message, cancel: confirm, actions: [settings]) { [weak self] index in
                    guard self != nil, index == 1 else {
                        return
                    }
                    let url = URL(string: UIApplication.openSettingsURLString)!
                    UIApplication.shared.open(url)
                }
            }
        case .didSetReminder:
            if let _payload = payload, let time = _payload["time"] as? String {
                User.current?.update(.profileReminderTime, withData: time)
                Analytics.logControlChanged(Analytics.Controls.reminders, action: Analytics.Action.on)
            } else {
                User.current?.update(.profileReminderTime, withData: nil)
                Analytics.logControlChanged(Analytics.Controls.reminders, action: Analytics.Action.off)
            }
        }
    }
    
}

// MARK: - Private Functions
extension ConsoleDashboardViewController {
    
    // MARK: Structs
    
    fileprivate struct WebMethods {
        static let didSubscribeToNotifications = "didSubscribeToNotifications()"
    }
    
    // MARK: Helpers
    
    fileprivate func loadWebView() {
        activityIndicator.startAnimating()
        if let _request = makeRequest() {
            webViewController.load(request: _request)
        }
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.NotificationProvider.didChangeAuthorizationStatus,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            if NotificationProvider.shared.status == .authorized {
                let javascriptString = WebMethods.didSubscribeToNotifications
                _self.webViewController.evaluateJavaScript(javascriptString)
            }
        }
    }
    
    // MARK: Make
    
    fileprivate func makeRequest() -> URLRequest? {
        guard let _urlPath = Localization.string(forKeyPath: "Console.urlPaths.dashboard"),
            var _components = URLComponents(string: "\(GoLexicAPI.host)\(_urlPath)") else {
            return nil
        }
        // Add query items to url
        var queryItems: [URLQueryItem] = _components.queryItems ?? []
        let notificationValue: String = {
            guard NotificationProvider.shared.isEnabled,
                NotificationProvider.shared.status == .authorized else {
                return "0"
            }
            return "1"
        }()
        let notificationItem = URLQueryItem(name: "notificationStatus", value: notificationValue)
        queryItems.append(notificationItem)
        _components.queryItems = queryItems
        guard let _url = _components.url else {
            return nil
        }
        // Add header items to url reqeust
        var request = URLRequest(url: _url)
        if let _token = User.current?.token {
            let tokenValue = "Bearer \(_token)"
            request.setValue(tokenValue, forHTTPHeaderField: "Authorization")
        }
        return request
    }
    
}
