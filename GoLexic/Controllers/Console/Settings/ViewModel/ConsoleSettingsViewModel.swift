//
//  ConsoleSettingsViewModel.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 23/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ConsoleSettingsViewModel: NSObject {
    
    // MARK: Enums
    
    enum UpdateCompletion {
        case success(data: NSDictionary)
        case failure(code: Int?)
    }
    
    enum DeleteCompletion {
        case success
        case failure(code: Int?)
    }
    
    // MARK: Properties
    
    fileprivate(set) lazy var settings: [ConsoleSetting] = {
       return makeSettingItems()
    }()
    fileprivate(set) lazy var privacies: [ConsolePrivacy] = {
       return makePrivacyItems()
    }()

}

// MARK: - Public
extension ConsoleSettingsViewModel {
    
    // MARK: Make
    
    func makeFeedbackText() -> NSAttributedString? {
        guard let text = Localization.string(forKeyPath: "Console.Settings.feedback.text"),
            let email = Localization.string(forKeyPath: "Console.Settings.feedback.email") else {
            return nil
        }
        let _text = NSString(string: text)
        let attributedText = NSMutableAttributedString(string: text)
        // Add base attributes
        let range = NSRange(location: 0, length: text.count)
        let font = UIFont(name: UIFont.Name.Montserrat.regular, size: 16)!
        attributedText.addAttribute(.font, value: font, range: range)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.1882352941, green: 0.1882352941, blue: 0.1882352941, alpha: 1), range: range)
        // Add terms attributes
        let emailRange = _text.range(of: email)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: emailRange)
        if let _emailUrlString = Localization.string(forKeyPath: "Console.Settings.feedback.emailLink") {
            attributedText.addAttribute(.link, value: _emailUrlString, range: emailRange)
        }
        return attributedText
    }
    
    func updateUser(isAcceptingReports: Bool? = nil,
                    isAcceptingNews: Bool? = nil,
                    isAcceptingAnalytics: Bool? = nil,
                    completion: @escaping ((UpdateCompletion) -> Void)) {
        let params = makePostBody(isAcceptingReports: isAcceptingReports,
                                  isAcceptingNews: isAcceptingNews,
                                  isAcceptingAnalytics: isAcceptingAnalytics)
        let target = GoLexicTarget(withEndpoint: .userEdit, params: params)
        GoLexicAPI.provider.request(target) { result in
            switch result {
            case let .success(moyaResponse):
                guard let response = try? moyaResponse.mapJSON(),
                    let data = response as? NSDictionary else {
                        completion(.failure(code: nil))
                        return
                }
                completion(.success(data: data))
            case let .failure(error):
                let code: Int? = {
                    guard let data = try? error.response?.mapJSON() as? NSDictionary,
                        let code = data.value(forKeyPath: "error.code") as? Int else {
                            return nil
                    }
                    return code
                }()
                completion(.failure(code: code))
            }
        }
    }
    
    func deleteUserData(withCompletion completion: @escaping ((DeleteCompletion) -> Void)) {
        let target = GoLexicTarget(withEndpoint: .userDeleteData)
        GoLexicAPI.provider.request(target) { result in
            switch result {
            case .success:
                completion(.success)
            case let .failure(error):
                let code: Int? = {
                    guard let data = try? error.response?.mapJSON() as? NSDictionary,
                        let code = data.value(forKeyPath: "error.code") as? Int else {
                            return nil
                    }
                    return code
                }()
                completion(.failure(code: code))
            }
        }
    }
    
}

// MARK: - Private
extension ConsoleSettingsViewModel {
    
    // MARK: Make
    
    fileprivate func makeSettingItems() -> [ConsoleSetting] {
        var items: [ConsoleSetting] = []
        guard let resourcePath = Bundle.main.path(forResource: "Console", ofType: "plist") else {
            return items
        }
        guard let data = NSDictionary(contentsOfFile: resourcePath),
            let settings = data["settings"] as? NSArray else {
            return items
        }
        for case let setting as [String: Any] in settings {
            let item = ConsoleSetting(withData: setting)
            items.append(item)
        }
        return items
    }
    
    fileprivate func makePrivacyItems() -> [ConsolePrivacy] {
        var items: [ConsolePrivacy] = []
        guard let resourcePath = Bundle.main.path(forResource: "Console", ofType: "plist") else {
            return items
        }
        guard let data = NSDictionary(contentsOfFile: resourcePath),
            let settings = data["privacy"] as? NSArray else {
            return items
        }
        for case let setting as [String: Any] in settings {
            let item = ConsolePrivacy(withData: setting)
            if item.identifier == .analytics {
                continue
            }
            items.append(item)
        }
        return items
    }
    
    fileprivate func makePostBody(isAcceptingReports: Bool?,
                                  isAcceptingNews: Bool?,
                                  isAcceptingAnalytics: Bool?) -> [String: Any] {
        var body: [String: Any] = [:]
        if let _isAcceptingReports = isAcceptingReports {
            body.updateValue(_isAcceptingReports, forKey: "isAcceptingReports")
        }
        if let _isAcceptingNews = isAcceptingNews {
            body.updateValue(_isAcceptingNews, forKey: "isAcceptingNews")
        }
        if let _isAcceptingAnalytics = isAcceptingAnalytics {
            body.updateValue(_isAcceptingAnalytics, forKey: "isAcceptingAnalytics")
        }
        return body
    }
    
}
