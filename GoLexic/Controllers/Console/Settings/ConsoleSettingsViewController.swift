//
//  ConsoleSettingsViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 27/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

protocol ConsoleSettingsViewControllerDelegate: class {
    
    func consoleSettingsViewController(_ viewController: ConsoleSettingsViewController,
                                       didChangeUpdateStateTo isUpdating: Bool)
    
}

class ConsoleSettingsViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var contentViews: [AttributedView]!
    @IBOutlet fileprivate var settingsTableView: UITableView!
    @IBOutlet fileprivate var versionLabel: UILabel!
    @IBOutlet fileprivate var feedbackTextView: UITextView!
    @IBOutlet fileprivate var privacyTableView: UITableView!
    fileprivate var pageViewController: UIPageViewController!
    fileprivate weak var privacyViewController: ConsoleSettingsPrivacyViewController?
    
    // Variables
    weak var delegate: ConsoleSettingsViewControllerDelegate?
    fileprivate var viewModel = ConsoleSettingsViewModel()
    
    // Flags
    fileprivate var isUpdating: Bool = false {
        didSet {
            settingsTableView.isUserInteractionEnabled = !isUpdating
            privacyTableView.isUserInteractionEnabled = !isUpdating
            if let _privacyVC = privacyViewController {
                _privacyVC.isUpdating = isUpdating
            }
            if let _delegate = delegate {
                _delegate.consoleSettingsViewController(self, didChangeUpdateStateTo: isUpdating)
            }
        }
    }
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        updateContent()
        updatePageViewController(withIndex: 0)
        subscribeToNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        settingsTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let _targetVC = segue.destination as? UIPageViewController {
            pageViewController = _targetVC
        }
    }

}

// MARK: - UITableViewDataSource
extension ConsoleSettingsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let _tableView = TableView(rawValue: tableView.tag)!
        switch _tableView {
        case .settings:
            return viewModel.settings.count
        case .privacy:
            return viewModel.privacies.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        let _tableView = TableView(rawValue: tableView.tag)!
        switch _tableView {
        case .settings:
            let settingCell = tableView.dequeueReusableCell(withIdentifier: ConsoleSettingTableViewCell.identifier,
                                                            for: indexPath) as! ConsoleSettingTableViewCell
            let setting = viewModel.settings[indexPath.row]
            settingCell.update(withSetting: setting)
            switch setting.identifier {
            case .notifications:
                let isAuthorized = NotificationProvider.shared.status == .authorized
                let isEnabled = NotificationProvider.shared.isEnabled
                settingCell.isOn = isAuthorized && isEnabled
            case .reports:
                settingCell.isOn = User.current?.profile?.isAcceptingReports ?? false
            case .news:
                settingCell.isOn = User.current?.profile?.isAcceptingNews ?? false
            }
            if indexPath.row == tableView.numberOfRows(inSection: 0)-1 {
                settingCell.isSeparatorHidden = true
            }
            settingCell.delegate = self
            cell = settingCell
        case .privacy:
            let privacyCell = tableView.dequeueReusableCell(withIdentifier: ConsolePrivacyTableViewCell.identifier,
                                                            for: indexPath) as! ConsolePrivacyTableViewCell
            let privacy = viewModel.privacies[indexPath.row]
            privacyCell.update(withPrivacy: privacy)
            if indexPath.row == tableView.numberOfRows(inSection: 0)-1 {
                privacyCell.isSeparatorHidden = true
            }
            cell = privacyCell
        }
        cell.tag = indexPath.row
        return cell
    }
    
}

// MARK: - UITableViewDelegate
extension ConsoleSettingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let _tableView = TableView(rawValue: tableView.tag)!
        guard _tableView == .privacy,
            tableView.indexPathForSelectedRow == nil else {
            return
        }
        let path = IndexPath(row: 0, section: 0)
        tableView.selectRow(at: path, animated: false, scrollPosition: .none)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let _tableView = TableView(rawValue: tableView.tag)!
        guard _tableView == .privacy else {
            return
        }
        updatePageViewController(withIndex: indexPath.row)
    }
    
}

// MARK: - ConsoleSettingTableViewCellDelegate
extension ConsoleSettingsViewController: ConsoleSettingTableViewCellDelegate {
    
    func consoleSettingTableViewCell(_ cell: ConsoleSettingTableViewCell, shouldChange toOn: Bool) -> Bool {
        let setting = viewModel.settings[cell.tag]
        switch setting.identifier {
        case .notifications:
            if NotificationProvider.shared.status == .notDetermined {
                NotificationProvider.shared.registerForNotifications()
                return false
            } else if NotificationProvider.shared.status == .authorized {
                NotificationProvider.shared.changeState(toIsOn: toOn)
            } else {
                let message = Localization.string(forKeyPath: "Console.Settings.alerts.enableNotifications")!
                let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                let settings = Localization.string(forKeyPath: "Globals.settings")!
                Globals.presentAlert(withTitle: "", message: message, cancel: confirm, actions: [settings]) { [weak self] index in
                    guard self != nil, index == 1 else {
                        return
                    }
                    let url = URL(string: UIApplication.openSettingsURLString)!
                    UIApplication.shared.open(url)
                }
                return false
            }
            if toOn {
                Analytics.logControlChanged(Analytics.Controls.notifications, action: Analytics.Action.on)
            } else {
                Analytics.logControlChanged(Analytics.Controls.notifications, action: Analytics.Action.off)
            }
        case .reports:
            isUpdating = true
            viewModel.updateUser(isAcceptingReports: toOn) { [weak self] result in
                guard let _self = self else {
                    return
                }
                switch result {
                case let .success(data):
                    User.current?.update(.profile, withData: data)
                case let .failure(code):
                    Globals.presentAlert(withErrorCode: code)
                }
                _self.isUpdating = false
                _self.settingsTableView.reloadData()
            }
        case .news:
            isUpdating = true
            viewModel.updateUser(isAcceptingNews: toOn) { [weak self] result in
                guard let _self = self else {
                    return
                }
                switch result {
                case let .success(data):
                    User.current?.update(.profile, withData: data)
                case let .failure(code):
                    Globals.presentAlert(withErrorCode: code)
                }
                _self.isUpdating = false
                _self.settingsTableView.reloadData()
            }
        }
        return true
    }
    
}

// MARK: - UITextViewDelegate
extension ConsoleSettingsViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL,
                  in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        guard isUpdating == false else {
            return false
        }
        return true
    }
    
}

// MARK: - ConsoleSettingsPrivacyAnalyticsViewController
extension ConsoleSettingsViewController: ConsoleSettingsPrivacyAnalyticsViewControllerDelegate {
    
    func consoleSettingsPrivacyAnalyticsViewController(_ viewController: ConsoleSettingsPrivacyAnalyticsViewController,
                                                       didChangeSwitchTo isOn: Bool) {
        isUpdating = true
        viewModel.updateUser(isAcceptingAnalytics: isOn) { [weak self] result in
            guard let _self = self else {
                return
            }
            switch result {
            case let .success(data):
                User.current?.update(.profile, withData: data)
            case let .failure(code):
                Globals.presentAlert(withErrorCode: code)
            }
            _self.isUpdating = false
        }
    }
    
}

// MARK: - ConsoleSettingsPrivacyDeleteViewControllerDelegate
extension ConsoleSettingsViewController: ConsoleSettingsPrivacyDeleteViewControllerDelegate {
    
    func consoleSettingsPrivacyDeleteViewController(_ viewController: ConsoleSettingsPrivacyDeleteViewController,
                                                    didSelect action: ConsoleSettingsPrivacyDeleteViewController.Action) {
        let message = Localization.string(forKeyPath: "Console.Settings.alerts.confirmDataDelete")!
        let yes = Localization.string(forKeyPath: "Globals.yes")!
        let no = Localization.string(forKeyPath: "Globals.no")!
        Globals.presentAlert(withTitle: "", message: message, cancel: no, actions: [yes]) { [weak self] index in
            guard let _self = self, index == 1 else {
                return
            }
            _self.isUpdating = true
            _self.viewModel.deleteUserData { [weak self] result in
                guard let _self = self else {
                    return
                }
                switch result {
                case .success:
                    User.current?.delete()
                    let message = Localization.string(forKeyPath: "Console.Settings.alerts.dataDeleted")!
                    let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                    Globals.presentAlert(withTitle: "", message: message, cancel: confirm)
                case let .failure(code):
                    Globals.presentAlert(withErrorCode: code)
                }
                _self.isUpdating = false
            }
        }
    }
    
}

// MARK: - Private Functions
extension ConsoleSettingsViewController {
    
    // MARK: Enums
    
    fileprivate enum TableView: Int {
        case settings
        case privacy
    }
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        for contentView in contentViews {
            contentView.shadowOffset = CGSize(width: 0, height: 6)
        }
        settingsTableView.register(UINib(withType: ConsoleSettingTableViewCell.self),
                                   forCellReuseIdentifier: ConsoleSettingTableViewCell.identifier)
        privacyTableView.register(UINib(withType: ConsolePrivacyTableViewCell.self),
                                  forCellReuseIdentifier: ConsolePrivacyTableViewCell.identifier)
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.NotificationProvider.didChangeAuthorizationStatus,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            _self.settingsTableView.reloadData()
        }
    }
    
    // MARK: Update
    
    fileprivate func updateContent() {
        versionLabel.text = {
            let version = AppInfo.appVersion()
            let versionText = Localization.string(forKeyPath: "Console.Settings.feedback.version")!
            return "\(versionText) \(version)"
        }()
        feedbackTextView.attributedText = viewModel.makeFeedbackText()
    }
    
    fileprivate func updatePageViewController(withIndex index: Int) {
        let privacy = viewModel.privacies[index]
        let viewController = makePrivacyViewController(forPrivacy: privacy)
        pageViewController.setViewControllers([viewController], direction: .forward,
                                              animated: false, completion: nil)
        privacyViewController = viewController
    }
    
    // MARK: Make
    
    fileprivate func makePrivacyViewController(forPrivacy privacy: ConsolePrivacy) -> ConsoleSettingsPrivacyViewController {
        var viewController: ConsoleSettingsPrivacyViewController!
        switch privacy.identifier {
        case .policy:
            viewController = instantiate(ConsoleSettingsPrivacyPolicyViewController.self)
        case .analytics:
            let analyticsVC = instantiate(ConsoleSettingsPrivacyAnalyticsViewController.self)
            analyticsVC.delegate = self
            viewController = analyticsVC
        case .data:
            viewController = instantiate(ConsoleSettingsPrivacyDataViewController.self)
        case .delete:
            let deleteVC = instantiate(ConsoleSettingsPrivacyDeleteViewController.self)
            deleteVC.delegate = self
            viewController = deleteVC
        }
        return viewController
    }
    
}
