//
//  ConsoleSettingsPrivacyDeleteViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 23/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

protocol ConsoleSettingsPrivacyDeleteViewControllerDelegate: class {
    
    func consoleSettingsPrivacyDeleteViewController(_ viewController: ConsoleSettingsPrivacyDeleteViewController,
                                                    didSelect action: ConsoleSettingsPrivacyDeleteViewController.Action)
    
}

class ConsoleSettingsPrivacyDeleteViewController: ConsoleSettingsPrivacyViewController {
    
    // MARK: Enums
    
    enum Action {
        case delete
    }

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var textView: UITextView!
    @IBOutlet fileprivate var deleteButton: AttributedButton!
    
    // Variables
    weak var delegate: ConsoleSettingsPrivacyDeleteViewControllerDelegate?
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateContent()
    }

    override var isUpdating: Bool {
        didSet {
            guard isViewLoaded else {
                return
            }
            deleteButton.setEnabled(!isUpdating, animated: false)
        }
    }
    
}

// MARK: - Targets
extension ConsoleSettingsPrivacyDeleteViewController {
    
    @IBAction fileprivate func deleteButtonPressed(_ sender: UIButton) {
        guard let _delegate = delegate else {
            return
        }
        _delegate.consoleSettingsPrivacyDeleteViewController(self, didSelect: .delete)
    }
    
}

// MARK: - UITextViewDelegate
extension ConsoleSettingsPrivacyDeleteViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL,
                  in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        guard isUpdating == false else {
            return false
        }
        let webVC = show(WebGenericViewController.self, useNavigationController: true)
        webVC.urlString = URL.absoluteString
        return false
    }
    
}

// MARK: - Private Functions
extension ConsoleSettingsPrivacyDeleteViewController {
    
    // MARK: Update
    
    fileprivate func updateContent() {
        textView.attributedText = makeText()
        deleteButton.setEnabled(!isUpdating, animated: false)
    }
    
    // MARK: Make
    
    fileprivate func makeText() -> NSAttributedString? {
        guard let text = Localization.string(forKeyPath: "Console.Settings.privacy.delete.text"),
            let learnMore = Localization.string(forKeyPath: "Console.Settings.privacy.delete.learnMore") else {
            return nil
        }
        let _text = NSString(string: text)
        let attributedText = NSMutableAttributedString(string: text)
        // Add base attributes
        let range = NSRange(location: 0, length: text.count)
        let font = UIFont(name: UIFont.Name.Montserrat.regular, size: 16)!
        attributedText.addAttribute(.font, value: font, range: range)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.1882352941, green: 0.1882352941, blue: 0.1882352941, alpha: 1), range: range)
        // Add learnMore attributes
        let learnMoreRange = _text.range(of: learnMore)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: learnMoreRange)
        if let _learnMoreUrlString = Localization.string(forKeyPath: "Console.Settings.privacy.delete.learnMoreLink") {
            attributedText.addAttribute(.link, value: _learnMoreUrlString, range: learnMoreRange)
        }
        return attributedText
    }
    
}
