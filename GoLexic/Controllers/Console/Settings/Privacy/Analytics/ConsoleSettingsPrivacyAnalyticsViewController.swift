//
//  ConsoleSettingsPrivacyAnalyticsViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 23/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

protocol ConsoleSettingsPrivacyAnalyticsViewControllerDelegate: class {
    
    func consoleSettingsPrivacyAnalyticsViewController(_ viewController: ConsoleSettingsPrivacyAnalyticsViewController,
                                                       didChangeSwitchTo isOn: Bool)
    
}

class ConsoleSettingsPrivacyAnalyticsViewController: ConsoleSettingsPrivacyViewController {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var textView: UITextView!
    @IBOutlet fileprivate var switchControl: UISwitch!
    
    // Variables
    weak var delegate: ConsoleSettingsPrivacyAnalyticsViewControllerDelegate?
    
    // Flags
    fileprivate var isAcceptingAnalytics: Bool {
        guard let profile = User.current?.profile else {
            return false
        }
        return profile.isAcceptingAnalytics
    }
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        subscribeToNotifications()
        updateContent(animated: false)
    }

    override var isUpdating: Bool {
        didSet {
            switchControl.isEnabled = !isUpdating
        }
    }

}

// MARK: - Targets
extension ConsoleSettingsPrivacyAnalyticsViewController {
    
    @IBAction fileprivate func switchControlValueChanged(_ sender: UISwitch) {
        let value = sender.isOn
        sender.setOn(!sender.isOn, animated: true)
        guard let _delegate = delegate else {
            return
        }
        DispatchQueue.main.async {
            _delegate.consoleSettingsPrivacyAnalyticsViewController(self, didChangeSwitchTo: value)
        }
    }
    
}


// MARK: - UITextViewDelegate
extension ConsoleSettingsPrivacyAnalyticsViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL,
                  in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        guard isUpdating == false else {
            return false
        }
        let webVC = show(WebGenericViewController.self, useNavigationController: true)
        webVC.urlString = URL.absoluteString
        return false
    }
    
}

// MARK: - Private Functions
extension ConsoleSettingsPrivacyAnalyticsViewController {
    
    // MARK: Helpers
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.User.DidUpdate,
                                               object: nil, queue: nil) { [unowned self] _ in
            self.updateContent(animated: true)
        }
    }
    
    // MARK: Update
    
    fileprivate func updateContent(animated: Bool) {
        textView.attributedText = makeText()
        switchControl.setOn(isAcceptingAnalytics, animated: animated)
    }
    
    // MARK: Make
    
    fileprivate func makeText() -> NSAttributedString? {
        guard let text = Localization.string(forKeyPath: "Console.Settings.privacy.analytics.text"),
            let learMore = Localization.string(forKeyPath: "Console.Settings.privacy.analytics.learnMore") else {
            return nil
        }
        let _text = NSString(string: text)
        let attributedText = NSMutableAttributedString(string: text)
        // Add base attributes
        let range = NSRange(location: 0, length: text.count)
        let font = UIFont(name: UIFont.Name.Montserrat.regular, size: 16)!
        attributedText.addAttribute(.font, value: font, range: range)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.1882352941, green: 0.1882352941, blue: 0.1882352941, alpha: 1), range: range)
        // Add learnMore attributes
        let learnMoreRange = _text.range(of: learMore)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: learnMoreRange)
        if let _learnMoreUrlString = Localization.string(forKeyPath: "Console.Settings.privacy.analytics.learnMoreLink") {
            attributedText.addAttribute(.link, value: _learnMoreUrlString, range: learnMoreRange)
        }
        return attributedText
    }
    
}
