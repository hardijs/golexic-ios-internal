//
//  ConsoleSettingsPrivacyViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 23/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit

class ConsoleSettingsPrivacyViewController: UIViewController {
    
    // MARK: Properties
    
    // Flags
    var isUpdating: Bool = false
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

}
