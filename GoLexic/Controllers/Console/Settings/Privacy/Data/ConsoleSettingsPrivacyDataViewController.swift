//
//  ConsoleSettingsPrivacyDataViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 23/04/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ConsoleSettingsPrivacyDataViewController: ConsoleSettingsPrivacyViewController {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var textView: UITextView!
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateContent()
    }

}

// MARK: - UITextViewDelegate
extension ConsoleSettingsPrivacyDataViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL,
                  in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        guard isUpdating == false else {
            return false
        }
        let webVC = show(WebGenericViewController.self, useNavigationController: true)
        webVC.urlString = URL.absoluteString
        return false
    }
    
}

// MARK: - Private Functions
extension ConsoleSettingsPrivacyDataViewController {
    
    // MARK: Update
    
    fileprivate func updateContent() {
        textView.attributedText = makeText()
    }
    
    // MARK: Make
    
    fileprivate func makeText() -> NSAttributedString? {
        guard let text = Localization.string(forKeyPath: "Console.Settings.privacy.data.text"),
            let learnMore = Localization.string(forKeyPath: "Console.Settings.privacy.data.learnMore"),
            let email = Localization.string(forKeyPath: "Console.Settings.privacy.data.email") else {
            return nil
        }
        let _text = NSString(string: text)
        let attributedText = NSMutableAttributedString(string: text)
        // Add base attributes
        let range = NSRange(location: 0, length: text.count)
        let font = UIFont(name: UIFont.Name.Montserrat.regular, size: 16)!
        attributedText.addAttribute(.font, value: font, range: range)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.1882352941, green: 0.1882352941, blue: 0.1882352941, alpha: 1), range: range)
        // Add learnMore attributes
        let learnMoreRange = _text.range(of: learnMore)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: learnMoreRange)
        if let _learnMoreUrlString = Localization.string(forKeyPath: "Console.Settings.privacy.data.learnMoreLink") {
            attributedText.addAttribute(.link, value: _learnMoreUrlString, range: learnMoreRange)
        }
        // Add email attributes
        let emailRange = _text.range(of: email)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: emailRange)
        if let _emailUrlString = Localization.string(forKeyPath: "Console.Settings.privacy.data.emailLink") {
            attributedText.addAttribute(.link, value: _emailUrlString, range: emailRange)
        }
        return attributedText
    }
    
}
