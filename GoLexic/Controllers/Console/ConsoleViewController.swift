//
//  ConsoleViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 27/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ConsoleViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var headerView: UIView!
    @IBOutlet fileprivate var buttonStackView: UIStackView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    fileprivate var tabController: UITabBarController!
    
    // Flags
    fileprivate var isUpdating: Bool = false {
        didSet {
            if isUpdating {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
            headerView.isUserInteractionEnabled = !isUpdating
            navigationItem.leftBarButtonItem?.isEnabled = !isUpdating
        }
    }
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        let screenName = makeScreenName(forTab: .dashboard)
        Analytics.logScreenView(screenName)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let _targetVC = segue.destination as? UITabBarController {
            tabController = _targetVC
        }
    }
    
}

// MARK: - Targets
extension ConsoleViewController {
    
    @IBAction fileprivate func backBarButtonPressed(_ sender: UIBarButtonItem) {
        navigationController!.popViewController(animated: true)
    }
    
    @IBAction fileprivate func tabButtonPressed(_ sender: UIButton) {
        guard sender.isSelected == false else {
            return
        }
        let selectedTab = Tab(rawValue: sender.tag)!
        for case let button as UIButton in buttonStackView.subviews {
            let tab = Tab(rawValue: button.tag)!
            button.isSelected = selectedTab == tab
        }
        tabController.selectedIndex = selectedTab.rawValue
        let screenName = makeScreenName(forTab: .dashboard)
        Analytics.logScreenView(screenName)
    }
    
}

// MARK: - ConsoleSettingsViewControllerDelegate
extension ConsoleViewController: ConsoleSettingsViewControllerDelegate {
    
    func consoleSettingsViewController(_ viewController: ConsoleSettingsViewController,
                                       didChangeUpdateStateTo isUpdating: Bool) {
        self.isUpdating = isUpdating
    }
    
}

// MAKR: - Private Functions
extension ConsoleViewController {
    
    // MARK: Enums
    
    enum Tab: Int {
        case dashboard
        case settings
        case about
        case help
    }
    
    fileprivate func configureController() {
        navigationItem.title = Localization.string(forKeyPath: "Console.title")
        if let _viewControllers = tabController.viewControllers {
            for viewController in _viewControllers {
                if let _settingsVC = viewController as? ConsoleSettingsViewController {
                    _settingsVC.delegate = self
                }
            }
        }
    }
    
    // MARK: Make
    
    fileprivate func makeScreenName(forTab tab: Tab) -> String {
        switch tab {
        case .dashboard:
            return Analytics.Screens.consoleDashboard
        case .settings:
            return Analytics.Screens.consoleSettings
        case .about:
            return Analytics.Screens.consoleAbout
        case .help:
            return Analytics.Screens.consoleHelp
        }
    }
    
}
