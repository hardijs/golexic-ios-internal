//
//  ExerciseViewModel.swift
//  GoLexic
//
//  Created by Armands L. on 02/08/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation
import Mixpanel

class ExerciseViewModel {
    
}

// MARK: - Public
extension ExerciseViewModel {
    
    // MARK: Make
    
    func makeExerciseAnalyticsParams(forPhaseExercise phaseExercise: PhaseExercise) -> [String: MixpanelType] {
        var params: [String: MixpanelType] = [:]
        params.updateValue(phaseExercise.exerciseType.makeAnalyticsValue(), forKey: Analytics.Parameters.exercise)
        if let phaseLesson = phaseExercise.firstAvailableLesson(), let lesson = phaseLesson.lesson {
            params.updateValue(lesson.lessonIndex, forKey: Analytics.Parameters.lesson)
        }
        return params
    }
    
    func makeMicAnalyticsParams(forPhaseExercise phaseExercise: PhaseExercise, state: Bool) -> [String: MixpanelType] {
        var params: [String: MixpanelType] = [:]
        params.updateValue(phaseExercise.exerciseType.makeAnalyticsValue(), forKey: Analytics.Parameters.exercise)
        if let phaseLesson = phaseExercise.firstAvailableLesson(), let lesson = phaseLesson.lesson {
            params.updateValue(lesson.lessonIndex, forKey: Analytics.Parameters.lesson)
        }
        let action = state == true ? Analytics.Action.on : Analytics.Action.off
        params.updateValue(action, forKey: Analytics.Parameters.action)
        return params
    }
    
    func makeHelpAnalyticsParams(forPhaseExercise phaseExercise: PhaseExercise) -> [String: MixpanelType] {
        var params: [String: MixpanelType] = [:]
        params.updateValue(phaseExercise.exerciseType.makeAnalyticsValue(), forKey: Analytics.Parameters.exercise)
        if let phaseLesson = phaseExercise.firstAvailableLesson(), let lesson = phaseLesson.lesson {
            params.updateValue(lesson.lessonIndex, forKey: Analytics.Parameters.lesson)
        }
        params.updateValue(Analytics.Action.open, forKey: Analytics.Parameters.action)
        return params
    }
    
    func makeIntroAnalyticsParams(forPhaseExercise phaseExercise: PhaseExercise,
                                  action: ExerciseInstructionsViewController.Action) -> [String: MixpanelType] {
        var params: [String: MixpanelType] = [:]
        params.updateValue(phaseExercise.exerciseType.makeAnalyticsValue(), forKey: Analytics.Parameters.exercise)
        if let phaseLesson = phaseExercise.firstAvailableLesson(), let lesson = phaseLesson.lesson {
            params.updateValue(lesson.lessonIndex, forKey: Analytics.Parameters.lesson)
        }
        let _action: String = {
            if action == .start {
                return Analytics.Action.start
            }
            return Analytics.Action.back
        }()
        params.updateValue(_action, forKey: Analytics.Parameters.action)
        return params
    }
    
}
