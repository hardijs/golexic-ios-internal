//
//  ExerciseAlphabetPracticeViewModel.swift
//  GoLexic
//
//  Created by Armands L. on 12/08/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import Cubemobile

class ExerciseAlphabetPracticeViewModel {
    
    // MARK: Properties
    
    // Variables
    var phase: Phase
    var phaseExercise: PhaseExercise
    fileprivate(set) lazy var exercise: ExerciseAlphabetPractice? = {
        return phaseExercise.exercise as? ExerciseAlphabetPractice
    }()
    fileprivate(set) lazy var letters: [ExerciseLetter] = {
        return makeExerciseLetters(fromExercise: exercise)
    }()
    fileprivate(set) var index: Int = 1
    var pickerLetters: [ExerciseLetter] {
        return makePickerLetters(fromLetters: letters, index: index)
    }
    fileprivate var results: [ExerciseLetter] = []
    
    fileprivate lazy var speechProcessor: SpeechProcessor = {
        return SpeechProcessor()
    }()
    
    // Flags
    fileprivate(set) var isCompleted: Bool = false
    var isPendingFinalResult: Bool = false
    
    // MARK: Initialziers
    
    init(withPhase phase: Phase, phaseExercise: PhaseExercise) {
        self.phase = phase
        self.phaseExercise = phaseExercise
        let resultLetters = makeResultLetters(fromLetters: letters)
        results.append(contentsOf: resultLetters)
    }
    
}

// MARK: - Public Functions
extension ExerciseAlphabetPracticeViewModel {
    
    // MARK: Exercise
    
    @discardableResult
    func completeExercise() -> Score {
        guard isCompleted == false else {
            return makeExerciseScore()
        }
        isCompleted = true
        let realm = try! Realm()
        try! realm.write {
            phaseExercise.markAsCompleted()
        }
        if let _exercise = exercise {
            let count = makeCount(forLetters: results, withConfidence: _exercise.passConfidence)
            submitResults(withScore: count)
        }
        submitStatistics(withResults: results)
        return makeExerciseScore()
    }
    
    func makeExerciseScore() -> Score {
        guard let _exercise = exercise else {
            return .none
        }
        let passedLetters = makeCount(forLetters: results, withConfidence: _exercise.passConfidence)
        let isExercisePassed = passedLetters >= _exercise.passThreshold
        return isExercisePassed ? .success : .failed
    }
    
    // MARK: Letters
    
    func isLetterNextInLine(_ letter: ExerciseLetter) -> Bool {
        var _nextLetter: ExerciseLetter?
        for letter in letters {
            if letter.status == .none {
                _nextLetter = letter
                break
            }
        }
        guard letter == _nextLetter else {
            return false
        }
        return true
    }
    
    func hasAnyLetter(withStatus status: RecognitionStatus) -> Bool {
        guard index < letters.count else {
            return false
        }
        for x in 0...index {
            let letter = letters[x]
            if letter.isFailed == false,
                letter.status == status {
                return true
            }
        }
        return false
    }
    
    func failCurrentLetter() -> Bool {
        for letter in letters {
            guard letter.isFailed == false,
                letter.status == .selected else {
                continue
            }
            letter.isFailed = true
            return true
        }
        return false
    }
    
    @discardableResult
    func moveToNextLetter() -> Bool {
        let nextIndex = index + 1
        guard nextIndex < letters.count else {
            return false
        }
        index = nextIndex
        letters = makeExerciseLetters(fromExercise: exercise)
        let resultLetters = makeResultLetters(fromLetters: letters)
        results.append(contentsOf: resultLetters)
        return true
    }
    
    // MARK: Speech Recognition
    
    @discardableResult
    func processSpeechRecognizerPartialResult(_ result: KIOSResult) -> Bool {
        var hasNewRecognitions: Bool = false
        let words = speechProcessor.processResult(result, isNewSearch: true)
        for letter in letters {
            guard letter.isFailed == false,
                letter.status == .selected else {
                continue
            }
            guard letter.isEqualToAny(words, includeSimilarities: true) else {
                break
            }
            letter.status = .recognized
            hasNewRecognitions = true
            break
        }
        return hasNewRecognitions
    }
    
    func processSpeechRecognizerFinalResult(_ result: KIOSResult) {
        for letter in letters {
            guard letter.status == .recognized else {
                continue
            }
            letter.updateConfidenceIfMatches(result.words, includeSimilarities: true)
        }
        speechProcessor.reset()
    }
    
    // MARK: Other
    
    func exerciseScoreDebugInfo() -> String? {
        guard let _exercise = exercise else {
            return nil
        }
        let passedLetters = makeCount(forLetters: results,
                                      withConfidence: _exercise.passConfidence)
        return "Completed \(passedLetters)/\(results.count) letters"
    }
    
}

// MARK: - Private Functions
extension ExerciseAlphabetPracticeViewModel {
    
    // MARK: Helpers
    
    fileprivate func submitResults(withScore score: Int) {
        let params = makeResultsBody(withScore: score)
        let target = GoLexicTarget(withEndpoint: .alphabetPracticeResults, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    fileprivate func submitStatistics(withResults results: [ExerciseLetter]) {
        guard let _exercise = exercise,
            let _letterScoring = _exercise.letterScoring else {
            return
        }
        let params = makeStatisticsBody(withResults: results, letterScoring: _letterScoring)
        let target = GoLexicTarget(withEndpoint: .statisticsLetters, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    // MARK: Make
    
    fileprivate func makeExerciseLetters(fromExercise exercise: ExerciseAlphabetPractice?) -> [ExerciseLetter] {
        var _letters: [ExerciseLetter] = []
        guard let _exercise = exercise,
            let _exerciseLetters = _exercise.letters else {
            return _letters
        }
        for x in 0..<_exerciseLetters.items.count {
            let item = _exerciseLetters.items[x]
            let letter = ExerciseLetter(withLetterItem: item)
            if x < index {
                letter.status = .selected
            }
            _letters.append(letter)
        }
        return _letters
    }
    
    fileprivate func makePickerLetters(fromLetters letters: [ExerciseLetter], index: Int) -> [ExerciseLetter] {
        guard index < letters.count else {
            return []
        }
        let letter = letters[index]
        return [letter]
    }
    
    fileprivate func makeCount(forLetters letters: [ExerciseLetter], withConfidence confidence: Double) -> Int {
        var count: Int = 0
        for letter in letters {
            guard letter.isFailed == false else {
                continue
            }
            guard letter.isFailed == false,
                letter.confidence >= confidence else {
                continue
            }
            count += 1
        }
        return count
    }
    
    fileprivate func makeResultLetters(fromLetters letters: [ExerciseLetter]) -> [ExerciseLetter] {
        return Array(letters[0...index])
    }
    
    fileprivate func makeResultsBody(withScore score: Int) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(score, forKey: "score")
        return body
    }
    
    fileprivate func makeStatisticsBody(withResults results: [ExerciseLetter], letterScoring: ScoringLetter) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(letterScoring.identifier, forKey: "letterScoringId")
        var _letters: [[String: Any]] = []
        for result in results {
            let item: [String : Any] = ["letter": result.value.uppercased(),
                                        "score": result.confidence]
            _letters.append(item)
        }
        body.updateValue(_letters, forKey: "letters")
        return body
    }
    
}
