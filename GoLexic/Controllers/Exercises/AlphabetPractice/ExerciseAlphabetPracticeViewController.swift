//
//  ExerciseAlphabetPracticeViewController.swift
//  GoLexic
//
//  Created by Armands L. on 11/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ExerciseAlphabetPracticeViewController: ExerciseViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var contentView: AttributedView!
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var recordButton: UIButton!
    @IBOutlet fileprivate var letterPickerView: LetterPickerView!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Constraints
    @IBOutlet fileprivate var collectionViewHeightConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate lazy var viewModel: ExerciseAlphabetPracticeViewModel = {
        return ExerciseAlphabetPracticeViewModel(withPhase: phase, phaseExercise: phaseExercise)
    }()
    fileprivate var failTimer: Timer?
    fileprivate var letterPadding: CGFloat = 8
    fileprivate var letterSpace: CGSize = .zero
    fileprivate var currentCollectionViewWidth: CGFloat = 0
    fileprivate var indexPathInFocus: IndexPath?
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
        updateContent()
        updateState()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if collectionView.frame.width != currentCollectionViewWidth {
            currentCollectionViewWidth = collectionView.frame.width
            // Update layout
            letterSpace = makeCollectionViewItemSpace()
            collectionView.reloadData()
            updateLayout()
        }
    }

}

// MARK: - Targets
extension ExerciseAlphabetPracticeViewController {
    
    @IBAction fileprivate func closeButtonPressed(_ sender: UIButton) {
        guard viewModel.isPendingFinalResult == false else {
            return
        }
        SpeechRecognition.shared.stopListening(withResults: false)
        logExerciseCancelEvent()
        dismiss(animated: true)
    }
    
    @IBAction fileprivate func recordButtonPressed(_ sender: UIButton) {
        if SpeechRecognition.shared.state == .listening {
            viewModel.isPendingFinalResult = true
            SpeechRecognition.shared.stopListening()
            logMicEvent(withState: false)
        } else {
            SpeechRecognition.shared.startListening()
            logMicEvent(withState: true)
        }
    }
    
    @objc fileprivate func failTimerDidTrigger(_ sender: Timer) {
        guard viewModel.failCurrentLetter() else {
            return
        }
        if viewModel.hasAnyLetter(withStatus: .selected) {
            scheduleFailTimer()
        } else if SpeechRecognition.shared.state == .listening {
            viewModel.isPendingFinalResult = true
            SpeechRecognition.shared.stopListening()
        }
        collectionView.reloadData()
    }
    
}

// MARK: - UICollectionViewDelegate
extension ExerciseAlphabetPracticeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.letters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LetterDroppableCollectionViewCell.identifier,
                                                      for: indexPath) as! LetterDroppableCollectionViewCell
        let letter = viewModel.letters[indexPath.row]
        cell.update(withLetter: letter, markFailed: true,
                    isHovering: indexPathInFocus == indexPath)
        cell.padding = letterPadding
        return cell
    }
    
}

// MARK: - UICollectionViewDropDelegate
extension ExerciseAlphabetPracticeViewController: UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        guard let _ = session.items.first?.localObject as? LetterView else {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession,
                        withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        guard let letterView = session.items.first?.localObject as? LetterView, let letter = letterView.letter,
            let _indexPath = destinationIndexPath, _indexPath.row < viewModel.letters.count else {
                return UICollectionViewDropProposal(operation: .cancel)
        }
        if indexPathInFocus != _indexPath {
            indexPathInFocus = _indexPath
            collectionView.reloadData()
        }
        let targetLetter = viewModel.letters[_indexPath.row]
        if letter == targetLetter,
            viewModel.isLetterNextInLine(targetLetter) {
            Analytics.logLetterDropSuccess(letter.value)
            return UICollectionViewDropProposal(operation: .move)
        } else {
            Analytics.logLetterDropFail(letter.value)
            return UICollectionViewDropProposal(operation: .cancel)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard let letterView = coordinator.session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return
        }
        letter.status = .selected
        collectionView.reloadData()
        if viewModel.hasAnyLetter(withStatus: .none) == false {
            SpeechRecognition.shared.startListening()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidEnd session: UIDropSession) {
        indexPathInFocus = nil
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidExit session: UIDropSession) {
        indexPathInFocus = nil
        collectionView.reloadData()
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExerciseAlphabetPracticeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return makeItemSize(fromLetterSpace: letterSpace)
    }
    
}

// MARK: - SpeechRecognitionDelegate
extension ExerciseAlphabetPracticeViewController: SpeechRecognitionDelegate {
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, didChange state: KIOSRecognizerState) {
        if let _timer = failTimer {
            _timer.invalidate()
        }
        if state == .listening {
            scheduleFailTimer()
        }
        updateState()
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, partial result: KIOSResult) {
        let hasNewResults = viewModel.processSpeechRecognizerPartialResult(result)
        if hasNewResults {
            Analytics.logVoiceRecognitionSuccess(result.cleanText)
        } else {
            Analytics.logVoiceRecognitionFailed(result.cleanText)
        }
        guard hasNewResults else {
            return
        }
        DispatchQueue.main.async {
            if self.viewModel.hasAnyLetter(withStatus: .selected) {
                self.scheduleFailTimer()
            } else if SpeechRecognition.shared.state == .listening {
                self.viewModel.isPendingFinalResult = true
                SpeechRecognition.shared.stopListening()
            }
            self.collectionView.reloadData()
        }
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, final result: KIOSResult) {
        guard viewModel.isPendingFinalResult else {
            return
        }
        viewModel.processSpeechRecognizerFinalResult(result)
        DispatchQueue.main.async {
            self.viewModel.isPendingFinalResult = false
            self.proceedAfterFinalResults()
            self.updateState()
        }
    }
    
    func speechRecognitionReadyToListenAfterInterruption(_ speechRecognition: SpeechRecognition) {
        updateState()
    }
    
}

// MARK: - ExerciseFeedbackViewControllerDelegate
extension ExerciseAlphabetPracticeViewController: ExerciseFeedbackViewControllerDelegate {
    
    func exerciseFeedbackViewController(_ viewController: ExerciseFeedbackViewController,
                                           didSelect action: ExerciseFeedbackViewController.Action) {
        guard let _delegate = delegate else {
            return
        }
        if action == .next {
            _delegate.exerciseViewController(self, didSelect: .phase)
        } else if action == .home {
            _delegate.exerciseViewController(self, didSelect: .home)
        }
    }
    
}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseAlphabetPracticeViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK - Private Functions
extension ExerciseAlphabetPracticeViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        SpeechRecognition.shared.delegate = self
        if let _exercise = phaseExercise.exercise as? ExerciseAlphabetPractice {
            SpeechRecognition.shared.prepareForListening(withDecodingGraph: _exercise.lettersId)
        }
        contentView.shadowOffset = CGSize(width: 0, height: 6)
        collectionView.register(UINib(withType: LetterDroppableCollectionViewCell.self),
                                forCellWithReuseIdentifier: LetterDroppableCollectionViewCell.identifier)
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            if _self.viewModel.isPendingFinalResult {
                _self.viewModel.isPendingFinalResult = false
                _self.proceedAfterFinalResults()
                _self.updateState()
            }
        }
    }
    
    fileprivate func scheduleFailTimer() {
        if let _timer = failTimer {
            _timer.invalidate()
        }
        failTimer = Timer.scheduledTimer(timeInterval: 4.0, target: self,
                                              selector: #selector(failTimerDidTrigger(_:)), userInfo: nil, repeats: false)
    }
    
    fileprivate func proceedAfterFinalResults() {
        guard viewModel.hasAnyLetter(withStatus: .selected) == false else {
            return
        }
        if viewModel.moveToNextLetter() {
            updateContent()
            collectionView.reloadData()
        } else if feedbackViewController == nil {
            logExerciseCompletedEvent()
            let score = self.viewModel.completeExercise()
            let debugInfo = self.viewModel.exerciseScoreDebugInfo()
            presentFeedbackViewController(withDelegate: self, score: score, debugInfo: debugInfo)
        }
    }
    
    // MARK: Update
    
    fileprivate func updateContent() {
        letterPickerView.update(withLetters: viewModel.pickerLetters)
    }
    
    fileprivate func updateLayout() {
        let itemSize = makeItemSize(fromLetterSpace: letterSpace)
        let collectionViewLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewHeightConstraint.constant = itemSize.height * 2 + collectionViewLayout.minimumLineSpacing
        let letterSize = CGSize(width: letterSpace.width-letterPadding*2,
                                height: letterSpace.height-letterPadding*2)
        letterPickerView.updateLayout(withLetterSize: letterSize)
    }
    
    fileprivate func updateState() {
        if viewModel.isCompleted ||
            viewModel.hasAnyLetter(withStatus: .none) ||
            viewModel.hasAnyLetter(withStatus: .selected) == false ||
            SpeechRecognition.shared.state == .needsDecodingGraph {
            recordButton.isHidden = true
        } else {
            recordButton.isHidden = false
        }
        if SpeechRecognition.shared.state == .readyToListen {
            recordButton.isSelected = false
        } else {
            recordButton.isSelected = true
        }
        if viewModel.isPendingFinalResult {
            recordButton.isUserInteractionEnabled = false
            activityIndicator.startAnimating()
        } else {
            recordButton.isUserInteractionEnabled = true
            activityIndicator.stopAnimating()
        }
    }
    
    // MARK: Make
    
    fileprivate func makeCollectionViewItemSpace() -> CGSize {
        guard viewModel.letters.isEmpty == false else {
            return .zero
        }
        let itemsPerLine = ceil(Double(viewModel.letters.count)/2.0)
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpacingWidth = CGFloat((itemsPerLine-1)*Double(layout.minimumInteritemSpacing))
        let availableWidth = collectionView.frame.width-totalSpacingWidth
        var size = floor(availableWidth/CGFloat(itemsPerLine))
        if size > view.frame.width/10 {
            size = view.frame.width/10
        } else if size < 0 {
            size = 0
        }
        return CGSize(width: size, height: size)
    }
    
    fileprivate func makeItemSize(fromLetterSpace letterSpace: CGSize) -> CGSize {
        var _size = letterSpace
        _size.height = _size.height + (_size.height/3)
        return _size
    }
    
}
