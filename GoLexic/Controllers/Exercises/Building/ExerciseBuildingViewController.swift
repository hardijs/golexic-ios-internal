//
//  ExerciseBuildingViewController.swift
//  GoLexic
//
//  Created by Armands L. on 29/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ExerciseBuildingViewController: ExerciseViewController {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var progressView: ProgressView!
    @IBOutlet fileprivate var textCollectionView: UICollectionView!
    @IBOutlet fileprivate var footerView: AttributedView!
    @IBOutlet fileprivate var consonantCollectionView: UICollectionView!
    @IBOutlet fileprivate var vowelCollectionView: UICollectionView!
    @IBOutlet fileprivate var recordButton: UIButton!
    @IBOutlet fileprivate var audioButton: UIButton!
    @IBOutlet fileprivate var infoLabel: UILabel!
    @IBOutlet fileprivate var helpButton: UIButton!
    @IBOutlet fileprivate var completeButton: UIButton!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    // Constraints
    @IBOutlet fileprivate var textCollectionViewWidth: NSLayoutConstraint!
    @IBOutlet fileprivate var wordCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet fileprivate var consonantCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var vowelCollectionViewHeightConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate lazy var viewModel: ExerciseBuildingViewModel = {
        return ExerciseBuildingViewModel(withPhase: phase, phaseExercise: phaseExercise)
    }()
    fileprivate var currentRecognizedState: KIOSRecognizerState = .needsDecodingGraph {
        didSet {
            audioButton.isUserInteractionEnabled = currentRecognizedState != .listening
        }
    }
    fileprivate var failTimer: Timer?
    fileprivate var letterPadding: CGFloat = 2
    fileprivate var letterSize: CGSize = .zero
    fileprivate var currentConsonantsCollectionViewWidth: CGFloat = 0
    fileprivate var indexPathInFocus: IndexPath?
    
    // Flags
    fileprivate var isPlayingInstruction: Bool = false {
        didSet {
            audioButton.isSelected = isPlayingInstruction
            recordButton.isUserInteractionEnabled = !isPlayingInstruction
        }
    }
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
        updateContent()
        updateState()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if consonantCollectionView.frame.width != currentConsonantsCollectionViewWidth {
            currentConsonantsCollectionViewWidth = consonantCollectionView.frame.width
            // Update layout
            letterSize = makeCollectionViewItemSize()
            updateLayout()
            reloadCollectionViews()
        }
    }
    
    override func handleInstructionAction(_ action: ExerciseInstructionsViewController.Action) {
        guard action == .start else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+0.4) {
            self.playCurrentInstruction()
        }
    }

}

// MARK: - Targets
extension ExerciseBuildingViewController {
    
    @IBAction fileprivate func closeButtonPressed(_ sender: UIButton) {
        guard viewModel.isPendingFinalResult == false else {
            return
        }
        SoundPlayer.shared.stop()
        SpeechRecognition.shared.stopListening(withResults: false)
        if viewModel.isCompleted == false {
            logExerciseCancelEvent()
        }
        dismiss(animated: true)
    }
    
    @IBAction fileprivate func recordButtonPressed(_ sender: UIButton) {
        if SpeechRecognition.shared.state == .listening {
            viewModel.isPendingFinalResult = true
            SpeechRecognition.shared.stopListening()
            logMicEvent(withState: false)
        } else {
            SpeechRecognition.shared.startListening()
            logMicEvent(withState: true)
        }
    }
    
    @IBAction fileprivate func audioButtonPressed(_ sender: UIButton) {
        guard isPlayingInstruction == false else {
            return
        }
        playCurrentInstruction()
    }
    
    @IBAction fileprivate func helpButtonPressed(_ sender: UIButton) {
        viewModel.markTextAsIsSuggestable()
        updateContent()
        logHelpEvent()
    }
    
    @IBAction fileprivate func completeButtonPressed(_ sender: UIButton) {
        guard feedbackViewController == nil else {
            return
        }
        let score = viewModel.makeLessonScore()
        let debugInfo = viewModel.exerciseScoreDebugInfo()
        presentFeedbackViewController(withDelegate: self, score: score, debugInfo: debugInfo)
    }
    
    @objc fileprivate func failTimerDidTrigger(_ sender: Timer) {
        viewModel.failCurrentText()
        if SpeechRecognition.shared.state == .listening {
            viewModel.isPendingFinalResult = true
            SpeechRecognition.shared.stopListening()
        }
    }
    
}

// MARK: - UICollectionViewDelegate
extension ExerciseBuildingViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let type = CollectionViewType(rawValue: collectionView.tag)!
        switch type {
        case .text:
            return viewModel.maskLetters.count
        case .consonants:
            return viewModel.consonants.count
        case .vowels:
            return viewModel.vowels.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell: UICollectionViewCell!
        let type = CollectionViewType(rawValue: collectionView.tag)!
        switch type {
        case .text:
            let letterCell = collectionView.dequeueReusableCell(withReuseIdentifier: LetterDroppableCollectionViewCell.identifier,
                                                              for: indexPath) as! LetterDroppableCollectionViewCell
            let letter = viewModel.maskLetters[indexPath.row]
            let isTextRecognized = viewModel.isTextStatusEqual(.recognized)
            letterCell.update(withLetter: letter, style: .white,
                              onlyFocus: !isTextRecognized,
                              isHovering: indexPathInFocus == indexPath)
            letterCell.padding = letterPadding
            cell = letterCell
        case .consonants:
            let consonantCell = collectionView.dequeueReusableCell(withReuseIdentifier: LetterDraggableCollectionViewCell.identifier,
                                                                    for: indexPath) as! LetterDraggableCollectionViewCell
            let letter = viewModel.consonants[indexPath.row]
            consonantCell.update(withLetter: letter)
            consonantCell.dragDelegate = self
            cell = consonantCell
        case .vowels:
            let vowelCell = collectionView.dequeueReusableCell(withReuseIdentifier: LetterDraggableCollectionViewCell.identifier,
                                                               for: indexPath) as! LetterDraggableCollectionViewCell
            let letter = viewModel.vowels[indexPath.row]
            vowelCell.update(withLetter: letter)
            vowelCell.dragDelegate = self
            cell = vowelCell
        }
        cell.tag = collectionView.tag
        return cell
    }
    
}

// MARK: - UICollectionViewDelegate
extension ExerciseBuildingViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let type = CollectionViewType(rawValue: collectionView.tag)!
        guard type == .text,
            viewModel.hasTextLetter(atIndex: indexPath.row, withStatus: .none) else {
            return
        }
        viewModel.removeTextMaskLetter(atIndex: indexPath.row)
        collectionView.reloadItems(at: [indexPath])
    }
    
}

// MARK: - UICollectionViewDropDelegate
extension ExerciseBuildingViewController: UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        let type = CollectionViewType(rawValue: collectionView.tag)!
        guard type == .text,
            let _ = session.items.first?.localObject as? LetterView else {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession,
                        withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        guard let _indexPath = destinationIndexPath,
            let letterView = session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return UICollectionViewDropProposal(operation: .cancel)
        }
        if indexPathInFocus != _indexPath {
            indexPathInFocus = _indexPath
            collectionView.reloadData()
        }
        guard viewModel.hasTextLetter(atIndex: _indexPath.row, withStatus: .none),
            viewModel.hasTextLetter(atIndex: _indexPath.row, equalTo: letter.value) else {
            Analytics.logLetterDropFail(letter.value)
            return UICollectionViewDropProposal(operation: .cancel)
        }
        Analytics.logLetterDropSuccess(letter.value)
        return UICollectionViewDropProposal(operation: .move)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard let indexPath = coordinator.destinationIndexPath else {
            return
        }
        viewModel.changeTextLetterStatus(forIndex: indexPath.row, status: .selected)
        collectionView.reloadData()
        if viewModel.hasAnyTextLetter(withStatus: .none) == false {
            SoundPlayer.shared.stop()
            DispatchQueue.main.asyncAfter(deadline: .now()+0.15) {
                SpeechRecognition.shared.startListening()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidEnd session: UIDropSession) {
        indexPathInFocus = nil
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidExit session: UIDropSession) {
        indexPathInFocus = nil
        collectionView.reloadData()
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExerciseBuildingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let type = CollectionViewType(rawValue: collectionView.tag)!
        switch type {
        case .text:
            let realLetterSize = CGSize(width: letterSize.width+letterPadding*2,
                                        height: letterSize.height+letterPadding*2)
            return realLetterSize
        default:
            return letterSize
        }
    }
    
}

// MARK: - UIDragInteractionDelegate
extension ExerciseBuildingViewController: UIDragInteractionDelegate {
    
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        guard let cell = interaction.view as? LetterDraggableCollectionViewCell,
            let letter = cell.letterView.letter else {
            return []
        }
        let provider = NSItemProvider(object: letter)
        let item = UIDragItem(itemProvider: provider)
        item.localObject = cell.letterView
        return [item]
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
        guard let letterView = item.localObject as? LetterView else {
            return nil
        }
        // Make preview view
        let _letterView = makeLetterView(withLetter: letterView.letter!, size: letterSize)
        _letterView.frame = letterView.frame
        // Make preview parameters
        let previewParameters = UIDragPreviewParameters()
        previewParameters.visiblePath = UIBezierPath(roundedRect: _letterView.bounds, cornerRadius: 7)
        previewParameters.backgroundColor = .clear
        // Make preview target
        let center = CGPoint(x: _letterView.bounds.midX, y: _letterView.bounds.midY)
        let target = UIDragPreviewTarget(container: letterView, center: center)
        return UITargetedDragPreview(view: _letterView, parameters: previewParameters, target: target)
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, sessionWillBegin session: UIDragSession) {
        guard let letterView = session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return
        }
        letterView.alpha = 0.6
        Analytics.logLetterLift(letter.value)
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, previewForCancelling item: UIDragItem,
                         withDefault defaultPreview: UITargetedDragPreview) -> UITargetedDragPreview? {
        return defaultPreview
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, session: UIDragSession, didEndWith operation: UIDropOperation) {
        guard let letterView = session.items.first?.localObject as? LetterView else {
            return
        }
        letterView.alpha = 1
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, sessionIsRestrictedToDraggingApplication session: UIDragSession) -> Bool {
        return true
    }
    
}

// MARK: - SpeechRecognitionDelegate
extension ExerciseBuildingViewController: SpeechRecognitionDelegate {
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, didChange state: KIOSRecognizerState) {
        currentRecognizedState = state
        if let _timer = failTimer {
            _timer.invalidate()
        }
        updateState()
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, partial result: KIOSResult) {
        let hasNewResults = viewModel.processSpeechRecognizerPartialResult(result)
        if hasNewResults {
            Analytics.logVoiceRecognitionSuccess(result.cleanText)
        } else {
            Analytics.logVoiceRecognitionFailed(result.cleanText)
        }
        guard hasNewResults else {
            return
        }
        DispatchQueue.main.async {
            if self.viewModel.hasAnyTextLetter(withStatus: .selected) == false {
                if self.viewModel.isTextStatusEqual(.none) {
                    self.scheduleFailTimer()
                } else if SpeechRecognition.shared.state == .listening {
                    self.viewModel.isPendingFinalResult = true
                    SpeechRecognition.shared.stopListening()
                }
            }
            self.textCollectionView.reloadData()
        }
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, final result: KIOSResult) {
        guard viewModel.isPendingFinalResult else {
            return
        }
        viewModel.processSpeechRecognizerFinalResult(result)
        DispatchQueue.main.async {
            self.viewModel.isPendingFinalResult = false
            self.proceedAfterFinalResults(withDelay: 0.5)
        }
    }
    
    func speechRecognitionReadyToListenAfterInterruption(_ speechRecognition: SpeechRecognition) {
        updateState()
    }
    
}

// MARK: - ExerciseFeedbackViewControllerDelegate
extension ExerciseBuildingViewController: ExerciseFeedbackViewControllerDelegate {
    
    func exerciseFeedbackViewController(_ viewController: ExerciseFeedbackViewController,
                                           didSelect action: ExerciseFeedbackViewController.Action) {
        guard let _delegate = delegate else {
            return
        }
        if action == .next {
            _delegate.exerciseViewController(self, didSelect: .phase)
        } else if action == .home {
            _delegate.exerciseViewController(self, didSelect: .home)
        }
    }
    
}

// MARK: - UIGestureRecognizerDelegate
extension ExerciseBuildingViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let _view = touch.view,
            _view.isDescendant(of: textCollectionView) {
            return false
        }
        return true
    }

}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseBuildingViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK - Private Functions
extension ExerciseBuildingViewController {
    
    // MARK: Enums
    
    fileprivate enum CollectionViewType: Int {
        case text = 1
        case consonants
        case vowels
    }
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        // Speech recognition
        SpeechRecognition.shared.delegate = self
        if let _exercise = phaseExercise.exercise as? ExerciseBuilding {
            SpeechRecognition.shared.prepareForListening(withDecodingGraph: _exercise.identifier)
        }
        // Other
        footerView.shadowOffset = CGSize(width: 0, height: 6)
        textCollectionView.register(UINib(withType: LetterDroppableCollectionViewCell.self),
                                    forCellWithReuseIdentifier: LetterDroppableCollectionViewCell.identifier)
        consonantCollectionView.register(UINib(withType: LetterDraggableCollectionViewCell.self),
                                         forCellWithReuseIdentifier: LetterDraggableCollectionViewCell.identifier)
        vowelCollectionView.register(UINib(withType: LetterDraggableCollectionViewCell.self),
                                     forCellWithReuseIdentifier: LetterDraggableCollectionViewCell.identifier)
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            if _self.viewModel.isPendingFinalResult {
                _self.viewModel.isPendingFinalResult = false
                _self.proceedAfterFinalResults(withDelay: 0)
            }
        }
    }
    
    fileprivate func reloadCollectionViews() {
        textCollectionView.reloadData()
        consonantCollectionView.reloadData()
        vowelCollectionView.reloadData()
    }
    
    fileprivate func playCurrentInstruction() {
        guard let _page = viewModel.currentPage(),
            let audioUrl = _page.instructionAudioUrl,
            let fileUrl = FileProvider.shared.fileUrl(forUrlString: audioUrl) else {
            return
        }
        let sound = Sound(withUrl: fileUrl)
        try? SoundPlayer.shared.play(sound: sound) { [weak self] status in
            guard let _self = self else {
                return
            }
            _self.isPlayingInstruction = status == .playing
        }
    }
    
    fileprivate func scheduleFailTimer() {
        if let _timer = failTimer {
            _timer.invalidate()
        }
        failTimer = Timer.scheduledTimer(timeInterval: 8.0, target: self,
                                              selector: #selector(failTimerDidTrigger(_:)), userInfo: nil, repeats: false)
    }
    
    fileprivate func proceedAfterFinalResults(withDelay delay: TimeInterval) {
        if viewModel.isCurrentTextCompleted() {
            // Text and letters have been recognized
            // Add some delay so user can see that text has been recognized
            DispatchQueue.main.asyncAfter(deadline: .now()+delay) {
                if self.viewModel.moveToNextPage() {
                    self.textCollectionView.reloadData()
                    self.updateTextCollectionViewLayout()
                    self.playCurrentInstruction()
                } else {
                    self.logExerciseCompletedEvent()
                    self.viewModel.completeLesson()
                }
                self.updateContent()
                self.updateState()
            }
        } else {
            updateState()
        }
    }
    
    // MARK: Update
    
    fileprivate func updateContent() {
        if let _lesson = viewModel.buildingLesson {
            progressView.update(withIndex: viewModel.index,
                                count: _lesson.pages.count,
                                isCompleted: viewModel.isCompleted)
        }
        if let _text = viewModel.text, _text.isSuggested,
            let _instruction = viewModel.instructionText(forIndex: viewModel.index) {
            infoLabel.text = _instruction
        } else {
            infoLabel.text = nil
        }
    }
    
    fileprivate func updateState() {
        if viewModel.text == nil ||
            viewModel.isCompleted ||
            viewModel.hasAnyTextLetter(withStatus: .none) ||
            SpeechRecognition.shared.state == .needsDecodingGraph {
            recordButton.isHidden = true
        } else {
            recordButton.isHidden = false
        }
        if SpeechRecognition.shared.state == .readyToListen {
            recordButton.isSelected = false
        } else {
            recordButton.isSelected = true
        }
        if viewModel.isPendingFinalResult {
            recordButton.isUserInteractionEnabled = false
            activityIndicator.startAnimating()
        } else {
            recordButton.isUserInteractionEnabled = true
            activityIndicator.stopAnimating()
        }
        if viewModel.isCompleted {
            audioButton.isHidden = true
            helpButton.isHidden = true
        } else {
            audioButton.isHidden = false
            helpButton.isHidden = false
        }
        if viewModel.isPendingFinalResult ||
            viewModel.isCompleted == false {
            completeButton.isHidden = true
        } else {
            completeButton.isHidden = false
        }
    }

    fileprivate func updateLayout() {
        updateTextCollectionViewLayout()
        let consonantsCollectionViewLayout = consonantCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        consonantCollectionViewHeightConstraint.constant = letterSize.height * 3 + consonantsCollectionViewLayout.minimumLineSpacing * 2
        vowelCollectionViewHeightConstraint.constant = letterSize.height
    }
    
    fileprivate func updateTextCollectionViewLayout() {
        guard let _text = viewModel.text else {
            return
        }
        let textCollectionViewLayout = textCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        textCollectionViewWidth.constant = {
            let count = CGFloat(_text.letters.count)
            let itemWidth = letterSize.width + letterPadding*2
            let totalItemWidth = count * itemWidth
            if count < 2 {
                return totalItemWidth
            }
            let totalSpaceWidth = (count-1) * textCollectionViewLayout.minimumInteritemSpacing
            return totalItemWidth + totalSpaceWidth
        }()
        wordCollectionViewHeight.constant = letterSize.height + letterPadding*2
    }
    
    // MARK: Make
    
    fileprivate func makeCollectionViewItemSize() -> CGSize {
        guard viewModel.consonants.isEmpty == false else {
            return .zero
        }
        let itemsPerLine = ceil(Double(viewModel.consonants.count)/3.0)
        let layout = consonantCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpacingWidth = CGFloat((itemsPerLine-1)*Double(layout.minimumInteritemSpacing))
        let availableWidth = consonantCollectionView.frame.width-totalSpacingWidth
        var size = floor(availableWidth/CGFloat(itemsPerLine))
        if size > view.frame.width/10 {
            size = view.frame.width/10
        } else if size < 0 {
            size = 0
        }
        return CGSize(width: size, height: size)
    }
    
    fileprivate func makeLetterView(withLetter letter: ExerciseLetter, size: CGSize) -> LetterView {
        let frame = CGRect(origin: .zero, size: size)
        let view = LetterView(frame: frame)
        view.letter = letter
        return view
    }

}
