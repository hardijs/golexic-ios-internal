//
//  ExerciseBuildingViewModel.swift
//  GoLexic
//
//  Created by Armands L. on 29/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import Cubemobile

class ExerciseBuildingViewModel {
    
    // MARK: Properties
    
    // Variables
    var phase: Phase
    var phaseExercise: PhaseExercise
    fileprivate lazy var lesson: PhaseExerciseLesson? = {
        return makePhaseExerciseLesson(fromPhaseExercise: phaseExercise)
    }()
    fileprivate(set) lazy var exercise: ExerciseBuilding? = {
        return phaseExercise.exercise as? ExerciseBuilding
    }()
    fileprivate(set) lazy var buildingLesson: ExerciseBuildingLesson? = {
        return lesson?.lesson as? ExerciseBuildingLesson
    }()
    fileprivate(set) var index: Int = 0
    fileprivate(set) var text: ExerciseText?
    fileprivate(set) var currentText: ExerciseText?
    fileprivate(set) var maskLetters: [ExerciseLetter] = []
    fileprivate var results: [ExerciseText] = []
    
    lazy var letters: [ExerciseLetter] = {
        return makeExerciseLetters(fromPhaseExercise: phaseExercise)
    }()
    lazy var vowels: [ExerciseLetter] = {
        return makeVowels(fromLetters: letters)
    }()
    lazy var consonants: [ExerciseLetter] = {
        return makeConsonants(fromLetters: letters)
    }()
    
    fileprivate lazy var speechProcessor: SpeechProcessor = {
        return SpeechProcessor()
    }()
    
    // Flags
    fileprivate(set) var isCompleted: Bool = false
    var isPendingFinalResult: Bool = false
    
    // MARK: Initialziers
    
    init(withPhase phase: Phase, phaseExercise: PhaseExercise) {
        self.phase = phase
        self.phaseExercise = phaseExercise
        updateCurrentText()
    }
    
}

// MARK: - Public
extension ExerciseBuildingViewModel {
    
    // MARK: Lessons
    
    func completeLesson() {
        guard isCompleted == false,
            let _lesson = lesson else {
            return
        }
        isCompleted = true
        let realm = try! Realm()
        try! realm.write {
            phaseExercise.markLessonAsCompleted(_lesson)
        }
        if let scoring = scoring(forTexts: results) {
            let count = makeCount(forTexts: results, withConfidence: scoring.confidence)
            submitResults(withScore: count, scoring: scoring)
        }
        submitStatistics(withResults: results)
    }
    
    func makeLessonScore() -> Score {
        guard let scoring = scoring(forTexts: results) else {
            return .none
        }
        return scoring.score
    }
    
    // MARK: Page
    
    func currentPage() -> ExerciseBuildingLessonPage? {
        guard let _buildingLesson = buildingLesson,
            index < _buildingLesson.pages.count else {
            return nil
        }
        return _buildingLesson.pages[index]
    }
    
    func hasNextPage() -> Bool {
        guard let _buildingLesson = buildingLesson else {
            return false
        }
        let nextIndex = index + 1
        return nextIndex < _buildingLesson.pages.count
    }
    
    @discardableResult
    func moveToNextPage() -> Bool {
        guard let _buildingLesson = buildingLesson else {
            return false
        }
        let nextPageIndex = index + 1
        guard nextPageIndex < _buildingLesson.pages.count else {
            return false
        }
        index = nextPageIndex
        updateCurrentText()
        return true
    }
    
    // MARK: Text
    
    func isCurrentTextCompleted() -> Bool {
        guard let _text = text,
            hasAnyTextLetter(withStatus: .selected) == false else {
            return false
        }
        return _text.status == .recognized || _text.isFailed
    }
    
    func isTextStatusEqual(_ status: RecognitionStatus) -> Bool {
        guard let _text = text else {
            return false
        }
        return _text.status == status
    }
    
    func hasTextLetter(atIndex index: Int, withStatus status: RecognitionStatus) -> Bool {
        guard let _text = text, index >= 0,
            index < _text.letters.count else {
            return false
        }
        let letter = _text.letters[index]
        return letter.status == status
    }
    
    func hasTextLetter(atIndex index: Int, equalTo value: String) -> Bool {
        guard let _text = text, index >= 0,
            index < _text.letters.count else {
            return false
        }
        let letter = _text.letters[index]
        return letter.value == value
    }
    
    func hasAnyTextLetter(withStatus status: RecognitionStatus) -> Bool {
        guard let _text = text else {
            return false
        }
        for letter in _text.letters {
            if letter.status == status {
                return true
            }
        }
        return false
    }
    
    func changeTextLetterStatus(forIndex index: Int, status: RecognitionStatus) {
        guard let _text = text, index >= 0,
            index < _text.letters.count else {
            return
        }
        let letter = _text.letters[index]
        letter.status = status
        maskLetters[index] = letter
    }
    
    func removeTextMaskLetter(atIndex index: Int) {
        guard index >= 0,
            index < maskLetters.count else {
            return
        }
        let letter = ExerciseLetter(withValue: "")
        maskLetters[index] = letter
        changeTextLetterStatus(forIndex: index, status: .none)
    }
    
    func markTextAsIsSuggestable() {
        guard let _text = text else {
            return
        }
        _text.isSuggested = true
    }
    
    func failCurrentText() {
        guard let _text = text else {
            return
        }
        _text.isFailed = true
    }
    
    // MARK: Speech Recognition
    
    @discardableResult
    func processSpeechRecognizerPartialResult(_ result: KIOSResult) -> Bool {
        guard let _text = text else {
            return false
        }
        var isNewSearch: Bool = false
        if currentText?.identifier != _text.identifier {
            currentText = _text
            isNewSearch = true
        }
        var hasNewRecognitions: Bool = false
        var hasUnrecognizedLetters: Bool = false
        let words = speechProcessor.processResult(result, isNewSearch: isNewSearch)
        for letter in _text.letters {
            guard letter.status == .selected else {
                continue
            }
            hasUnrecognizedLetters = true
            guard letter.isEqualToAny(words, includeSimilarities: true) else {
                break
            }
            letter.status = .recognized
            hasNewRecognitions = true
            break
        }
        if hasUnrecognizedLetters == false,
           _text.isEqualToAny(words, includeSimilarities: false) {
            _text.status = .recognized
            hasNewRecognitions = true
        }
        return hasNewRecognitions
    }
    
    func processSpeechRecognizerFinalResult(_ result: KIOSResult) {
        for text in results {
            guard text.status == .recognized else {
                continue
            }
            text.updateConfidenceIfMatches(result.words, includeSimilarities: false)
        }
        speechProcessor.reset()
    }
    
    // MARK: Other
    
    func instructionText(forIndex index: Int) -> String? {
        guard let _buildingLesson = buildingLesson,
            index < _buildingLesson.pages.count else {
            return nil
        }
        let _page = _buildingLesson.pages[index]
        return _page.instructionText
    }
    
    func exerciseScoreDebugInfo() -> String? {
        guard let scoring = scoring(forTexts: results) else {
            return nil
        }
        let count = makeCount(forTexts: results, withConfidence: scoring.confidence)
        let convertedConfidence = Int(scoring.confidence*100)
        return "Completed with \(count)/\(results.count) avaraging above \(convertedConfidence)%"
    }
    
}

// MARK: - Private Functions
extension ExerciseBuildingViewModel {
    
    // MARK: Helpers
    
    fileprivate func updateCurrentText() {
        guard let _buildingLesson = buildingLesson,
            index < _buildingLesson.pages.count else {
            return
        }
        let _page = _buildingLesson.pages[index]
        text = ExerciseText(withValue: _page.text)
        results.append(text!)
        // Create mask letters & select letters matching mask
        var _maskLetters: [ExerciseLetter] = []
        for x in 0..<text!.letters.count {
            let letter = text!.letters[x]
            let maskValue = _page.maskValue(forIndex: x)
            if letter.value == maskValue {
                letter.status = .selected
                _maskLetters.append(letter)
            } else {
                let maskLetter = ExerciseLetter(withValue: maskValue ?? "")
                if maskLetter.value.isEmpty == false {
                    maskLetter.status = .selected
                }
                _maskLetters.append(maskLetter)
            }
        }
        maskLetters = _maskLetters
    }
    
    fileprivate func scoring(forTexts texts: [ExerciseText]) -> ExerciseBuildingScoring? {
        guard let _exercise = exercise else {
            return nil
        }
        let sortedScorings = _exercise.scorings.sorted(byKeyPath: "threshold", ascending: false)
        for scoring in sortedScorings {
            let passedTexts = makeCount(forTexts: texts, withConfidence: scoring.confidence)
            let score: Double = {
                guard passedTexts > 0 else {
                    return 9
                }
                return Double(results.count/passedTexts)
            }()
            if score >= scoring.threshold {
                return scoring
            }
        }
        return nil
    }
    
    fileprivate func submitResults(withScore score: Int, scoring: ExerciseBuildingScoring) {
        let params = makeResultsBody(withScore: score, scoring: scoring)
        let target = GoLexicTarget(withEndpoint: .buildingResults, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    fileprivate func submitStatistics(withResults results: [ExerciseText]) {
        guard let _exercise = exercise,
            let _wordScoring = _exercise.wordScoring else {
            return
        }
        let params = makeStatisticsBody(withResults: results, wordScoring: _wordScoring)
        let target = GoLexicTarget(withEndpoint: .statisticsWords, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    // MARK: Make
    
    fileprivate func makePhaseExerciseLesson(fromPhaseExercise phaseExercise: PhaseExercise) -> PhaseExerciseLesson? {
        for lesson in phaseExercise.exerciseLessons {
            if lesson.isCompleted == false {
                return lesson
            }
        }
        return nil
    }
    
    fileprivate func makeExerciseLetters(fromPhaseExercise phaseExercise: PhaseExercise) -> [ExerciseLetter] {
        var _letters: [ExerciseLetter] = []
        guard let _exercise = phaseExercise.exercise as? ExerciseBuilding,
            let _exerciseLetters = _exercise.letters else {
            return _letters
        }
        for item in _exerciseLetters.items {
            let letter = ExerciseLetter(withLetterItem: item)
            _letters.append(letter)
        }
        return _letters
    }
    
    fileprivate func makeVowels(fromLetters letters: [ExerciseLetter]) -> [ExerciseLetter] {
        var _vowels: [ExerciseLetter] = []
        for letter in letters {
            if letter.isVowel {
                _vowels.append(letter)
            }
        }
        return _vowels
    }
    
    fileprivate func makeConsonants(fromLetters letters: [ExerciseLetter]) -> [ExerciseLetter] {
        var _consonants: [ExerciseLetter] = []
        for letter in letters {
            if letter.isVowel == false {
                _consonants.append(letter)
            }
        }
        return _consonants
    }
    
    fileprivate func makeCount(forTexts texts: [ExerciseText], withConfidence confidence: Double) -> Int {
        var count: Int = 0
        for text in texts {
            guard text.isFailed == false else {
                continue
            }
            if text.confidence >= confidence {
                count += 1
            }
        }
        return count
    }
    
    fileprivate func makeResultsBody(withScore score: Int, scoring: ExerciseBuildingScoring) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        if let _buildingLesson = buildingLesson {
            body.updateValue(_buildingLesson.identifier, forKey: "lessonId")
        }
        body.updateValue(scoring.identifier, forKey: "scoringId")
        body.updateValue(score, forKey: "score")
        return body
    }
    
    fileprivate func makeStatisticsBody(withResults results: [ExerciseText], wordScoring: ScoringWord) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(wordScoring.identifier, forKey: "wordScoringId")
        var _letters: [[String: Any]] = []
        for result in results {
            let item: [String : Any] = ["word": result.value,
                                        "score": result.confidence]
            _letters.append(item)
        }
        body.updateValue(_letters, forKey: "words")
        return body
    }
    
}
