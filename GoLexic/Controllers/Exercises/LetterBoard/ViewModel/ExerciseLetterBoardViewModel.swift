//
//  ExerciseLetterBoardViewModel.swift
//  GoLexic
//
//  Created by Armands L. on 12/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import Cubemobile

class ExerciseLetterBoardViewModel {
    
    // MARK: Properties
    
    // Variables
    var phase: Phase
    var phaseExercise: PhaseExercise
    fileprivate(set) lazy var exercise: ExerciseLetterBoard? = {
        return phaseExercise.exercise as? ExerciseLetterBoard
    }()
    fileprivate(set) lazy var letters: [ExerciseLetter] = {
        return makeExerciseLetters(fromExercise: exercise)
    }()
    fileprivate(set) lazy var vowels: [ExerciseLetter] = {
        return makeVowels(fromLetters: letters)
    }()
    fileprivate(set) lazy var consonants: [ExerciseLetter] = {
        return makeConsonants(fromLetters: letters)
    }()
    
    // Flags
    fileprivate(set) var isCompleted: Bool = false
    
    // MARK: Initialziers
    
    init(withPhase phase: Phase, phaseExercise: PhaseExercise) {
        self.phase = phase
        self.phaseExercise = phaseExercise
    }
    
}

// MARK: - Public Functions
extension ExerciseLetterBoardViewModel {
    
    // MARK: Exercise
    
    @discardableResult
    func completeExercise() -> Score {
        guard isCompleted == false else {
            return makeExerciseScore()
        }
        isCompleted = true
        let realm = try! Realm()
        try! realm.write {
            phaseExercise.markAsCompleted()
        }
        let count = countLetters(withStatus: .selected)
        submitResults(withScore: count)
        submitStatistics(withResults: letters)
        return makeExerciseScore()
    }
    
    func makeExerciseScore() -> Score {
        guard let _exercise = exercise else {
            return .none
        }
        let passedLetters = countLetters(withStatus: .selected)
        let isExercisePassed = passedLetters >= _exercise.passThreshold
        return isExercisePassed ? .success : .failed
    }
    
    // MARK: Letter
    
    func isLetterNextInLine(_ letter: ExerciseLetter, withStatus status: RecognitionStatus) -> Bool {
        var _nextLetter: ExerciseLetter?
        for letter in letters {
            if letter.status == status {
                _nextLetter = letter
                break
            }
        }
        guard letter == _nextLetter else {
            return false
        }
        return true
    }
    
    func hasAnyLetter(withStatus status: RecognitionStatus) -> Bool {
        for letter in letters {
            if letter.status == status {
                return true
            }
        }
        return false
    }
    
    // MARK: Other
    
    func markNextUnrecognizedLetterAsIsSuggestable() {
        for letter in letters {
            guard letter.status == .none else {
                continue
            }
            letter.isFailed = true
            letter.isSuggested = true
            break
        }
    }
    
    func exerciseScoreDebugInfo() -> String? {
        let passedLetters = countLetters(withStatus: .selected)
        return "Completed \(passedLetters)/\(letters.count) letters"
    }
    
}

// MARK: - Private Functions
extension ExerciseLetterBoardViewModel {
    
    // MARK: Helpers
    
    fileprivate func countLetters(withStatus status: RecognitionStatus) -> Int {
        var count: Int = 0
        for letter in letters {
            guard letter.isFailed == false,
                letter.status == status else {
                continue
            }
            count += 1
        }
        return count
    }
    
    fileprivate func submitResults(withScore score: Int) {
        let params = makeResultsBody(withScore: score)
        let target = GoLexicTarget(withEndpoint: .letterBoardResults, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    fileprivate func submitStatistics(withResults results: [ExerciseLetter]) {
        guard let _exercise = exercise,
            let _letterScoring = _exercise.letterScoring else {
            return
        }
        let params = makeStatisticsBody(withResults: results, letterScoring: _letterScoring)
        let target = GoLexicTarget(withEndpoint: .statisticsLetters, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    // MARK: Make
    
    fileprivate func makeExerciseLetters(fromExercise exercise: ExerciseLetterBoard?) -> [ExerciseLetter] {
        var _letters: [ExerciseLetter] = []
        guard let _exercise = exercise,
            let _exerciseLetters = _exercise.letters else {
            return _letters
        }
        for item in _exerciseLetters.items {
            let letter = ExerciseLetter(withLetterItem: item)
            _letters.append(letter)
        }
        return _letters
    }
    
    fileprivate func makeVowels(fromLetters letters: [ExerciseLetter]) -> [ExerciseLetter] {
        var _vowels: [ExerciseLetter] = []
        for letter in letters {
            if letter.isVowel {
                _vowels.append(letter)
            }
        }
        return _vowels
    }
    
    fileprivate func makeConsonants(fromLetters letters: [ExerciseLetter]) -> [ExerciseLetter] {
        var _consonants: [ExerciseLetter] = []
        for letter in letters {
            if letter.isVowel == false {
                _consonants.append(letter)
            }
        }
        return _consonants
    }
    
    fileprivate func makeResultsBody(withScore score: Int) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(score, forKey: "score")
        return body
    }
    
    fileprivate func makeStatisticsBody(withResults results: [ExerciseLetter], letterScoring: ScoringLetter) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(letterScoring.identifier, forKey: "letterScoringId")
        var _letters: [[String: Any]] = []
        for result in results {
            let score: Double = {
                guard result.isFailed == false,
                    result.status == .selected else {
                    return 0
                }
                return 1
            }()
            let item: [String : Any] = ["letter": result.value.uppercased(),
                                        "score": score]
            _letters.append(item)
        }
        body.updateValue(_letters, forKey: "letters")
        return body
    }
    
}
