//
//  ExerciseLetterBoardViewController.swift
//  GoLexic
//
//  Created by Armands L. on 12/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ExerciseLetterBoardViewController: ExerciseViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var contentView: AttributedView!
    @IBOutlet fileprivate var consonantTitleLabel: UILabel!
    @IBOutlet fileprivate var consonantCollectionView: UICollectionView!
    @IBOutlet fileprivate var vowelTitleLabel: UILabel!
    @IBOutlet fileprivate var vowelCollectionView: UICollectionView!
    @IBOutlet fileprivate var letterPickerView: LetterPickerView!
    
    // Constraints
    @IBOutlet fileprivate var consonantCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var vowelCollectionViewHeightConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate lazy var viewModel: ExerciseLetterBoardViewModel = {
        return ExerciseLetterBoardViewModel(withPhase: phase, phaseExercise: phaseExercise)
    }()
    fileprivate var letterPadding: CGFloat = 8
    fileprivate var letterSize: CGSize = .zero
    fileprivate var currentConsonantsCollectionViewWidth: CGFloat = 0
    fileprivate var collectionViewInFocus: CollectionViewType?
    fileprivate var indexPathInFocus: IndexPath?
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        updateContent()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if consonantCollectionView.frame.width != currentConsonantsCollectionViewWidth {
            currentConsonantsCollectionViewWidth = consonantCollectionView.frame.width
            // Update layout
            letterSize = makeCollectionViewItemSize()
            consonantCollectionView.reloadData()
            vowelCollectionView.reloadData()
            updateLayout()
        }
    }

}

// MARK: - Targets
extension ExerciseLetterBoardViewController {
    
    @IBAction fileprivate func closeButtonPressed(_ sender: UIButton) {
        logExerciseCancelEvent()
        dismiss(animated: true)
    }
    
    @IBAction fileprivate func helpButtonPressed(_ sender: UIButton) {
        viewModel.markNextUnrecognizedLetterAsIsSuggestable()
        letterPickerView.updateAppearances()
        logHelpEvent()
    }
    
}

// MARK: - UICollectionViewDelegate
extension ExerciseLetterBoardViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let type = CollectionViewType(rawValue: collectionView.tag)!
        switch type {
        case .consonants:
            return viewModel.consonants.count
        case .vowels:
            return viewModel.vowels.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LetterDroppableCollectionViewCell.identifier,
                                                      for: indexPath) as! LetterDroppableCollectionViewCell
        let type = CollectionViewType(rawValue: collectionView.tag)!
        let inFocus = collectionViewInFocus == type
        switch type {
        case .consonants:
            let letter = viewModel.consonants[indexPath.row]
            cell.update(withLetter: letter, style: .suggested,
                        isHovering: inFocus && indexPathInFocus == indexPath)
        case .vowels:
            let letter = viewModel.vowels[indexPath.row]
            cell.update(withLetter: letter, style: .suggested,
                        isHovering: inFocus && indexPathInFocus == indexPath)
        }
        cell.padding = letterPadding
        return cell
    }
    
}

// MARK: - UICollectionViewDropDelegate
extension ExerciseLetterBoardViewController: UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        guard let _ = session.items.first?.localObject as? LetterView else {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession,
                        withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        guard let letterView = session.items.first?.localObject as? LetterView, let letter = letterView.letter,
            viewModel.isLetterNextInLine(letter, withStatus: .none), let _indexPath = destinationIndexPath else {
                return UICollectionViewDropProposal(operation: .cancel)
        }
        let type = CollectionViewType(rawValue: collectionView.tag)!
        if indexPathInFocus != _indexPath || collectionViewInFocus != type {
            collectionViewInFocus = type
            indexPathInFocus = _indexPath
            collectionView.reloadData()
        }
        if type == .consonants, _indexPath.row >= viewModel.consonants.count {
            return UICollectionViewDropProposal(operation: .cancel)
        } else if type == .vowels, _indexPath.row >= viewModel.vowels.count {
            return UICollectionViewDropProposal(operation: .cancel)
        }
        switch type {
        case .consonants:
            let targetLetter = viewModel.consonants[_indexPath.row]
            if letter == targetLetter {
                Analytics.logLetterDropSuccess(letter.value)
                return UICollectionViewDropProposal(operation: .move)
            } else {
                Analytics.logLetterDropFail(letter.value)
                return UICollectionViewDropProposal(operation: .cancel)
            }
        case .vowels:
            let targetLetter = viewModel.vowels[_indexPath.row]
            if letter == targetLetter {
                Analytics.logLetterDropSuccess(letter.value)
                return UICollectionViewDropProposal(operation: .move)
            } else {
                Analytics.logLetterDropFail(letter.value)
                return UICollectionViewDropProposal(operation: .cancel)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard let letterView = coordinator.session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return
        }
        letter.status = .selected
        collectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
            if self.feedbackViewController == nil,
                self.viewModel.hasAnyLetter(withStatus: .none) == false {
                self.logExerciseCompletedEvent()
                let score = self.viewModel.completeExercise()
                let debugInfo = self.viewModel.exerciseScoreDebugInfo()
                self.presentFeedbackViewController(withDelegate: self, score: score, debugInfo: debugInfo)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidEnd session: UIDropSession) {
        collectionViewInFocus = nil
        indexPathInFocus = nil
        collectionView.reloadData()
        // Change letter status
        guard let letterView = session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return
        }
        if letter.status == .none {
            letter.isFailed = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidExit session: UIDropSession) {
        collectionViewInFocus = nil
        indexPathInFocus = nil
        collectionView.reloadData()
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExerciseLetterBoardViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return letterSize
    }
    
}

// MARK: - ExerciseFeedbackViewControllerDelegate
extension ExerciseLetterBoardViewController: ExerciseFeedbackViewControllerDelegate {
    
    func exerciseFeedbackViewController(_ viewController: ExerciseFeedbackViewController,
                                           didSelect action: ExerciseFeedbackViewController.Action) {
        guard let _delegate = delegate else {
            return
        }
        if action == .next {
            _delegate.exerciseViewController(self, didSelect: .phase)
        } else if action == .home {
            _delegate.exerciseViewController(self, didSelect: .home)
        }
    }
    
}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseLetterBoardViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK - Private Functions
extension ExerciseLetterBoardViewController {
    
    // MARK: Enums
    
    fileprivate enum CollectionViewType: Int {
        case consonants = 1
        case vowels
    }
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        contentView.shadowOffset = CGSize(width: 0, height: 6)
        consonantCollectionView.register(UINib(withType: LetterDroppableCollectionViewCell.self),
                                          forCellWithReuseIdentifier: LetterDroppableCollectionViewCell.identifier)
        vowelCollectionView.register(UINib(withType: LetterDroppableCollectionViewCell.self),
                                      forCellWithReuseIdentifier: LetterDroppableCollectionViewCell.identifier)
    }
    
    fileprivate func reloadCollectionViews() {
        consonantCollectionView.reloadData()
        vowelCollectionView.reloadData()
    }
    
    // MARK: Update
    
    fileprivate func updateContent() {
        let consonantsTitle: String = {
            let postfix = Localization.string(forKeyPath: "Exercise.LetterBoard.consonants")!
            return "\(viewModel.consonants.count) \(postfix)"
        }()
        consonantTitleLabel.text = consonantsTitle
        let vowelsTitle: String = {
            let postfix = Localization.string(forKeyPath: "Exercise.LetterBoard.vowels")!
            return "\(viewModel.vowels.count) \(postfix)"
        }()
        vowelTitleLabel.text = vowelsTitle
        letterPickerView.update(withLetters: viewModel.letters)
    }
    
    fileprivate func updateLayout() {
        let consonantsCollectionViewLayout = consonantCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        consonantCollectionViewHeightConstraint.constant = letterSize.height * 3 + consonantsCollectionViewLayout.minimumLineSpacing * 2
        vowelCollectionViewHeightConstraint.constant = letterSize.height
        let realLetterSize = CGSize(width: letterSize.width-letterPadding*2,
                                    height: letterSize.height-letterPadding*2)
        letterPickerView.updateLayout(withLetterSize: realLetterSize)
    }
    
    // MARK: Make
    
    fileprivate func makeCollectionViewItemSize() -> CGSize {
        guard viewModel.consonants.isEmpty == false else {
            return .zero
        }
        let itemsPerLine = ceil(Double(viewModel.consonants.count)/3.0)
        let layout = consonantCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpacingWidth = CGFloat((itemsPerLine-1)*Double(layout.minimumInteritemSpacing))
        let availableWidth = consonantCollectionView.frame.width-totalSpacingWidth
        var size = floor(availableWidth/CGFloat(itemsPerLine))
        if size > view.frame.width/10 {
            size = view.frame.width/10
        } else if size < 0 {
            size = 0
        }
        return CGSize(width: size, height: size)
    }
    
}
