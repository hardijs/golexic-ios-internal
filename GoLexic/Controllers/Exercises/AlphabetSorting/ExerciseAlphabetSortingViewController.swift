//
//  ExerciseAlphabetSortingViewController.swift
//  GoLexic
//
//  Created by Armands L. on 09/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ExerciseAlphabetSortingViewController: ExerciseViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var contentView: AttributedView!
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var letterPickerView: LetterPickerView!
    
    // MARK: Constraints
    @IBOutlet fileprivate var collectionViewHeightConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate lazy var viewModel: ExerciseAlphabetSortingViewModel = {
        return ExerciseAlphabetSortingViewModel(withPhase: phase, phaseExercise: phaseExercise)
    }()
    fileprivate var letterPadding: CGFloat = 8
    fileprivate var letterSpace: CGSize = .zero
    fileprivate var currentCollectionViewWidth: CGFloat = 0
    fileprivate var indexPathInFocus: IndexPath?
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        updateContent()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if collectionView.frame.width != currentCollectionViewWidth {
            currentCollectionViewWidth = collectionView.frame.width
            // Update layout
            letterSpace = makeCollectionViewItemSpace()
            collectionView.reloadData()
            updateLayout()
        }
    }

}

// MARK: - Targets
extension ExerciseAlphabetSortingViewController {
    
    @IBAction fileprivate func closeButtonPressed(_ sender: UIButton) {
        logExerciseCancelEvent()
        dismiss(animated: true)
    }
    
    @IBAction fileprivate func helpButtonPressed(_ sender: UIButton) {
        viewModel.markNextUnrecognizedLetterAsIsSuggestable()
        collectionView.reloadData()
        logHelpEvent()
    }
    
}

// MARK: - UICollectionViewDelegate
extension ExerciseAlphabetSortingViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.letters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LetterDroppableCollectionViewCell.identifier,
                                                      for: indexPath) as! LetterDroppableCollectionViewCell
        let letter = viewModel.letters[indexPath.row]
        cell.update(withLetter: letter, isHovering: indexPathInFocus == indexPath)
        cell.padding = letterPadding
        return cell
    }
    
}

// MARK: - UICollectionViewDropDelegate
extension ExerciseAlphabetSortingViewController: UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        guard let _ = session.items.first?.localObject as? LetterView else {
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        guard let letterView = session.items.first?.localObject as? LetterView, let letter = letterView.letter,
            let _indexPath = destinationIndexPath, _indexPath.row < viewModel.letters.count else {
            return UICollectionViewDropProposal(operation: .cancel)
        }
        if indexPathInFocus != _indexPath {
            indexPathInFocus = _indexPath
            collectionView.reloadData()
        }
        let targetLetter = viewModel.letters[_indexPath.row]
        if letter == targetLetter,
            viewModel.isLetterNextInLine(targetLetter) {
            Analytics.logLetterDropSuccess(letter.value)
            return UICollectionViewDropProposal(operation: .move)
        } else {
            Analytics.logLetterDropFail(letter.value)
            return UICollectionViewDropProposal(operation: .cancel)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        guard let letterView = coordinator.session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return
        }
        letter.status = .selected
        collectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now()+0.3) {
            if self.feedbackViewController == nil,
                self.viewModel.hasAnyLetter(withStatus: .none) == false {
                self.logExerciseCompletedEvent()
                let score = self.viewModel.completeExercise()
                let debugInfo = self.viewModel.exerciseScoreDebugInfo()
                self.presentFeedbackViewController(withDelegate: self, score: score, debugInfo: debugInfo)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidEnd session: UIDropSession) {
        indexPathInFocus = nil
        collectionView.reloadData()
        // Change letter status
        guard let letterView = session.items.first?.localObject as? LetterView,
            let letter = letterView.letter else {
            return
        }
        if letter.status == .none {
            letter.isFailed = true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidExit session: UIDropSession) {
        indexPathInFocus = nil
        collectionView.reloadData()
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExerciseAlphabetSortingViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return makeItemSize(fromLetterSpace: letterSpace)
    }
    
}

// MARK: - ExerciseFeedbackViewControllerDelegate
extension ExerciseAlphabetSortingViewController: ExerciseFeedbackViewControllerDelegate {
    
    func exerciseFeedbackViewController(_ viewController: ExerciseFeedbackViewController,
                                           didSelect action: ExerciseFeedbackViewController.Action) {
        guard let _delegate = delegate else {
            return
        }
        if action == .next {
            _delegate.exerciseViewController(self, didSelect: .phase)
        } else if action == .home {
            _delegate.exerciseViewController(self, didSelect: .home)
        }
    }
    
}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseAlphabetSortingViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK - Private Functions
extension ExerciseAlphabetSortingViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        contentView.shadowOffset = CGSize(width: 0, height: 6)
        collectionView.register(UINib(withType: LetterDroppableCollectionViewCell.self),
                                forCellWithReuseIdentifier: LetterDroppableCollectionViewCell.identifier)
    }
    
    // MARK: Update
    
    fileprivate func updateContent() {
        letterPickerView.update(withLetters: viewModel.letters)
    }
    
    fileprivate func updateLayout() {
        let itemSize = makeItemSize(fromLetterSpace: letterSpace)
        let collectionViewLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        collectionViewHeightConstraint.constant = itemSize.height * 2 + collectionViewLayout.minimumLineSpacing
        let letterSize = CGSize(width: letterSpace.width-letterPadding*2,
                                height: letterSpace.height-letterPadding*2)
        letterPickerView.updateLayout(withLetterSize: letterSize)
    }
 
    // MARK: Make
    
    fileprivate func makeCollectionViewItemSpace() -> CGSize {
        guard viewModel.letters.isEmpty == false else {
            return .zero
        }
        let itemsPerLine = ceil(Double(viewModel.letters.count)/2.0)
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpacingWidth = CGFloat((itemsPerLine-1)*Double(layout.minimumInteritemSpacing))
        let availableWidth = collectionView.frame.width-totalSpacingWidth
        var size = floor(availableWidth/CGFloat(itemsPerLine))
        if size > view.frame.width/10 {
            size = view.frame.width/10
        } else if size < 0 {
            size = 0
        }
        return CGSize(width: size, height: size)
    }
    
    fileprivate func makeItemSize(fromLetterSpace letterSpace: CGSize) -> CGSize {
        var _size = letterSpace
        _size.height = _size.height + (_size.height/3)
        return _size
    }
    
}
