//
//  ExerciseVowelsViewModel.swift
//  GoLexic
//
//  Created by Armands L. on 15/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import Cubemobile

class ExerciseVowelsViewModel {
    
    // MARK: Enums
    
    enum State {
        case findVowels
        case confirmVowels
    }
    
    // MARK: Properties
    
    // Variables
    var phase: Phase
    var phaseExercise: PhaseExercise
    var state: State = .findVowels
    fileprivate(set) lazy var exercise: ExerciseVowels? = {
        return phaseExercise.exercise as? ExerciseVowels
    }()
    lazy var letters: [ExerciseLetter] = {
        return makeExerciseLetters(fromExercise: exercise)
    }()
    lazy var vowels: [ExerciseLetter] = {
        return makeVowels(fromLetters: letters)
    }()
    lazy var consonants: [ExerciseLetter] = {
        return makeConsonants(fromLetters: letters)
    }()
    fileprivate(set) var moves: Int = 0
    fileprivate(set) var checks: Int = 0
    
    fileprivate lazy var speechProcessor: SpeechProcessor = {
        return SpeechProcessor()
    }()
    
    // Flags
    var isCheckLimitReached: Bool {
        return checks >= 3
    }
    fileprivate(set) var isLettersCorrect: Bool = false
    fileprivate(set) var isCompleted: Bool = false
    var isPendingFinalResult: Bool = false
    
    // MARK: Initialziers
    
    init(withPhase phase: Phase, phaseExercise: PhaseExercise) {
        self.phase = phase
        self.phaseExercise = phaseExercise
    }
    
}

// MARK: - Public Functions
extension ExerciseVowelsViewModel {
    
    // MARK: Exercise
    
    @discardableResult
    func completeExercise() -> Score {
        guard isCompleted == false else {
            return makeExerciseScore()
        }
        isCompleted = true
        let realm = try! Realm()
        try! realm.write {
            phaseExercise.markAsCompleted()
        }
        submitResults(withScore: moves)
        submitStatistics(withResults: letters)
        return makeExerciseScore()
    }
    
    func makeExerciseScore() -> Score {
        guard let _exercise = exercise else {
            return .none
        }
        let isExercisePassed = moves <= _exercise.passThreshold
        return isExercisePassed ? .success : .failed
    }
    
    // MARK: Letter
    
    func letter(forIdentifier identifier: String) -> ExerciseLetter? {
        return letters.first(where: { $0.identifier == identifier})
    }
    
    func hasAnyLetter(withStatus status: RecognitionStatus, state: State) -> Bool {
        for letter in vowels {
            if letter.status == status {
                return true
            }
        }
        return false
    }
    
    @discardableResult
    func validateLetters() -> Bool {
        var isCorrect: Bool = true
        for letter in letters {
            if letter.isVowel, letter.status == .none {
                isCorrect = false
                break
            } else if letter.isVowel == false, letter.status != .none {
                isCorrect = false
                break
            }
        }
        isLettersCorrect = isCorrect
        return isLettersCorrect
    }
    
    func incrementCheckCounter() {
        checks += 1
    }
    
    // MARK: Speech Recognition
    
    @discardableResult
    func processSpeechRecognizerPartialResult(_ result: KIOSResult) -> Bool {
        var hasNewRecognitions: Bool = false
        let words = speechProcessor.processResult(result, isNewSearch: true)
        for letter in vowels {
            guard letter.status == .selected else {
                continue
            }
            guard letter.isEqualToAny(words, includeSimilarities: true) else {
                break
            }
            hasNewRecognitions = true
            letter.status = .recognized
            break
        }
        return hasNewRecognitions
    }
    
    func processSpeechRecognizerFinalResult(_ result: KIOSResult) {
        for letter in vowels {
            guard letter.status == .recognized else {
                continue
            }
            letter.updateConfidenceIfMatches(result.words, includeSimilarities: true)
        }
        speechProcessor.reset()
    }
    
    // MARK: Other
    
    func incrementMoveCounter() {
        moves += 1
    }
    
    func decrementMoveCounter() {
        guard moves > 0 else {
            return
        }
        moves -= 1
    }
    
    func exerciseScoreDebugInfo() -> String? {
        guard let _exercise = exercise else {
            return nil
        }
        return "Completed \(moves)/\(_exercise.passThreshold) moves"
    }
    
}

// MARK: - Private Functions
extension ExerciseVowelsViewModel {
    
    // MARK: Helpers
    
    fileprivate func submitResults(withScore score: Int) {
        let params = makeResultsBody(withScore: score)
        let target = GoLexicTarget(withEndpoint: .vowelsResults, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    fileprivate func submitStatistics(withResults results: [ExerciseLetter]) {
        guard let _exercise = exercise,
            let _letterScoring = _exercise.letterScoring else {
            return
        }
        let params = makeStatisticsBody(withResults: results, letterScoring: _letterScoring)
        let target = GoLexicTarget(withEndpoint: .statisticsLetters, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    // MARK: Make
    
    fileprivate func makeExerciseLetters(fromExercise exercise: ExerciseVowels?) -> [ExerciseLetter] {
        var _letters: [ExerciseLetter] = []
        guard let _exercise = exercise,
            let _exerciseLetters = _exercise.letters else {
            return _letters
        }
        for item in _exerciseLetters.items {
            let letter = ExerciseLetter(withLetterItem: item)
            _letters.append(letter)
        }
        return _letters
    }
    
    fileprivate func makeVowels(fromLetters letters: [ExerciseLetter]) -> [ExerciseLetter] {
        var _vowels: [ExerciseLetter] = []
        for letter in letters {
            if letter.isVowel {
                _vowels.append(letter)
            }
        }
        return _vowels
    }
    
    fileprivate func makeConsonants(fromLetters letters: [ExerciseLetter]) -> [ExerciseLetter] {
        var _consonants: [ExerciseLetter] = []
        for letter in letters {
            if letter.isVowel == false {
                _consonants.append(letter)
            }
        }
        return _consonants
    }
    
    fileprivate func makeResultsBody(withScore score: Int) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(score, forKey: "score")
        return body
    }
    
    fileprivate func makeStatisticsBody(withResults results: [ExerciseLetter], letterScoring: ScoringLetter) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(letterScoring.identifier, forKey: "letterScoringId")
        var _letters: [[String: Any]] = []
        for result in results {
            let item: [String : Any] = ["letter": result.value.uppercased(),
                                        "score": result.confidence]
            _letters.append(item)
        }
        body.updateValue(_letters, forKey: "letters")
        return body
    }
    
}
