//
//  ExerciseVowelsViewController.swift
//  GoLexic
//
//  Created by Armands L. on 15/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ExerciseVowelsViewController: ExerciseViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var collectionView: UICollectionView!
    @IBOutlet fileprivate var infoLabel: UILabel!
    @IBOutlet fileprivate var recordButton: UIButton!
    @IBOutlet fileprivate var checkButton: UIButton!
    @IBOutlet fileprivate var invalidButton: UIButton!
    @IBOutlet fileprivate var correctButton: UIButton!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Constraints
    @IBOutlet fileprivate var collectionViewHeightConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate lazy var viewModel: ExerciseVowelsViewModel = {
        return ExerciseVowelsViewModel(withPhase: phase, phaseExercise: phaseExercise)
    }()
    fileprivate var letterSize: CGSize = .zero
    fileprivate var currentCollectionViewWidth: CGFloat = 0
    
    // Flags
    fileprivate var adjustConsonantsAfterReload: Bool = false
    fileprivate var adjustVowelsAfterReload: Bool = false
    fileprivate var isShowingCorrectStatus: Bool = false
    fileprivate var isShowingFailedStatus: Bool = false
    
    // MARK: Overridden Functipns

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
        updateState()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if collectionView.frame.width != currentCollectionViewWidth {
            currentCollectionViewWidth = collectionView.frame.width
            // Update layout
            letterSize = makeCollectionViewItemSize()
            collectionView.reloadData()
            updateLayout()
        }
    }
    
}

// MARK: - Targets
extension ExerciseVowelsViewController {
    
    @IBAction fileprivate func closeButtonPressed(_ sender: UIButton) {
        guard viewModel.isPendingFinalResult == false else {
            return
        }
        SpeechRecognition.shared.stopListening(withResults: false)
        logExerciseCancelEvent()
        dismiss(animated: true)
    }
    
    @IBAction fileprivate func recordButtonPressed(_ sender: UIButton) {
        if SpeechRecognition.shared.state == .listening {
            viewModel.isPendingFinalResult = true
            SpeechRecognition.shared.stopListening()
            logMicEvent(withState: false)
        } else {
            SpeechRecognition.shared.startListening()
            logMicEvent(withState: true)
        }
    }
    
    @IBAction fileprivate func checkButtonPressed(_ sender: UIButton) {
        viewModel.incrementMoveCounter()
        viewModel.incrementCheckCounter()
        if viewModel.validateLetters() {
            isShowingCorrectStatus = true
        } else {
            if viewModel.isCheckLimitReached {
                adjustVowelsAfterReload = true
            }
            adjustConsonantsAfterReload = true
            collectionView.reloadData()
            collectionView.performBatchUpdates(nil, completion: { _ in
                self.adjustConsonantsAfterReload = false
                self.adjustVowelsAfterReload = false
            })
            isShowingFailedStatus = true
        }
        updateState()
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            if self.viewModel.isCheckLimitReached {
                self.viewModel.validateLetters()
            }
            self.isShowingCorrectStatus  = false
            self.isShowingFailedStatus = false
            if self.viewModel.isLettersCorrect {
                self.viewModel.state = .confirmVowels
                SpeechRecognition.shared.startListening()
            } else {
                self.updateState()
            }
        }
    }
    
}

// MARK: - UICollectionViewDelegate
extension ExerciseVowelsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.letters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LetterMovableCollectionViewCell.identifier,
                                                      for: indexPath) as! LetterMovableCollectionViewCell
        let letter = viewModel.letters[indexPath.row]
        let isMoved: Bool = {
            if letter.isVowel {
                return letter.status != .none
            }
            return letter.status == .selected
        }()
        cell.update(withLetter: letter, isMoved: isMoved)
        if adjustConsonantsAfterReload, letter.isVowel == false, letter.status != .none {
            letter.status = .none
            cell.updateState(toIsMoved: false, animated: true)
        }
        if adjustVowelsAfterReload, letter.isVowel, letter.status == .none {
            letter.status = .selected
            viewModel.incrementMoveCounter()
            cell.updateState(toIsMoved: true, animated: true)
        }
        cell.delegate = self
        cell.tag = indexPath.row
        return cell
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExerciseVowelsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemHeight: CGFloat = letterSize.height * 2 + LetterMovableCollectionViewCell.spacing
        return CGSize(width: letterSize.width, height: itemHeight)
    }
    
}

// MARK: - LetterMovableCollectionViewCellDelegate
extension ExerciseVowelsViewController: LetterMovableCollectionViewCellDelegate {
    
    func letterMovableCollectionViewCellShouldInteract(_ cell: LetterMovableCollectionViewCell) -> Bool {
        guard viewModel.state == .findVowels,
            isShowingCorrectStatus == false,
            isShowingFailedStatus == false,
            viewModel.isCheckLimitReached == false else {
            return false
        }
        return true
    }
    
    func letterMovableCollectionViewCell(_ cell: LetterMovableCollectionViewCell,
                                         didChangeStateTo isMoved: Bool) {
        let letter = viewModel.letters[cell.tag]
        if isMoved {
            letter.status = .selected
            viewModel.incrementMoveCounter()
        } else {
            letter.status = .none
            viewModel.decrementMoveCounter()
        }
    }
    
}

// MARK: - SpeechRecognitionDelegate
extension ExerciseVowelsViewController: SpeechRecognitionDelegate {
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, didChange state: KIOSRecognizerState) {
        updateState()
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, partial result: KIOSResult) {
        let hasNewResults = viewModel.processSpeechRecognizerPartialResult(result)
        if hasNewResults {
            Analytics.logVoiceRecognitionSuccess(result.cleanText)
        } else {
            Analytics.logVoiceRecognitionFailed(result.cleanText)
        }
        guard hasNewResults else {
            return
        }
        DispatchQueue.main.async {
            if self.viewModel.hasAnyLetter(withStatus: .none, state: .confirmVowels) == false,
                self.viewModel.hasAnyLetter(withStatus: .selected, state: .confirmVowels) == false {
                // No vowels with status .none or .selected
                if SpeechRecognition.shared.state == .listening {
                    self.viewModel.isPendingFinalResult = true
                    SpeechRecognition.shared.stopListening()
                }
            }
            self.collectionView.reloadData()
        }
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, final result: KIOSResult) {
        guard viewModel.isPendingFinalResult else {
            return
        }
        viewModel.processSpeechRecognizerFinalResult(result)
        DispatchQueue.main.async {
            self.viewModel.isPendingFinalResult = false
            self.proceedAfterFinalResults()
            self.updateState()
        }
    }
    
    func speechRecognitionReadyToListenAfterInterruption(_ speechRecognition: SpeechRecognition) {
        updateState()
    }
    
}

// MARK: - ExerciseFeedbackViewControllerDelegate
extension ExerciseVowelsViewController: ExerciseFeedbackViewControllerDelegate {
    
    func exerciseFeedbackViewController(_ viewController: ExerciseFeedbackViewController,
                                           didSelect action: ExerciseFeedbackViewController.Action) {
        guard let _delegate = delegate else {
            return
        }
        if action == .next {
            _delegate.exerciseViewController(self, didSelect: .phase)
        } else if action == .home {
            _delegate.exerciseViewController(self, didSelect: .home)
        }
    }
    
}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseVowelsViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK - Private Functions
extension ExerciseVowelsViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        SpeechRecognition.shared.delegate = self
        if let _exercise = phaseExercise.exercise as? ExerciseVowels {
            SpeechRecognition.shared.prepareForListening(withDecodingGraph: _exercise.lettersId)
        }
        collectionView.register(UINib(withType: LetterMovableCollectionViewCell.self),
                                forCellWithReuseIdentifier: LetterMovableCollectionViewCell.identifier)
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            if _self.viewModel.isPendingFinalResult {
                _self.viewModel.isPendingFinalResult = false
                _self.proceedAfterFinalResults()
                _self.updateState()
            }
        }
    }
    
    fileprivate func proceedAfterFinalResults() {
        guard viewModel.hasAnyLetter(withStatus: .none, state: .confirmVowels) == false,
            viewModel.hasAnyLetter(withStatus: .selected, state: .confirmVowels) == false,
            feedbackViewController == nil else {
            return
        }
        // No vowels with status .none or .recognized
        logExerciseCompletedEvent()
        let score = self.viewModel.completeExercise()
        let debugInfo = self.viewModel.exerciseScoreDebugInfo()
        presentFeedbackViewController(withDelegate: self, score: score, debugInfo: debugInfo)
    }
    
    // MARK: Update
    
    fileprivate func updateLayout() {
        let collectionViewLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let itemHeight: CGFloat = letterSize.height * 2 + LetterMovableCollectionViewCell.spacing
        collectionViewHeightConstraint.constant = itemHeight * 2 + collectionViewLayout.minimumLineSpacing
    }
    
    fileprivate func updateState() {
        if viewModel.isCompleted ||
            viewModel.state == .findVowels ||
            SpeechRecognition.shared.state == .needsDecodingGraph {
            recordButton.isHidden = true
        } else {
            recordButton.isHidden = false
        }
        if SpeechRecognition.shared.state == .readyToListen {
            recordButton.isSelected = false
        } else {
            recordButton.isSelected = true
        }
        if viewModel.isPendingFinalResult {
            recordButton.isUserInteractionEnabled = false
            activityIndicator.startAnimating()
        } else {
            recordButton.isUserInteractionEnabled = true
            activityIndicator.stopAnimating()
        }
        if isShowingCorrectStatus ||
            isShowingFailedStatus ||
            viewModel.isLettersCorrect ||
            viewModel.state != .findVowels {
            checkButton.isHidden = true
        } else {
            checkButton.isHidden = false
        }
        if isShowingCorrectStatus,
            viewModel.state == .findVowels {
            correctButton.isHidden = false
        } else {
            correctButton.isHidden = true
        }
        if isShowingFailedStatus,
            viewModel.state == .findVowels {
            invalidButton.isHidden = false
        } else {
            invalidButton.isHidden = true
        }
        if viewModel.isCompleted ||
            viewModel.state == .findVowels ||
            SpeechRecognition.shared.state != .listening {
            infoLabel.isHidden = true
        } else {
            infoLabel.isHidden = false
        }
    }
    
    // MARK: Make
    
    fileprivate func makeCollectionViewItemSize() -> CGSize {
        guard viewModel.letters.isEmpty == false else {
            return .zero
        }
        let itemsPerLine = ceil(Double(viewModel.letters.count)/2.0)
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpacingWidth = CGFloat((itemsPerLine-1)*Double(layout.minimumInteritemSpacing))
        let availableWidth = collectionView.frame.width-totalSpacingWidth
        var size = floor(availableWidth/CGFloat(itemsPerLine))
        if size > view.frame.width/10 {
            size = view.frame.width/10
        } else if size < 0 {
            size = 0
        }
        return CGSize(width: size, height: size)
    }
    
}
