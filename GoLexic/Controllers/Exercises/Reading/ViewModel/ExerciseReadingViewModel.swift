//
//  ExerciseReadingViewModel.swift
//  GoLexic
//
//  Created by Armands L. on 16/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import RealmSwift
import Cubemobile

class ExerciseReadingViewModel {
    
    // MARK: Structs
    
    struct Index {
        let page: Int
        let line: Int
        let text: Int
    }
    
    // MARK: Properties
    
    // Variables
    var phase: Phase
    var phaseExercise: PhaseExercise
    fileprivate lazy var lesson: PhaseExerciseLesson? = {
        return makePhaseExerciseLesson(fromPhaseExercise: phaseExercise)
    }()
    fileprivate(set) lazy var exercise: ExerciseReading? = {
        return phaseExercise.exercise as? ExerciseReading
    }()
    fileprivate(set) lazy var readingLesson: ExerciseReadingLesson? = {
        return lesson?.lesson as? ExerciseReadingLesson
    }()
    fileprivate(set) var index = Index(page: 0, line: 0, text: 0)
    fileprivate(set) var text: ExerciseText?
    fileprivate(set) var currentText: ExerciseText?
    fileprivate var results: [ExerciseText] = []
    
    fileprivate lazy var speechProcessor: SpeechProcessor = {
        return SpeechProcessor()
    }()
    
    fileprivate lazy var normalTextAttributes: [NSAttributedString.Key: Any] = {
        return makeNormalTextAttributes()
    }()
    fileprivate lazy var focusedTextAttributes: [NSAttributedString.Key: Any] = {
        return makeFocusedTextAttributes()
    }()
    fileprivate lazy var paragraphStyle: NSParagraphStyle = {
        return makeParagraphStyle()
    }()
    
    // Flags
    fileprivate(set) var isCompleted: Bool = false
    var isCurrentPageCompleted: Bool = false
    var isPendingFinalResult: Bool = false
    
    // MARK: Initialziers
    
    init(withPhase phase: Phase, phaseExercise: PhaseExercise) {
        self.phase = phase
        self.phaseExercise = phaseExercise
        updateCurrentText()
    }
    
}

// MARK: - Public Functions
extension ExerciseReadingViewModel {
    
    // MARK: Lesson
    
    func isLessonCompletable() -> Bool {
        return hasNextText() == false &&
            hasNextLine() == false &&
            hasNextPage() == false
    }
    
    func completeLesson() {
        guard isCompleted == false,
            let _lesson = lesson else {
            return
        }
        isCompleted = true
        let realm = try! Realm()
        try! realm.write {
            phaseExercise.markLessonAsCompleted(_lesson)
        }
        let confidence = makeAvarageConfidence(forTexts: results)
        if let scoring = scoring(forConfidence: confidence) {
            submitResults(withScore: confidence, scoring: scoring)
        }
        submitStatistics(withResults: results)
    }
    
    func makeLessonScore() -> Score {
        let confidence = makeAvarageConfidence(forTexts: results)
        guard let scoring = scoring(forConfidence: confidence) else {
            return .none
        }
        return scoring.score
    }
    
    // MARK: Page
    
    func currentPage() -> ExerciseReadingLessonPage? {
        guard let _readingLesson = readingLesson,
            index.page < _readingLesson.pages.count else {
            return nil
        }
        return _readingLesson.pages[index.page]
    }
    
    func hasNextPage() -> Bool {
        guard let _readingLesson = readingLesson else {
            return false
        }
        let nextIndex = index.page + 1
        return nextIndex < _readingLesson.pages.count
    }
    
    @discardableResult
    func moveToNextPage() -> Bool {
        guard let _readingLesson = readingLesson else {
            return false
        }
        let nextPageIndex = index.page + 1
        guard nextPageIndex < _readingLesson.pages.count else {
            return false
        }
        index = Index(page: nextPageIndex, line: 0, text: 0)
        isCurrentPageCompleted = false
        updateCurrentText()
        return true
    }
    
    // MARK: Line
    
    func currentLine() -> ExerciseReadingLessonPageLine? {
        guard let _currentPage = currentPage(),
            index.line < _currentPage.lines.count else {
            return nil
        }
        return _currentPage.lines[index.line]
    }
    
    func hasNextLine() -> Bool {
        guard let _currentPage = currentPage() else {
            return false
        }
        let nextLineIndex = index.line + 1
        return nextLineIndex < _currentPage.lines.count
    }
    
    @discardableResult
    func moveToNextLine() -> Bool {
        guard let _currentPage = currentPage() else {
            return false
        }
        let nextLineIndex = index.line + 1
        guard nextLineIndex < _currentPage.lines.count else {
            return false
        }
        let pageIndex = index.page
        index = Index(page: pageIndex, line: nextLineIndex, text: 0)
        updateCurrentText()
        return true
    }
	
	// MARK: Text
    
    func hasNextText() -> Bool {
        guard let _currentLine = currentLine() else {
            return false
        }
        let nextTextIndex = index.text + 1
        return nextTextIndex < _currentLine.texts.count
    }
	
    @discardableResult
	func moveToNextText() -> Bool {
        guard let _currentLine = currentLine() else {
            return false
        }
        let nextTextIndex = index.text + 1
        guard nextTextIndex < _currentLine.texts.count  else {
            return false
        }
        let pageIndex = index.page
        let lineIndex = index.line
        index = Index(page: pageIndex, line: lineIndex, text: nextTextIndex)
        updateCurrentText()
        return true
	}
    
    // MARK: Speech Recognition
    
    @discardableResult
    func processSpeechRecognizerPartialResult(_ result: KIOSResult) -> Bool {
        guard let _text = text else {
            return false
        }
        var isNewSearch: Bool = false
        if currentText?.identifier != _text.identifier {
            currentText = _text
            isNewSearch = true
        }
        let words = speechProcessor.processResult(result, isNewSearch: isNewSearch)
        guard _text.isEqualToAny(words, includeSimilarities: false) else {
            return false
        }
        _text.status = .recognized
        return true
    }
    
    func processSpeechRecognizerFinalResult(_ result: KIOSResult) {
        for text in results {
            guard text.status == .recognized else {
                continue
            }
            text.updateConfidenceIfMatches(result.words, includeSimilarities: false)
        }
        speechProcessor.reset()
    }
    
    // MARK: Make
    
    func makeText(forPage page: ExerciseReadingLessonPage,
                  isFocused: Bool, isCompleted: Bool) -> NSAttributedString? {
        let text = NSMutableAttributedString()
        for x in 0..<page.lines.count {
            let line = page.lines[x]
            let isFocused = isFocused && !isCompleted && x == index.line
            let lineText = makeText(forLine: line,
                                    isFocused: isFocused,
                                    isLast: x == page.lines.count-1)
            text.append(lineText)
        }
        return text
    }
    
    func makeLessonTitleText() -> String? {
        guard let _lesson = readingLesson else {
            return nil
        }
        let lessonText = Localization.string(forKeyPath: "Exercise.Reading.lesson")!
        return "\(lessonText) \(_lesson.index): \(_lesson.name)"
    }
    
    func exerciseScoreDebugInfo() -> String? {
        let avarageConfidence = makeAvarageConfidence(forTexts: results)
        let convertedConfidence = Int(avarageConfidence*100)
        return "Completed with avarage confidence of \(convertedConfidence)%"
    }
    
}

// MARK: - Private Functions
extension ExerciseReadingViewModel {
    
    // MARK: Helpers
    
    fileprivate func updateCurrentText() {
        guard let _currentLine = currentLine(),
            index.text < _currentLine.texts.count else {
            return
        }
        let _text = _currentLine.texts[index.text]
        text = ExerciseText(withReadingText: _text)
        results.append(text!)
    }
    
    fileprivate func scoring(forConfidence confidence: Double) -> ExerciseReadingScoring? {
        guard let _exercise = exercise else {
            return nil
        }
        let sortedScorings = _exercise.scorings.sorted(byKeyPath: "confidence", ascending: false)
        for scoring in sortedScorings {
            if confidence >= scoring.confidence {
                return scoring
            }
        }
        return nil
    }
    
    fileprivate func submitResults(withScore score: Double, scoring: ExerciseReadingScoring) {
        let params = makeResultsBody(withScore: score, scoring: scoring)
        let target = GoLexicTarget(withEndpoint: .readingResults, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    fileprivate func submitStatistics(withResults results: [ExerciseText]) {
        guard let _exercise = exercise,
            let _wordScoring = _exercise.wordScoring else {
            return
        }
        let params = makeStatisticsBody(withResults: results, wordScoring: _wordScoring)
        let target = GoLexicTarget(withEndpoint: .statisticsWords, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    // MARK: Make
    
    fileprivate func makePhaseExerciseLesson(fromPhaseExercise phaseExercise: PhaseExercise) -> PhaseExerciseLesson? {
        for lesson in phaseExercise.exerciseLessons {
            if lesson.isCompleted == false {
                return lesson
            }
        }
        return nil
    }
    
    fileprivate func makeAvarageConfidence(forTexts texts: [ExerciseText]) -> Double {
        var total: Double = 0
        for text in texts {
            total += text.confidence
        }
        return total/Double(texts.count)
    }
    
    fileprivate func makeText(forLine line: ExerciseReadingLessonPageLine,
                              isFocused: Bool, isLast: Bool) -> NSAttributedString {
        
        let attributedLine = NSMutableAttributedString()
        for x in 0..<line.texts.count {
            let text = line.texts[x]
            let activeAttributes: [NSAttributedString.Key: Any] = {
                if isFocused, index.text == x {
                    return focusedTextAttributes
                }
                return normalTextAttributes
            }()
            let value: String = {
                if x < line.texts.count-1 {
                    return "\(text.text),  "
                } else {
                    return text.text
                }
            }()
            let _value = NSString(string: value)
            let attributedText = NSMutableAttributedString(string: value, attributes: normalTextAttributes)
            attributedText.addAttributes(activeAttributes, range: _value.range(of: text.text))
            attributedLine.append(attributedText)
        }
        if isLast == false {
            let newLineText = NSAttributedString(string: "\n", attributes: normalTextAttributes)
            attributedLine.append(newLineText)
        }
        attributedLine.addAttributes([.paragraphStyle: paragraphStyle],
                                     range: NSRange(location: 0, length: attributedLine.length))
        return attributedLine
    }
    
    fileprivate func makeNormalTextAttributes() -> [NSAttributedString.Key: Any] {
        let normalTextAttribute: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: UIFont.Name.Montserrat.medium, size: 27)!,
            .foregroundColor: #colorLiteral(red: 0.7294117647, green: 0.737254902, blue: 0.7450980392, alpha: 1)
        ]
        return normalTextAttribute
    }
    
    fileprivate func makeFocusedTextAttributes() -> [NSAttributedString.Key: Any] {
        let normalTextAttribute: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: UIFont.Name.Montserrat.medium, size: 27)!,
            .foregroundColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        ]
        return normalTextAttribute
    }
    
    fileprivate func makeParagraphStyle() -> NSParagraphStyle {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 30
        paragraphStyle.alignment = .left
        return paragraphStyle
    }
    
    fileprivate func makeResultsBody(withScore score: Double, scoring: ExerciseReadingScoring) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        if let _readingLesson = readingLesson {
            body.updateValue(_readingLesson.identifier, forKey: "lessonId")
        }
        body.updateValue(scoring.identifier, forKey: "scoringId")
        body.updateValue(score, forKey: "score")
        return body
    }
    
    fileprivate func makeStatisticsBody(withResults results: [ExerciseText], wordScoring: ScoringWord) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(wordScoring.identifier, forKey: "wordScoringId")
        var _letters: [[String: Any]] = []
        for result in results {
            let item: [String : Any] = ["word": result.value,
                                        "score": result.confidence]
            _letters.append(item)
        }
        body.updateValue(_letters, forKey: "words")
        return body
    }
    
}
