//
//  ExerciseReadingViewController.swift
//  GoLexic
//
//  Created by Armands L. on 15/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ExerciseReadingViewController: ExerciseViewController {
    
    // MARK: Properties

    // IB
    @IBOutlet fileprivate var contentView: AttributedView!
    @IBOutlet fileprivate var lessonLabel: UILabel!
    @IBOutlet fileprivate var progressView: ProgressView!
    @IBOutlet fileprivate var textLabel: UILabel!
    @IBOutlet fileprivate var nextPageButton: AttributedButton!
    @IBOutlet fileprivate var completeButton: UIButton!
    @IBOutlet fileprivate var recordButton: UIButton!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    fileprivate var pageViewController: UIPageViewController!
    
    // Variables
    fileprivate lazy var viewModel: ExerciseReadingViewModel = {
        return ExerciseReadingViewModel(withPhase: phase, phaseExercise: phaseExercise)
    }()
    fileprivate var currentRecognizedState: KIOSRecognizerState = .needsDecodingGraph
    fileprivate var moveTimer: Timer?

    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
        updateContent()
        updateState()
    }

}

// MARK: - Targets
extension ExerciseReadingViewController {
    
    @IBAction fileprivate func closeButtonPressed(_ sender: UIButton) {
        guard viewModel.isPendingFinalResult == false else {
            return
        }
        SpeechRecognition.shared.stopListening(withResults: false)
        if viewModel.isCompleted == false {
            logExerciseCancelEvent()
        }
        dismiss(animated: true)
    }
    
    @IBAction fileprivate func nextPageButtonPressed(_ sender: UIButton) {
        viewModel.moveToNextPage()
        updateState()
        updateContent()
        logMoveToNextExercisePageEvent()
    }
    
    @IBAction fileprivate func recordButtonPressed(_ sender: UIButton) {
        if SpeechRecognition.shared.state == .listening {
            viewModel.isPendingFinalResult = true
            SpeechRecognition.shared.stopListening()
            logMicEvent(withState: false)
        } else {
            SpeechRecognition.shared.startListening()
            logMicEvent(withState: true)
        }
    }
    
    @IBAction fileprivate func completeButtonPressed(_ sender: UIButton) {
        guard feedbackViewController == nil else {
            return
        }
        let score = viewModel.makeLessonScore()
        let debugInfo = viewModel.exerciseScoreDebugInfo()
        presentFeedbackViewController(withDelegate: self, score: score, debugInfo: debugInfo)
    }
    
    @objc fileprivate func moveTimerDidTrigger(_ sender: Timer) {
        if viewModel.moveToNextText() {
            // Do nothing
        } else if viewModel.moveToNextLine() {
            // Do nothing
        } else {
            viewModel.isCurrentPageCompleted = true
            if SpeechRecognition.shared.state == .listening {
                viewModel.isPendingFinalResult = true
                SpeechRecognition.shared.stopListening()
            }
        }
        updateContent()
    }
    
}

// MARK: - SpeechRecognitionDelegate
extension ExerciseReadingViewController: SpeechRecognitionDelegate {
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, didChange state: KIOSRecognizerState) {
        currentRecognizedState = state
        if let _timer = moveTimer {
            _timer.invalidate()
        }
        if state == .listening {
            scheduleMoveTimer()
        }
        updateState()
        updateContent()
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, partial result: KIOSResult) {
        let hasNewResults = viewModel.processSpeechRecognizerPartialResult(result)
        if hasNewResults {
            Analytics.logVoiceRecognitionSuccess(result.cleanText)
        } else {
            Analytics.logVoiceRecognitionFailed(result.cleanText)
        }
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, final result: KIOSResult) {
        guard viewModel.isPendingFinalResult else {
            return
        }
        viewModel.processSpeechRecognizerFinalResult(result)
        DispatchQueue.main.async {
            self.viewModel.isPendingFinalResult = false
            if self.viewModel.isLessonCompletable() {
                self.logExerciseCompletedEvent()
                self.viewModel.completeLesson()
            }
            self.updateState()
        }
    }
    
    func speechRecognitionReadyToListenAfterInterruption(_ speechRecognition: SpeechRecognition) {
        updateState()
    }
    
}

// MARK: - ExerciseFeedbackViewControllerDelegate
extension ExerciseReadingViewController: ExerciseFeedbackViewControllerDelegate {
    
    func exerciseFeedbackViewController(_ viewController: ExerciseFeedbackViewController,
                                           didSelect action: ExerciseFeedbackViewController.Action) {
        guard let _delegate = delegate else {
            return
        }
        if action == .next {
            _delegate.exerciseViewController(self, didSelect: .phase)
        } else if action == .home {
            _delegate.exerciseViewController(self, didSelect: .home)
        }
    }
    
}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseReadingViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension ExerciseReadingViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        SpeechRecognition.shared.delegate = self
        if let _exercise = phaseExercise.exercise as? ExerciseReading {
            SpeechRecognition.shared.prepareForListening(withDecodingGraph: _exercise.identifier)
        }
        contentView.shadowOffset = CGSize(width: 0, height: 6)
        nextPageButton.highlightEntireView = true
    }
	
	fileprivate func scheduleMoveTimer() {
        if let _timer = moveTimer {
            _timer.invalidate()
        }
		moveTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self,
                                              selector: #selector(moveTimerDidTrigger(_:)), userInfo: nil, repeats: true)
	}
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            if _self.viewModel.isPendingFinalResult {
                _self.viewModel.isPendingFinalResult = false
                _self.updateState()
            }
        }
    }
    
    // MARK: Update
    
    fileprivate func updateContent() {
        lessonLabel.text = viewModel.makeLessonTitleText()
        if let _readingLesson = viewModel.readingLesson {
            progressView.update(withIndex: viewModel.index.page,
                                count: _readingLesson.pages.count,
                                isCompleted: viewModel.isCurrentPageCompleted)
        }
        if let currentPage = viewModel.currentPage() {
            let markTextInFocus = currentRecognizedState == .listening
            textLabel.attributedText = viewModel.makeText(forPage: currentPage,
                                                          isFocused: markTextInFocus,
                                                          isCompleted: viewModel.isCompleted)
        }
    }
	
    fileprivate func updateState() {
        let currentPage = viewModel.currentPage()
        if currentPage == nil ||
            viewModel.isCompleted ||
            viewModel.isCurrentPageCompleted ||
            SpeechRecognition.shared.state == .needsDecodingGraph {
            recordButton.isHidden = true
        } else {
            recordButton.isHidden = false
        }
        if SpeechRecognition.shared.state == .readyToListen {
            recordButton.isSelected = false
        } else {
            recordButton.isSelected = true
        }
        if viewModel.isPendingFinalResult {
            recordButton.isUserInteractionEnabled = false
            activityIndicator.startAnimating()
        } else {
            recordButton.isUserInteractionEnabled = true
            activityIndicator.stopAnimating()
        }
        if viewModel.isPendingFinalResult ||
            viewModel.isCurrentPageCompleted == false {
            nextPageButton.isHidden = true
            completeButton.isHidden = true
        } else {
            if viewModel.hasNextPage() {
                nextPageButton.isHidden = false
                completeButton.isHidden = true
            } else {
                nextPageButton.isHidden = true
                completeButton.isHidden = false
            }
        }
    }
    
}
