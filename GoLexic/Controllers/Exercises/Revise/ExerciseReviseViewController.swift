//
//  ExerciseReviseViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class ExerciseReviseViewController: ExerciseViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var progressView: ProgressView!
    @IBOutlet fileprivate var textLabel: UILabel!
    @IBOutlet fileprivate var nextPageButton: AttributedButton!
    @IBOutlet fileprivate var recordButton: UIButton!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    // Variables
    fileprivate lazy var viewModel: ExerciseReviseViewModel = {
        return ExerciseReviseViewModel(withPhase: phase, phaseExercise: phaseExercise)
    }()
    fileprivate var moveTimer: Timer?
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
        updateContent()
        updateState()
    }

}

// MARK: - Targets
extension ExerciseReviseViewController {
    
    @IBAction fileprivate func closeButtonPressed(_ sender: UIButton) {
        guard viewModel.isPendingFinalResult == false else {
            return
        }
        SpeechRecognition.shared.stopListening(withResults: false)
        logExerciseCancelEvent()
        dismiss(animated: true)
    }
    
    @IBAction fileprivate func nextPageButtonPressed(_ sender: UIButton) {
        viewModel.moveToNextPage()
        updateContent()
        SpeechRecognition.shared.startListening()
        logMoveToNextExercisePageEvent()
    }
    
    @IBAction fileprivate func recordButtonPressed(_ sender: UIButton) {
        if SpeechRecognition.shared.state == .listening {
            viewModel.isPendingFinalResult = true
            SpeechRecognition.shared.stopListening()
            logMicEvent(withState: false)
        } else {
            SpeechRecognition.shared.startListening()
            logMicEvent(withState: true)
        }
    }
    
    @objc fileprivate func moveTimerDidTrigger(_ sender: Timer) {
        viewModel.isCurrentPageCompleted = true
        if SpeechRecognition.shared.state == .listening {
            viewModel.isPendingFinalResult = true
            SpeechRecognition.shared.stopListening()
        }
        updateContent()
    }
    
}

// MARK: - SpeechRecognitionDelegate
extension ExerciseReviseViewController: SpeechRecognitionDelegate {
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, didChange state: KIOSRecognizerState) {
        if let _timer = moveTimer {
            _timer.invalidate()
        }
        if state == .listening {
            scheduleMoveTimer()
        }
        updateState()
        updateContent()
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, partial result: KIOSResult) {
        let hasNewResults = viewModel.processSpeechRecognizerPartialResult(result)
        if hasNewResults {
            Analytics.logVoiceRecognitionSuccess(result.cleanText)
        } else {
            Analytics.logVoiceRecognitionFailed(result.cleanText)
        }
    }
    
    func speechRecognition(_ speechRecognition: SpeechRecognition, final result: KIOSResult) {
        guard viewModel.isPendingFinalResult else {
            return
        }
        viewModel.processSpeechRecognizerFinalResult(result)
        DispatchQueue.main.async {
            self.viewModel.isPendingFinalResult = false
            self.proceedAfterFinalResults()
            self.updateState()
        }
    }
    
    func speechRecognitionReadyToListenAfterInterruption(_ speechRecognition: SpeechRecognition) {
        updateState()
    }
    
}

// MARK: - ExerciseFeedbackViewControllerDelegate
extension ExerciseReviseViewController: ExerciseFeedbackViewControllerDelegate {
    
    func exerciseFeedbackViewController(_ viewController: ExerciseFeedbackViewController,
                                           didSelect action: ExerciseFeedbackViewController.Action) {
        guard let _delegate = delegate else {
            return
        }
        if action == .next {
            _delegate.exerciseViewController(self, didSelect: .phase)
        } else if action == .home {
            _delegate.exerciseViewController(self, didSelect: .home)
        }
    }
    
}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseReviseViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension ExerciseReviseViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        SpeechRecognition.shared.delegate = self
        if let _exercise = phaseExercise.exercise as? ExerciseRevise {
            SpeechRecognition.shared.prepareForListening(withDecodingGraph: _exercise.identifier)
        }
        nextPageButton.highlightEntireView = true
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            if _self.viewModel.isPendingFinalResult {
                _self.viewModel.isPendingFinalResult = false
                _self.proceedAfterFinalResults()
                _self.updateState()
            }
        }
    }
    
    fileprivate func scheduleMoveTimer() {
        if let _timer = moveTimer {
            _timer.invalidate()
        }
        moveTimer = Timer.scheduledTimer(timeInterval: 3.0, target: self,
                                              selector: #selector(moveTimerDidTrigger(_:)), userInfo: nil, repeats: true)
    }
    
    fileprivate func proceedAfterFinalResults() {
        if viewModel.isExerciseCompletable() {
            logExerciseCompletedEvent()
            viewModel.completeExercise()
        }
        guard viewModel.isCompleted,
            feedbackViewController == nil else {
            return
        }
        let score = viewModel.makeExerciseScore()
        let debugInfo = viewModel.exerciseScoreDebugInfo()
        presentFeedbackViewController(withDelegate: self, score: score, debugInfo: debugInfo)
    }
    
    // MARK: Update
    
    fileprivate func updateContent() {
        progressView.update(withIndex: viewModel.index,
                            count: viewModel.pages.count,
                            isCompleted: viewModel.isCurrentPageCompleted)
        if let _text = viewModel.text {
            textLabel.text = _text.value
        }
    }
    
    fileprivate func updateState() {
        if viewModel.text == nil ||
            viewModel.isCompleted ||
            viewModel.isCurrentPageCompleted ||
            viewModel.isPendingFinalResult ||
            SpeechRecognition.shared.state == .needsDecodingGraph {
            recordButton.isHidden = true
        } else {
            recordButton.isHidden = false
        }
        if SpeechRecognition.shared.state == .readyToListen {
            recordButton.isSelected = false
        } else {
            recordButton.isSelected = true
        }
        if viewModel.isPendingFinalResult ||
            viewModel.isCurrentPageCompleted == false {
            nextPageButton.isHidden = true
        } else {
            nextPageButton.isHidden = !viewModel.hasNextPage()
        }
        if viewModel.isPendingFinalResult {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
    }
    
}
