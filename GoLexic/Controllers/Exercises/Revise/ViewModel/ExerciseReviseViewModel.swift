//
//  ExerciseReviseViewModel.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 26/03/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import Cubemobile

class ExerciseReviseViewModel {
    
    // MARK: Properties
    
    // Variables
    var phase: Phase
    var phaseExercise: PhaseExercise
    fileprivate(set) lazy var exercise: ExerciseRevise? = {
        return phaseExercise.exercise as? ExerciseRevise
    }()
    fileprivate(set) lazy var pages: [ExerciseRevisePage] = {
        return makeExercisePages(fromPhaseExercise: phaseExercise)
    }()
    fileprivate(set) var index: Int = 0
    fileprivate(set) var text: ExerciseText?
    fileprivate(set) var currentText: ExerciseText?
    fileprivate var results: [ExerciseText] = []
    
    fileprivate lazy var speechProcessor: SpeechProcessor = {
        return SpeechProcessor()
    }()
    
    // Flags
    fileprivate(set) var isCompleted: Bool = false
    var isCurrentPageCompleted: Bool = false
    var isPendingFinalResult: Bool = false
    
    // MARK: Initialziers
    
    init(withPhase phase: Phase, phaseExercise: PhaseExercise) {
        self.phase = phase
        self.phaseExercise = phaseExercise
        updateCurrentText()
    }

}

// MARK: - Public Functions
extension ExerciseReviseViewModel {
    
    // MARK: Exercise
    
    func isExerciseCompletable() -> Bool {
        return hasNextPage() == false
    }
    
    func completeExercise() {
        guard isCompleted == false else {
            return
        }
        isCompleted = true
        let realm = try! Realm()
        try! realm.write {
            phaseExercise.markAsCompleted()
        }
        let confidence = makeAvarageConfidence(forTexts: results)
        if let scoring = scoring(forConfidence: confidence) {
            submitResults(withScore: confidence, scoring: scoring)
        }
        submitStatistics(withResults: results)
    }
    
    func makeExerciseScore() -> Score {
        let confidence = makeAvarageConfidence(forTexts: results)
        guard let scoring = scoring(forConfidence: confidence) else {
            return .none
        }
        return scoring.score
    }
    
    // MARK: Page
    
    func hasNextPage() -> Bool {
        let nextIndex = index + 1
        return nextIndex < pages.count
    }
    
    @discardableResult
    func moveToNextPage() -> Bool {
        let nextPageIndex = index + 1
        guard nextPageIndex < pages.count else {
            return false
        }
        index = nextPageIndex
        isCurrentPageCompleted = false
        updateCurrentText()
        return true
    }
    
    // MARK: Speech Recognition
    
    @discardableResult
    func processSpeechRecognizerPartialResult(_ result: KIOSResult) -> Bool {
        guard let _text = text else {
            return false
        }
        var isNewSearch: Bool = false
        if currentText?.identifier != _text.identifier {
            currentText = _text
            isNewSearch = true
        }
        let words = speechProcessor.processResult(result, isNewSearch: isNewSearch)
        guard _text.isEqualToAny(words, includeSimilarities: false) else {
            return false
        }
        _text.status = .recognized
        return true
    }
    
    func processSpeechRecognizerFinalResult(_ result: KIOSResult) {
        for text in results {
            guard text.status == .recognized else {
                continue
            }
            text.updateConfidenceIfMatches(result.words, includeSimilarities: false)
        }
        speechProcessor.reset()
    }
    
    // MARK: Other
    
    func exerciseScoreDebugInfo() -> String? {
        let avarageConfidence = makeAvarageConfidence(forTexts: results)
        let convertedConfidence = Int(avarageConfidence*100)
        return "Completed with avarage confidence of \(convertedConfidence)%"
    }
    
}

// MARK: - Private Functions
extension ExerciseReviseViewModel {
    
    // MARK: Helpers
    
    fileprivate func updateCurrentText() {
        guard index < pages.count else {
            return
        }
        let page = pages[index]
        text = ExerciseText(withRevisePage: page)
        results.append(text!)
    }
    
    fileprivate func scoring(forConfidence confidence: Double) -> ExerciseReviseScoring? {
        guard let _exercise = exercise else {
            return nil
        }
        let sortedScorings = _exercise.scorings.sorted(byKeyPath: "confidence", ascending: false)
        for scoring in sortedScorings {
            if confidence >= scoring.confidence {
                return scoring
            }
        }
        return nil
    }
    
    fileprivate func submitResults(withScore score: Double, scoring: ExerciseReviseScoring) {
        let params = makeResultsBody(withScore: score, scoring: scoring)
        let target = GoLexicTarget(withEndpoint: .reviseResults, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    fileprivate func submitStatistics(withResults results: [ExerciseText]) {
        guard let _exercise = exercise,
            let _wordScoring = _exercise.wordScoring else {
            return
        }
        let params = makeStatisticsBody(withResults: results, wordScoring: _wordScoring)
        let target = GoLexicTarget(withEndpoint: .statisticsWords, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    // MARK: Make
    
    fileprivate func makeExercisePages(fromPhaseExercise phaseExercise: PhaseExercise) -> [ExerciseRevisePage] {
        var _pages: [ExerciseRevisePage] = []
        guard let _exercise = phaseExercise.exercise as? ExerciseRevise else {
            return _pages
        }
        _pages.append(contentsOf: _exercise.pages)
        return _pages
    }
    
    fileprivate func makeAvarageConfidence(forTexts texts: [ExerciseText]) -> Double {
        var total: Double = 0
        for text in texts {
            total += text.confidence
        }
        return total/Double(texts.count)
    }
    
    fileprivate func makeResultsBody(withScore score: Double, scoring: ExerciseReviseScoring) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(scoring.identifier, forKey: "scoringId")
        body.updateValue(score, forKey: "score")
        return body
    }
    
    fileprivate func makeStatisticsBody(withResults results: [ExerciseText], wordScoring: ScoringWord) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(wordScoring.identifier, forKey: "wordScoringId")
        var _letters: [[String: Any]] = []
        for result in results {
            let item: [String : Any] = ["word": result.value,
                                        "score": result.confidence]
            _letters.append(item)
        }
        body.updateValue(_letters, forKey: "words")
        return body
    }
    
}
