//
//  ExerciseLostLettersViewModel.swift
//  GoLexic
//
//  Created by Armands L. on 11/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import Foundation
import RealmSwift
import Cubemobile

class ExerciseLostLettersViewModel {
    
    // MARK: Properties
    
    // Variables
    var phase: Phase
    var phaseExercise: PhaseExercise
    fileprivate(set) lazy var exercise: ExerciseLostLetters? = {
        return phaseExercise.exercise as? ExerciseLostLetters
    }()
    fileprivate(set) lazy var letters: [ExerciseLetter] = {
        return makeExerciseLetters(fromExercise: exercise)
    }()
    
    // Flags
    fileprivate(set) var isCompleted: Bool = false
    
    // MARK: Initialziers
    
    init(withPhase phase: Phase, phaseExercise: PhaseExercise) {
        self.phase = phase
        self.phaseExercise = phaseExercise
    }
    
}

// MARK: - Public Functions
extension ExerciseLostLettersViewModel {
    
    // MARK: Exercise
    
    @discardableResult
    func completeExercise() -> Score {
        guard isCompleted == false else {
            return makeExerciseScore()
        }
        isCompleted = true
        let realm = try! Realm()
        try! realm.write {
            phaseExercise.markAsCompleted()
        }
        let count = countLetters(withStatus: .selected)
        submitResults(withScore: count)
        submitStatistics(withResults: letters)
        return makeExerciseScore()
    }
    
    func makeExerciseScore() -> Score {
        guard let _exercise = exercise else {
            return .none
        }
        let count = countLetters(withStatus: .selected)
        let isExercisePassed = count >= _exercise.passThreshold
        return isExercisePassed ? .success : .failed
    }
    
    // MAKR: Letter
    
    func isLetterNextInLine(_ letter: ExerciseLetter) -> Bool {
        var _nextLetter: ExerciseLetter?
        for letter in letters {
            if letter.status == .none {
                _nextLetter = letter
                break
            }
        }
        guard letter == _nextLetter else {
            return false
        }
        return true
    }
    
    func hasAnyLetter(withStatus status: RecognitionStatus) -> Bool {
        for letter in letters {
            if letter.status == status {
                return true
            }
        }
        return false
    }
    
    // MARK: Other
    
    func markNextUnrecognizedLetterAsIsSuggestable() {
        for letter in letters {
            guard letter.status == .none else {
                continue
            }
            letter.isFailed = true
            letter.isSuggested = true
            break
        }
    }
    
    func exerciseScoreDebugInfo() -> String? {
        let passedLetters = countLetters(withStatus: .selected)
        return "Completed \(passedLetters)/\(letters.count) letters"
    }
    
}

// MARK: - Private Functions
extension ExerciseLostLettersViewModel {
    
    // MARK: Helpers
    
    fileprivate func countLetters(withStatus status: RecognitionStatus) -> Int {
        var count: Int = 0
        for letter in letters {
            guard letter.isFailed == false,
                letter.status == status else {
                continue
            }
            count += 1
        }
        return count
    }
    
    fileprivate func submitResults(withScore score: Int) {
        let params = makeResultsBody(withScore: score)
        let target = GoLexicTarget(withEndpoint: .lostLettersResults, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    fileprivate func submitStatistics(withResults results: [ExerciseLetter]) {
        guard let _exercise = exercise,
            let _letterScoring = _exercise.letterScoring else {
            return
        }
        let params = makeStatisticsBody(withResults: results, letterScoring: _letterScoring)
        let target = GoLexicTarget(withEndpoint: .statisticsLetters, params: params)
        TargetEngine.execute(target, queue: .sync)
    }
    
    // MARK: Make
    
    fileprivate func makeExerciseLetters(fromExercise exercise: ExerciseLostLetters?) -> [ExerciseLetter] {
        var _letters: [ExerciseLetter] = []
        guard let _exercise = exercise,
            let _exerciseLetters = _exercise.letters else {
            return _letters
        }
        for item in _exerciseLetters.items {
            let letter = ExerciseLetter(withLetterItem: item)
            let random = arc4random_uniform(100)
            if random > 50 {
                letter.status = .selected
            }
            _letters.append(letter)
        }
        return _letters
    }
    
    fileprivate func makeResultsBody(withScore score: Int) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(score, forKey: "score")
        return body
    }
    
    fileprivate func makeStatisticsBody(withResults results: [ExerciseLetter], letterScoring: ScoringLetter) -> [String: Any] {
        var body: [String: Any] = [:]
        body.updateValue(phase.identifier, forKey: "phaseId")
        body.updateValue(phaseExercise.exerciseId, forKey: "exerciseId")
        body.updateValue(letterScoring.identifier, forKey: "letterScoringId")
        var _letters: [[String: Any]] = []
        for result in results {
            let score: Double = {
                guard result.isFailed == false,
                    result.status == .selected else {
                    return 0
                }
                return 1
            }()
            let item: [String : Any] = ["letter": result.value.uppercased(),
                                        "score": score]
            _letters.append(item)
        }
        body.updateValue(_letters, forKey: "letters")
        return body
    }
    
}
