//
//  ExerciseViewController.swift
//  GoLexic
//
//  Created by Armands L. on 12/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Mixpanel
import SwiftyUserDefaults

protocol ExerciseViewControllerDelegate: class {

    func exerciseViewController(_ viewController: ExerciseViewController,
                                didSelect action: ExerciseViewController.Action)
    
}

class ExerciseViewController: UIViewController {
    
    // MARK: Enums
    
    enum Action {
        case home
        case phase
    }
    
    // MARK: Properties
    
    // IB
    weak var instructionViewController: ExerciseInstructionsViewController?
    weak var feedbackViewController: ExerciseFeedbackViewController?
    
    // Variables
    var phase: Phase!
    var phaseExercise: PhaseExercise!
    weak var delegate: ExerciseViewControllerDelegate?
    fileprivate let exerciseViewModel = ExerciseViewModel()
    
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        logExerciseStartEvent()
        if shouldShowIntro(forExerciseType: phaseExercise.exerciseType) {
            embedInstructionViewController()
            logInstructionsViewedEvent()
        } else {
            incrementIntro(forExerciseType: phaseExercise.exerciseType)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let params = exerciseViewModel.makeExerciseAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.screenTouch, properties: params)
    }
    
    // MARK: Main
    
    func handleInstructionAction(_ action: ExerciseInstructionsViewController.Action) {
        // Do nothing
    }

}

// MARK: - Public Functions
extension ExerciseViewController {
    
    // MARK: Helpers
    
    func presentFeedbackViewController(withDelegate delegate: ExerciseFeedbackViewControllerDelegate?,
                                       score: Score, debugInfo: String? = nil) {
        let feedbackVC = show(ExerciseFeedbackViewController.self)
        feedbackVC.delegate = delegate
        feedbackVC.phaseExercise = phaseExercise
        feedbackVC.score = score
        feedbackVC.debugInfo = debugInfo
        feedbackViewController = feedbackVC
    }
    
    // MARK: Analytics
    
    func logInstructionsViewedEvent() {
        let params = exerciseViewModel.makeExerciseAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.instructionsViewed, properties: params)
    }
    
    func logExerciseStartEvent() {
        let params = exerciseViewModel.makeExerciseAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.startExercise, properties: params)
    }
    
    func logExerciseCompletedEvent() {
        let params = exerciseViewModel.makeExerciseAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.completeExercise, properties: params)
    }
    
    func logExerciseCancelEvent() {
        let params = exerciseViewModel.makeExerciseAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.cancelExercise, properties: params)
    }
    
    func logMoveToNextExercisePageEvent() {
        let params = exerciseViewModel.makeExerciseAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.nextExercisePage, properties: params)
    }
    
    func logMoveToNextExerciseLessonEvent() {
        let params = exerciseViewModel.makeExerciseAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.nextExerciseLesson, properties: params)
    }
    
    func logRedoExerciseEvent() {
        let params = exerciseViewModel.makeExerciseAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.redoExercise, properties: params)
    }
    
    func logMicEvent(withState state: Bool) {
        let params = exerciseViewModel.makeMicAnalyticsParams(forPhaseExercise: phaseExercise, state: state)
        Mixpanel.mainInstance().track(event: Analytics.Events.mic, properties: params)
    }
    
    func logHelpEvent() {
        let params = exerciseViewModel.makeHelpAnalyticsParams(forPhaseExercise: phaseExercise)
        Mixpanel.mainInstance().track(event: Analytics.Events.help, properties: params)
    }
    
}

// MARK: - ExerciseIntroViewControllerDelegate
extension ExerciseViewController: ExerciseInstructionsViewControllerDelegate {
    
    func exerciseInstructionsViewController(_ viewController: ExerciseInstructionsViewController,
                                           didSelect action: ExerciseInstructionsViewController.Action) {
        switch action {
        case .close:
            dismiss(animated: true)
        case .start:
            dismissIntroViewController()
        }
        handleInstructionAction(action)
        let params = exerciseViewModel.makeIntroAnalyticsParams(forPhaseExercise: phaseExercise, action: action)
        Mixpanel.mainInstance().track(event: Analytics.Events.intro, properties: params)
    }
    
}

// MARK: - Private Functions
extension ExerciseViewController {
    
    // MARK: Helpers
    
    fileprivate func embedInstructionViewController() {
        let _instructionVC = instantiate(ExerciseInstructionsViewController.self)
        _instructionVC.phaseExercise = phaseExercise
        _instructionVC.delegate = self
        instructionViewController = _instructionVC
        // Embed in view
        addChild(_instructionVC)
        _instructionVC.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(_instructionVC.view)
        addConstraints(toIntroView: _instructionVC.view)
        _instructionVC.didMove(toParent: self)
    }
    
    fileprivate func addConstraints(toIntroView introView: UIView) {
        view.leadingAnchor.constraint(equalTo: introView.leadingAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: introView.topAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: introView.trailingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: introView.bottomAnchor, constant: 0).isActive = true
    }
    
    fileprivate func dismissIntroViewController() {
        guard let _instructionVC = instructionViewController else {
            return
        }
        UIView.animate(withDuration: 0.23, animations: {
            _instructionVC.view.alpha = 0
        }) { _ in
            _instructionVC.view.removeFromSuperview()
            _instructionVC.removeFromParent()
        }
    }
    
    fileprivate func shouldShowIntro(forExerciseType exerciseType: ExerciseType) -> Bool {
        switch exerciseType {
        case .alphabetPractice:
            return Defaults[\.introViewsAlphabetPractice] < 2
        case .lostLetters:
            return Defaults[\.introViewsLostLetters] < 2
        case .alphabetSorting:
            return Defaults[\.introViewsAlphabetSorting] < 2
        case .letterBoard:
            return Defaults[\.introViewsLetterBoard] < 2
        case .vowels:
            return Defaults[\.introViewsVowels] < 2
        case .reading:
            return Defaults[\.introViewsReading] < 2
        case .building:
            return Defaults[\.introViewsBuilding] < 2
        case .revise:
            return Defaults[\.introViewsRevise] < 2
        default:
            return false
        }
    }
    
    fileprivate func incrementIntro(forExerciseType exerciseType: ExerciseType) {
        switch exerciseType {
        case .alphabetPractice:
            Defaults[\.introViewsAlphabetPractice] += 1
        case .lostLetters:
            Defaults[\.introViewsLostLetters] += 1
        case .alphabetSorting:
            Defaults[\.introViewsAlphabetSorting] += 1
        case .letterBoard:
            Defaults[\.introViewsLetterBoard] += 1
        case .vowels:
            Defaults[\.introViewsVowels] += 1
        case .reading:
            Defaults[\.introViewsReading] += 1
        case .building:
            Defaults[\.introViewsBuilding] += 1
        case .revise:
            Defaults[\.introViewsRevise] += 1
        default:
            break
        }
    }
    
}
