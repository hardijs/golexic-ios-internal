//
//  ExerciseInstructionsViewController.swift
//  GoLexic
//
//  Created by Armands L. on 12/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import BonMot
import RealmSwift

protocol ExerciseInstructionsViewControllerDelegate: class {
    
    func exerciseInstructionsViewController(_ viewController: ExerciseInstructionsViewController,
                                            didSelect action: ExerciseInstructionsViewController.Action)
    
}

class ExerciseInstructionsViewController: UIViewController {
    
    // MARK: Enums
    
    enum Action: Int {
        case close
        case start
    }

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var navigationView: NavigationView!
    @IBOutlet fileprivate var contentView: AttributedView!
    @IBOutlet fileprivate var containerView: UIView!
    @IBOutlet fileprivate var pageControl: UIPageControl!
    @IBOutlet fileprivate var startButton: AttributedButton!
    @IBOutlet fileprivate var audioButton: UIButton!
    fileprivate var pageViewController: UIPageViewController!
    
    // Variables
    weak var delegate: ExerciseInstructionsViewControllerDelegate?
    var phaseExercise: PhaseExercise!
    fileprivate(set) var index: Int = 0 {
        didSet {
            pageControl.currentPage = index
        }
    }
    fileprivate var pendingIndex: Int = 0
    fileprivate lazy var images: [String] = {
        return makeImages(fromPhaseExercise: phaseExercise)
    }()
    
    // Flags
    fileprivate var automaticallyPlaybackAudio: Bool = true
    fileprivate var isPlayingInstruction: Bool = false {
        didSet {
            audioButton.isSelected = isPlayingInstruction
        }
    }
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        updateContent()
        loadItemViewController(withIndex: index, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if automaticallyPlaybackAudio {
            automaticallyPlaybackAudio = false
            playInstruction()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let _targetVC = segue.destination as? UIPageViewController {
            pageViewController = _targetVC
            pageViewController.delegate = self
            pageViewController.dataSource = self
        }
    }

}

// MARK: - Targets
extension ExerciseInstructionsViewController {
    
    @IBAction fileprivate func startButtonPressed(_ sender: UIButton) {
        SoundPlayer.shared.stop()
        if let _delegate = delegate {
            _delegate.exerciseInstructionsViewController(self, didSelect: .start)
        }
    }
    
    @IBAction fileprivate func audioButtonPressed(_ sender: UIButton) {
        guard isPlayingInstruction == false else {
            return
        }
        playInstruction()
    }
    
}

// MARK: - NavigationViewDelegate
extension ExerciseInstructionsViewController: NavigationViewDelegate {
    
    func navigationView(_ view: NavigationView, didSelect action: NavigationView.Action) {
        SoundPlayer.shared.stop()
        guard action == .back else {
            return
        }
        dismiss(animated: true)
    }
    
}

// MARK: - UIPageViewControllerDataSource
extension ExerciseInstructionsViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return makePageViewController(withIndex: index+1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return makePageViewController(withIndex: index-1)
    }
    
}

// MARK: - UIPageViewControllerDelegate
extension ExerciseInstructionsViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            willTransitionTo pendingViewControllers: [UIViewController]) {
        guard pendingViewControllers.count > 0 else {
            return
        }
        let viewController = pendingViewControllers.first! as! ExerciseInstructionPageViewController
        pendingIndex = viewController.index
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed == true else {
            return
        }
        if previousViewControllers.count > 0 {
            let viewController = previousViewControllers.last! as! ExerciseInstructionPageViewController
            guard viewController.index != pendingIndex else {
                return
            }
        }
        index = pendingIndex
        if startButton.isHidden {
            let isStartHidden = index < images.count-1
            startButton.setHidden(isStartHidden, animated: true)
        }
    }
    
}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseInstructionsViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension ExerciseInstructionsViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        navigationView.delegate = self
        containerView.layer.cornerRadius = 10
        containerView.layer.masksToBounds = true
        contentView.shadowOffset = CGSize(width: 0, height: 4)
    }
    
    fileprivate func updateContent() {
        navigationView.titleKey = phaseExercise.exerciseType.makeLocalizationKey()
        pageControl.numberOfPages = images.count
        let isStartHidden = index < images.count-1
        startButton.setHidden(isStartHidden, animated: false)
    }
    
    fileprivate func loadItemViewController(withIndex index: Int, animated: Bool) {
        guard let viewController = makePageViewController(withIndex: index) else {
            return
        }
        pageViewController.setViewControllers([viewController], direction: .forward, animated: animated)
    }
    
    fileprivate func playInstruction() {
        guard let _exercise = phaseExercise.exercise,
            let _instruction = _exercise.instruction,
            let audioUrl = _instruction.audioUrl,
            let fileUrl = FileProvider.shared.fileUrl(forUrlString: audioUrl) else {
            return
        }
        let sound = Sound(withUrl: fileUrl)
        try? SoundPlayer.shared.play(sound: sound) { [weak self] status in
            guard let _self = self else {
                return
            }
            _self.isPlayingInstruction = status == .playing
        }
    }
    
    // MARK: Make
    
    fileprivate func makePageViewController(withIndex index: Int) -> ExerciseInstructionPageViewController? {
        guard index >= 0,
            index < images.count else {
            return nil
        }
        let imageUrl = images[index]
        let viewController = instantiate(ExerciseInstructionPageViewController.self)
        viewController.index = index
        viewController.imageUrl = imageUrl
        return viewController
    }
    
    fileprivate func makeImages(fromPhaseExercise phaseExercise: PhaseExercise) -> [String] {
        guard let _exercise = phaseExercise.exercise,
            let _instruction = _exercise.instruction else {
            return []
        }
        return Array(_instruction.images)
    }
    
}
