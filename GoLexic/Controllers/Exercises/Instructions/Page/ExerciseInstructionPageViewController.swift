//
//  ExerciseInstructionPageViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 11/05/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Kingfisher

class ExerciseInstructionPageViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var imageView: UIImageView!
    
    // Variables
    var index: Int!
    var imageUrl: String!

    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateContent()
    }

}

// MARK: - Private Functions
extension ExerciseInstructionPageViewController {
    
    fileprivate func updateContent() {
        guard let _imageUrl = imageUrl else {
            return
        }
        if let url = URL(string: _imageUrl) {
            let options: KingfisherOptionsInfo = [.transition(.fade(0.2))]
            imageView.kf.setImage(with: url, options: options)
        }
    }
    
}
