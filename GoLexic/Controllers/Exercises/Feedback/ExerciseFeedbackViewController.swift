//
//  ExerciseFeedbackViewController.swift
//  GoLexic
//
//  Created by Armands L. on 11/07/2019.
//  Copyright © 2019 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import RealmSwift

protocol ExerciseFeedbackViewControllerDelegate: class {
    
    func exerciseFeedbackViewController(_ viewController: ExerciseFeedbackViewController,
                                           didSelect action: ExerciseFeedbackViewController.Action)
    
}

class ExerciseFeedbackViewController: UIViewController {
    
    // MARK: Enums
    
    enum Action {
        case home
        case next
    }

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var navigationView: NavigationView!
    @IBOutlet fileprivate var contentView: AttributedView!
    @IBOutlet fileprivate var statusLabel: UILabel!
    @IBOutlet fileprivate var imageView: UIImageView!
    @IBOutlet fileprivate var nextButton: AttributedButton!
    @IBOutlet fileprivate var audioButton: UIButton!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var infoLabel: UILabel!
    
    // Variables
    weak var delegate: ExerciseFeedbackViewControllerDelegate?
    var phaseExercise: PhaseExercise!
    var score: Score!
    var debugInfo: String?
    fileprivate lazy var feedback: Feedback? = {
        guard let _exercise = phaseExercise.exercise else {
            return nil
        }
       return makeRandomFeedback(forScore: score, feedbacks: _exercise.feedbacks)
    }()
    
    // Flags
    fileprivate var automaticallyPlaybackAudio: Bool = true
    fileprivate var isPlayingInstruction: Bool = false {
        didSet {
            audioButton.isSelected = isPlayingInstruction
        }
    }
    fileprivate var isUpdating: Bool = false {
        didSet {
            updateState()
        }
    }
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        updateContent()
        updateState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if automaticallyPlaybackAudio {
            automaticallyPlaybackAudio = false
            playFeedback()
        }
    }

}

// MARK: - Targets
extension ExerciseFeedbackViewController {
    
    @IBAction fileprivate func nextButtonPressed(_ sender: UIButton) {
        SoundPlayer.shared.stop()
        if let _delegate = delegate {
            _delegate.exerciseFeedbackViewController(self, didSelect: .next)
        }
    }
    
    @IBAction fileprivate func audioButtonPressed(_ sender: UIButton) {
        guard isPlayingInstruction == false else {
            return
        }
        playFeedback()
    }
    
}

// MARK: - NavigationViewDelegate
extension ExerciseFeedbackViewController: NavigationViewDelegate {
    
    func navigationView(_ view: NavigationView, didSelect action: NavigationView.Action) {
        guard let _delegate = delegate else {
            return
        }
        SoundPlayer.shared.stop()
        if action == .back {
            _delegate.exerciseFeedbackViewController(self, didSelect: .home)
        }
    }
    
}

//MARK: - UIViewControllerTransitioningDelegate
extension ExerciseFeedbackViewController: UIViewControllerTransitioningDelegate {
    
    open func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    open func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = FadeTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension ExerciseFeedbackViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        navigationView.delegate = self
    }
    
    fileprivate func updateContent() {
        imageView.image = makeRandomFeedbackImage()
        navigationView.titleKey = phaseExercise.exerciseType.makeLocalizationKey()
        if let _feedback = feedback {
            statusLabel.text = _feedback.info
        }
        infoLabel.text = debugInfo
    }
    
    fileprivate func updateState() {
        if isUpdating {
            activityIndicator.startAnimating()
        } else {
            activityIndicator.stopAnimating()
        }
        navigationView.isBackEnabled = !isUpdating
        nextButton.setEnabled(!isUpdating, animated: false)
    }
    
    fileprivate func playFeedback() {
        guard let _feedback = feedback,
            let audioUrl = _feedback.audioUrl,
            let fileUrl = FileProvider.shared.fileUrl(forUrlString: audioUrl) else {
            return
        }
        let sound = Sound(withUrl: fileUrl)
        try? SoundPlayer.shared.play(sound: sound) { [weak self] status in
            guard let _self = self else {
                return
            }
            _self.isPlayingInstruction = status == .playing
        }
    }
    
    // MARK: Make
    
    fileprivate func makeRandomFeedbackImage() -> UIImage? {
        let random = Int.random(in: 1..<5)
        let imageName = "img_feedback_\(random)"
        return UIImage(named: imageName)
    }
    
    fileprivate func makeRandomFeedback(forScore score: Score,
                                        feedbacks: Results<Feedback>) -> Feedback? {
        let status = makeFeedbackStatus(forScore: score)
        let validFeedbacks = feedbacks.filter("statusRaw == \(status.rawValue)")
        guard validFeedbacks.isEmpty == false else {
            return nil
        }
        if validFeedbacks.count == 1 {
            return validFeedbacks.first
        }
        let random = Int.random(in: 0..<validFeedbacks.count)
        return validFeedbacks[random]
    }
    
    fileprivate func makeFeedbackStatus(forScore score: Score) -> Feedback.Status {
        switch score {
        case .success:
            return .success
        case .maybe:
            return .maybe
        case .failed:
            return .failed
        default:
            return .unknown
        }
    }
    
}
