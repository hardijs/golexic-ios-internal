//
//  SignupViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 24/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class SignupViewController: UIViewController {

    // MARK: Properties
    
    // IB
    @IBOutlet var backButton: AttributedButton!
    @IBOutlet fileprivate var imageView: UIImageView!
    @IBOutlet fileprivate var scrollView: UIScrollView!
    @IBOutlet fileprivate var inputStackView: UIStackView!
    
    @IBOutlet fileprivate var termsToggleButton: UIButton!
    @IBOutlet fileprivate var termsTextView: UITextView!
    
    @IBOutlet fileprivate var dataToggleButton: UIButton!
    @IBOutlet fileprivate var dataTextView: UITextView!
        
    @IBOutlet fileprivate var createButton: LocalizedButton!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    // Constraints
    @IBOutlet fileprivate var inputViewTopConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate var viewModel = SignupViewModel()
    
    // Flags
    fileprivate var didUpdateContentOffset: Bool = false
    fileprivate var isUpdating: Bool = false {
        didSet {
            if isUpdating {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
            backButton.isUserInteractionEnabled = !isUpdating
            termsToggleButton.isUserInteractionEnabled = !isUpdating
            createButton.isUserInteractionEnabled = !isUpdating
        }
    }
    
    // MAKR: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
        updateContent()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if didUpdateContentOffset == false {
            didUpdateContentOffset = true
            updateContentInsets(withKeyboardHeight: 0)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

// MARK: - Targets
extension SignupViewController {
    
    @IBAction fileprivate func backButtonPressed(_ sender: UIButton) {
        navigationController!.popViewController(animated: true)
    }
    
    @IBAction fileprivate func termsToggleButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let isNextEnabled = isNextPossible()
        createButton.setEnabled(isNextEnabled, animated: false)
    }
    
    @IBAction fileprivate func dataToggleButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let isNextEnabled = isNextPossible()
        createButton.setEnabled(isNextEnabled, animated: false)
    }
    
    @IBAction fileprivate func createButtonPressed(_ sender: UIButton) {
        view.endEditing(true)
        updateInputViewState()
        guard validateAllFields() else {
            return
        }
        isUpdating = true
        viewModel.signup(isAcceptingDataProcessing: dataToggleButton.isSelected,
                         isAcceptingAnalytics: dataToggleButton.isSelected) { [weak self] result in
            guard let _self = self else {
                return
            }
            switch result {
            case let .success(token):
                User.create(withToken: token)
                _self.viewModel.updateUser { result in
                    switch result {
                    case let .success(profile):
                        User.current!.update(.profile, withData: profile)
                        _self.navigationController!.push(GoalsViewController.self)
                    case let .failure(code):
                        User.current?.delete()
                        Globals.presentAlert(withErrorCode: code)
                        _self.isUpdating = false
                    }
                }
            case let .failure(code):
                Globals.presentAlert(withErrorCode: code)
                _self.isUpdating = false
            }
        }
    }
    
    @objc fileprivate func viewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
}

// MARK: - InputViewDelegate
extension SignupViewController: InputViewDelegate {
    
    func inputViewShouldBeginEditting(_ view: InputView) -> Bool {
        return !isUpdating
    }
    
    func inputViewShouldReturn(_ view: InputView) -> Bool {
        view.resignFirstResponder()
        return true
    }
    
    func inputView(_ view: InputView, didChange text: String?) {
        let input = SignupViewModel.Input(rawValue: view.tag)!
        viewModel.updateInput(input, value: text)
        // Update nextButton state
        let isNextEnabled = isNextPossible()
        createButton.setEnabled(isNextEnabled, animated: false)
    }
    
}

// MARK: - UITextViewDelegate
extension SignupViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL,
                  in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        guard isUpdating == false else {
            return false
        }
        let webVC = show(WebGenericViewController.self, useNavigationController: true)
        webVC.urlString = URL.absoluteString
        return false
    }
    
}

// MARK: - UIViewControllerTransitioningDelegate
extension SignupViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension SignupViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        createButton.disableEntireView = true
        createButton.setEnabled(false, animated: false)
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        view.addGestureRecognizer(tapRecognizer)
        for case let inputView as InputView in inputStackView.subviews {
            let input = SignupViewModel.Input(rawValue: inputView.tag)!
            switch input {
            case .email:
                inputView.textContentType = .emailAddress
                inputView.keyboardType = .emailAddress
            case .password:
                inputView.textContentType = .password
                inputView.isSecureTextEntry = true
            }
            inputView.capitalization = .none
            inputView.delegate = self
        }
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self,
                let userInfo = notification.userInfo else {
                return
            }
            _self.updateContentInsets(withUserInfo: userInfo, isAppearing: true)
        }
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self,
                let userInfo = notification.userInfo else {
                return
            }
            _self.updateContentInsets(withUserInfo: userInfo, isAppearing: false)
        }
        NotificationCenter.default.addObserver(forName: Notification.Name.Localization.DidChange,
                                               object: nil, queue: nil) { [weak self] _ in
            guard let _self = self else {
                return
            }
            _self.updateContent()
        }
    }
    
    fileprivate func validateAllFields() -> Bool {
        for case let inputView as InputView in inputStackView.subviews {
            let input = SignupViewModel.Input(rawValue: inputView.tag)!
            if viewModel.validateInput(input) == false {
                return false
            }
        }
        return true
    }
    
    fileprivate func isNextPossible() -> Bool {
        if viewModel.email == nil || viewModel.email!.isEmpty {
            return false
        }
        if viewModel.password == nil || viewModel.password!.isEmpty {
            return false
        }
        if termsToggleButton.isSelected == false {
            return false
        }
        if dataToggleButton.isSelected == false {
            return false
        }
        return true
    }
    
    // MARK: Update
    
    fileprivate func updateContent() {
        termsTextView.attributedText = makeTermsText()
        dataTextView.attributedText = makeDataText()
    }
    
    fileprivate func updateInputViewState() {
        for case let inputView as InputView in inputStackView.subviews {
            let input = SignupViewModel.Input(rawValue: inputView.tag)!
            if viewModel.validateInput(input) {
                inputView.change(state: .normal)
            } else {
                inputView.change(state: .error)
            }
        }
    }
    
    fileprivate func updateContentInsets(withUserInfo userInfo: [AnyHashable: Any], isAppearing: Bool) {
        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let animationCurve: UIView.AnimationOptions = {
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
            return UIView.AnimationOptions(rawValue: curve)
        }()
        let keyboardHeight = isAppearing ? keyboardFrame.height : 0
        updateContentInsets(withKeyboardHeight: keyboardHeight)
        UIView.animate(withDuration: animationDuration, delay: 0, options: animationCurve, animations: {
            self.view.layoutIfNeeded()
            self.imageView.alpha = isAppearing ? 0 : 1
        }, completion: nil)
    }
    
    fileprivate func updateContentInsets(withKeyboardHeight keyboardHeight: CGFloat) {
        let height = view.frame.height-keyboardHeight
        let inset = makeScrollViewTopInset(forHeight: height, isEditing: keyboardHeight > 0)
        inputViewTopConstraint.constant = inset
    }

    // MARK: Make
    
    fileprivate func makeTermsText() -> NSAttributedString? {
        guard let text = Localization.string(forKeyPath: "Signup.terms.text"),
            let terms = Localization.string(forKeyPath: "Signup.terms.terms"),
            let privacy = Localization.string(forKeyPath: "Signup.terms.privacy"),
            let age = Localization.string(forKeyPath: "Signup.terms.age") else {
            return nil
        }
        let _text = NSString(string: text)
        let attributedText = NSMutableAttributedString(string: text)
        // Add base attributes
        let range = NSRange(location: 0, length: text.count)
        let font = UIFont(name: UIFont.Name.Montserrat.regular, size: 18)!
        attributedText.addAttribute(.font, value: font, range: range)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.2156862745, green: 0.2392156863, blue: 0.2470588235, alpha: 1), range: range)
        // Add privacy attributes
        let privacyRange = _text.range(of: privacy)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1), range: privacyRange)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: privacyRange)
        if let _termsUrlString = Localization.string(forKeyPath: "Signup.terms.privacyLink") {
            attributedText.addAttribute(.link, value: _termsUrlString, range: privacyRange)
        }
        // Add terms attributes
        let termsRange = _text.range(of: terms)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1), range: termsRange)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: termsRange)
        if let _termsUrlString = Localization.string(forKeyPath: "Signup.terms.termsLink") {
            attributedText.addAttribute(.link, value: _termsUrlString, range: termsRange)
        }
        // Add age attributes
        let ageRange = _text.range(of: age)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1), range: ageRange)
        return attributedText
    }
    
    fileprivate func makeDataText() -> NSAttributedString? {
        guard let text = Localization.string(forKeyPath: "Signup.data.text"),
            let personal = Localization.string(forKeyPath: "Signup.data.personal"),
            let note = Localization.string(forKeyPath: "Signup.data.note") else {
            return nil
        }
        let _text = NSString(string: text)
        let attributedText = NSMutableAttributedString(string: text)
        // Add base attributes
        let textRange = NSRange(location: 0, length: text.count)
        let textFont = UIFont(name: UIFont.Name.Montserrat.regular, size: 18)!
        attributedText.addAttribute(.font, value: textFont, range: textRange)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.2156862745, green: 0.2392156863, blue: 0.2470588235, alpha: 1), range: textRange)
        // Add personal attributes
        let personalRange = _text.range(of: personal)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1), range: personalRange)
        // Add note attributes
        let noteAttributedText = NSMutableAttributedString(string: " \(note)")
        let noteRange = NSRange(location: 0, length: noteAttributedText.length)
        let noteFont = UIFont(name: UIFont.Name.Montserrat.italic, size: 18)!
        noteAttributedText.addAttribute(.font, value: noteFont, range: noteRange)
        noteAttributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.2156862745, green: 0.2392156863, blue: 0.2470588235, alpha: 1), range: noteRange)
        // Combine
        attributedText.append(noteAttributedText)
        return attributedText
    }
    
    fileprivate func makeAnalyticsText() -> NSAttributedString? {
        guard let text = Localization.string(forKeyPath: "Signup.analytics.text"),
            let personal = Localization.string(forKeyPath: "Signup.analytics.personal"),
            let note = Localization.string(forKeyPath: "Signup.analytics.note") else {
            return nil
        }
        let _text = NSString(string: text)
        let attributedText = NSMutableAttributedString(string: text)
        // Add base attributes
        let textRange = NSRange(location: 0, length: text.count)
        let textFont = UIFont(name: UIFont.Name.Montserrat.regular, size: 18)!
        attributedText.addAttribute(.font, value: textFont, range: textRange)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.2156862745, green: 0.2392156863, blue: 0.2470588235, alpha: 1), range: textRange)
        // Add personal attributes
        let personalRange = _text.range(of: personal)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.05882352941, green: 0.1450980392, blue: 0.462745098, alpha: 1), range: personalRange)
        // Add note attributes
        let noteAttributedText = NSMutableAttributedString(string: " \(note)")
        let noteRange = NSRange(location: 0, length: noteAttributedText.length)
        let noteFont = UIFont(name: UIFont.Name.Montserrat.italic, size: 18)!
        noteAttributedText.addAttribute(.font, value: noteFont, range: noteRange)
        noteAttributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 0.2156862745, green: 0.2392156863, blue: 0.2470588235, alpha: 1), range: noteRange)
        // Combine
        attributedText.append(noteAttributedText)
        return attributedText
    }
    
    fileprivate func makeScrollViewTopInset(forHeight height: CGFloat, isEditing: Bool) -> CGFloat {
        let offset: CGFloat = {
            if isEditing {
                return floor(inputStackView.frame.height*0.7)
            } else {
                return floor(inputStackView.frame.height*1.05)
            }
        }()
        let verticalCenter = height/2
        let contentOffset = verticalCenter-offset
        let minimumOffset = view.safeAreaInsets.top+16
        if contentOffset < minimumOffset {
            return minimumOffset
        }
        return floor(contentOffset)
    }
    
}
