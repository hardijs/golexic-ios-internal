//
//  SignupViewModel.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 25/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class SignupViewModel: NSObject {
    
    // MARK: Enum
    
    enum Input: Int {
        case email
        case password
    }
    
    enum SignupCompletion {
        case success(token: String)
        case failure(code: Int?)
    }
    
    enum UpdateCompletion {
        case success(data: NSDictionary)
        case failure(code: Int?)
    }
    
    // MARK: Properties
    
    // Variables
    var email: String?
    var password: String?

}

// MARK: - Public
extension SignupViewModel {
    
    func updateInput(_ input: Input, value: String?) {
        switch input {
        case .email:
            email = value?.trimmingCharacters(in: .whitespacesAndNewlines)
        case .password:
            password = value
        }
    }
    
    func validateInput(_ input: Input) -> Bool {
        switch input {
        case .email:
            return Globals.isEmailValid(email)
        case .password:
            return password?.isEmpty == false
        }
    }
    
    func signup(isAcceptingDataProcessing: Bool, isAcceptingAnalytics: Bool,
                withCompletion completion: @escaping ((SignupCompletion) -> Void)) {
        let params = makePostBody(isAcceptingDataProcessing: isAcceptingDataProcessing,
                                  isAcceptingAnalytics: isAcceptingAnalytics)
        let target = GoLexicTarget(withEndpoint: .register, params: params)
        GoLexicAPI.provider.request(target) { result in
            switch result {
            case let .success(moyaResponse):
                guard let response = try? moyaResponse.mapJSON(),
                    let data = response as? NSDictionary,
                    let token = data["accessToken"] as? String else {
                        completion(.failure(code: nil))
                        return
                }
                completion(.success(token: token))
            case let .failure(error):
                let code: Int? = {
                    guard let data = try? error.response?.mapJSON() as? NSDictionary,
                        let code = data.value(forKeyPath: "error.code") as? Int else {
                            return nil
                    }
                    return code
                }()
                completion(.failure(code: code))
            }
        }
    }
    
    func updateUser(withCompletion completion: @escaping ((UpdateCompletion) -> Void)) {
        let target = GoLexicTarget(withEndpoint: .user)
        GoLexicAPI.provider.request(target) { result in
            switch result {
            case let .success(moyaResponse):
                guard let response = try? moyaResponse.mapJSON(),
                    let data = response as? NSDictionary else {
                        completion(.failure(code: nil))
                        return
                }
                completion(.success(data: data))
            case let .failure(error):
                let code: Int? = {
                    guard let data = try? error.response?.mapJSON() as? NSDictionary,
                        let code = data.value(forKeyPath: "error.code") as? Int else {
                            return nil
                    }
                    return code
                }()
                completion(.failure(code: code))
            }
        }
    }
    
}

// MARK: - Private
extension SignupViewModel {
    
    // MARK: Make
    
    fileprivate func makePostBody(isAcceptingDataProcessing: Bool,
                                  isAcceptingAnalytics: Bool) -> [String: Any] {
        var body: [String: Any] = [:]
        if let _email = email {
            body.updateValue(_email, forKey: "email")
        }
        if let _password = password {
            body.updateValue(_password, forKey: "password")
        }
        if let _deviceUUID = AppInfo.deviceUUID() {
            body.updateValue(_deviceUUID, forKey: "deviceId")
        }
        body.updateValue(true, forKey: "isAcceptingTerms")
        body.updateValue(isAcceptingDataProcessing,
                         forKey: "isAcceptingDataProcessing")
        body.updateValue(isAcceptingAnalytics,
                         forKey: "isAcceptingAnalytics")
        return body
    }
    
}
