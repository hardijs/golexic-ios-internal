//
//  GoalsViewController.swift
//  GoLexic
//
//  Created by Denis Kuznetsov on 05.03.2021.
//  Copyright © 2021 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class GoalsViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var questionLabel: UILabel!
    @IBOutlet fileprivate weak var continueButton: LocalizedButton!
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }

}

// MARK: - Targets
extension GoalsViewController {
    
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        if User.current!.isBetaUser == false, PurchaseProvider.shared.hasActiveSubscription == false {
            self.navigationController!.push(SubscriptionViewController.self)
        } else {
            let tipsVC = self.navigationController!.push(TipsViewController.self, animated: false)
            tipsVC.canGoBack = false
        }
    }
    
    @IBAction func variantButtonPressed(_ sender: StandardButton) {
        sender.isSelected = !sender.isSelected
        sender.setTitleColor(sender.isSelected ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0.4001449049, green: 0.3929838538, blue: 0.5259512067, alpha: 1), for: .normal)
        sender.backgroundColor = sender.isSelected ? #colorLiteral(red: 0.4392156863, green: 0.4274509804, blue: 0.5921568627, alpha: 1) : #colorLiteral(red: 0.9729318023, green: 0.7684162855, blue: 0.489200592, alpha: 1)
        sender.shadowColor = sender.isSelected ? #colorLiteral(red: 0.4001449049, green: 0.3929838538, blue: 0.5259512067, alpha: 1) : #colorLiteral(red: 0.9607843137, green: 0.7215686275, blue: 0.4156862745, alpha: 1)
    }
    
}
