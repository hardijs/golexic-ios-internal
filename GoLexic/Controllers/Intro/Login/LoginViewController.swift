//
//  LoginViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 21/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import SwiftyUserDefaults

class LoginViewController: UIViewController {

    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var backButton: AttributedButton!
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var scrollView: UIScrollView!
    @IBOutlet fileprivate var inputStackView: UIStackView!
    @IBOutlet fileprivate var nextButton: UIButton!
    @IBOutlet fileprivate var forgotButtonIPhone: UIButton!
    @IBOutlet fileprivate var forgotButtonIPad: UIButton!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    // Constraints
    @IBOutlet fileprivate var inputViewTopConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate var viewModel = LoginViewModel()
    
    // Flags
    fileprivate var didUpdateContentOffset: Bool = false
    fileprivate var isUpdating: Bool = false {
        didSet {
            if isUpdating {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
            backButton.isUserInteractionEnabled = !isUpdating
            nextButton.isUserInteractionEnabled = !isUpdating
            forgotButtonIPhone.isUserInteractionEnabled = !isUpdating
            forgotButtonIPad.isUserInteractionEnabled = !isUpdating
        }
    }
    
    // MAKR: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if didUpdateContentOffset == false {
            didUpdateContentOffset = true
            updateContentInsets(withKeyboardHeight: 0)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

// MARK: - Targets
extension LoginViewController {
    
    @IBAction fileprivate func backButtonPressed(_ sender: UIButton) {
        navigationController!.popViewController(animated: true)
    }
    
    @IBAction fileprivate func nextButtonPressed(_ sender: UIButton) {
        view.endEditing(true)
        updateInputViewState()
        guard validateAllFields() else {
            return
        }
        isUpdating = true
        viewModel.login { [weak self] result in
            guard let _self = self else {
                return
            }
            switch result {
            case let .success(token):
                User.create(withToken: token)
                _self.viewModel.updateUser { result in
                    switch result {
                    case let .success(profile):
                        User.current!.update(.profile, withData: profile)
                        if User.current!.isBetaUser == false, PurchaseProvider.shared.hasActiveSubscription == false {
                            _self.navigationController!.push(SubscriptionViewController.self)
                        } else {
                            if Defaults[\.didShowOnboardingTips] == false {
                                let tipsVC = _self.navigationController!.push(TipsViewController.self, animated: false)
                                tipsVC.canGoBack = false
                            } else {
                                _self.dismiss(animated: true)
                            }
                        }
                    case let .failure(code):
                        User.current?.delete()
                        Globals.presentAlert(withErrorCode: code)
                        _self.isUpdating = false
                    }
                }
            case let .failure(code):
                Globals.presentAlert(withErrorCode: code)
                _self.isUpdating = false
            }
        }
    }
    
    @IBAction fileprivate func forgotButtonPressed(_ sender: UIButton) {
        self.navigationController?.push(PasswordViewController.self)
    }
    
    @objc fileprivate func viewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
}

// MARK: - InputViewDelegate
extension LoginViewController: InputViewDelegate {
    
    func inputViewShouldBeginEditting(_ view: InputView) -> Bool {
        return !isUpdating
    }
    
    func inputViewShouldReturn(_ view: InputView) -> Bool {
        view.resignFirstResponder()
        return true
    }
    
    func inputView(_ view: InputView, didChange text: String?) {
        let input = LoginViewModel.Input(rawValue: view.tag)!
        viewModel.updateInput(input, value: text)
    }
    
}

// MARK: - UIViewControllerTransitioningDelegate
extension LoginViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension LoginViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        if navigationController!.viewControllers.count < 2 {
            backButton.isHidden = true
        }
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        view.addGestureRecognizer(tapRecognizer)
        for case let inputView as InputView in inputStackView.subviews {
            let input = LoginViewModel.Input(rawValue: inputView.tag)!
            switch input {
            case .email:
                inputView.textContentType = .emailAddress
                inputView.keyboardType = .emailAddress
            case .password:
                inputView.textContentType = .password
                inputView.isSecureTextEntry = true
            }
            inputView.capitalization = .none
            inputView.delegate = self
        }
        
        forgotButtonIPhone.isHidden = !Globals.isPhone
        forgotButtonIPad.isHidden = Globals.isPhone
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self,
                let userInfo = notification.userInfo else {
                return
            }
            _self.updateContentInsets(withUserInfo: userInfo, isAppearing: true)
        }
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self,
                let userInfo = notification.userInfo else {
                return
            }
            _self.updateContentInsets(withUserInfo: userInfo, isAppearing: false)
        }
    }
    
    fileprivate func validateAllFields() -> Bool {
        for case let inputView as InputView in inputStackView.subviews {
            let input = LoginViewModel.Input(rawValue: inputView.tag)!
            if viewModel.validateInput(input) == false {
                return false
            }
        }
        return true
    }
    
    // MARK: Update
    
    fileprivate func updateInputViewState() {
        for case let inputView as InputView in inputStackView.subviews {
            let input = LoginViewModel.Input(rawValue: inputView.tag)!
            if viewModel.validateInput(input) {
                inputView.change(state: .normal)
            } else {
                inputView.change(state: .error)
            }
        }
    }
    
    fileprivate func updateContentInsets(withUserInfo userInfo: [AnyHashable: Any], isAppearing: Bool) {
        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let animationCurve: UIView.AnimationOptions = {
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
            return UIView.AnimationOptions(rawValue: curve)
        }()
        let keyboardHeight = isAppearing ? keyboardFrame.height : 0
        updateContentInsets(withKeyboardHeight: keyboardHeight)
        UIView.animate(withDuration: animationDuration, delay: 0, options: animationCurve, animations: {
            self.view.layoutIfNeeded()
            self.titleLabel.alpha = isAppearing ? 0 : 1
            self.forgotButtonIPhone.alpha = isAppearing ? 0 : 1
            self.forgotButtonIPad.alpha = isAppearing ? 0 : 1
        }, completion: nil)
    }
    
    fileprivate func updateContentInsets(withKeyboardHeight keyboardHeight: CGFloat) {
        let height = view.frame.height-keyboardHeight
        let inset = makeScrollViewTopInset(forHeight: height, isEditing: keyboardHeight > 0)
        inputViewTopConstraint.constant = inset
    }
    
    // MARK: Make
    
    fileprivate func makeScrollViewTopInset(forHeight height: CGFloat, isEditing: Bool) -> CGFloat {
        let offset: CGFloat = {
            if isEditing {
                return floor(inputStackView.frame.height*0.7)
            } else {
                return floor(inputStackView.frame.height*0.8)
            }
        }()
        let verticalCenter = height/2
        let contentOffset = verticalCenter-offset
        let minimumOffset = view.safeAreaInsets.top+16
        if contentOffset < minimumOffset {
            return minimumOffset
        }
        return floor(contentOffset)
    }
    
}
