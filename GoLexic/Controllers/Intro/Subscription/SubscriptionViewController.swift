//
//  SubscriptionViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 10/08/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile
import BonMot
import StoreKit
import SwiftyUserDefaults

class SubscriptionViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var backButton: AttributedButton!
    @IBOutlet fileprivate var infoStackView: UIStackView!
    @IBOutlet fileprivate var monthSubscriptionView: SubscriptionView!
    @IBOutlet fileprivate var halfYearSubscriptionView: SubscriptionView!
    @IBOutlet fileprivate var purchaseButton: AttributedButton!
    @IBOutlet fileprivate var restoreIPadButton: LocalizedButton!
    @IBOutlet fileprivate var restoreIPhoneButton: LocalizedButton!
    @IBOutlet fileprivate var privacyButton: LocalizedButton!
    @IBOutlet fileprivate var termsButton: LocalizedButton!
    @IBOutlet fileprivate var infoTextView: UITextView!
    @IBOutlet fileprivate var productActivityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var purchaseActivityIndicator: UIActivityIndicatorView!
    @IBOutlet fileprivate var restoreIPhoneButtonHeight: NSLayoutConstraint!

    // Variables
    var products = [SKProduct]()
    
    var dismissing = false
    
    fileprivate var selectedProduct = -1
    
    fileprivate var isUpdatingProducts: Bool = false {
        didSet {
            infoStackView.isHidden = isUpdatingProducts
            if isUpdatingProducts {
                productActivityIndicator.startAnimating()
            } else {
                productActivityIndicator.stopAnimating()
            }
            purchaseButton.isUserInteractionEnabled = !isUpdatingProducts
            restoreIPadButton.isUserInteractionEnabled = !isUpdatingProducts
            restoreIPhoneButton.isUserInteractionEnabled = !isUpdatingProducts
        }
    }
    fileprivate var isPurchasingProduct: Bool = false {
        didSet {
            if isPurchasingProduct {
                purchaseActivityIndicator.startAnimating()
            } else {
                purchaseActivityIndicator.stopAnimating()
            }
            backButton.isUserInteractionEnabled = !isPurchasingProduct
            purchaseButton.isUserInteractionEnabled = !isPurchasingProduct
            restoreIPadButton.isUserInteractionEnabled = !isPurchasingProduct
            restoreIPhoneButton.isUserInteractionEnabled = !isPurchasingProduct
        }
    }
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        updateProducts()
    }
    
}

// MARK: - Targets
extension SubscriptionViewController {
    
    @IBAction fileprivate func backButtonPressed(_ sender: UIButton) {
        if dismissing {
            dismiss(animated: true)
        } else {
            self.navigationController?.push(TryForFreeViewController.self)
        }
    }
    
    @IBAction fileprivate func purchaseButtonPressed(_ sender: UIButton) {
        if selectedProduct >= 0,
           let product = products.first(where: { $0.subscriptionPeriod?.numberOfUnits == (selectedProduct == 1 ? 1 : 6) }) {
            isPurchasingProduct = true
            PurchaseProvider.shared.purchase(product) { [weak self] success in
                guard let _self = self else {
                    return
                }
                if success {
                    _self.completePurchase()
                } else {
                    let message = Localization.string(forKeyPath: "Subscription.alerts.purchaseFailed")!
                    let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                    Globals.presentAlert(withTitle: "", message: message, cancel: confirm)
                }
                _self.isPurchasingProduct = false
            }
        }
    }
    
    @IBAction fileprivate func restoreButtonPressed(_ sender: UIButton) {
        completePurchase()
        return;
        
        isPurchasingProduct = true
        PurchaseProvider.shared.restorePurchases { [weak self] success in
            guard let _self = self else {
                return
            }
            if success {
                let message = Localization.string(forKeyPath: "Subscription.alerts.restoreSuccessful")!
                let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                Globals.presentAlert(withTitle: "", message: message, cancel: confirm) { _ in
                    _self.completePurchase()
                }
            } else {
                let message = Localization.string(forKeyPath: "Subscription.alerts.restoreFailed")!
                let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                Globals.presentAlert(withTitle: "", message: message, cancel: confirm)
            }
            _self.isPurchasingProduct = false
        }
    }
    
    @IBAction fileprivate func privacyButtonPressed(_ sender: UIButton) {
        let webVC = show(WebGenericViewController.self, useNavigationController: true)
        webVC.urlString = Localization.string(forKeyPath: "Subscription.info.privacyLink")
    }
    
    @IBAction fileprivate func termsButtonPressed(_ sender: UIButton) {
        let webVC = show(WebGenericViewController.self, useNavigationController: true)
        webVC.urlString = Localization.string(forKeyPath: "Subscription.info.termsLink")
    }
    
}

// MARK: - UITextViewDelegate
extension SubscriptionViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL,
                  in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        guard isPurchasingProduct == false else {
            return false
        }
        let webVC = show(WebGenericViewController.self, useNavigationController: true)
        webVC.urlString = URL.absoluteString
        return false
    }
    
}

// MARK: - UIViewControllerTransitioningDelegate
extension SubscriptionViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension SubscriptionViewController {
    
    // MARK: Enums
    
    fileprivate enum Pending {
        case none
        case purchase
        case restoration
    }
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        
        monthSubscriptionView.tag = 1
        halfYearSubscriptionView.tag = 0

        monthSubscriptionView.delegate = self
        halfYearSubscriptionView.delegate = self
        
        restoreIPadButton.isHidden = Globals.isPhone
        restoreIPhoneButton.isHidden = !Globals.isPhone
        restoreIPhoneButtonHeight.constant = Globals.isPhone ? 40.0 : 0.0

        infoTextView.attributedText = makeInfoText()
    }
    
    fileprivate func updateContent() {
        
        for product in products {
            if let period = product.subscriptionPeriod,
               period.numberOfUnits == 1,
               period.unit == .month {
                monthSubscriptionView.titleText = makeCostText(forProduct: product, scheme: Localization.string(forKeyPath: "Subscription.month"))
                monthSubscriptionView.descriptionText = Localization.string(forKeyPath: "Subscription.trial")
                monthSubscriptionView.discountText = nil
            } else {
                halfYearSubscriptionView.titleText = makeCostText(forProduct: product, scheme: Localization.string(forKeyPath: "Subscription.halfYear"))
                halfYearSubscriptionView.descriptionText = Localization.string(forKeyPath: "Subscription.trial")
                halfYearSubscriptionView.discountText = Localization.string(forKeyPath: "Subscription.discount")
            }
        }
        
        purchaseButton.setEnabled(products.count > 0, animated: false)
    }
    
    fileprivate func completePurchase() {
        if Defaults[\.didShowOnboardingTips] == false {
            if dismissing {
                dismiss(animated: true) {
                    self.pushTips()
                }
            } else {
                pushTips()
            }
        } else {
            dismiss(animated: true)
        }
    }
    
    fileprivate func pushTips() {
        if dismissing {
            let tipsVC = UIApplication.topMostViewController?.navigationController?.push(TipsViewController.self)
            tipsVC?.canGoBack = false
        } else {
            let tipsVC = self.navigationController?.push(TipsViewController.self)
            tipsVC?.canGoBack = false
        }
    }
    
    // MARK: Update
    
    fileprivate func updateProducts() {
        guard isUpdatingProducts == false else {
            return
        }
        isUpdatingProducts = true
        PurchaseProvider.shared.fetchProducts { [weak self] products in
            guard let _self = self else {
                return
            }
            if products.count > 0 {
                _self.products = products
                _self.updateContent()
            } else {
                let message = Localization.string(forKeyPath: "Globals.alerts.connectionError")!
                let retry = Localization.string(forKeyPath: "Globals.retry")!
                Globals.presentAlert(withTitle: "", message: message, cancel: retry) { _ in
                    _self.updateProducts()
                }
            }
            _self.isUpdatingProducts = false
        }
    }
    
    // MARK: Make
    
    fileprivate func makeCostText(forProduct product: SKProduct?, scheme: String?) -> String? {
        guard let scheme = scheme else {
            return nil
        }
        guard let _product = product else {
            return String(format: scheme, "-")
        }
        let formatter = Globals.currrencyFormatter(withLocale: _product.priceLocale)
        guard let costText = formatter.string(from: _product.price) else {
            return String(format: scheme, "-")
        }
        return String(format: scheme, costText)
    }
    
    fileprivate func makeInfoText() -> NSAttributedString? {
        guard let text = Localization.string(forKeyPath: "Subscription.info.text"),
              let terms = Localization.string(forKeyPath: "Subscription.info.terms"),
              let privacy = Localization.string(forKeyPath: "Subscription.info.privacy") else {
            return nil
        }
        let _text = NSString(string: text)
        let attributedText = NSMutableAttributedString(string: text)
        // Add base attributes
        let textRange = NSRange(location: 0, length: text.count)
        let textFont = UIFont(name: UIFont.Name.Montserrat.regular, size: 14)!
        attributedText.addAttribute(.font, value: textFont, range: textRange)
        attributedText.addAttribute(.foregroundColor, value: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), range: textRange)
        // Add terms attributes
        let termsRange = _text.range(of: terms)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: termsRange)
        if let _termsUrlString = Localization.string(forKeyPath: "Subscription.info.termsLink") {
            attributedText.addAttribute(.link, value: _termsUrlString, range: termsRange)
        }
        // Add privacy attributes
        let privacyRange = _text.range(of: privacy)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: privacyRange)
        if let _privacyUrlString = Localization.string(forKeyPath: "Subscription.info.privacyLink") {
            attributedText.addAttribute(.link, value: _privacyUrlString, range: privacyRange)
        }
        // Add paragraph attributes
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        attributedText.addAttribute(.paragraphStyle, value: style, range: textRange)
        return attributedText
    }
    
}

extension SubscriptionViewController: SubscriptionViewDelegate {
    func subscriptionSelected(_ index: Int, _ isSelected: Bool) {
        if selectedProduct != index {
            switch selectedProduct {
            case 1:
                monthSubscriptionView.isSelected = false
            default:
                halfYearSubscriptionView.isSelected = false
            }
        }
        selectedProduct = index
        purchaseButton.isEnabled = true
    }
}
