//
//  PasswordViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 11/05/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class PasswordViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var scrollView: UIScrollView!
    @IBOutlet fileprivate var inputStackView: UIStackView!
    @IBOutlet fileprivate var recoverButton: UIButton!
    @IBOutlet fileprivate var activityIndicator: UIActivityIndicatorView!
    
    // Constraints
    @IBOutlet fileprivate var inputViewTopConstraint: NSLayoutConstraint!
    
    // Variables
    fileprivate var viewModel = PasswordViewModel()
    
    // Flags
    fileprivate var didUpdateContentOffset: Bool = false
    fileprivate var isUpdating: Bool = false {
        didSet {
            if isUpdating {
                activityIndicator.startAnimating()
            } else {
                activityIndicator.stopAnimating()
            }
            recoverButton.isUserInteractionEnabled = !isUpdating
        }
    }

    // MAKR: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        subscribeToNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if didUpdateContentOffset == false {
            didUpdateContentOffset = true
            updateContentInsets(withKeyboardHeight: 0)
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}

// MARK: - Targets
extension PasswordViewController {
    
    @IBAction fileprivate func closeButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction fileprivate func nextButtonPressed(_ sender: UIButton) {
        view.endEditing(true)
        updateInputViewState()
        guard validateAllFields() else {
            return
        }
        isUpdating = true
        viewModel.resetPassword { [weak self] result in
            guard let _self = self else {
                return
            }
            switch result {
            case .success:
                let message = Localization.string(forKeyPath: "Password.alerts.checkEmail")!
                let confirm = Localization.string(forKeyPath: "Globals.confirm")!
                Globals.presentAlert(withTitle: "", message: message, cancel: confirm) { [weak self] _ in
                    guard let _self = self else {
                        return
                    }
                    _self.dismiss(animated: true)
                }
            case let .failure(code):
                Globals.presentAlert(withErrorCode: code)
            }
            _self.isUpdating = false
        }
    }
    
    @objc fileprivate func viewTapped(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
}

// MARK: - InputViewDelegate
extension PasswordViewController: InputViewDelegate {
    
    func inputViewShouldBeginEditting(_ view: InputView) -> Bool {
        return !isUpdating
    }
    
    func inputViewShouldReturn(_ view: InputView) -> Bool {
        view.resignFirstResponder()
        return true
    }
    
    func inputView(_ view: InputView, didChange text: String?) {
        let input = PasswordViewModel.Input(rawValue: view.tag)!
        viewModel.updateInput(input, value: text)
    }
    
}

// MARK: - UIViewControllerTransitioningDelegate
extension PasswordViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension PasswordViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
        view.addGestureRecognizer(tapRecognizer)
        for case let inputView as InputView in inputStackView.subviews {
            let input = PasswordViewModel.Input(rawValue: inputView.tag)!
            switch input {
            case .email:
                inputView.textContentType = .emailAddress
                inputView.keyboardType = .emailAddress
            }
            inputView.capitalization = .none
            inputView.delegate = self
        }
    }
    
    fileprivate func subscribeToNotifications() {
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillShowNotification,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self,
                let userInfo = notification.userInfo else {
                return
            }
            _self.updateContentInsets(withUserInfo: userInfo, isAppearing: true)
        }
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                               object: nil, queue: nil) { [weak self] notification in
            guard let _self = self,
                let userInfo = notification.userInfo else {
                return
            }
            _self.updateContentInsets(withUserInfo: userInfo, isAppearing: false)
        }
    }
    
    fileprivate func validateAllFields() -> Bool {
        for case let inputView as InputView in inputStackView.subviews {
            let input = PasswordViewModel.Input(rawValue: inputView.tag)!
            if viewModel.validateInput(input) == false {
                return false
            }
        }
        return true
    }
    
    // MARK: Update
    
    fileprivate func updateInputViewState() {
        for case let inputView as InputView in inputStackView.subviews {
            let input = PasswordViewModel.Input(rawValue: inputView.tag)!
            if viewModel.validateInput(input) {
                inputView.change(state: .normal)
            } else {
                inputView.change(state: .error)
            }
        }
    }
    
    fileprivate func updateContentInsets(withUserInfo userInfo: [AnyHashable: Any], isAppearing: Bool) {
        let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let animationCurve: UIView.AnimationOptions = {
            let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
            return UIView.AnimationOptions(rawValue: curve)
        }()
        let keyboardHeight = isAppearing ? keyboardFrame.height : 0
        updateContentInsets(withKeyboardHeight: keyboardHeight)
        UIView.animate(withDuration: animationDuration, delay: 0, options: animationCurve, animations: {
            self.view.layoutIfNeeded()
            self.titleLabel.alpha = isAppearing ? 0 : 1
        }, completion: nil)
    }
    
    fileprivate func updateContentInsets(withKeyboardHeight keyboardHeight: CGFloat) {
        let height = view.frame.height-keyboardHeight
        let inset = makeScrollViewTopInset(forHeight: height, isEditing: keyboardHeight > 0)
        inputViewTopConstraint.constant = inset
    }
    
    // MARK: Make
    
    fileprivate func makeScrollViewTopInset(forHeight height: CGFloat, isEditing: Bool) -> CGFloat {
        let offset: CGFloat = {
            if isEditing {
                return floor(inputStackView.frame.height)
            } else {
                return floor(inputStackView.frame.height*1.86)
            }
        }()
        let verticalCenter = height/2
        let contentOffset = verticalCenter-offset
        let minimumOffset = view.safeAreaInsets.top+16
        if contentOffset < minimumOffset {
            return minimumOffset
        }
        return floor(contentOffset)
    }
    
}
