//
//  PasswordViewModel.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 11/05/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import Foundation
import Moya
import Cubemobile

class PasswordViewModel {
    
    // MARK: Enum
    
    enum Input: Int {
        case email
    }
    
    enum Completion {
        case success
        case failure(code: Int?)
    }
    
    // MARK: Properties
    
    // Variables
    var email: String?

}

// MARK: - Public
extension PasswordViewModel {
    
    func updateInput(_ input: Input, value: String?) {
        switch input {
        case .email:
            email = value?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
    func validateInput(_ input: Input) -> Bool {
        switch input {
        case .email:
            return Globals.isEmailValid(email)
        }
    }
    
    func resetPassword(withCompletion completion: @escaping ((Completion) -> Void)) {
        let params = makePostBody()
        let target = GoLexicTarget(withEndpoint: .userPassword, params: params)
        GoLexicAPI.provider.request(target) { result in
            switch result {
            case .success:
                completion(.success)
            case let .failure(error):
                let code: Int? = {
                    guard let data = try? error.response?.mapJSON() as? NSDictionary,
                        let code = data.value(forKeyPath: "error.code") as? Int else {
                            return nil
                    }
                    return code
                }()
                completion(.failure(code: code))
            }
        }
    }
    
}

// MARK: - Private
extension PasswordViewModel {
    
    // MARK: Make
    
    fileprivate func makePostBody() -> [String: Any] {
        var body: [String: Any] = [:]
        if let _email = email {
            body.updateValue(_email, forKey: "email")
        }
        return body
    }
    
}
