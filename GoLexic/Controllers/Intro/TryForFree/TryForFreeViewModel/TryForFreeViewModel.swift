//
//  TryForFreeViewModel.swift
//  GoLexic
//
//  Created by Denis Kuznetsov on 09.03.2021.
//  Copyright © 2021 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class TryForFreeViewModel: NSObject {
    
    // MARK: Enums
    
    enum UpdateCompletion {
        case success(_ success: Bool)
        case failure(code: Int?)
    }
    
    enum DeleteCompletion {
        case success
        case failure(code: Int?)
    }
    
    // MARK: Properties
    
    fileprivate(set) var modules = [ModuleItem]()
    
    func getData(withCompletion completion: @escaping ((UpdateCompletion) -> Void)) {
        let target = GoLexicTarget(withEndpoint: .tryForFree)
        GoLexicAPI.provider.request(target) { result in
            switch result {
            case let .success(moyaResponse):
                guard let response = try? moyaResponse.mapJSON(),
                    let data = response as? NSArray else {
                        completion(.failure(code: nil))
                        return
                }
                self.modules = self.makingModuleItems(data as! [[String : Any]])
                completion(.success(data.count > 0))
            case let .failure(error):
                let code: Int? = {
                    guard let data = try? error.response?.mapJSON() as? NSDictionary,
                        let code = data.value(forKeyPath: "error.code") as? Int else {
                            return nil
                    }
                    return code
                }()
                completion(.failure(code: code))
            }
        }
    }
    
    fileprivate func makingModuleItems(_ items: [[String : Any]]) -> [ModuleItem]{
        var modules: [ModuleItem] = []
        for module in items {
            let item = ModuleItem(withData: module)
            modules.append(item)
        }
        return modules
    }

}
