//
//  TryForFreeViewController.swift
//  GoLexic
//
//  Created by Denis Kuznetsov on 09.03.2021.
//  Copyright © 2021 CUBE Mobile. All rights reserved.
//


import UIKit
import Cubemobile

class TryForFreeViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate weak var tableView: LocalizedTableView!
    
    // Variables
    fileprivate var viewModel = TryForFreeViewModel()
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureController()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
}

// MARK: - Targets
extension TryForFreeViewController {
    
    // MARK: Helpers
    
    fileprivate func configureController() {
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
        tableView.register(UINib(withType: ModuleItemTableViewCell.self),
                               forCellReuseIdentifier: ModuleItemTableViewCell.identifier)
        tableView.register(UINib(withType: MoreModulesTableViewCell.self),
                               forCellReuseIdentifier: MoreModulesTableViewCell.identifier)
    }
    
    fileprivate func loadData() {
        viewModel.getData { (complition) in
            self.tableView.reloadData()
        }
    }
    
}

// MARK: - UITableViewDataSource
extension TryForFreeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.modules.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row >= viewModel.modules.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: MoreModulesTableViewCell.identifier,
                                                     for: indexPath) as! MoreModulesTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ModuleItemTableViewCell.identifier,
                                                     for: indexPath) as! ModuleItemTableViewCell
            let module = viewModel.modules[indexPath.row]
            cell.update(withModule: module)
            
            return cell
        }
    }
    
}

// MARK: - UITableViewDelegate
extension TryForFreeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let subscriptionVC = show(SubscriptionViewController.self)
        subscriptionVC.dismissing = true
    }
    
}
