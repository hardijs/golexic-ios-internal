//
//  OnboardingPageViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 21/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class OnboardingPageViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var titleLabel: UILabel!
    @IBOutlet fileprivate var imageView: UIImageView!
    
    // Variables
    var index: Int!
    var onboarding: Onboarding!
    
    // MARK: Overridden Functions

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateContent()
    }
    

}

// MARK: - Private Functions
extension OnboardingPageViewController {
    
    fileprivate func updateContent() {
        guard let _onboarding = onboarding else {
            return
        }
        titleLabel.text = Localization.string(forKeyPath: _onboarding.titleKey)
        imageView.image = UIImage(named: _onboarding.imageName)
    }
    
}
