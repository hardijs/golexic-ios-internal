//
//  OnboardingViewController.swift
//  GoLexic
//
//  Created by Armands Lazdiņš on 21/02/2020.
//  Copyright © 2020 CUBE Mobile. All rights reserved.
//

import UIKit
import Cubemobile

class OnboardingViewController: UIViewController {
    
    // MARK: Properties
    
    // IB
    @IBOutlet fileprivate var pageControl: UIPageControl!
    fileprivate var pageViewController: UIPageViewController!
	@IBOutlet fileprivate weak var createAccountButton: LocalizedButton!
	
    // Variables
    fileprivate(set) var index: Int = 0 {
        didSet {
            pageControl.currentPage = index
        }
    }
    fileprivate var pendingIndex: Int = 0
    fileprivate lazy var onboardings: [Onboarding] = {
       return makeOnboardingItems()
    }()
    
    // MARK: Overridden Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        updateContent()
        loadItemViewController(withIndex: index, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let _targetVC = segue.destination as? UIPageViewController {
            pageViewController = _targetVC
            pageViewController.delegate = self
            pageViewController.dataSource = self
        }
    }

}

// MARK: - Targets
extension OnboardingViewController {
    
    @IBAction func createAccountButtonPressed(_ sender: UIButton) {
        navigationController!.push(SignupViewController.self)
    }
    
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        navigationController!.push(LoginViewController.self)
    }
    
}

// MARK: - UIPageViewControllerDataSource
extension OnboardingViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return makePageViewController(withIndex: index+1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return makePageViewController(withIndex: index-1)
    }
    
}

// MARK: - UIPageViewControllerDelegate
extension OnboardingViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            willTransitionTo pendingViewControllers: [UIViewController]) {
        guard pendingViewControllers.count > 0 else {
            return
        }
        let viewController = pendingViewControllers.first! as! OnboardingPageViewController
        pendingIndex = viewController.index
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed == true else {
            return
        }
        if previousViewControllers.count > 0 {
            let viewController = previousViewControllers.last! as! OnboardingPageViewController
            guard viewController.index != pendingIndex else {
                return
            }
        }
        index = pendingIndex
    }
    
}

// MARK: - UIViewControllerTransitioningDelegate
extension OnboardingViewController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController,
                                  source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .forward
        transition.dimmOpacity = 0
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let transition = SpringTransition()
        transition.direction = .backward
        return transition
    }
    
}

// MARK: - Private Functions
extension OnboardingViewController {
    
    // MARK: Helpers
    
    fileprivate func updateContent() {
        pageControl.numberOfPages = onboardings.count
    }
    
    fileprivate func loadItemViewController(withIndex index: Int, animated: Bool) {
        guard let viewController = makePageViewController(withIndex: index) else {
            return
        }
        pageViewController.setViewControllers([viewController], direction: .forward, animated: animated)
    }
    
    // MARK: Make
    
    fileprivate func makePageViewController(withIndex index: Int) -> OnboardingPageViewController? {
        guard index >= 0,
            index < onboardings.count else {
            return nil
        }
        let onboarding = onboardings[index]
        let viewController = instantiate(OnboardingPageViewController.self)
        viewController.index = index
        viewController.onboarding = onboarding
        return viewController
    }
    
    fileprivate func makeOnboardingItems() -> [Onboarding] {
        var items: [Onboarding] = []
        guard let resourcePath = Bundle.main.path(forResource: "Onboarding", ofType: "plist") else {
            return items
        }
        guard let data = NSArray(contentsOfFile: resourcePath) else {
            return items
        }
        for case let _data as [String: Any] in data {
            let item = Onboarding(withData: _data)
            items.append(item)
        }
        return items
    }
    
}
